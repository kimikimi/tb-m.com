<?php

require($_SERVER["DOCUMENT_ROOT"] . '/wp-blog-header.php'); 

get_header();
?>
<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>CONTACT</h2>
		</div>
	</div>
	<h3>お問い合わせ</h3>
	
	<div class="wrapper">
		<p class="note">※ は必須入力です。</p>
		<form name="frm_main" action="sendmail.php" method="post" id="form">
			<dl>
				<dt>目的</dt>
				<dd>
<select name="目的" id="目的">
<option selected="selected">お選び下さい</option>
<option>LIMEXに関するお問い合わせ</option>
<option>株式会社TBMに関するお問い合わせ</option>
<option>メディア取材・講演依頼</option>
<option>資料請求</option>
<option>その他</option>
</select>
				</dd>
				<dt>業種</dt>
				<dd>
<select name="業種" size="1" id="業種">
<option value="業種をお選び下さい" selected="selected">業種をお選び下さい</option>
<option value="出版印刷・紙製造業">■出版印刷・紙製造業</option>
<option value="製紙業"> ├ 製紙業</option>
<option value="紙器製造業"> ├ 紙器製造業</option>
<option value="紙工品製造業"> ├ 紙工品製造業</option>
<option value="紙商"> ├ 紙商</option>
<option value="紙製品製造業"> ├ 紙製品製造業</option>
<option value="加工紙製造業"> ├ 加工紙製造業</option>
<option value="紙袋製造業"> ├ 紙袋製造業</option>
<option value="出版業"> ├ 出版業</option>
<option value="新聞社（業界紙含）"> ├ 新聞社（業界紙含）</option>
<option value="印刷業"> ├ 印刷業</option>
<option value="製本業・写植・製版・コピー"> ├ 製本業・写植・製版・コピー</option>
<option value="デザイン"> ├ デザイン</option>
<option value="製紙原料"> ├ 製紙原料</option>
<option value="印刷材料"> └ 印刷材料</option>
<option value="製造業" style="color: #f30;">■製造業</option>
<option value="食料・飲料・衣料品"> ├ 食料・飲料・衣料品</option>
<option value="化学加工品"> ├ 化学加工品</option>
<option value="鉄鋼・金属"> ├ 鉄鋼・金属</option>
<option value="自動車"> ├ 自動車</option>
<option value="機械・精密機器"> ├ 機械・精密機器</option>
<option value="医療用機器・医療関連"> ├ 医療用機器・医療関連</option>
<option value="その他製造業"> └ その他製造業</option>
<option value="農林水産・鉱業">農林水産・鉱業</option>
<option value="電気・ガス・水道業">電気・ガス・水道業</option>
<option value="運輸業">運輸業</option>
<option value="卸業・貿易・商社">卸業・貿易・商社</option>
<option value="金融・保険業">金融・保険業</option>
<option value="住宅・建材・エクステリア">住宅・建材・エクステリア</option>
<option value="建設・工事・設計">建設・工事・設計</option>
<option value="不動産業">不動産業</option>
<option value="小売業">小売業</option>
<option value="飲食業">飲食業</option>
<option value="サービス業（BtoC）">サービス業（BtoC）</option>
<option value="サービス業（BtoB）">サービス業（BtoB）</option>
<option value="放送・メディア・通信">放送・メディア・通信</option>
<option value="ソフトウェア・情報処理">ソフトウェア・情報処理</option>
<option value="インターネット関連">インターネット関連</option>
<option value="環境・リサイクル">環境・リサイクル</option>
<option value="病院・医療機関">病院・医療機関</option>
<option value="学校・教育・宗教">学校・教育・宗教</option>
<option value="公的機関・政治経済団体">公的機関・政治経済団体</option>
</select>
				</dd>
				<dt>会社名 ※</dt>
				<dd>
<input type="text" name="会社名" id="会社名" class="input_001" />
				</dd>
				<dt>部署</dt>
				<dd>
<input type="text" name="部署" id="部署" class="input_001" />
				</dd>
				<dt>ホームページURL</dt>
				<dd>
<input type="text" name="ホームページURL" id="ホームページURL" class="input_001" value="http://" />
				</dd>
				<dt>お名前 ※</dt>
				<dd>
<input type="text" name="お名前" id="お名前" class="input_001" />
				</dd>
				<dt>ふりがな</dt>
				<dd>
<input type="text" name="ふりがな" id="ふりがな" class="input_001" />
				</dd>
				<dt>郵便番号 ※</dt>
				<dd>
<input type="text" name="郵便番号" id="郵便番号" class="post ph" title="例）542-0081" />
				</dd>
				<dt>都道府県 ※</dt>
				<dd>
<select name="都道府県" id="都道府県">
<option value="" selected="selected">都道府県をお選び下さい</option>
<option value="北海道">北海道</option>
<option value="青森県">青森県</option>
<option value="岩手県">岩手県</option>
<option value="宮城県">宮城県</option>
<option value="秋田県">秋田県</option>
<option value="山形県">山形県</option>
<option value="福島県">福島県</option>
<option value="茨城県">茨城県</option>
<option value="栃木県">栃木県</option>
<option value="群馬県">群馬県</option>
<option value="埼玉県">埼玉県</option>
<option value="千葉県">千葉県</option>
<option value="東京都">東京都</option>
<option value="神奈川県">神奈川県</option>
<option value="新潟県">新潟県</option>
<option value="富山県">富山県</option>
<option value="石川県">石川県</option>
<option value="福井県">福井県</option>
<option value="山梨県">山梨県</option>
<option value="長野県">長野県</option>
<option value="岐阜県">岐阜県</option>
<option value="静岡県">静岡県</option>
<option value="愛知県">愛知県</option>
<option value="三重県">三重県</option>
<option value="滋賀県">滋賀県</option>
<option value="京都府">京都府</option>
<option value="大阪府">大阪府</option>
<option value="兵庫県">兵庫県</option>
<option value="奈良県">奈良県</option>
<option value="和歌山県">和歌山県</option>
<option value="鳥取県">鳥取県</option>
<option value="島根県">島根県</option>
<option value="岡山県">岡山県</option>
<option value="広島県">広島県</option>
<option value="山口県">山口県</option>
<option value="徳島県">徳島県</option>
<option value="香川県">香川県</option>
<option value="愛媛県">愛媛県</option>
<option value="高知県">高知県</option>
<option value="福岡県">福岡県</option>
<option value="佐賀県">佐賀県</option>
<option value="長崎県">長崎県</option>
<option value="熊本県">熊本県</option>
<option value="大分県">大分県</option>
<option value="宮崎県">宮崎県</option>
<option value="鹿児島県">鹿児島県</option>
<option value="沖縄県">沖縄県</option>
</select>
				</dd>
				<dt>ご住所 ※</dt>
				<dd>
<input type="text" name="ご住所" id="ご住所" class="input_001 ph" title="例）大阪市中央区南船場×××" />
				</dd>
				<dt>電話番号 ※</dt>
				<dd>
<input type="text" name="電話番号" id="電話番号" class="input_001" />
				</dd>
				<dt>FAX番号</dt>
				<dd>
<input type="text" name="FAX番号" id="FAX番号" class="input_001" />
				</dd>
				<dt>メールアドレス ※</dt>
				<dd>
<input type="text" name="email" id="email" class="input_001" />
				</dd>
				<dt>ご希望の連絡方法 ※</dt>
				<dd>
<label><input type="radio" name="ご希望の連絡方法" id="radio" value="お電話" />&nbsp;お電話</label>
<label><input type="radio" name="ご希望の連絡方法" id="radio2" value="メール" />&nbsp;メール</label>
<label><input type="radio" name="ご希望の連絡方法" id="radio3" value="どちらでも構わない" />&nbsp;どちらでも構わない</label>
				</dd>
				<dt>何でLIMEXをお知りに<br />なりましたか？</dt>
				<dd>
<input type="text" name="何でLIMEXをお知りになりましたか?" id="何でLIMEXをお知りになりましたか?" class="input_001" />
				</dd>
				<dt>お問い合わせ内容 ※</dt>
				<dd>
<textarea name="お問い合わせ内容" id="お問い合わせ内容" class="input_001"></textarea>
<div class="agree01"><p><a href="http://tb-m.com/privacy/" target="_blank">個人情報の取得・利用について</a>をご確認いただき、同意するにチェックをつけてください。</p></div>
<div class="agree02">
<label><input type="radio" name="form-agree" id="form-agree1" value="ok" />&nbsp;同意する</label>
<label><input type="radio" name="form-agree" id="form-agree2" value="ng" />&nbsp;同意しない</label>
</div>

				</dd>
				<dd class="btn"><a href="javascript:void(0);" onclick="form_check01()"><img src="img/button01.gif" alt="送信確認" /></a></dd>
			</dl>
		</form>
	</div>
</section><!-- /#body -->
<?php
get_footer();