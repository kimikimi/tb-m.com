<?php header("Content-Type:text/html;charset=utf-8"); ?>
<?php
require($_SERVER["DOCUMENT_ROOT"] . '/wp-blog-header.php'); 
?>
<?php
// このファイルの名前
$script ="sendmail.php";
// メールを送信するアドレス(複数指定する場合は「,」で区切る)
$to = "infomail@tb-m.com";
// 送信されるメールのタイトル　
$sbj = "Limex お問い合わせフォーム";
// 送信確認画面の表示(する=1, しない=0)
$chmail = 1;
// 送信後に自動的にジャンプする(する=1, しない=0)
// 0にすると、送信終了画面が表示されます。
$jpage = 0;
// 送信後にジャンプするページ(送信後にジャンプする場合)
$next = "";
// 差出人は、送信者のメールアドレスにする(する=1, しない=0)
// する場合は、メール入力欄のname属性を「email」にしてください。
$from_add = 0;
// Add 2013.10.30
// $from_add=0にした場合、アドレスを設定
$from = 'infomail@tb-m.com';
// 差出人に送信内容確認メールを送る(送る=1, 送らない=0)
// 送る場合は、メール入力欄のname属性を「email」にしてください。
$remail = 0;
// 差出人に送信確認メールを送る場合のメールのタイトル
$resbj = "Limex お問い合わせフォーム";
// 必須入力項目を設定する(する=1, しない=0)
$esse = 1;
// 必須入力項目(入力フォームで指定したname)
$eles = array('会社名','お名前','電話番号','郵便番号','ご住所','都道府県','email','ご希望の連絡方法','お問い合わせ内容');
$sendm = 0;
foreach($_POST as $key=>$var) {
	if($var == "eweb_submit") $sendm = 1;
}
// 文字の置き換え
$string_from = "＼";
$string_to = "ー";
// 未入力項目のチェック
if($esse == 1) {
	$flag = 0;
	$length = count($eles) - 1;
	foreach($_POST as $key=>$var) {
		$key = strtr($key, $string_from, $string_to);
		if($var == "eweb_submit") ;
		else {
			for($i=0; $i<=$length; $i++) {
				if($key == $eles[$i] && empty($var)) {
					$errm .= "<p><font color=\"#ff0000\">「".$key."」は必須入力項目です。</font></p>\n";
					$flag = 1;
				}
			}
		}
	}
	foreach($_POST as $key=>$var) {
		$key = strtr($key, $string_from, $string_to);
		for($i=0; $i<=$length; $i++) {
			if($key == $eles[$i]) {
				$eles[$i] = "eweb_ok";
			}
		}
	}
	for($i=0; $i<=$length; $i++) {
		if($eles[$i] != "eweb_ok") {
			$errm .= "<p><font color=\"#ff0000\">「".$eles[$i]."」が未選択です。</font></p>\n";
			$eles[$i] = "eweb_ok";
			$flag = 1;
		}
	}
	// エラー画面の表示
	if($flag == 1) {
		htmlHeader();
///// エラー画面のレイアウトの編集 --- 開始 ///////////////////////////////////
?>
<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>CONTACT</h2>
		</div>
	</div>
	<h3>お問い合わせ</h3>
	
	<div class="wrapper">
		<form name="frm_main" id="form">
		<dl>
				<dd>入力エラーがあります。</dd>
			<dd>
			<?php echo $errm; ?>
			</dd>
		
		<dd class="btn" style="width:480px;padding-top:50px;">
			<a href="javascript:void(0);" onclick="history.back();"><img src="img/button02.gif" alt="戻る" /></a>
			</dd>
		</dl>
		</form>
	</div>
</section>

<?php
///// エラー画面のレイアウトの編集 --- 終了 ///////////////////////////////////
		htmlFooter();
		exit(0);
	}
}
///// メールのレイアウトの編集 --- 開始 ///////////////////////////////////////
$body="「".$sbj."」からの発信です\n\n";
$body.="-------------------------------------------------\n\n";
foreach($_POST as $key=>$var) {
  $key = strtr($key, $string_from, $string_to);
  if(get_magic_quotes_gpc()) $var = stripslashes($var);
  if($var == "eweb_submit") ;
  else $body.="[".$key."] ".$var."\n";
}
$body.="\n-------------------------------------------------\n\n";
$body.="送信日時：".date( "Y/m/d (D) H:i:s", time() )."\n";
$body.="ホスト名：".getHostByAddr(getenv('REMOTE_ADDR'))."\n\n";
///// メールのレイアウトの編集 --- 終了 ///////////////////////////////////////
if($remail == 1) {
///// 差出人への核にメールのレイアウトの編集 --- 開始 /////////////////////////
	$rebody.="下記の内容にてお問い合わせを承りました。\n\n";
	$rebody.="-------------------------------------------------\n\n";
	foreach($_POST as $key=>$var) {
		$key = strtr($key, $string_from, $string_to);
		if(get_magic_quotes_gpc()) $var = stripslashes($var);
		if($var == "eweb_submit") ;
		else $rebody.="[".$key."] ".$var."\n";
	}
	$rebody.="\n-------------------------------------------------\n\n";
	$rebody  .= "送信日時：".date( "Y/m/d (D) H:i:s", time() )."\n";
	$reto     = $_POST['email'];
	$rebody   = mb_convert_encoding($rebody,"JIS","UTF-8");
	$resbj    = "=?iso-2022-jp?B?".base64_encode(mb_convert_encoding($resbj,"JIS","UTF-8"))."?=";
	$reheader = "From: $to\nReply-To: ".$to."\nContent-Type: text/plain;charset=iso-2022-jp\nX-Mailer: PHP/".phpversion();
///// 差出人への核にメールのレイアウトの編集 --- 終了 /////////////////////////
}
$body = mb_convert_encoding($body,"JIS","UTF-8");
$sbj  = "=?iso-2022-jp?B?".base64_encode(mb_convert_encoding($sbj,"JIS","UTF-8"))."?=";
if(($from_add == 1) || ($from == '')) {
	$from = $_POST['email'];
}
// メールのヘッダ作成
$GMT = date('Z');
$GMT_ABS  = abs($GMT);
$GMT_HOUR = floor($GMT_ABS / 3600);
$GMT_MIN  = floor(($GMT_ABS - $GMT_HOUR * 3600) / 60);
$GMT_FLG  = $GMT>=0 ? '+' : '-';
$GMT_RFC  = date('D, d M Y H:i:s ').sprintf($GMT_FLG.'%02d%02d', $GMT_HOUR, $GMT_MIN);
$header  = "Date: ".$GMT_RFC."\n";
$header .= "From: $from\n";
$header .= "Subject: $sbj\n";
$header .= "MIME-Version: 1.0\n";
$header .= "X-Mailer: PHP/".phpversion()."\n";
$header .= "Content-type: text/plain; charset=ISO-2022-JP\n";
$header .= "Content-Transfer-Encoding: 7bit";
// envelope-from
$opt = '-f'.$from;
if($chmail == 0 || $sendm == 1) {
	//mail($to,$sbj,$body,$header);
	mail($to, $sbj, $body, $header, $opt);
	//if($remail == 1) { mail($reto,$resbj,$rebody,$reheader); }
	if($remail == 1) { mail($reto, $resbj, $rebody, $reheader, $opt); }
// 確認画面の表示
} else {
	htmlHeader();
///// 送信確認画面のレイアウトの編集 --- 開始 /////////////////////////////////
?>
<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>CONTACT</h2>
		</div>
	</div>
	<h3>お問い合わせ</h3>
	<div class="wrapper">

		<!--<p><span class="must">※</span>は必須入力です。</p>-->
		<form name="frm_main" action="<?php echo $script; ?>" method="post" id="form">

				<dl>
				<dt><? echo $err_message; ?></dt>
<?php
foreach($_POST as $key => $var) {
	$key = strtr($key, $string_from, $string_to);
	if(get_magic_quotes_gpc()) $var = stripslashes($var);
	$var = htmlspecialchars($var);
?>


					<dt><?php echo $key; ?></dt>
					<dd><?php echo $var; ?></dd>
					<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $var; ?>" />
				
				

<?php
}
?>
		
			<dd class="btn" style="width:540px;padding-top:30px;">
			<a href="javascript:void(0)" onclick="history.back()" class="alpha back"><img src="img/button02.gif" alt="戻る" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="javascript:void(0)" onclick="document.frm_main.submit();" class="alpha"><img src="img/button03.gif" alt="送信" /></a>
		</dd>
		</dl>
		<input type="hidden" name="eweb_set" value="eweb_submit">
		</form>
	</div>
</section>
<?php
///// 送信確認画面のレイアウトの編集 --- 終了 /////////////////////////////////
	htmlFooter();
}
if(($jpage == 0 && $sendm == 1) || ($jpage == 0 && ($chmail == 0 && $sendm == 0))) {
htmlHeader();
?>
<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<form name="frm_main" id="form">
		<dl>
				<dd style="width:760px;text-align:center;line-height:1.7em;">
					お問い合わせありがとうございます。<br>
					担当者からご連絡いたしますので、今しばらくお待ちくださいませ。

				</dd>
		
		<dd class="btn" style="width:760px;padding-top:100px;text-align:center;">
			<a href="http://tb-m.com/"><img src="img/button04.gif" alt="トップへ戻る" /></a>
			</dd>
		</dl>
		</form>
		</div>
	</div>
</section>

<?php htmlFooter();
} else if (($jpage == 1 && $sendm == 1) || $chmail == 0) {
	header("Location: ".$next);
}
/*------------------------------------------------
 Functions
------------------------------------------------*/
/**
 * ヘッダー部分の出力
 *
 */
function htmlHeader() {
	return get_header();
}
/**
 * フッター部分の出力
 *
 */
function htmlFooter() {
	return get_footer();
}
// end htmlFooter()
?>