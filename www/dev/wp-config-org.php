<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

$IS_DEV = getenv('IS_DEV');
$IS_STAGING = getenv('IS_STAGING');

switch ($IS_DEV):
    case true:
	    define('WP_DEBUG', false);

        if ($IS_STAGING) {
            define('WP_SITEURL', 'http://limex-12s5e84d2.devenv.space/');
            define('WP_HOME', 'http://limex-12s5e84d2.devenv.space/');
        } else {
            define('WP_SITEURL', 'http://localhost');
            define('WP_HOME', 'http://localhost');
        }

        define('WP_CACHE', false); //Added by WP-Cache Manager

        define('DB_NAME', 'xsvx1010717_tbmaction99');
        define('DB_USER', 'root');
        if ($IS_STAGING) {
            define('DB_PASSWORD', 'limexadmin');
        } else {
            define('DB_PASSWORD', '');
        }
        define('DB_HOST', 'localhost');
        define('DB_CHARSET', 'utf8');
        define('DB_COLLATE', '');

        /**#@+
         * 認証用ユニークキー
         *
         * それぞれを異なるユニーク (一意) な文字列に変更してください。
         * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
         * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
         *
         * @since 2.6.0
         */
        define('AUTH_KEY', 'u1n,@%7i+YzzntV+jxTA|*sOT<@>~sh;_Rz2yT]8UvK+USV_HibXWtDZg`)`y&Pv');
        define('SECURE_AUTH_KEY', 'D]3vYB%B`r;}]&k{wR#Nd|%KlGR)|6%tE*cyqr}cT7=@I%Qs{_%(?.Yr-otIKHq0');
        define('LOGGED_IN_KEY', 'P`OPOahdzQ2(Mo(#3j.:Lt7R KEJQUd^0W^-!}|`0bd+3V?}=3BwrX_OUKN-9?e4');
        define('NONCE_KEY', '/8~??Q]tL+[+9k%V6E|fWV>?./9=2{iW0D/:V`+xwOs6`Z`_/lQ|;+RK5B+NrPRP');
        define('AUTH_SALT', 'poFyK=j~I&3Z/cQ/y1Qubzy2-RjB,~|s{QZFK>j$I=0(e[<(Z|#c+^KnL:fN{v+*');
        define('SECURE_AUTH_SALT', 'XT)e/Fsa&--ef:gDMm6#4)yyx m9E4_O<Q-8+qz`{: *g(h]9{n+K8Q$8eb)RO+;');
        define('LOGGED_IN_SALT', '{{D%S$hbd@l$LsS.y+,Qew&|`Z-]e0Sr.1O620ZJ42jN},k+v,Aq;<|RXIn<LD}m');
        define('NONCE_SALT', 'V%`jeZqX#:n2pJEj#OZX:-&_ Cr)+Nj>pks=/-87uRU s.E[<]&=+ |1/u{to~k)');

        /**#@-*/

        /**
         * WordPress データベーステーブルの接頭辞
         *
         * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
         * インストールすることができます。半角英数字と下線のみを使用してください。
         */
        $table_prefix = 'wp_';
        break;
    case false:
        define('WP_CACHE', true); //Added by WP-Cache Manager
        //define('WPCACHEHOME', '/home/xsvx1010717/tb-m.com/public_html/dev/wp-content/plugins/wp-super-cache/'); 
        //Added by WP-Cache Manager
        define('WPCACHEHOME', '/Applications/MAMP/htdocs/dev/wp-content/plugins/wp-super-cache/'); 

        define('DB_NAME', 'xsvx1010717_tbmaction99');
        define('DB_USER', 'root');
        define('DB_PASSWORD', 'root');
        define('DB_HOST', 'localhost');
        define('DB_CHARSET', 'utf8');
        define('DB_COLLATE', '');

        /**#@+
         * 認証用ユニークキー
         *
         * それぞれを異なるユニーク (一意) な文字列に変更してください。
         * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
         * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
         *
         * @since 2.6.0
         */
        define('AUTH_KEY', 'u1n,@%7i+YzzntV+jxTA|*sOT<@>~sh;_Rz2yT]8UvK+USV_HibXWtDZg`)`y&Pv');
        define('SECURE_AUTH_KEY', 'D]3vYB%B`r;}]&k{wR#Nd|%KlGR)|6%tE*cyqr}cT7=@I%Qs{_%(?.Yr-otIKHq0');
        define('LOGGED_IN_KEY', 'P`OPOahdzQ2(Mo(#3j.:Lt7R KEJQUd^0W^-!}|`0bd+3V?}=3BwrX_OUKN-9?e4');
        define('NONCE_KEY', '/8~??Q]tL+[+9k%V6E|fWV>?./9=2{iW0D/:V`+xwOs6`Z`_/lQ|;+RK5B+NrPRP');
        define('AUTH_SALT', 'poFyK=j~I&3Z/cQ/y1Qubzy2-RjB,~|s{QZFK>j$I=0(e[<(Z|#c+^KnL:fN{v+*');
        define('SECURE_AUTH_SALT', 'XT)e/Fsa&--ef:gDMm6#4)yyx m9E4_O<Q-8+qz`{: *g(h]9{n+K8Q$8eb)RO+;');
        define('LOGGED_IN_SALT', '{{D%S$hbd@l$LsS.y+,Qew&|`Z-]e0Sr.1O620ZJ42jN},k+v,Aq;<|RXIn<LD}m');
        define('NONCE_SALT', 'V%`jeZqX#:n2pJEj#OZX:-&_ Cr)+Nj>pks=/-87uRU s.E[<]&=+ |1/u{to~k)');

        /**#@-*/

        /**
         * WordPress データベーステーブルの接頭辞
         *
         * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
         * インストールすることができます。半角英数字と下線のみを使用してください。
         */
        $table_prefix = 'wp_';

        define('WP_DEBUG', false);
        break;

endswitch;

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH'))
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
