$(function(){
	function scroller() {
		var divIndex = $("body"),
		offsetIndex = divIndex.offset().top,
		buttonIndex = $("#logo"),
		divCompany = $(".box_company"),
		offsetCompany = divCompany.offset().top,
		buttonCompany = $("#company"),
		divIdentity = $(".box_identity"),
		offsetIdentity = divIdentity.offset().top,
		buttonIdentity = $("#identity"),
		divWork = $(".box_work"),
		offsetWork = divWork.offset().top,
		buttonWork = $("#work"),
		divCc = $(".box_cc"),
		offsetCc = divCc.offset().top,
		buttonCc = $("#cc"),
		divEntry = $(".box_entry"),
		offsetEtry = divEntry.offset().top,
		buttonEntry = $("#entry");

		buttonIndex.on('click',function(){
			$("html,body").animate({scrollTop: divIndex.offset().top},"normal");
		});
		buttonCompany.on('click',function(){
			$("html,body").animate({scrollTop: divCompany.offset().top-90},"normal");
		});
		buttonIdentity.on('click',function(){
			$("html,body").animate({scrollTop: divIdentity.offset().top-90},"normal");
		});
		buttonWork.on('click',function(){
			$("html,body").animate({scrollTop: divWork.offset().top-90},"normal");
            $('.w01 .filter').delay(500).animate({
            left: '+=10000', 
            }, 1500,'easeInCubic');
            $('.w01_dl').delay(1500).animate({'opacity':'1'},1000,'linear');
            
		});
		buttonCc.on('click',function(){
			$("html,body").animate({scrollTop: divCc.offset().top-90},"normal");
		});
		buttonEntry.on('click',function(){
			$("html,body").animate({scrollTop: divEntry.offset().top-90},"normal");
		});
	};

	function slider() {
		var w = ($(window).width()-648)/2;
		$('.filter_left').css({width: w});
		$('.filter_right').css({width: w});
		$('.film_roll_prev').css({position: 'absolute',left: w-72});
		$('.film_roll_next').css({position: 'absolute',right: w-72});
	}

	function entryList() {
		var side_list = $(".box_entry dl"),
			name = $(".box_entry li dl dt"),
			current = $(".box_entry li dl dt.current"),
			detail_list = $(".box_entry dd");

		detail_list.css({display: "none"});
		current.siblings().css({display: "block"});

		name.on("click",function(){
			$(this).toggleClass("current");
			$(this).siblings().slideToggle();
		});			
	}
	function movieHeight() {
		var h = $('#box_movie').outerHeight();
		$('#box_movie').css({height: h});
	}
    
    function kvslider() {
        // 設定
        var $width =1280; // 横幅
        var $height =525; // 高さ
        var $interval = 4000; // 切り替わりの間隔（ミリ秒）
        var $fade_speed = 2500; // フェード処理の早さ（ミリ秒）
        $("#box_identity_04 ul li").css({"position":"relative","overflow":"hidden","width":$width,"height":$height});
        $("#box_identity_04 ul li").hide().css({"position":"absolute","top":0,"left":0});
        $("#box_identity_04 ul li:first").addClass("active").show();
        setInterval(function(){
        var $active = $("#box_identity_04 ul li.active");
        var $next = $active.next("li").length?$active.next("li"):$("#box_identity_04 ul li:first");
        $active.fadeOut($fade_speed).removeClass("active");
        $next.fadeIn($fade_speed).addClass("active");
        },$interval);
    }

	//実行
//UserAgent で判定
    var userAgent = window.navigator.userAgent.toLowerCase();
    var appVersion = window.navigator.appVersion.toLowerCase();
 
 	if (appVersion.indexOf("msie 8.") != -1 || appVersion.indexOf("msie 9.") != -1) {
        //IE8,9での処理
			$('#kv,#box_about,#box_image_slider,#box_company_h2,#box_company_h3,#box_company_01,#box_company_button,#box_movie,#box_identity_h2,#box_identity_01,#box_identity_02,#box_identity_03,#box_work_title,#box_work_01,#box_work_02,#box_cc_h2,#box_cc_h3,#box_cc_01,#box_cc_02,#box_cc_03,#box_cc_04,#box_cc_05,#box_cc_06,#box_cc_07,#box_cc_08,#box_cc_09,#box_cc_10,#box_cc_11,#box_cc_12,#box_cc_13,#box_entry_01,#box_entry_02,#box_entry_03').css({opacity:'1'});
			$('.w01 .filter,.w02 .filter,.w03 .filter,.w04 .filter,.w05 .filter').css({display:'none'});
			$('.w01_dl,.w02_dl,.w03_dl,.w04_dl,.w05_dl').css({'opacity':'1'});
        } else {
		 $('#kv,#box_about,#box_company_h2,#box_company_h3,#box_company_01,#box_company_button,#box_movie,#box_identity_h2,#box_identity_01,#box_identity_02,#box_identity_03,#box_work_title,#box_work_01,#box_work_02,#box_cc_h2,#box_cc_h3,#box_cc_01,#box_cc_02,#box_cc_03,#box_cc_04,#box_cc_05,#box_cc_06,#box_cc_07,#box_cc_08,#box_cc_09,#box_cc_10,#box_cc_11,#box_cc_12,#box_cc_13,#box_entry_01,#box_entry_02,#box_entry_03').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
		 	$(this).animate({opacity:'1'},200);
		  });
		 $('#box_image_slider').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
		 	$(this).delay(400).animate({opacity:'1'},50);
		  });
		 $('.w01 .filter').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
			if (isInView) {
                console.log(visiblePartY);
				if (visiblePartY == 'top'){
					//要素の全体が見えるようになったときに実行する処理
				    $(this).delay(500).animate({
					left: '+=10000', 
					}, 1500,'easeInCubic');
					$('.w01_dl').delay(1500).animate({'opacity':'1'},1000,'linear');
				}
			}
		  });
		 $('.w02 .filter').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
			if (isInView) {
				if (visiblePartY == 'top'){
					//要素の全体が見えるようになったときに実行する処理
				    $(this).delay(500).animate({
					left: '-=10000', 
					}, 1500,'easeInCubic');
					$('.w02_dl').delay(1500).animate({'opacity':'1'},1000,'linear');
				}
			}
		  });
		 $('.w03 .filter').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
			if (isInView) {
				if (visiblePartY == 'top'){
					//要素の全体が見えるようになったときに実行する処理
				    $(this).delay(500).animate({
					left: '+=10000', 
					}, 1500,'easeInCubic');
					$('.w03_dl').delay(1500).animate({'opacity':'1'},1000,'linear');
				}
			}
		  });
		 $('.w04 .filter').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
			if (isInView) {
				if (visiblePartY == 'top'){
					//要素の全体が見えるようになったときに実行する処理
				    $(this).delay(500).animate({
					left: '-=10000', 
					}, 1500,'easeInCubic');
					$('.w04_dl').delay(1500).animate({'opacity':'1'},1000,'linear');
				}
			}
		  });
		 $('.w05 .filter').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
			if (isInView) {
				if (visiblePartY == 'top'){
					//要素の全体が見えるようになったときに実行する処理
				    $(this).delay(500).animate({
					left: '+=10000', 
					}, 1500,'easeInCubic');
					$('.w05_dl').delay(1500).animate({'opacity':'1'},1000,'linear');
				}
			}
		  });
		 $('.w06 .filter').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
			if (isInView) {
				if (visiblePartY == 'top'){
					//要素の全体が見えるようになったときに実行する処理
				    $(this).delay(500).animate({
					left: '-=10000', 
					}, 1500,'easeInCubic');
					$('.w06_dl').delay(1500).animate({'opacity':'1'},1000,'linear');
				}
			}
		  });
        }

	scroller();
	entryList();
	$(window).load(function(){
		slider();
		movieHeight();
	});
	$(window).resize(function(){
		slider();
		movieHeight();
	});
    kvslider();

});
