<?php
// WordPres用
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
require_once $_SERVER['DOCUMENT_ROOT']. '/wp-blog-header.php';

?>
<?php get_header(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/order-sp.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css" type="text/css" media="all" />
<script type="text/javascript" src="/order/js/order.js"></script>

<style type="text/css">

.explain h2 {
  background-color: #2E3F4E;
  color: #fff;
  line-height:150%;
  font-size:17px !important;
  padding:15px 0 15px 0;
  letter-spacing:1.5px;
}

.explain h2 .msCC {
  font-size: 12px;
}

</style>

<img class="full" src="img/explain-img-camcard.jpg">


<div class="wrapper explain">
	<div class="center mt20">
		<img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/order/img/sp/explain-title.png" width="280">
	</div>
	<div class="lead">

		<p>
		LIMEXとは、石灰石を主原料とする革命的新素材。<br>
		これまで、紙の生産には膨大な水と木が必要でした。<br>
		しかし、世界中に無尽蔵に存在する<br>
		石灰石が主原料のLIMEXを使用すれば、<br>
		水と木を使うことなく、紙がつくれます。
		</p>
		<p>
		これから、世界の紙の消費量は2030年に<br>
		現在の2倍になると予測されています。<br>
		さらに、2050年には世界人口の40%が<br>
		深刻なストレスに直面し、<br>
		100年後には世界の主要な森林が<br>
		消失するといわれています。<br>
		地球と水と木の資源問題は、<br>
		加速的に進行しているのです。
		</p>
		<p>
		地球を守る新素材革命は、名刺１枚からはじまります。
		</p>
			<a href="https://tb-m.com/action/" target="_blank" alt="LIMEX ACTION"><img class="full mt20" src="img/explain-action.jpg"></a>
		<p style="text-align:left;">LIMEXによる水や森林資源保護への貢献度を可視化し、地球の環境問題を啓発するプラットフォーム “LIMEX ACTION”。LIMEXを導入されるみなさまの企業名、LIMEXを活用したCSRへの取り組みを、当サイトにてご紹介していきます。</p>
		<p style="text-align:left;">＊LIMEXを活用したCSRへの取り組みをご紹介させていただく場合、LIMEXの販売元である株式会社TBMよりご連絡させていただきます。</p>
	</div>
	<div class="center mt20">
			<a href="/order/?q=130012" class="button">ご注文・お見積</a>
	</div>
	<p class="center mt10">プロモーションコード "130012" および、備考欄に「CAMCARD BUSINESSユーザー様向け特別プロモーション価格適用」と自動で入力されます。<br />
	<br>
    ご質問や不明な点がある方は<br>お電話にてお問い合わせ下さい<br>
	TEL : 03-3538-6777</p>
	<section>
		<h2>CAMCARD BUSINESS ユーザー様<br>
		プロモーション価格<br>
		<span class="msCC">直販価格の20%割引にてご提供</span></h2>
		<h3>プロモーション価格(税抜)</h3>
		<table>
			<tr>
				<th rowspan="3">片面<br>
				カラー</th>
				<th>100枚</th>
				<td>¥1,400</td>
			</tr>
			<tr>
				<th>200枚※</th>
				<td>¥2,690</td>
			</tr>
			<tr>
				<th>300枚※</th>
				<td>¥3,860</td>
			</tr>
			<tr>
				<th rowspan="3">片面<br>
				黒</th>
				<th>100枚</th>
				<td>¥1,000</td>
			</tr>
			<tr>
				<th>200枚※</th>
				<td>¥1,920</td>
			</tr>
			<tr>
				<th>300枚※</th>
				<td>¥2,760</td>
			</tr>
			<tr>
				<th rowspan="3">両面<br>
				カラー/カラー	</th>
				<th>100枚</th>
				<td>¥2,000</td>
			</tr>
			<tr>
				<th>200枚※</th>
				<td>¥3,840</td>
			</tr>
			<tr>
				<th>300枚※</th>
				<td>¥5,520</td>
			</tr>
			<tr>
				<th rowspan="3">両面<br>
				カラー/黒	</th>
				<th>100枚</th>
				<td>¥1,800</td>
			</tr>
			<tr>
				<th>200枚※</th>
				<td>¥3,460</td>
			</tr>
			<tr>
				<th>300枚※</th>
				<td>¥4,970</td>
			</tr>
			<tr>
				<th rowspan="3">両面<br>
				黒/黒</th>
				<th>100枚</th>
				<td>¥1,200</td>
			</tr>
			<tr>
				<th>200枚※</th>
				<td>¥2,300</td>
			</tr>
			<tr>
				<th>300枚※</th>
				<td>¥3,310</td>
			</tr>
		</table>
		<p class="note mt10">※当該料金は、同一データを複数セット(200枚以上)ご注文いただく場合の料金となります。400枚以上ご注文の場合、単価は300枚ご注文頂く場合の単価と同一となります。</p>
		<h3>オプション料金(税抜)</h3>
		<table class="option">
            <tr>
              <th colspan="2">サイズ</th>
            </tr>
            <tr>
               <td class="head">イレギュラーサイズ対応料</td>
               <td>+¥250～<br>
               ※一般的な名刺サイズ(91mm×55mm)以外の場合</td>
            </tr>
            <tr>
              <th colspan="2">デザイン</th>
            </tr>
            <tr>
               <td class="head">ゼロベースから新規デザインをご希望の場合</td>
               <td>+¥35,000</td>
            </tr>
            <tr>
               <td class="head">大体のデザインをご自身で制作し、アップロードする場合<br>(Excel・PowerPoint・Word等)</td>
               <td>+¥10,000~¥20,000</td>
            </tr>
            <tr>
               <td class="head">ご希望のデザインに近い名刺のスキャンデータ等、明確なイメージをアップロードする場合</td>
               <td>片面につき+¥3,000～</td>
            </tr>
            <tr>
               <td class="head">入稿データがないため、ご希望の名刺デザインのスキャンデータ(及びロゴデータ)等をアップロードする場合</td>
               <td>片面につき＋¥2,000～<br>＊トレース(データ起こし)料</td>
            </tr>
            <tr>
               <td class="head">入稿データをアップロードする場合<br>(イラストレーター・Excel・PowerPoint・Word 等)</td>
               <td>+¥0<br>＊データによってはトレース(データ起こし)料が発生する場合があります</td>
            </tr>
            <tr>
               <td class="head">LIMEX名刺デザインテンプレートから選択する場合</td>
               <td>+¥0</td>
            </tr>
						<tr>
              <th colspan="2">加工</th>
            </tr>
						<tr>
							 <td class="head">加工をご希望の場合<br>（角丸、箔押し等）</td>
							 <td>応相談<br>※別途見積もりとなります</td>
						</tr>
        </table>

           <dl class="cf card-template">
                <dt>
                    名刺テンプレート
                    <input type="hidden" name="card-type" class="card-type" value="TYPE01_G">
                </dt>
                <dd>
                    <ul class="select-type cf">
                        <li class="now-type">ゴシック体</li>
                        <li>明朝体</li>
                    </ul>
                    <ul class="card-slider bxslider">
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/gosick/TYPE01_G-001.png" alt="TYPE01_G">
                                </div>
                                <p>TYPE1 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/mintyou/TYPE01_M-001.png" alt="TYPE01_M">
                                </div>
                                <p>TYPE1 明朝体</p>
                            </div>
                        </li>
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/gosick/TYPE02_G-001.png" alt="TYPE02_G">
                                </div>
                                <p>TYPE2 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/mintyou/TYPE02_M-001.png" alt="TYPE02_M">
                                </div>
                                <p>TYPE2 明朝体</p>
                            </div>
                        </li>
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/gosick/TYPE03_G-001.png" alt="TYPE03_G">
                                </div>
                                <p>TYPE3 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/mintyou/TYPE03_M-001.png" alt="TYPE03_M">
                                </div>
                                <p>TYPE3 明朝体</p>
                            </div>
                        </li>
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/gosick/TYPE04_G-001.png" alt="TYPE04_G">
                                </div>
                                <p>TYPE4 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/mintyou/TYPE04_M-001.png" alt="TYPE04_M">
                                </div>
                                <p>TYPE4 明朝体</p>
                            </div>
                        </li>
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/gosick/TYPE05_G-001.png" alt="TYPE05_G">
                                </div>
                                <p>TYPE5 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/mintyou/TYPE05_M-001.png" alt="TYPE05_M">
                                </div>
                                <p>TYPE5 明朝体</p>
                            </div>
                        </li>
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/gosick/TYPE06_G-001.png" alt="TYPE06_G">
                                </div>
                                <p>TYPE6 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img class="small" src="../../order/img/mintyou/TYPE06_M-001.png" alt="TYPE06_M">
                                </div>
                                <p>TYPE6 明朝体</p>
                            </div>
                        </li>
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img src="../../order/img/gosick/TYPE07_G-001.png" alt="TYPE07_G">
                                </div>
                                <p>TYPE7 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img src="../../order/img/mintyou/TYPE07_M-001.png" alt="TYPE07_M">
                                </div>
                                <p>TYPE7 明朝体</p>
                            </div>
                        </li>
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img src="../../order/img/gosick/TYPE08_G-001.png" alt="TYPE08_G">
                                </div>
                                <p>TYPE8 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img src="../../order/img/mintyou/TYPE08_M-001.png" alt="TYPE08_M">
                                </div>
                                <p>TYPE8 明朝体</p>
                            </div>
                        </li>
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img src="../../order/img/gosick/TYPE09_G-001.png" alt="TYPE09_G">
                                </div>
                                <p>TYPE9 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img src="../../order/img/mintyou/TYPE09_M-001.png" alt="TYPE09_M">
                                </div>
                                <p>TYPE9 明朝体</p>
                            </div>
                        </li>
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img src="../../order/img/gosick/TYPE10_G-001.png" alt="TYPE10_G">
                                </div>
                                <p>TYPE10 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img src="../../order/img/mintyou/TYPE10_M-001.png" alt="TYPE10_M">
                                </div>
                                <p>TYPE10 明朝体</p>
                            </div>
                        </li>
                        <li>
                            <div class="gosick">
                                <div class="templateimage">
                                    <img src="../../order/img/gosick/TYPE11_G-001.png" alt="TYPE11_G">
                                </div>
                                <p>TYPE11 ゴシック体</p>
                            </div>
                            <div class="mintyou">
                                <div class="templateimage">
                                    <img src="../../order/img/mintyou/TYPE11_M-001.png" alt="TYPE11_M">
                                </div>
                                <p>TYPE11 明朝体</p>
                            </div>
                        </li>
                    </ul>
                </dd>
            </dl>

		<h3>納品スケジュール</h3>
		<ul class="points">
                    <li>データ入稿日(※16時までの入稿)から3営業日後<br />にご納品。</li>
                    <li>弊社にてデータ制作が必要なご注文については、<br />内容により制作期間が異なりますので、お早めにお間い合わせください。</li>
                    <li>通常、土日祝日、年未年始を除く平日納品となりますが、ご希望により土曜日のお届けは可能です。</li>
                    <li>加工名刺などは、仕様により納期が異なります。</li>
                    <li>天災など予期せぬ事故によって、納期遅延が生じる場合があります。</li>
		</ul>
		<div class="center mt20">
			<a href="/order/?q=130012" class="button">ご注文・お見積</a>
		</div>
	<p class="center mt10">プロモーションコード "130012" および、備考欄に「CAMCARD BUSINESSユーザー様向け特別プロモーション価格適用」と自動で入力されます。<br />
	<br>
    ご質問や不明な点がある方は<br>お電話にてお問い合わせ下さい<br>
	TEL : 03-3538-6777</p>
	</section>
	<section>
		<h2>注意事項</h2>
		<dl>
			<dt>【お支払について】</dt>
			<dd>基本的に初回ご注文のお客様には事前の銀行お振り込みをお願いしています。<br>
			詳細についてはお申し込み後、メールにてご案内させていただきます。</dd>
			<dt>【印刷の仕上がりについて】</dt>
			<dd>
                            他業者様にて印刷されていた名刺と、仕上がりに若干違いが出る場合があります。<br />
                            特に広い塗り面積(ベタ色)・グラデーション等のデザインでは、色の仕上がりに差異が生じる場合があります。<br />
                            断裁の際、若干のズレが生じる場合があり、2ｍｍ以内の断裁ズレについては良品扱いとなります。<br />
                            その為、枠や左右対称になるデザインはズレが非常に目立ち易くなりますので、避けていただきますようお願いします。
			</dd>
			<dt>【著作権、肖像権について】</dt>
			<dd>
			 著作権のあるもの（ロゴマーク、著名人やアニメキャラクター）は、権利元の許可がなければ製作いたしかねます。<br />
                        お客様より支給頂いた画像等は、権利処理がされたとみなしてお取り扱いいたします。<br />
                        賠償責任などの問題が発生した場合、弊社では責任を負いかねますので、十分ご注意の上支給いただきますようお願いたします。
			</dd>
			<dt>【キャンセル、返品について】</dt>
			<dd>
                            入稿手配後、速やかに印刷・加工作業を行う為、入稿当日１６:００以降のキャンセルはお受けできません。ご注文内容をよくご確認の上、ご注文をお願い致します。<br />
                        万一不良品が含まれていた場合は、商品到着後14日以内にご連絡をお願いします。<br />
                        商品の交換については未使用の場合の対応いたしますので、料金着払いにてご返送ください。<br />
                        いかなる場合におきましてもご注文代金を超える補償は弊社でお受けできません。あらかじめご了承ください。<br />
                        お客様からいただいた入稿データ、および校了後の不備等により発生した不良品につきましては弊社は責任を負いかねます。<br />
                        また運送会社による商品破損などの場合、弊社は免責とさせていただきます。
                        </dd>
		</dl>

		<div class="center mt20">
			<a href="/order/?q=130012" class="button">ご注文・お見積</a>
		</div>
	<p class="center mt10">プロモーションコード "130012" および、備考欄に「CAMCARD BUSINESSユーザー様向け特別プロモーション価格適用」と自動で入力されます。<br>
	<br>
    ご質問や不明な点がある方は<br>お電話にてお問い合わせ下さい<br>
	TEL : 03-3538-6777</p>
	</section>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/card.js"></script>
<script>



$(document).ready(function(){





    $('.bxslider').bxSlider({
touchEnabled: true,
    });
});
var LIMEX = LIMEX || {};
LIMEX.globalNav = (function($){
  var buttonMenu;
  var navGlobal;
  var _init = function() {
    buttonMenu = $("#button-menu");
    navGlobal = $("#nav-global");
    navGlobal.find(".bg").on("click", function(){
      close();
    })
    $("body").css({
      minHeight: $(window).height()
    });
    navGlobal.css({
      minHeight: $(window).height() - 50
    });
    buttonMenu.on("click", function(){
      if(buttonMenu.hasClass('active')) {
        close();
        if($("div.story").css("display") == "block") {
          $("div.story").fadeOut(200);
        }
      } else {
        open();
      }
    })
    navGlobal.find("li:has('li')").children('a')
      .addClass('close')
      .on("click", function(){
        $(this).next().slideToggle(300, function(){
          setBodyHeight();
        });
        $(this).toggleClass('open');
        return false;
      })


    $("#key").on("click", ".show-story", function(){
      $("div.story").fadeIn(200);
      buttonMenu.addClass('active');
      $("body").css({
        height: $("div.story").height() + 100,
        overflowY: "hidden"
      })
      setBodyHeight();
      return false;
    })
  }
  var setBodyHeight = function(){
    $("body").css({
      //height: navGlobal.height() + 100,
      height: navGlobal.height() + 150,
      overflowY: "hidden"
    })
  }
  var open = function() {
    buttonMenu.addClass('active');
    navGlobal.fadeIn('200');
    setBodyHeight();
  }
  var close = function(){
    buttonMenu.removeClass('active');
    navGlobal.fadeOut('200');
    $("body").css({
      height: "auto",
      overflowY: ""
    })
  }
  var _self = {
    init: _init
  }
  return _self;
}(jQuery));

$(LIMEX.globalNav.init);
</script>
<?php
get_footer();
