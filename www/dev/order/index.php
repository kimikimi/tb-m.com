<?php
$IS_DEV = getenv('IS_DEV');

if (($_SERVER['SERVER_PORT']) == 80 && !$IS_DEV) {
    header("Location: https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
    exit;
}
?><?php
// silex
require_once __DIR__ . '/vendor/autoload.php';
$app = new Silex\Application();

use Symfony\Component\HttpFoundation\Response;

// wordpress依存
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-blog-header.php';

// カレントの言語を日本語に設定する
mb_language("ja");
// 内部文字エンコードを設定する
mb_internal_encoding("UTF-8");
// sessionスタート
session_start();

require_once __DIR__ . "/include/Config.class.php";
// 設定ファイルのロード
$config = require_once __DIR__ . "/include/form_data.php";
Config::set($config);

require_once __DIR__ . "/include/config.php";
require_once __DIR__ . "/include/Validate.class.php";
require_once __DIR__ . "/include/Order.class.php";
require_once __DIR__ . "/include/Value.class.php";
require_once __DIR__ . "/include/arr.php";
require_once __DIR__ . "/include/view.php";
require_once __DIR__ . "/include/mail.php";
require_once __DIR__ . "/include/helper.php";

// session初期化
if (!isset($_SESSION[Order::$sessPagePrefix])) {
    $_SESSION = array(Order::$sessPagePrefix => '');
}


// root /input
$app->get('/', function () {
    // セッションに格納したオブジェクトを取得、なければインスタンス生成
    $valueInstance = unserialize(Order::getSession('post_data'));
    if (!$valueInstance) {
        $valueInstance = new Value();
    }

    // 初回アクセス時初期化
    if (!isset($_GET['status'])) {
        Order::deleteSession();
    }
    // 完了後のブラウザバック用トークン
    $token = getToken();
    Order::setSession('contentsToken', $token);

    $data = array(
        'valueInstance' => $valueInstance,
    );
    return View::render(__DIR__ . getTempletePath() . '/input.php', $data);
});

// 確認画面
$app->post('/confirm', function () {

    // 直アクセスはエラー（トップへ）
    if (empty($_POST) || !Order::getSession('contentsToken')) {
        Order::doSystemErr();
    }

    // 確認処理
    // ページ読み込み時イニシャル処理
    // POSTされたデータをインスタンスにセットする
    $valueInstance = setPost('Value');

    // インスタンスをセッションにセット
    Order::setSession('post_data', serialize($valueInstance));

    // 入力値チェック
    $valid = new Validate(null);
    if (!$valid->start($valueInstance)) {
        // メッセージを取得し、入力画面読み込み
        Order::setSession('errmsg', $valid->getMessage(), true);
        Order::location(Order::ORDER_DIR . Order::ORDER_TOP . '?status=err');
    }
    Order::setSession('errmsg', array(), true);

    // トークン生成
    $token = getToken();
    Order::setSession('token', $token);
    $data = array(
        'valueInstance' => $valueInstance,
        'token' => $token,
    );
    return View::render(__DIR__ . getTempletePath() . '/confirm.php', $data);
});

// 処理
$app->post('/order', function () {
    // 事前チェック
    if (empty($_POST) || Order::getSession('token') !== $_POST['token']) {
        Order::doSystemErr();
    }
    // トークン削除
//    unset($_SESSION[Order::$sessPagePrefix]['token']);

    // セッションからインスタンス取得
    $valueInstance = unserialize(Order::getSession('post_data'));

    // メールで展開する変数のセット
    $mailValues = array(
        'orderId' => date('ymdHi') . sprintf('%06d', rand(10000, 999999)),
        // 'valueInstance' => $valueInstance,
        'meishi_name' => $valueInstance->meishi_name,
        'meishi_company' => $valueInstance->meishi_company,
        'meishi_employees' => $valueInstance->meishi_employees,
        'meishi_name' => $valueInstance->meishi_name,
        'meishi_kana' => $valueInstance->meishi_kana,
        'meishi_post1' => $valueInstance->meishi_post1,
        'meishi_post2' => $valueInstance->meishi_post2,
        'meishi_email' => $valueInstance->meishi_email,
        'meishi_orders' => $valueInstance->meishi_orders,
        'meishi_tel' => $valueInstance->meishi_tel,
        'meishi_know_sales' => $valueInstance->meishi_know_sales ? $valueInstance->meishi_know_sales : null,
        'meishi_know_site' => $valueInstance->meishi_know_site ? $valueInstance->meishi_know_site : null,
        'meishi_know_program' => $valueInstance->meishi_know_program ? $valueInstance->meishi_know_program : null,
        'meishi_know_paper' => $valueInstance->meishi_know_paper ? $valueInstance->meishi_know_paper : null,
        'meishi_know_seminar' => $valueInstance->meishi_know_seminar ? $valueInstance->meishi_know_seminar : null,
        'meishi_use' => Config::get('useNumList.' . $valueInstance->meishi_use),
        'meishi_file' => $valueInstance->meishi_file,
        'related_news' => $valueInstance->related_news,
        'meishi_note' => $valueInstance->meishi_note,
        'hasNouhin' => $valueInstance->hasNouhin(),
        'hasPromoPrice' => $valueInstance->hasPromoPrice(),
        'nouhin_company' => $valueInstance->nouhin_company,
        'nouhin_name' => $valueInstance->nouhin_name,
        'nouhin_kana' => $valueInstance->nouhin_kana,
        'nouhin_post1' => $valueInstance->nouhin_post1,
        'nouhin_post2' => $valueInstance->nouhin_post2,
        'nouhin_tel' => $valueInstance->nouhin_tel,
        'user_subject' => '[LIMEX] ご依頼内容の確認',
        'user_subject_template' => '[LIMEX] 名刺に記載するテキスト情報ご返信のお願い',
        'admin_subject' => '[LIMEX] 注文確定(admin)',
        'related_news' => $valueInstance->related_news == 1 ? '配信する' : '配信しない',
        'pr_agree' => $valueInstance->pr_agree == 1 ? '同意する' : '同意しない',
        'colorNumString' => $valueInstance->getColorNumString(Config::get('colorNumList')),
        'colorNumArray' => $valueInstance->getColorNumArray(Config::get('colorNumList')),
        'priceNumString' => $valueInstance->getPriceNumString(),
        'promo' => $valueInstance->meishi_promo ? $valueInstance->meishi_promo : 'なし',
        'file_url' => $valueInstance->meishi_file_unique ? 'http://' . $_SERVER["HTTP_HOST"] . '/order/upload/' . $valueInstance->meishi_file_unique : 'なし',
        'address' => Config::get('prefectures.' . $valueInstance->meishi_pref) . $valueInstance->meishi_addr,
        'setNum' => Config::get('perPersonSetNumList.' . $valueInstance->meishi_per_person_set),
        'orderSet' => $valueInstance->meishi_per_person_set * $valueInstance->meishi_orders,
        'totalSet' => $valueInstance->meishi_per_person_set * $valueInstance->meishi_orders * 100,
        'ai' => Config::get('aiNumList.' . $valueInstance->meishi_ai),
        'knowPattern' => Config::get('knowPatternNumlist.' . $valueInstance->meishi_know_pattern),
        'nouhinAddress' => Config::get('prefectures.' . $valueInstance->nouhin_pref) . $valueInstance->nouhin_addr,
        'meishi_tmpnum' => Config::get('templateNumList.' . $valueInstance->meishi_tmpnum),
        'answer' => $valueInstance->getAnswer(),
        'file_name' => $valueInstance->meishi_file_unique,
        // post先の管理でしか使わない
        'status' => 0,
        'meishi_post' => $valueInstance->meishi_post1 . '-' . $valueInstance->meishi_post2,
        'nouhin_post' => $valueInstance->nouhin_post1 . '-' . $valueInstance->nouhin_post2,
        'meishi_pref' => Config::get('prefectures.' . $valueInstance->meishi_pref),
        'meishi_addr' => $valueInstance->meishi_addr,
        'nouhin_pref' => Config::get('prefectures.' . $valueInstance->nouhin_pref),
        'nouhin_addr' => $valueInstance->nouhin_addr,
        'colors' => $valueInstance->meishi_colornum,
        'price' => $valueInstance->meishi_promo_price,
    );

    // $meishi_buy_pattern
    $is_buy_pattern_6 = false;
    foreach ($valueInstance->meishi_buy_pattern as $key => $value) {
        $mailValues['buyPattern'][$key] = Config::get('buyPatternNumList.' . $value);
        $mailValues['buyPatternId'][$key] = $value;

        if ($key == 6) {
            $is_buy_pattern_6 = true;
        }
    }

    // $meishi_card_type
    foreach ($valueInstance->meishi_card_type as $key => $value) {
        $mailValues['meishi_card_type'][$key] = $value;
    }

    // $meishi_design_pattern
    foreach ($valueInstance->meishi_design_pattern as $key => $value) {
        $mailValues['meishi_design_pattern'][$key] = $value;
    }

    // $meishi_design_pattern_back
    foreach ($valueInstance->meishi_design_pattern_back as $key => $value) {
        $mailValues['meishi_design_pattern_back'][$key] = $value;
    }


    if ($valueInstance->meishi_per_person_set > 10) {
        $mailValues['meishi_order_total_text'] = '合計セット数: -';
        $mailValues['meishi_order_total_text_admin'] = ' -';

    } else {
        $mailValues['meishi_order_total_text'] = '合計セット数: ' . $mailValues['orderSet'] . 'セット (' . $mailValues['totalSet'] . '枚)';
        $mailValues['meishi_order_total_text_admin'] = '' . $mailValues['orderSet'] . 'セット (' . $mailValues['totalSet'] . '枚)';

    }

    if (isset($valueInstance->meishi_colornum[1]) && !empty($valueInstance->meishi_colornum[1])) {
        $mailValues['side_colorful'] = $valueInstance->meishi_colornum[1];
    } else {
        unset($mailValues['buyPattern'][1]);
    }
    if (isset($valueInstance->meishi_colornum[2]) && !empty($valueInstance->meishi_colornum[2])) {
        $mailValues['side_black'] = $valueInstance->meishi_colornum[2];
    } else {
        unset($mailValues['buyPattern'][2]);
    }
    if (isset($valueInstance->meishi_colornum[3]) && !empty($valueInstance->meishi_colornum[3])) {
        $mailValues['both_colorful_colorful'] = $valueInstance->meishi_colornum[3];
    } else {
        unset($mailValues['buyPattern'][3]);
    }
    if (isset($valueInstance->meishi_colornum[4]) && !empty($valueInstance->meishi_colornum[4])) {
        $mailValues['both_colorful_black'] = $valueInstance->meishi_colornum[4];
    } else {
        unset($mailValues['buyPattern'][4]);
    }
    if (isset($valueInstance->meishi_colornum[5]) && !empty($valueInstance->meishi_colornum[5])) {
        $mailValues['both_black_black'] = $valueInstance->meishi_colornum[5];
    } else {
        unset($mailValues['buyPattern'][5]);
    }
    if (isset($valueInstance->meishi_promo_price['44CQ54mH6Z2i44CRIOOCq+ODqeODvA==']) && !empty($valueInstance->meishi_promo_price['44CQ54mH6Z2i44CRIOOCq+ODqeODvA=='])) {
        $mailValues['price_side_colorful'] = $valueInstance->meishi_promo_price['44CQ54mH6Z2i44CRIOOCq+ODqeODvA=='];
    }
    if (isset($valueInstance->meishi_promo_price['44CQ54mH6Z2i44CRIOm7kg==']) && !empty($valueInstance->meishi_promo_price['44CQ54mH6Z2i44CRIOm7kg=='])) {
        $mailValues['price_side_black'] = $valueInstance->meishi_promo_price['44CQ54mH6Z2i44CRIOm7kg=='];
    }
    if (isset($valueInstance->meishi_promo_price['44CQ5Lih6Z2i44CRIOOCq+ODqeODvCAvIOOCq+ODqeODvA==']) && !empty($valueInstance->meishi_promo_price['44CQ5Lih6Z2i44CRIOOCq+ODqeODvCAvIOOCq+ODqeODvA=='])) {
        $mailValues['price_both_colorful_colorful'] = $valueInstance->meishi_promo_price['44CQ5Lih6Z2i44CRIOOCq+ODqeODvCAvIOOCq+ODqeODvA=='];
    }
    if (isset($valueInstance->meishi_promo_price['44CQ5Lih6Z2i44CRIOOCq+ODqeODvCAvIOm7kg==']) && !empty($valueInstance->meishi_promo_price['44CQ5Lih6Z2i44CRIOOCq+ODqeODvCAvIOm7kg=='])) {
        $mailValues['price_both_colorful_black'] = $valueInstance->meishi_promo_price['44CQ5Lih6Z2i44CRIOOCq+ODqeODvCAvIOm7kg=='];
    }
    if (isset($valueInstance->meishi_promo_price['44CQ5Lih6Z2i44CRIOm7kiAvIOm7kg==']) && !empty($valueInstance->meishi_promo_price['44CQ5Lih6Z2i44CRIOm7kiAvIOm7kg=='])) {
        $mailValues['price_both_black_black'] = $valueInstance->meishi_promo_price['44CQ5Lih6Z2i44CRIOm7kiAvIOm7kg=='];
    }

    // 管理へデータをpostする
    $response = postToAdmin($mailValues);
    if (!$response) {
        return new Response('システムエラーが発生しました。', 500);
    }

    try {
        global $IS_DEV;
        if (!$IS_DEV) {
            if ($is_buy_pattern_6) {
                // 管理送信
                Mail::send(
                    Mail::SUPPORT_MAIL_TO,
                    $mailValues['admin_subject'],
                    view::render(__DIR__ . '/include/mail_template_admin.php', $mailValues),
                    Mail::USER_MAIL_FROM
                );
                // ユーザー送信
                Mail::send(
                    $valueInstance->meishi_email,
                    $mailValues['user_subject_template'],
                    view::render(__DIR__ . '/include/mail_template_client.php', $mailValues),
                    Mail::USER_MAIL_FROM,
                    __DIR__ . '/include/LIMEX_businesscard_text.xlsx'
                );
            } else {
                // 管理送信
                Mail::send(
                    Mail::SUPPORT_MAIL_TO,
                    $mailValues['admin_subject'],
                    view::render(__DIR__ . '/include/mail_template_admin.php', $mailValues),
                    Mail::USER_MAIL_FROM
                );
                // ユーザー送信
                Mail::send(
                    $valueInstance->meishi_email,
                    $mailValues['user_subject'],
                    view::render(__DIR__ . '/include/mail_template_client.php', $mailValues),
                    Mail::USER_MAIL_FROM
                );
            }

            // プロモーションコードがあった場合代理店へ送信
            if ($valueInstance->promo) {
                // 代理店送信
                // Mail::send(
                //     $valueInstance->meishi_email,
                //     Arr::get($mailValues, 'user_subject'),
                //     view::render(__DIR__ . '/include/mail_template_client.php', $mailValues),
                //     Mail::USER_MAIL_FROM
                // );
            }
        } else {
            // 開発環境の場合
            $mailBodies = array();
            $mailBodies['to_admin'] = view::render(__DIR__ . '/include/mail_template_admin.php', $mailValues);
            $mailBodies['to_client'] = view::render(__DIR__ . '/include/mail_template_client.php', $mailValues);

            file_put_contents(__DIR__.'/mailbody.txt', $mailBodies);
            exit;
        }
    } catch (Exception $e) {
        // エラー
//        Order::setSession('errmsg', array('system' => Order::SYSTEM_ERR_MSG_01 . '(2)'));
//        Order::location(Order::ORDER_DIR . Order::ORDER_TOP . '?status=err', true);
    }

//    Order::deleteSession();
    Order::location(Order::ORDER_DIR . '/complete?order_id=' . Arr::get($mailValues, 'orderId'));
});

// 完了画面
$app->get('/complete', function () {
    return View::render(__DIR__ . getTempletePath() . '/complete.php');
});

// ファイルアップロード
$app->post('/ajax/upload', function () use ($app) {
    $dir = __DIR__ . '/upload/';
    $tmpFileName = Arr::get($_FILES, 'file.tmp_name');
    $origineFileName = Arr::get($_FILES, 'file.name');

    if (!isset($_FILES['file']['error']) || !is_int($_FILES['file']['error'])) {
        return new Response('bad request', 400);
    }

    //バリデーション
    try {
        switch ($_FILES['file']['error']) {
            case UPLOAD_ERR_OK: // OK
                break;
            case UPLOAD_ERR_NO_FILE:   // ファイル未選択
                throw new RuntimeException('ファイルが選択されていません');
            case UPLOAD_ERR_INI_SIZE:  // php.ini定義の最大サイズ超過
            case UPLOAD_ERR_FORM_SIZE: // フォーム定義の最大サイズ超過 (設定した場合のみ)
                throw new RuntimeException('ファイルサイズが大きすぎます');
            default:
                throw new RuntimeException('その他のエラーが発生しました');
        }

        // // ここで定義するサイズ上限のオーバーチェック
        // // (必要がある場合のみ)
        // 10MB
        $mb = 104857600;
        if ($_FILES['file']['size'] > $mb) {
            throw new RuntimeException('ファイルサイズが大きすぎます');
        }
    } catch (Exception $e) {
        // エラー発生時システムにメール送信
        Mail::send(
            'endo@impv.co.jp,nozawa@impv.co.jp',
            'LIMEX 名刺オーダーのアップロードエラー',
            $e->getMessage(),
            Mail::USER_MAIL_FROM
        );
        return new Response('bad request', 400);
    }

    // アップされたファイル名に現在に時刻を足し、ユニークなファイル名をつくる
    $createUniqueFileName = function ($origineFileName) {
        $strings = explode('.', $origineFileName);
        list($fileName, $fileType) = $strings;
        return date('YmdHis') . '.' . $fileType;
    };
    $uniqueFileName = $createUniqueFileName($origineFileName);
    $deployFilePath = $dir . $uniqueFileName;

    // ファイルのデプロイが成功したらファイル名をjsonで返却する]
    $moveResult = @move_uploaded_file($tmpFileName, $deployFilePath);
    if ($moveResult) {
        chmod($deployFilePath, 0644);
        // 成功
        $data = array(
            'deployFileName' => $uniqueFileName,
            'origineFileName' => $origineFileName,
        );
        $json = json_encode($data);
        header('content-type: application/json; charset=utf-8');
        return new Response($json, 200);
    } else {
        // 失敗
        return new Response('Internal Server Error', 500);
    }
});

$app->run();
