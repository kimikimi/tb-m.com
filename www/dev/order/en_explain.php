<?php
// WordPres用
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
require_once $_SERVER['DOCUMENT_ROOT']. '/wp-blog-header.php';

// 英語order stop
header( "Location: /en/" ) ;
exit();

if(wp_is_mobile()){
    include('./sp_en_explain.php');
    exit();
}

?>
<?php get_header('en'); ?>

<script>
    $('body').addClass('explain');
    $('body').removeClass('explain.php');
</script>
<style type="text/css">
.contact_text{
text-align:center;
margin-top:25px;
font-size:18px;
line-height:1.8;
}
</style>
<link type="text/css" rel="stylesheet" href="./css/style.css" />
<script type="text/javascript" src="./js/order.js"></script>

<style type="text/css">
.explain .keyv .img {
    width: 940px;
    background: url("img/en/explain-kv.png") no-repeat;
    min-height: 620px;
}
.explain .keyv h3 {
    /* margin: 40px auto 20px; */
    width: auto; 
    height: 74px;
    margin: 40px auto 40px;
    min-height: 35px;
    background: url("img/en/keyv-title.png") center no-repeat;
    background-size: contain;
}
.explain .about .des.column_2 li.text {
    min-height: 180px;
    background: url("img/en/about-des-text.png") center left no-repeat;
    background-size: contain;
    width: 517px; 
}
.explain .keyv .des {
    width: auto;
    background: url("img/en/keyv-title-sub.png") center bottom no-repeat;
/*    background-size: contain; */
    min-height: 288px;
    padding: 20px 0 0;
}

.price.contents > div > ul > li {
    line-height: 18px;
}

.price.contents > table.option > tbody > tr td {
    line-height: 18px;
}

</style>

<section id="body">
    <!-- <div class="pageTtl">
        <h2>ORDER</h2>
    </div> -->
     <div class="wrapper">
        <div class="keyv">
            <p class="img"></p>
            <h3></h3>
            <p class="des"></p>
        </div>

        <div class="about">
            <ul class="column_2 des">
                <li>
                     <p class="img"><img src="img/en/explain-img01.png" alt=""></p>
                </li>
                <li class="text">
                   
                </li>
            </ul>
        </div>

        

         <p class="btn_black">
            <a href="mailto:infomail@tb-m.com">&nbsp;Orders & Quotations</a>
        </p>
        <div class="contact_text">
            <div>If there is anything we can help you with,<br>please do not hesitate to contact us.</div>
            <div>TEL&nbsp;:&nbsp;+81 03-6212-7272</div>
        </div>

        <div class="price contents">
            <h3>Pricing</h3>
            <p class="title pt0">Direct sales price(excluding tax)</p>
            <table class="sheets">
                <tr class="head">
                    <td></td>
                    <td>single sided</td>
                    <td>single sided</td>
                    <td>double sided</td>
                    <td>double sided</td>
                    <td>double sided</td>
                </tr>
                <tr>
                    <th>quantity</th>
                    <td>Color</td>
                    <td>Black</td>
                    <td>Color/Color</td>
                    <td>Color/Black</td>
                    <td>Black/Black</td>
                </tr>
                <tr>
                    <th>100</th>
                    <td>¥1,750</td>
                    <td>¥1,250</td>
                    <td>¥2,500</td>
                    <td>¥2,250</td>
                    <td>¥1,500</td>
                </tr>
                <tr>
                    <th>200<span>※</span></th>
                    <td>¥3,360</td>
                    <td>¥2,400</td>
                    <td>¥4,800</td>
                    <td>¥4,320</td>
                    <td>¥2,880</td>
                </tr>
                <tr>
                    <th>300<span>※</span></th>
                    <td>¥4,830</td>
                    <td>¥3,450</td>
                    <td>¥6,900</td>
                    <td>¥6,210</td>
                    <td>¥4,140</td>
                </tr>
            </table>
            <p class="note"><span>*</span>This price applies when more than 2 sets ( 200 business cards) with the same data are ordered at the same time.</p>

            <p class="title">Optional Fees (excluding tax)</p>
            <table class="option">
                <tr>
                    <th>Tracing Fee (per side)</th>
                    <td>¥2,000〜</td>
                    <th>Cutting Fee</th>
                    <td>¥250<br>* If your business card size is smaller than<br>91mm × 55mm</td>
                </tr>
                <tr>
                    <th>Designing fee</th>
                    <td>Will be charged separately</td>
                    <th>Delivery fee</th>
                    <td>Depending on delivering method and area.</td>
                </tr>
            </table>

            <p class="note">
                <span>*</span>&nbsp;Ordering Unit : 1set (100 business cards) per order, or its multiplied quantity.<br>
                <span>*</span>&nbsp;Delivering Fee will be informed after the delivering address is designated.<br>
            </p>

            <p class="title">Delivery Schedule</p>
            <div class="schedule">
                <ul class="disc">
                    <li>If data production is needed, the schedule depends upon its contents, so kindly order with some time allowance.</li>
                    <li>We will contact you for details when the order is placed, but kindly note that at least one to two days are needed for data production.</li>
                    <li>We are closed on Saturdays, Sundays and National Holidays, so shipping cannot be made on these dates , but delivering on Saturdays can be made upon request.</li>
                    <li>For additional process business cards, please contact for further delivery schedule details.</li>
                </ul>
            </div>
        </div>

        <p class="btn_black">
            <a href="mailto:infomail@tb-m.com">&nbsp;Orders & Quotations</a>
        </p>
        <div class="contact_text">
            <div>If there is anything we can help you with,<br>please do not hesitate to contact us.</div>
            <div>TEL&nbsp;:&nbsp;+81 03-6212-7272</div>
        </div>


        <div class="contents points">
            <h3 class="title03">Precaution</h3>

            <ul>
                <li>
                    <p class="title">About Payment</p>
                    <p class="text">Please kindly pre pay by bank transfer.<br />Details will be informed after orders are placed.</p>
                </li>
                <li>
                    <p class="title">About Printing Appearance</p>
                    <p class="text">
                        Slight appearance difference can be expected for data uploaded business cards  compared to cards printed by different companies, due to printing machine and paper difference.<br />
                        Especially when the design is colored in a wide area, gradation design , difference in color appearance is possible.<br />
                        Slight cutting difference is possible from the data. Difference within 2 mm will be taken as a non-defective product.<br />
                        Therefore, depending on the cutting, designs with a frame or bilateral symmetry the difference from the original data can stand out,  so please kindly avoid such designs.
                    </p>
                </li>
                <li>
                    <p class="title">About Copyright and Portrait Rights</p>
                    <p class="text">
                        Designs with Copyrights( including Logo marks, talents and animation pictures etc.) cannot be used unless it is allowed by the right holder. <br />
                        We will not be responsible in checking any of the copyright or portrait right matters of the pictures and designs we receive. It will be considered that all rights are cleared.<br />
                        In case of any Liability for Damages , we will not be responsible, so please be careful when placing your orders with pictures and designs.
                    </p>
                </li>
                <li>
                    <p class="title">Delivery Schedule and Delivery Methods</p>
                    <p class="text">
                        After uploading and finalizing the data, we will inform the delivering schedule by e-mail.<br />
                        Up loading cut off time is 12:00 (excluding Sat. Sun. and Holidays).<br />
                        Delivery Schedule may be changed due to  unanticipated problems such as natural disasters.
                    </p>
                </li>
                <li>
                    <p class="title">Cancel and Return Policy</p>
                    <p class="text">
                        After you submit your order, we start working quickly as possible, so please note that we cannot accept any cancels after 16:00 of the day that the data was uploaded. Please kindly double-check before submitting your order.<br />
                        Before shipping , all the business cards will go through our quality checking process, but please kindly check the received business cards as well.<br />
                        In case any defective products are included, please inform within 14 days counting from the delivery date.<br />
                        The business cards will be replaced only if it is unused. Please send back to us by cash on delivery.<br />
                        Please note that in any case, we cannot make any compensation that will exceed the ordering price.<br />
                        Please kindly note that we cannot compensate for any defective products, due to faults in your loaded data or any faults found after the final check.<br />
                        We will not be responsible for any product damages made by  transportation companies.  Please kindly communicate with the transportation company for the damages occurred.
                    </p>
                </li>
            </ul>
        </div>

        <p class="btn_black">
            <a href="mailto:infomail@tb-m.com">&nbsp;Orders & Quotations</a>
        </p>
        <div class="contact_text">
            <div>If there is anything we can help you with,<br>please do not hesitate to contact us.</div>
            <div>TEL&nbsp;:&nbsp;+81 03-6212-7272</div>
        </div>
    </div>
</section><!-- /#body -->
<?php 
get_footer('en');