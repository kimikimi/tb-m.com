<?php get_header(); ?>
    <script>
        $('body').addClass('order');
    </script>
    <link type="text/css" rel="stylesheet" href="./css/style.css"/>
    <script type="text/javascript" src="./js/order.js"></script>
    <style type="text/css">
        body.order #form dl dd {
            width: 500px;
        }

        body.order #form dl dd {
            line-height: 1.5;

        }
    </style>
    <section id="body">
        <div class="pageTtl">
            <h2>ORDER</h2>
        </div>
        <div class="wrapper cf cofirm">
            <h3>ご注文</h3>
            <form action="<?php echo HOST ?>/order/order" method="post" name="form_order" id="form">
                <!-- 名刺情報 -->
                <div class="head">
                    <div class="pt_ssss pb_ssss">入力内容をご確認し、よろしければ「この内容で申し込む」ボタンを押してください。</div>
                    <div class="order_title" style="padding: 20px 0;">ご注文者情報</div>
                </div>
                <dl class="cf">
                    <dt>用途</dt>
                    <dd>
                        <?php echo Config::get('useNumList.' . $valueInstance->meishi_use); ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>会社名</dt>
                    <dd>
                        <?php echo h($valueInstance->meishi_company); ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>従業員数</dt>
                    <dd>
                        <?php echo h($valueInstance->meishi_employees); ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>お名前</dt>
                    <dd>
                        <?php echo h($valueInstance->meishi_name); ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>ふりがな</dt>
                    <dd>
                        <?php echo h($valueInstance->meishi_kana); ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>住所</dt>
                    <dd style="line-height: 1.5;">
                        <p class="mb_sssssss">〒<?php echo h($valueInstance->meishi_post1); ?>
                            - <?php echo h($valueInstance->meishi_post2); ?></p>
                        <?php echo Config::get('prefectures.' . $valueInstance->meishi_pref) . h($valueInstance->meishi_addr); ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>電話番号</dt>
                    <dd>
                        <?php echo h($valueInstance->meishi_tel); ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>メールアドレス</dt>
                    <dd>
                        <?php echo h($valueInstance->meishi_email); ?>
                    </dd>
                </dl>
                <div class="order_title" style="padding: 20px 0;margin-top:20px;">ご注文内容</div>

                <dl class="cf">
                    <dt>ご注文人数</dt>
                    <dd>
                        <?php echo h($valueInstance->meishi_orders); ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>一人あたりのセット数<span class="caution">※</span><br/>（100枚 / 1セット）</dt>
                    <dd>
                        <?php echo Config::get('perPersonSetNumList.' . $valueInstance->meishi_per_person_set); ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>合計セット数</dt>
                    <dd>
                        <?php if ($valueInstance->meishi_per_person_set > 10): ?>
                            -
                        <?php else: ?>
                            <?php echo $valueInstance->meishi_orders * $valueInstance->meishi_per_person_set ?>セット (<?php echo $valueInstance->meishi_orders * $valueInstance->meishi_per_person_set * 100 ?>枚)
                        <?php endif; ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>色数</dt>
                    <dd>
                        <?php foreach ($valueInstance->meishi_colornum as $key => $value): ?>
                            <?php if ($value): ?>
                                <?php echo Arr::get(Config::get('colorNumList'), $value); ?><br>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </dd>
                </dl>
                <?php foreach ($valueInstance->meishi_colornum as $key => $value): ?>
                    <?php
                    if ($value == "") continue;
                    $is_multi_side_print = (strpos(Arr::get(Config::get('colorNumList'), $value), '両面') !== false) ? true : false;
                    ?>
                    <dl class="cf">
                        <dt style="font-weight: bold;"><?php echo Arr::get(Config::get('colorNumList'), $value); ?></dt>
                        <dd>
                        </dd>
                    </dl>
                    <dl class="cf">
                        <dt>ご購入パターン</dt>
                        <dd>
                            <?php echo Config::get('buyPatternNumList.' . $valueInstance->meishi_buy_pattern[$key]); ?>
                        </dd>
                    </dl>
                    <?php if ($valueInstance->meishi_card_type[$key]): ?>
                        <dl class="cf">
                            <dt>LIMEX名刺デザインテンプレート</dt>
                            <dd>
                                <?php echo Config::get('cardTypeList.' . $valueInstance->meishi_card_type[$key]); ?>
                            </dd>
                        </dl>
                    <?php endif; ?>
                    <dl class="cf">
                        <dt>名刺下部のデザイン（表面）</dt>
                        <dd>
                            <?php echo $valueInstance->meishi_design_pattern[$key]; ?>
                        </dd>
                    </dl>
                    <?php if ($is_multi_side_print): ?>
                        <dl class="cf">
                            <dt>名刺下部のデザイン（裏面）</dt>
                            <dd>
                                <?php echo $valueInstance->meishi_design_pattern_back[$key]; ?>
                            </dd>
                        </dl>
                    <?php endif; ?>
                <?php endforeach; ?>
                <dl class="cf">
                    <dt>ファイルのアップロード</dt>
                    <dd>
                        <?php echo $valueInstance->meishi_file ? h($valueInstance->meishi_file) : 'なし'; ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>プロモーションコード</dt>
                    <dd>
                        <?php echo $valueInstance->meishi_promo ? $valueInstance->meishi_promo : 'なし'; ?>
                    </dd>
                </dl>
                <?php /* 20161110 プロモ単価なし対応 start */ ?>
                <?php $promo_price_flg = false; ?>
                <?php foreach ($valueInstance->meishi_promo_price as $key => $value): ?>
                    <?php if ($value > 0): ?>
                        <?php $promo_price_flg = true; ?>
                    <?php endif; ?>
                <?php endforeach; ?>

                <?php if ($valueInstance->meishi_promo && $promo_price_flg): ?>
                    <?php /* 20161110 プロモ単価なし対応 end */ ?>
                    <dl class="cf">
                        <dt>プロモーション単価</dt>
                        <dd>
                            <?php foreach ($valueInstance->meishi_promo_price as $key => $value): ?>
                                <?php if ($value !== ""): ?>
                                    <span
                                        style="display: inline-block; width: 200px;"><?php echo base64_decode($key) ?></span><?php echo h($value) ?>円
                                    <br>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </dd>
                    </dl>
                <?php endif; ?>
                <dl class="cf">
                    <dt>何でLIMEXの名刺を<br/>お知りになりましたか？</dt>
                    <dd>
                        <?php echo Config::get('knowPatternNumlist.' . $valueInstance->meishi_know_pattern); ?><br>
                        <?php echo $valueInstance->meishi_know_sales ? h($valueInstance->meishi_know_sales) : null ?>
                        <?php echo $valueInstance->meishi_know_site ? h($valueInstance->meishi_know_site) : null ?>
                        <?php echo $valueInstance->meishi_know_program ? h($valueInstance->meishi_know_program) : null ?>
                        <?php echo $valueInstance->meishi_know_paper ? h($valueInstance->meishi_know_paper) : null ?>
                        <?php echo $valueInstance->meishi_know_seminar ? h($valueInstance->meishi_know_seminar) : null ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>備考欄</dt>
                    <dd>
                        <?php echo nl2br(h($valueInstance->meishi_note)); ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>LIMEXに関するニュースとお知らせ</dt>
                    <dd>
                        <?php echo $valueInstance->related_news == 1 ? '配信する' : '配信しない'; ?>
                    </dd>
                </dl>
                <dl class="cf">
                    <dt>名刺発注システムの導入を検討したい</dt>
                    <dd>
                        <?php echo $valueInstance->pr_agree == 1 ? '検討する' : '検討しない'; ?>
                    </dd>
                </dl>
                <div class="head mb_sss">
                    <div class="order_title" style="padding: 20px 0;">納品先</div>
                </div>
                <?php if ($valueInstance->hasNouhin()): ?>
                    <!-- 納品先 -->
                    <dl class="cf">
                        <dt>会社名</dt>
                        <dd>
                            <?php echo h($valueInstance->nouhin_company); ?>
                        </dd>
                    </dl>
                    <dl class="cf">
                        <dt>お名前</dt>
                        <dd>
                            <?php echo h($valueInstance->nouhin_name); ?>
                        </dd>
                    </dl>
                    <dl class="cf">
                        <dt>ふりがな</dt>
                        <dd>
                            <?php echo h($valueInstance->nouhin_kana); ?>
                        </dd>
                    </dl>
                    <dl class="cf">
                        <dt>住所</dt>
                        <dd>
                            <p class="mb_sssssss">〒<?php echo h($valueInstance->nouhin_post1); ?>
                                - <?php echo h($valueInstance->nouhin_post2); ?></p>
                            <?php echo Config::get('prefectures.' . $valueInstance->nouhin_pref) . h($valueInstance->nouhin_addr); ?>
                        </dd>
                    </dl>
                    <dl class="cf">
                        <dt>電話番号</dt>
                        <dd>
                            <?php echo h($valueInstance->nouhin_tel); ?>
                        </dd>
                    </dl>
                <?php else : ?>
                    <dl class="cf">
                        <dd>ご注文者情報と同じ</dd>
                    </dl>
                <?php endif; ?>
                <dl class="cf" style="text-align: center;">
                    <input type="hidden" name="token" value="<?php echo $token ?>">
                    <img src="img/button_back.png" onclick="window.location.href = './?status=back';">
                    <input type="image" name="btn_confirm" src="img/button_confirm.png"/>
                </dl>
            </form>
        </div>
    </section><!-- /#body -->
<?php
get_footer();