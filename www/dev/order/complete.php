<?php get_header(); ?>
<link type="text/css" rel="stylesheet" href="./css/style.css" />
<script type="text/javascript" src="./js/order.js"></script>
<section id="body" style="margin-bottom: 50px;">
    <div class="pageTtl">
        <h2>ORDER</h2>
    </div>
    <div class="complate_text">
      <p class="thanks">
        ご注文、誠にありがとうございました。<br />
      </p>
    <?php if(isset($_GET['order_id'])) : ?>
      <div class="orderid">
        ご注文番号：<?php echo h($_GET['order_id']); ?>
      </div>
    <?php endif; ?>
    <p class="text">
      ※ご注文受領のお知らせメールを自動で配信します。<br>メールが届かない場合は「<a href="https://tb-m.com/form/contact_lime-x/contact/">お問い合わせ</a>」よりご連絡ください。
    </p>
      </div>

    <div class="share-sns">
      <p class="title">LIMEXで地球の環境問題に貢献することを友達に伝えよう</p>
      <p class="txt-share">SHARE</p>
      <ul class="share-btns">
        <li><a title="facebookでシェアする" href="http://www.facebook.com/sharer.php?u=https://tb-m.com/order/explain.php&t=" target="_blank"><img src="img/share-facebook.png" alt="Facebookでシェア"></a></li>
        <li><a href="http://twitter.com/share?text=&url=https://tb-m.com/order/explain.php&hashtags=" onClick="window.open(encodeURI(decodeURI(this.href)), 'tweetwindow', 'width=650, height=470, personalbar=0, toolbar=0, scrollbars=1, sizable=1'); return false;" rel="nofollow"><img src="img/share-twitter.png" alt="Twitterでシェア"></a></li>
      </ul>
    </div>
</section><!-- /#body -->

<?php 
get_footer();