<?php
// WordPres用
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
require_once $_SERVER['DOCUMENT_ROOT']. '/wp-blog-header.php';

// 英語order stop
header( "Location: /en/" ) ;
exit();

?>
<?php get_header('en'); ?>

<style type="text/css">
.explain .points li {
    padding-left: 1em;
    text-indent: -8px;
    font-size: 14px;
}
</style>

<img class="full" src="/order/img/sp/en/explain-img.jpg">


<div class="wrapper explain">
	<div class="center mt20">
		<img src="/order/img/sp/en/explain-title.png" style="width:95%;">
	</div>
	<div class="lead">

		<p>
		LIMEX is a revolutionary material made mainly<br />
		out of limestone.<br>
		For paper production water and pulp was needed.<br>
		But with LIMEX paper made from an abundant<br />
		material -limestone-,<br>
		no more water nor pulp is needed for the production.
		</p>
		<p>
		Paper consumption is estimated to double in 2030.<br>
		In 2050, 40 percent of all the world’s population is expected to face severe water shortage,<br>
		and we might lose all our main forests in the<br />
		 next 100 years.<br>
		Water shortage and deforestation is accelerating.<br>
		</p>
		<p>
		Start a material revolution and protect our earth with the LIMEX business cards.
		</p>
			<img class="full mt20" src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/order/img/sp/explain-img-paper.jpg">
		<p style="text-align:left;">Environmental friendly LIMEX business cards, made from limestone without using water nor pulp has improved tremendously compared to the previous stone papers. Competitive lightness, kink resistance, water resistance and easily used.</p>
		<p style="text-align:left;">To improve lightness and to be able to apply to various designs in the future, we are investigating constantly.</p>
	</div>
	<div class="center mt20">
		<a href="mailto:infomail@tb-m.com" class="button">Orders & Quotations</a>
	</div>
	<p class="center mt10">If there is anything we can help you with,<br />please do not hesitate to contact us.<br />
		TEL : +81 03-6212-7272</p>
	<section>
		<h2>Pricing</h2>
		<h3>Direct sales price(excluding tax)</h3>
		<table>
			<tr>
				<th rowspan="3">single sided<br>
				Color</th>
				<th>100</th>
				<td>¥1,750</td>
			</tr>
			<tr>
				<th>200※</th>
				<td>¥3,360</td>
			</tr>
			<tr>
				<th>300※</th>
				<td>¥4,830</td>
			</tr>
			<tr>
				<th rowspan="3">single sided<br>
				Black</th>
				<th>100</th>
				<td>¥1,250 </td>
			</tr>
			<tr>
				<th>200※</th>
				<td>¥2,400 </td>
			</tr>
			<tr>
				<th>300※</th>
				<td>¥3,450 </td>
			</tr>
			<tr>
				<th rowspan="3">double sided<br>
				Color/Color</th>
				<th>100</th>
				<td>¥2,500 </td>
			</tr>
			<tr>
				<th>200※</th>
				<td>¥4,800 </td>
			</tr>
			<tr>
				<th>300※</th>
				<td>¥6,900 </td>
			</tr>
			<tr>
				<th rowspan="3">double sided<br>
				Color/Black</th>
				<th>100</th>
				<td>¥2,250 </td>
			</tr>
			<tr>
				<th>200※</th>
				<td>¥4,320 </td>
			</tr>
			<tr>
				<th>300※</th>
				<td>¥6,210 </td>
			</tr>
			<tr>
				<th rowspan="3">double sided<br>
				Black/Black</th>
				<th>100</th>
				<td>¥1,500</td>
			</tr>
			<tr>
				<th>200※</th>
				<td>¥2,880</td>
			</tr>
			<tr>
				<th>300※</th>
				<td>¥4,140</td>
			</tr>
		</table>
		<p class="note mt10">*This price applies when more than 2 sets ( 200 business cards) with the same data are ordered at the same time.</p>
		<h3>Optional Fees(excluding tax)</h3>
		<table>
			<tr>
				<th>Tracing Fee (per side)</th>
				<td>¥2,000〜 </td>
			</tr>
			<tr>
				<th>Designing fee</th>
				<td>Will be charged separately </td>
			</tr>
			<tr>
				<th>Cutting Fee</th>
				<td>¥250<br>
				*If your business card size is smaller than 91mm × 55mm</td>
			</tr>
			<tr>
				<th>Delivery fee</th>
				<td>Depending on delivering method and area.</td>
			</tr>
		</table>
		<p class="note mt10">*Ordering Unit : 1set (100 business cards) per order, or its multiplied quantity</p>
		<p class="note">*Delivering Fee will be informed after the delivering address is designated.</p>

		<h3>Delivery Schedule</h3>
		<ul class="points">
			<li>*If data production is needed, the schedule depends upon its contents, so kindly order with some time allowance.</li>
			<li>*We will contact you for details when the order is placed, but kindly note that at least one to two days are needed for data production.</li>
			<li>*We are closed on Saturdays, Sundays and National Holidays, so shipping cannot be made on these dates , but delivering on Saturdays can be made upon request. </li>
			<li>*For additional process business cards, please contact for further delivery schedule details. </li>
		</ul>
		<div class="center mt20">
			<a href="mailto:infomail@tb-m.com" class="button">Orders & Quotations</a>
		</div>
		<p class="center mt10">If there is anything we can help you with,<br />please do not hesitate to contact us.<br />
			TEL : +81 03-6212-7272</p>
	</section>
	<section>
		<h2>Precaution</h2>
		<dl>
			<dt>About Payment</dt>
			<dd>Please kindly pre pay by bank transfer.<br>
			Details will be informed after orders are placed.</dd>
			<dt>About Printing Appearance</dt>
			<dd>
						Slight appearance difference can be expected for data uploaded business cards compared to cards printed by different companies, due to printing machine and paper difference.<br>
						Especially when the design is colored in a wide area, gradation design , difference in color appearance is possible.<br>
						Slight cutting difference is possible from the data. Difference within 2 mm will be taken as a non-defective product.<br>
						Therefore, depending on the cutting, designs with a frame or bilateral symmetry the difference from the original data can stand out, so please kindly avoid such designs.
			</dd>
			<dt>About Copyright and Portrait Rights</dt>
			<dd>
						Designs with Copyrights( including Logo marks, talents and animation pictures etc.) cannot be used unless it is allowed by the right holder.<br>
						We will not be responsible in checking any of the copyright or portrait right matters of the pictures and designs we receive. It will be considered that all rights are cleared.<br>
						In case of any Liability for Damages , we will not be responsible, so please be careful when placing your orders with pictures and designs.
				<br>
			</dd>
			<dt>Delivery Schedule and Delivery Methods</dt>
			<dd>
				After uploading and finalizing the data, we will inform the delivering schedule by e-mail.<br>
				Up loading cut off time is 12:00 (excluding Sat. Sun. and Holidays).<br>
				Delivery Schedule may be changed due to unanticipated problems such as natural disasters.
			</dd>
			<dt>Cancel and Return Policy</dt>
			<dd>After you submit your order, we start working quickly as possible, so please note that we cannot accept any cancels after 16:00 of the day that the data was uploaded. Please kindly double-check before submitting your order.<br>
				Before shipping , all the business cards will go through our quality checking process, but please kindly check the received business cards as well.<br>
				In case any defective products are included, please inform within 14 days counting from the delivery date.<br>
				The business cards will be replaced only if it is unused. Please send back to us by cash on delivery.<br>
				Please note that in any case, we cannot make any compensation that will exceed the ordering price.<br>
				Please kindly note that we cannot compensate for any defective products, due to faults in your loaded data or any faults found after the final check.<br>
				We will not be responsible for any product damages made by transportation companies. Please kindly communicate with the transportation company for the damages occurred.</dd>
		</dl>

		<div class="center mt20">
			<a href="mailto:infomail@tb-m.com" class="button">Orders & Quotations</a>
		</div>
		<p class="center mt10">If there is anything we can help you with,<br />please do not hesitate to contact us.<br />
			TEL : +81 03-6212-7272</p>
	</section>
</div>
<script>
var LIMEX = LIMEX || {};
LIMEX.globalNav = (function($){
  var buttonMenu;
  var navGlobal;
  var _init = function() {
    buttonMenu = $("#button-menu");
    navGlobal = $("#nav-global");
    navGlobal.find(".bg").on("click", function(){
      close();
    })
    $("body").css({
      minHeight: $(window).height()
    });
    navGlobal.css({
      minHeight: $(window).height() - 50
    });
    buttonMenu.on("click", function(){
      if(buttonMenu.hasClass('active')) {
        close();
        if($("div.story").css("display") == "block") {
          $("div.story").fadeOut(200);
        }
      } else {
        open();
      }
    })
    navGlobal.find("li:has('li')").children('a')
      .addClass('close')
      .on("click", function(){
        $(this).next().slideToggle(300, function(){
          setBodyHeight();
        });
        $(this).toggleClass('open');
        return false;
      })


    $("#key").on("click", ".show-story", function(){
      $("div.story").fadeIn(200);
      buttonMenu.addClass('active');
      $("body").css({
        height: $("div.story").height() + 100,
        overflowY: "hidden"
      })
      setBodyHeight();
      return false;
    })
  }
  var setBodyHeight = function(){
    $("body").css({
      //height: navGlobal.height() + 100,
      height: navGlobal.height() + 150,
      overflowY: "hidden"
    })
  }
  var open = function() {
    buttonMenu.addClass('active');
    navGlobal.fadeIn('200');
    setBodyHeight();
  }
  var close = function(){
    buttonMenu.removeClass('active');
    navGlobal.fadeOut('200');
    $("body").css({
      height: "auto",
      overflowY: ""
    })
  }
  var _self = {
    init: _init
  }
  return _self;
}(jQuery));

$(LIMEX.globalNav.init);
</script>
<?php
get_footer('en');
