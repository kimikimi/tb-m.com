<?php get_header(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/order-sp.css" type="text/css" media="all" />

<script type="text/javascript" src="./js/order.js"></script>
<section id="body" class="order-sp" style="margin-bottom: 50px;">
    <div class="pageTtl">
        <h2>ORDER</h2>
    </div>
    <div class="complate_text">
      <p class="thanks">
        ご注文、誠にありがとうございました。<br />
      </p>
    <?php if(isset($_GET['order_id'])) : ?>
      <div class="orderid">
        ご注文番号：<?php echo h($_GET['order_id']); ?>
      </div>
    <?php endif; ?>
    <p class="text">
      ※ご注文受領のお知らせメールを自動で配信します。<br>メールが届かない場合は「<a href="https://tb-m.com/form/contact_lime-x/contact/">お問い合わせ</a>」よりご連絡ください。
    </p>
      </div>

    <div class="share-sns">
      <p class="title">LIMEXで地球の環境問題に貢献することを友達に伝えよう</p>
      <p class="txt-share">SHARE</p>
      <ul class="share-btns">
        <li><a title="facebookでシェアする" href="http://www.facebook.com/sharer.php?u=https://tb-m.com/order/explain.php&t=" target="_blank"><img src="img/share-facebook.png" alt="Facebookでシェア"></a></li>
        <li><a href="http://twitter.com/share?text=&url=https://tb-m.com/order/explain.php&hashtags=" onClick="window.open(encodeURI(decodeURI(this.href)), 'tweetwindow', 'width=650, height=470, personalbar=0, toolbar=0, scrollbars=1, sizable=1'); return false;" rel="nofollow"><img src="img/share-twitter.png" alt="Twitterでシェア"></a></li>
      </ul>
    </div>

</section><!-- /#body -->
<script>
var LIMEX = LIMEX || {};
LIMEX.globalNav = (function($){
  var buttonMenu;
  var navGlobal;
  var _init = function() {
    buttonMenu = $("#button-menu");
    navGlobal = $("#nav-global");
    navGlobal.find(".bg").on("click", function(){
      close();
    })
    $("body").css({
      minHeight: $(window).height()
    });
    navGlobal.css({
      minHeight: $(window).height() - 50
    });
    buttonMenu.on("click", function(){
      if(buttonMenu.hasClass('active')) {
        close();
        if($("div.story").css("display") == "block") {
          $("div.story").fadeOut(200);
        }
      } else {
        open();
      }
    })
    navGlobal.find("li:has('li')").children('a')
      .addClass('close')
      .on("click", function(){
        $(this).next().slideToggle(300, function(){
          setBodyHeight();
        });
        $(this).toggleClass('open');
        return false;
      })


    $("#key").on("click", ".show-story", function(){
      $("div.story").fadeIn(200);
      buttonMenu.addClass('active');
      $("body").css({
        height: $("div.story").height() + 100,
        overflowY: "hidden"
      })
      setBodyHeight();
      return false;
    })
  }
  var setBodyHeight = function(){
    $("body").css({
      //height: navGlobal.height() + 100,
      height: navGlobal.height() + 150,
      overflowY: "hidden"
    })
  }
  var open = function() {
    buttonMenu.addClass('active');
    navGlobal.fadeIn('200');
    setBodyHeight();
  }
  var close = function(){
    buttonMenu.removeClass('active');
    navGlobal.fadeOut('200');
    $("body").css({
      height: "auto",
      overflowY: ""
    })
  }
  var _self = {
    init: _init
  }
  return _self;
}(jQuery));

$(LIMEX.globalNav.init);
</script>
<?php
get_footer();
