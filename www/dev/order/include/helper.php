<?php

function h($value) {
	return htmlspecialchars($value, ENT_QUOTES);
}

define('HOST', (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"]);

function setPost($className) {
	$instance = new $className();
	foreach (get_class_vars($className) as $names_key => $name)
	{
		// 一次元
		if (! is_array($name))
		{
			$instance->{$names_key} = filter_input(INPUT_POST, $names_key);
		}
		// 二次元
		else
		{
			// 値が入っている場合の処理
			if (! is_null(filter_input(INPUT_POST, $names_key)))
			{
				foreach ($_POST[$names_key] as $_key => $_id)
				{
					$instance->{$names_key}[$_key] = $_id;
				}
			}
			// 値が入って無い
			else
			{
				$instance->{$names_key} = array();
			}
		}
	}
	return $instance;
}

function getToken() {
	return sha1(uniqid(mt_rand(), true));
}

function getTempletePath() {
	if(is_sp()){
		return '/sp';
	}
	return;
}


function is_sp () {
	$useragents = array(
		'iPad',
		'iPhone',
		'iPod',
		'Android',
		'dream',
		'CUPCAKE',
		'blackberry',
		'webOS',
		'incognito',
		'webmate'
	);
	$pattern = '/'.implode('|', $useragents).'/i';
	return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}

function postToAdmin($mailValues) {
    global $IS_DEV;
    global $IS_STAGING;
    $endpoint = "https://tbm:limex123@dev.tb-m.com/framework/public/api/order";
    if ($IS_DEV) {
        $endpoint = "http://localhost/framework/public/api/order";
        if ($IS_STAGING) {
            $endpoint = "http://limex-12s5e84d2.devenv.space/framework/public/api/order";
        }
    }

	$data = http_build_query($mailValues);
	$header = array(
		"Content-Type: application/x-www-form-urlencoded",
		"Content-Length: ".strlen($data)
	);
	$options = array(
		'http' => array(
			'method' => 'POST',
			'header' => implode("\r\n", $header),
			'content' => $data,
		),
	);
	$context = stream_context_create($options);
	$response = @file_get_contents($endpoint, false, $context);
	return $response;
}
