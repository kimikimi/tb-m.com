<?php

// フォームのポストデータをマッピングして使いやすくしたいクラス
class Value
{
	public $meishi_company;
	public $meishi_name;
	public $meishi_kana;
	public $meishi_post1;
	public $meishi_post2;
	public $meishi_pref;
	public $meishi_addr;
	public $meishi_tel;
	public $meishi_email;
	public $meishi_data;
	public $meishi_setnum;
	public $meishi_colornum = array();
	public $meishi_buy_pattern = array();
	public $meishi_card_type = array();
	public $meishi_lan;
	public $meishi_design_pattern = array();
	public $meishi_design_pattern_back = array();
	public $meishi_ai;
	public $meishi_use;
	public $meishi_per_person_set;
	public $meishi_know_sales;
	public $meishi_know_pattern;
	public $meishi_know_site;
	public $meishi_know_program;
	public $meishi_know_paper;
	public $meishi_know_seminar;
	public $meishi_promo;
	public $meishi_employees;
	public $meishi_orders;
	public $meishi_note;
	public $meishi_price;
	public $meishi_promo_price  = array();
	public $meishi_file;
	public $meishi_file_unique;
	public $meishi_tmpnum;
	public $nouhin_company;
	public $nouhin_name;
	public $nouhin_kana;
	public $nouhin_post1;
	public $nouhin_post2;
	public $nouhin_pref;
	public $nouhin_addr;
	public $nouhin_tel;
	public $agree;
	public $related_news;
	public $pr_agree;

	/**
	 * 納品先が違う場合のpostがあったかどうか
	 * @return bool
	 */
	public function hasNouhin() {
		foreach (get_object_vars($this) as $key => $value) {
			if (preg_match('#nouhin#', $key) && $value) {
				return true;
			}
		}
	}

	/**
	 * 20161110
	 * プロモーション単価非表示用コードか
	 * @return bool
	 */
	public function hasPromoPrice() {
		if ( $this->meishi_promo != "130012"){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 色数の出力用整形
	 * @param $list array
	 * @return string
	 */
	public function getColorNumString(array $list) {
		$colorNum = array();
		foreach ($this->meishi_colornum as $key => $value) {
			if (! $value) {
				continue;
			}
			$colorNum[] = Arr::get($list, $value);
		}
		return implode(', ', $colorNum);
	}

	public function getColorNumArray(array $list) {
        $colorNum = array();
        foreach ($this->meishi_colornum as $key => $value) {
            if (! $value) {
                continue;
            }
            $colorNum[$key] = Arr::get($list, $value);
        }

        return $colorNum;
    }

	/**
	 * 単価の出力用整形
	 * @return string
	 */
	public function getPriceNumString() {
		$priceNum = array();
		if ($this->meishi_promo) {
			foreach ($this->meishi_promo_price as $key => $value) {
				if (! $value) {
					continue;
				}
				$priceNum[] = base64_decode($key) . ' : ' . $value . '円';
			}
			$priceNumString = implode(PHP_EOL, $priceNum);
		} else {
			$priceNumString = 'なし';
		}
		return $priceNumString;
	}

	public function hasAi() {
		return $this->meishi_ai == 1 ? true : false;
	}

	public function getAnswer() {
		switch (true) {
			case $this->meishi_know_sales:
				return $this->meishi_know_sales;
				break;
			case $this->meishi_know_site:
				return $this->meishi_know_site;
				break;
			case $this->meishi_know_program:
				return $this->meishi_know_site;
				break;
			case $this->meishi_know_paper:
				return $this->meishi_know_site;
				break;
			case $this->meishi_know_seminar:
				return $this->meishi_know_site;
				break;
			default:
			return null;
				break;
		}
	}
}