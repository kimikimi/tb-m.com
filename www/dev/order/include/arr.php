<?php

class Arr
{
	public static function get($array, $key, $default = null)
	{

		if ( ! is_array($array) and ! $array instanceof ArrayAccess)
		{
			throw new InvalidArgumentException('First parameter must be an array or ArrayAccess object.');
		}

		if (is_null($key))
		{
			return $array;
		}

		if (is_array($key))
		{
			$return = array();
			foreach ($key as $k)
			{
				$return[$k] = self::get($array, $k, $default);
			}
			return $return;
		}

		foreach (explode('.', $key) as $key_part)
		{
			if (($array instanceof ArrayAccess and isset($array[$key_part])) === false)
			{
				if ( ! is_array($array) or ! array_key_exists($key_part, $array))
				{
					return $default;
				}
			}

			$array = $array[$key_part];
		}

		return $array;
	}

	public static function is_assoc($arr)
	{
		if ( ! is_array($arr))
		{
			throw new \InvalidArgumentException('The parameter must be an array.');
		}

		$counter = 0;
		foreach ($arr as $key => $unused)
		{
			if ( ! is_int($key) or $key !== $counter++)
			{
				return true;
			}
		}
		return false;
	}

	public static function search($array, $value, $default = null, $recursive = true, $delimiter = '.', $strict = false)
	{
		if ( ! is_array($array) and ! $array instanceof \ArrayAccess)
		{
			throw new \InvalidArgumentException('First parameter must be an array or ArrayAccess object.');
		}

		if ( ! is_null($default) and ! is_int($default) and ! is_string($default))
		{
			throw new \InvalidArgumentException('Expects parameter 3 to be an string or integer or null.');
		}

		if ( ! is_string($delimiter))
		{
			throw new \InvalidArgumentException('Expects parameter 5 must be an string.');
		}

		$key = array_search($value, $array, $strict);

		if ($recursive and $key === false)
		{
			$keys = array();
			foreach ($array as $k => $v)
			{
				if (is_array($v))
				{
					$rk = static::search($v, $value, $default, true, $delimiter, $strict);
					if ($rk !== $default)
					{
						$keys = array($k, $rk);
						break;
					}
				}
			}
			$key = count($keys) ? implode($delimiter, $keys) : false;
		}

		return $key === false ? $default : $key;
	}

}