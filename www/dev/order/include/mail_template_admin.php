<?php
/**
 * お問い合わせ管理者用メールテンプレート
 */

$admin_body = "
以下の注文が入りましたので、対応をお願いします。

[注文番号]
{$orderId}

----------------------------------------------------------
◆ご注文者情報
----------------------------------------------------------
[用途]
{$meishi_use}

[会社名]
{$meishi_company}

[従業員数]
{$meishi_employees}

[お名前(ふりがな)]
{$meishi_name}({$meishi_kana})

[住所]
〒{$meishi_post1}-{$meishi_post2}
{$address}

[電話番号]
{$meishi_tel}

[メールアドレス]
{$meishi_email}

----------------------------------------------------------
◆ご注文内容
----------------------------------------------------------
[ご注文人数]
{$meishi_orders}

[セット数]
{$setNum}

[合計セット数]
{$meishi_order_total_text_admin}

[色数]
{$colorNumString}
";
foreach ($buyPattern as $pattern_key => $pattern):
$admin_body .= "

{$colorNumArray[$pattern_key]}
[購入パターン]
{$pattern}

[名刺の下部デザイン（表面）]
{$meishi_design_pattern[$pattern_key]}

[名刺の下部デザイン（裏面）]
{$meishi_design_pattern_back[$pattern_key]}

[LIMEX名刺デザインテンプレート]
{$meishi_card_type[$pattern_key]}
";
endforeach;

$admin_body .= "
[ファイルアップロード]
{$file_url}

[プロモーションコード]
{$promo}
";
if ($hasPromoPrice) {
	$admin_body .= "
[プロモーション単価]
{$priceNumString}";

}
$admin_body .= "

[何でLIMEXの名刺をお知りになりましたか？]
{$knowPattern}
{$answer}

[LIMEXに関するニュースとお知らせ]
{$related_news}

[名刺発注システムの導入を検討したい]
{$pr_agree}

[備考欄]
{$meishi_note}

";

// 納品先指定
if ($hasNouhin) {
	$admin_body .= "
----------------------------------------------------------
◆納品先
----------------------------------------------------------
[会社名]
{$nouhin_company}

[お名前(ふりがな)]
{$nouhin_name}({$nouhin_kana})

[住所]
〒{$nouhin_post1}-{$nouhin_post2}
{$nouhinAddress}

[電話番号]
{$nouhin_tel}
";

}

$admin_body = preg_replace("/\r\n|\r|\n/s", "\n", $admin_body);
echo $admin_body;
