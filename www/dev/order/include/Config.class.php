<?php

class Config {

    public static $data;

    public static function set(array $configData) {
        self::$data = $configData;
    }

    public static function get($key = null, $default = null) {
        $array = self::$data;
        if (! $key) {
            return $array;
        }
        foreach (explode('.', $key) as $key_part)
        {
            if (($array instanceof ArrayAccess and isset($array[$key_part])) === false)
            {
                if ( ! is_array($array) or ! array_key_exists($key_part, $array))
                {
                    return $default;
                }
            }

            $array = $array[$key_part];
        }

        return $array;
    }
}