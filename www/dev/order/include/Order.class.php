<?php

class Order
{
    /**
     * お問い合わせディレクトリ
     */
    const ORDER_DIR = '/order';

    /**
     * お問い合わせTOP
     */
    const ORDER_TOP = '/';

    /**
     * 注文確定までの入稿データ一時ディレクトリ
     */
    const ORDER_FILE_TMP_DIR = '/upload_tmp';

    /**
     * メール送信時システムエラーメッセージ
     */
    const SYSTEM_ERR_MSG_01 = <<<EOF
            大変申しわけございませんが、時間をおきまして、再度お試しください。<br />
            何度も表示されるようでしたら、お手数ですが、管理者までお問い合わせください。
EOF;

    /**
     * メール送信時システムエラーメッセージ
     */
    const SYSTEM_ERR_MSG_02 = <<<EOF
            お手数ですが、もう一度始めからお願いします。
EOF;

    // session接頭語
    public static $sessPagePrefix = 'order';

    /**
     * 要素別エラーメッセージ取得
     * @param  string $element 要素名(指定なしで全部)
     * @return string エラーメッセージ
     */
    public static function getErrMessage($element = '')
    {
        if (empty($element)) {
            return self::getSession('errmsg');
        } else {
            return self::getSession('errmsg.' . $element);
        }
    }

    /**
     * エラーメッセージの有無
     * @return boolean 有無
     */
    public static function hasErrMessage($element = null)
    {
        $search = 'errmsg';
        if (! $element) {
            $search .= '.' . $element;
        }
        $retval = self::getSession($search);
        return !empty($retval);
    }

    /**
     * システムエラー処理。メッセージ付きで問い合わせトップへ
     * @return void
     */
    public static function doSystemErr()
    {
        self::setSession('errmsg', array('system' => self::SYSTEM_ERR_MSG_02));
        self::location(self::ORDER_DIR . self::ORDER_TOP . '?status=err', true);
    }

    /**
     * session変数への代入(追加)
     * @param string $key 配列キー
     * @param mix    $val 値
     */
    public static function setSession($key, $val, $override = false)
    {
        if (is_array(self::getSession($key))) {
            if ($override) {
                $_SESSION[self::$sessPagePrefix][$key] = $val;
            } else {
                $_SESSION[self::$sessPagePrefix][$key] = array_merge($_SESSION[self::$sessPagePrefix][$key], $val);
            }
        } else {
            $_SESSION[self::$sessPagePrefix][$key] = $val;
        }
    }

    /**
     * session変数呼び出し
     * @param string $key 配列キー(.区切りで子階層)
     */
    public static function getSession($fullkey, $session = array())
    {
        if (! isset($_SESSION[self::$sessPagePrefix])) {
            return false;
        }

        if (empty($session)) {
            $session = $_SESSION[self::$sessPagePrefix];
        }

        $pos = strpos($fullkey, '.');
        $key = substr($fullkey, 0, $pos);

        if ($pos !== false) {
            if (isset($session[$key])) {
                return self::getSession(substr($fullkey, $pos + 1), $session[$key]);
            } else {
                return false;
            }
        } else {
            if (isset($session[$fullkey])) {
                return $session[$fullkey];
            } else {
                return false;
            }
        }
    }

    /**
     * 注文ページのsession削除
     * @return void
     */
    public static function deleteSession()
    {
        unset($_SESSION[self::$sessPagePrefix]);
    }

    /**
     * リダイレクト（同一ドメイン内のみ）（常時SSL対応15/10/8）
     * @param string  $path     ドキュメントルート以下のパス
     * @param boolean $ssl_flg  true:HTTP, false=HTTPS
     */
    public static function location($path, $ssl_flg = false)
    {
        $scheme = ($ssl_flg) ? SSL : 'http';
        header('Location: ' . $scheme . '://'. $_SERVER["HTTP_HOST"] . $path);
        exit;
    }
}
