<?php 
class View
{
	public static function render($path, $data = array())
	{
		$return = function($path, array $data)
		{
			extract($data, EXTR_REFS);

			ob_start();
			try
			{
				include $path;
			}
			catch (\Exception $e)
			{
				ob_end_clean();

				throw $e;
			}

			return ob_get_clean();
		};

		return $return($path, $data);
	}
}