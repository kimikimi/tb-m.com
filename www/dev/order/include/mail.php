<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/order/vendor/phpmailer/PHPMailerAutoload.php';

class Mail
{
	/**
     * ユーザ側メールFROM
     */
	const USER_MAIL_FROM = 'infomail@tb-m.com';

	/**
     * サポート側メールアドレス（宛先）
     */
	const SUPPORT_MAIL_TO = ADMIN_MAIL_TO;
	const SUPPORT_MAIL_FROM = 'LIMEX';

	private static function getHeader() {
		return implode("\n", array(
			'Content-Type: text/plain; charset=ISO-2022-JP',
			'From: ' . mb_encode_mimeheader(self::SUPPORT_MAIL_FROM, 'ISO-2022-JP-MS') . ' <' .self::USER_MAIL_FROM . '>',
		));
	}

	public static function send($to, $subject, $body, $from, $file = null) {
            try {
                $mail = new PHPMailer;
                $mail->IsSendmail();
                $mail->Host = MAIL_HOST;
		$mail->Port = MAIL_PORT;
                $mail->CharSet = "UTF-8";
                $mail->setFrom($from);
                $mail->addAddress($to);
                if ($file) {
                    $mail->addAttachment($file);
                }
                $mail->Subject = mb_encode_mimeheader($subject, 'ISO-2022-JP-MS');
                $mail->Body    = $body;

                if (!$mail->send()) {
                    echo 'Message could not be sent.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                    exit();
                } else {
                    echo 'Message has been sent';
                }
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
	}
}
