<?php
/**
 * お問い合わせ入力値チェック
 *
 * @package    limex
 * @subpackage Order
 * @version    2015/10/22 1.0
 * @author     yutaka.sudo
 */


/**
 * Validateコンポーネントクラス
 */
class Validate
{
    /**
     * エラーエッセージ
     * @var array
     */
    private $message = array();

    /**
     * フォーム要素
     * @var array
     */
    private $formData = array();

    /**
     * コンストラクター
     * @param array $formData フォーム要素
     */
    public function __construct($formData)
    {
        $this->formData = $formData;
    }

    /**
     * 入力値チェック
     * NGの場合は入力画面を読み込む
     * @return boolean true:エラーなし, false:エラー
     */
    public function start($data)
    {
        $result = true;

        // 名刺情報：用途
        $element = 'meishi_use';

        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文者情報：用途は必須項目です。');
            $result = false;
        }

        // 名刺情報：会社
        $element = 'meishi_company';
        $element2 = 'meishi_use';
        if (! $data->{$element} && $data->{$element2} == '2') {
            $this->setMessage($element, 'ご注文者情報：会社名は必須項目です。');
            $result = false;
        } elseif ($data->{$element} == true && $this->check_length($data->{$element}, 0, 30) == false ) {
            $this->setMessage($element, 'ご注文者情報：会社名は30文字以内でお願いします。');
            $result = false;
        }

        // 名刺情報：従業員数
        $element = 'meishi_employees';
        $element2 = 'meishi_use';
        if (! $data->{$element} && $data->{$element2} == '2') {
            $this->setMessage($element, 'ご注文者情報：従業員数は必須項目です。');
            $result = false;
        } elseif ($data->{$element}  == true && is_numeric($data->{$element}) == false) {
            $this->setMessage($element, 'ご注文者情報：従業員数は半角数字でお願いします。');
            $result = false;
        }

        // 名刺情報：名前
        $element = 'meishi_name';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文者情報：お名前は必須項目です。');
            $result = false;
        } elseif (!$this->check_length($data->{$element}, 0, 30)) {
            $this->setMessage($element, 'ご注文者情報：お名前は30文字以内でお願いします。');
            $result = false;
        }

        // 名刺情報：ふりがな
        $element = 'meishi_kana';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文者情報：ふりがなは必須項目です。');
            $result = false;
        } elseif (! $this->check_hiragana($data->{$element})) {
            $this->setMessage($element, 'ご注文者情報：ふりがなはひらがなでお願いします。');
            $result = false;
        } elseif (!$this->check_length($data->{$element}, 0, 30)) {
            $this->setMessage($element, 'ご注文者情報：ふりがなは30文字以内でお願いします。');
            $result = false;
        }

        // 名刺情報：郵便番号
        $element = 'meishi_post1';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文者情報：郵便番号（左）は必須項目です。');
            $result = false;
        } elseif (! $this->check_length($data->{$element}, 3, 3) || ! is_numeric($data->{$element})) {
            $this->setMessage($element, 'ご注文者情報：郵便番号（左）は半角数字3桁でお願いします。');
            $result = false;
        }
        $element = 'meishi_post2';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文者情報：郵便番号（右）は必須項目です。');
            $result = false;
        } elseif (! $this->check_length($data->{$element}, 4, 4) || ! is_numeric($data->{$element})) {
            $this->setMessage($element, 'ご注文者情報：郵便番号（右）は半角数字4桁でお願いします。');
            $result = false;
        }
        // 都道府県
        $element = 'meishi_pref';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文者情報：都道府県は必須項目です。');
            $result = false;
        } elseif (! in_array($data->{$element}, array_keys(Config::get('prefectures')))) {
            $this->setMessage($element, 'ご注文者情報：都道府県に誤りがあります。');
            $result = false;
        }
        // 都道府県以降の住所
        $element = 'meishi_addr';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文者情報：住所(都道府県以降)は必須項目です。');
            $result = false;
        } elseif (!$this->check_length($data->{$element}, 0, 100)) {
            $this->setMessage($element, 'ご注文者情報：住所(都道府県以降)は100文字以内でお願いします。');
            $result = false;
        }

        // 名刺情報：電話
        $element = 'meishi_tel';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文者情報：電話番号は必須項目です。');
            $result = false;
        } elseif (!$this->check_tel($data->{$element})) {
            $this->setMessage($element, 'ご注文者情報：電話番号は数字と「-」(ハイフン)で13桁以内でお願いします。');
            $result = false;
        }

        // メール
        $element = 'meishi_email';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文者情報：メールアドレスは必須項目です。');
            $result = false;
        } elseif (!$this->check_email($data->{$element})) {
            $this->setMessage($element, 'ご注文者情報：メールアドレスの形式に誤りがあります。');
            $result = false;
        } elseif (!$this->check_length($data->{$element}, 0, 255)) {
            $this->setMessage($element, 'ご注文者情報：メールアドレスは255文字以内でお願いします。');
            $result = false;
        } elseif (!filter_var($data->{$element}, FILTER_VALIDATE_EMAIL)) {
            // invalid emailaddress
            $this->setMessage($element, 'ご注文者情報：無効なメールアドレスです。');
            $result = false;
        }

        // 名刺情報：注文人数
        $element = 'meishi_orders';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文内容：注文人数は必須項目です。');
            $result = false;
        } elseif (!is_numeric($data->{$element})) {
            $this->setMessage($element, 'ご注文内容：注文人数は半角数字でお願いします。');
            $result = false;
        }

        // 一人あたりのセット数
        $element = 'meishi_per_person_set';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文内容：一人あたりのセット数は必須です。');
            $result = false;
        } elseif (! in_array($data->{$element}, array_keys(Config::get('perPersonSetNumList')))) {
            $this->setMessage($element, 'ご注文内容：一人あたりのセット数に誤りがあります。');
            $result = false;
        }

        // 色数
        $element = 'meishi_colornum';
        $colorNum = array_filter($data->{$element});
        if (empty($colorNum)) {
            $this->setMessage($element, 'ご注文内容：色数は必須項目です。');
            $result = false;
        } elseif (! is_array($data->{$element})) {
            $this->setMessage($element, 'ご注文内容：色数に誤りがあります。');
            $result = false;
        }

        // 名刺情報：購入パターン
        $element = 'meishi_buy_pattern';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文内容：ご購入パターンは必須項目です。');
            $result = false;
        }

        // 名刺情報：購入パターン
        $element = 'meishi_know_pattern';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文内容：アンケートは必須項目です。');
            $result = false;
        }

        // 名刺情報：下部デザインパターン
        $element = 'meishi_design_pattern';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文内容：名刺の下部デザイン（表面）は必須項目です。');
            $result = false;
        }

        // 名刺情報：下部デザインパターン
        $element = 'meishi_design_pattern_back';
        if (! $data->{$element}) {
            $this->setMessage($element, 'ご注文内容：名刺の下部デザイン（裏面）は必須項目です。');
            $result = false;
        }

        $element = 'meishi_note';
        if (! $this->check_length($data->{$element}, 0, 400)) {
            $this->setMessage($element, 'ご注文内容：備考欄は400文字以内でお願いします。');
            $result = false;
        }

        $element = 'meishi_promo';
        if ($data->{$element} ) {
	        global $IS_DEV;
	        global $IS_STAGING;
	        $promotion_check_endpoint = "https://tbm:limex123@dev.tb-m.com/framework/public/api/checkpromotion";
	        if ($IS_DEV) {
		        $promotion_check_endpoint = "http://localhost/framework/public/api/checkpromotion";
				if ($IS_STAGING) {
					$promotion_check_endpoint = "http://limex-12s5e84d2.devenv.space/framework/public/api/checkpromotion";
				}
			}

            $url = $promotion_check_endpoint;
            $request = array('promotion' => $data->{$element});

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($request)
                )
        );

            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            $json = str_replace('},]',"}]",$result);
            $jsondata = json_decode($json);

            if ( !$jsondata->exists ) {
                $this->setMessage($element, 'ご注文内容：指定されたプロモーションコードは存在しません。');
                $result = false;
            }
        }

        // 名刺情報：単価
        // $element = 'meishi_price';
        // if (!is_numeric($data->{$element})) {
        //     $this->setMessage($element, '名刺情報：単価は半角数字でお願いします。');
        //     $result = false;
        // }

        //----------------------------------------------------------//
        //                          納品先                          //
        //----------------------------------------------------------//
        //どれか１つでも入力されていたら全て必須チェックをかける
        $requireFlg = false;
        $elements = array('company', 'name', 'kana', 'post1', 'post2', 'pref', 'addr', 'tel');
        $buff = '';
        foreach ($elements as $value) {
            $buff .= $data->{'nouhin_' . $value};
        }
        if (! empty($buff)) {
            $requireFlg = true;
        }

        // 納品先：会社名
        $element = 'nouhin_company';
        if ($requireFlg && ! $data->{$element}) {
            $this->setMessage($element, '納品先：会社名は必須項目です。');
            $result = false;
        } elseif (! $this->check_length($data->{$element}, 0, 30)) {
            $this->setMessage($element, '納品先：会社名は30文字以内でお願いします。');
            $result = false;
        }

        // 納品先：名前
        $element = 'nouhin_name';
        if ($requireFlg && ! $data->{$element}) {
            $this->setMessage($element, '納品先：お名前は必須項目です。');
            $result = false;
        } elseif (! $this->check_length($data->{$element}, 0, 30)) {
            $this->setMessage($element, '納品先：お名前は30文字以内でお願いします。');
            $result = false;
        }

        // 納品先：ふりがな
        $element = 'nouhin_kana';
        if ($requireFlg && ! $data->{$element}) {
            $this->setMessage($element, '納品先：ふりがなは必須項目です。');
            $result = false;
        } elseif (! $this->check_hiragana($data->{$element})) {
            $this->setMessage($element, '納品先：ふりがなはひらがなでお願いします。');
            $result = false;
        } elseif (! $this->check_length($data->{$element}, 0, 30)) {
            $this->setMessage($element, '納品先：ふりがなは30文字以内でお願いします。');
            $result = false;
        }

        // 納品先：郵便番号
        $element = 'nouhin_post1';
        if ($requireFlg && ! $data->{$element}) {
            $this->setMessage($element, '納品先：郵便番号（左）は必須項目です。');
            $result = false;
        } elseif ($data->{$element}!= '' && (! $this->check_length($data->{$element}, 3, 3) || ! is_numeric($data->{$element}))) {
            $this->setMessage($element, '納品先：郵便番号（左）は半角数字3桁でお願いします。');
            $result = false;
        }
        $element = 'nouhin_post2';
        if ($requireFlg && ! $data->{$element}) {
            $this->setMessage($element, '納品先：郵便番号（右）は必須項目です。');
            $result = false;
        } elseif ($data->{$element}!= '' && (! $this->check_length($data->{$element}, 4, 4) || ! is_numeric($data->{$element}))) {
            $this->setMessage($element, '納品先：郵便番号（右）は半角数字4桁でお願いします。');
            $result = false;
        }
        // 都道府県
        $element = 'nouhin_pref';
        if ($requireFlg && ! $data->{$element}) {
            $this->setMessage($element, '納品先：都道府県は必須項目です。');
            $result = false;
        } elseif (! in_array($data->{$element}, array_keys(Config::get('prefectures')))) {
            $this->setMessage($element, '納品先：都道府県に誤りがあります。');
            $result = false;
        }
        // 都道府県以降の住所
        $element = 'nouhin_addr';
        if ($requireFlg && ! $data->{$element}) {
            $this->setMessage($element, '納品先：住所(都道府県以降)は必須項目です。');
            $result = false;
        } elseif (!$this->check_length($data->{$element}, 0, 100)) {
            $this->setMessage($element, '納品先：住所(都道府県以降)は100文字以内でお願いします。');
            $result = false;
        }

        // 納品先：電話
        $element = 'nouhin_tel';
        if ($requireFlg && ! $data->{$element}) {
            $this->setMessage($element, '納品先：電話番号は必須項目です。');
            $result = false;
        } elseif ($data->{$element}!= '' &&!$this->check_tel($data->{$element})) {
            $this->setMessage($element, '納品先：電話番号は数字と「-」(ハイフン)で13桁以内でお願いします。');
            $result = false;
        }

        // プライバシーポリシー
        $element = 'agree';
        if (! $data->{$element} || $data->{$element} == 2) {
            $this->setMessage($element, '個人情報の取得・利用への同意は必須です。');
            $result = false;
        }

        return $result;
    }

    /**
     * 要素別エラーメッセージ格納
     * @param string $element 要素名
     * @param string $value エラーメッセージ
     */
    private function setMessage($element, $message)
    {
        // なければ初期化
        if (!isset($this->message[$element])) {
            $this->message[$element] = "";
        }
        $this->message[$element] .= $message . "<br />";
    }

    /**
     * 要素別エラーメッセージ格納
     * @param  string $element 要素名
     * @return エラーメッセージ（要素指定時は要素別）
     */
    public function getMessage($element = null)
    {
        return (! $element)?$this->message:$this->message[$element];
    }


    ///////////////////////////////////////////////////////////////////////////
    /**
     * 文字列の長さチェック
     *
     * @param  string $str        チェック文字列
     * @param  int    $min_length 最小値
     * @param  int    $max_length 最大値
     * @return bool   true=OK, false=NG
     */
    private function check_length($str, $min_length, $max_length)
    {
        $length = mb_strlen($str, 'UTF8');
        return  ($length >= $min_length && $max_length >= $length)?true:false;
    }

    /**
     * E-mailの妥当性チェック
     *
     * @param  string $email メールアドレス
     * @return bool   true=OK, false=NG
     */
    private function check_email($email)
    {
        $pattern = '/^[a-zA-Z0-9_¥.¥-]+?@[A-Za-z0-9_¥.¥-]+$/';
        return preg_match($pattern, $email)?true:false;
    }

    /**
     * 電話番号の妥当性チェック
     *
     * @param  string $tel 電話
     * @return bool   true=OK, false=NG
     */
    private function check_tel($tel)
    {
        $pattern = '/^[0-9¥-]{1,13}$/';
        return preg_match($pattern, $tel)?true:false;
    }

    /**
     * 入力文字列の平仮名チェック+全角空白
     *
     * @param  string $tel 電話
     * @return bool   true=OK, false=NG
     */
    private function check_hiragana($str)
    {
        return preg_match("/^[　\sぁ-んー]*$/u", $str);
    }
}
