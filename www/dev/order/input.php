<?php
global $IS_DEV;
global $IS_STAGING;
$promotion_check_endpoint = "https://tbm:limex123@dev.tb-m.com/framework/public/api/checkpromotion";
if ( $IS_DEV ) {
    $promotion_check_endpoint = "http://localhost/framework/public/api/checkpromotion";
    if ( $IS_STAGING ) {
        $promotion_check_endpoint = "http://limex-12s5e84d2.devenv.space/framework/public/api/checkpromotion";
    }
}
?>
<?php get_header(); ?>
<link type="text/css" rel="stylesheet" href="./css/style.css"/>
<script type="text/javascript">
    window.onload = function onLoad() {
        param = GetQueryString();
        target = document.getElementById("param");
        target.innerHTML = param["q"];
        if(param["q"]!=null){
        document.getElementsByName("meishi_promo")[0].value = param["q"];
        if(param["q"]=="130012"){
            document.getElementsByName("meishi_know_pattern")[0].value='other';
            knowPatternManager();
            document.getElementsByName("meishi_note")[0].value="CAMCARD BUSINESSユーザー様向け特別プロモーション価格適用\n(※↑こちらの文章は削除しないで下さい。)";
        }
        }

        
    }

    function GetQueryString() {
        if (1 < document.location.search.length) {
            // 最初の1文字 (?記号) を除いた文字列を取得する
            var query = document.location.search.substring(1);

            // クエリの区切り記号 (&) で文字列を配列に分割する
            var parameters = query.split('&');

            var result = new Object();
            for (var i = 0; i < parameters.length; i++) {
                // パラメータ名とパラメータ値に分割する
                var element = parameters[i].split('=');

                var paramName = decodeURIComponent(element[0]);
                var paramValue = decodeURIComponent(element[1]);

                // パラメータ名をキーとして連想配列に追加する
                result[paramName] = decodeURIComponent(paramValue);
            }
            return result;
        }
        // was return null;
        var result= new Object();
        return result['q']=decodeURIComponent('nothing');
    }
</script>
<script type="text/javascript" src="./js/order.js"></script>
<!-- モーダル用 -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/action/assets/css/colorbox.css" type="text/css"
      media="all"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/action/assets/css/modal.css" type="text/css"
      media="all"/>
<script type="text/javascript"
        src="<?php echo get_template_directory_uri(); ?>/action/assets/js/jquery.colorbox-min.js"></script>
<script type="text/javascript"
        src="<?php echo get_template_directory_uri(); ?>/action/assets/js/jquery.modal.js"></script>
<!-- モーダル用 -->
<style type="text/css">
    .caution_text {
        line-height: 1.5;
        margin-top: 8px;
        font-size: 12px;
    }

    .file_caution {
        line-height: 1.5;
        margin-top: 8px;
        font-size: 12px;
    }

    .bottom_text {
        margin-top: 8px;
        margin-bottom: 35px;
    }

    body.order #form select {
        width: 340px;
    }

    textarea {
        font-size: 16px;

    }

    /* add */
    .file_caution {
        font-size: 14px;
    }

    .caution_text {
        font-size: 14px;
    }

    .slider_caution {
        width: 750px;
        text-align: left;
        font-size: 14px;
        font-family: Arial;
        font-weight: bold;
        color: #666;
        padding-top: 410px;
        margin-left: 115px;
    }

    .meishi_color_div .head {
        border-bottom: solid 1px black;
        padding-bottom: 10px;
        margin-top: 20px;
    }

    .meishi_color_div .head:after {
        display: block;
        content: "";
        clear: both;
    }

    .meishi_color_div .head .order_detail {
        font-weight: bold;
    }

    .meishi_color_div .head .order_detail_desc {
        margin-top: -12px;
        float: right;
        font-size: 10px;
        color: #333333;
    }

    .meishi_color_div .head .order_detail_desc:after {
        content: "▼";
    }

    .meishi_color_div .head .order_detail_desc.open:after {
        content: "▲";
    }
</style>
<section id="body">
    <div class="pageTtl">
        <h2>ORDER</h2>
    </div>
    <h3>ご注文</h3>
    <div class="wrapper input">
        <form action="<?php echo HOST ?>/order/confirm" method="post" name="form_order" id="form"
              enctype="multipart/form-data">
            <?php if ( Order::hasErrMessage( 'system' ) ) : ?>
                <div class="errmsg"><?php echo Order::getErrMessage( 'system' ); ?></div>
            <?php endif; ?>
            <!-- 名刺情報 -->
            <div class="head">
                <div class="order_title">ご注文者情報</div>
                <div class="pt_ssss pb_ssss mt20">以下情報をご入力ください。</div>
                <div class="note"><span class="caution">※は必須項目です。</span></div>
            </div>
            <dl class="cf">
                <dt>用途<span class="caution">※</span></dt>
                <dd>
                    <div class="mb_sss">
                        <?php foreach ( Config::get( 'useNumList' ) as $key => $value ): ?>
                            <label class="mb_sssss"><input onchange="useNum.cautionControl()" type="radio"
                                                           name="meishi_use" id="" class="meishi_use_list"
                                                           value="<?php echo $key ?>" <?php echo $valueInstance->meishi_use == $key ? 'checked="checked"' : null ?>>&nbsp;<?php echo $value ?>
                            </label>
                        <?php endforeach; ?>
                        <?php if ( Order::getSession( 'errmsg.meishi_use' ) ) {
                            printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_use' ) );
                        } ?>
                    </div>
                </dd>
            </dl>
            <dl class="cf">
                <dt>会社名<span class="company_caution caution" style="display:none;">※</span></dt>
                <dd>
                    <input type="text" class="input_001" id="meishi_company" name="meishi_company"
                           value="<?php echo h( $valueInstance->meishi_company ); ?>" placeholder="例：株式会社TBM"/>
                    <?php if ( Order::getSession( 'errmsg.meishi_company' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_company' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf">
                <dt>従業員数<span class="employees_caution caution" style="display:none;">※</span></dt>
                <dd>
                    <input type="text" class="input_001" id="meishi_employees" name="meishi_employees"
                           value="<?php echo h( $valueInstance->meishi_employees ); ?>" placeholder="100"/>
                    <?php if ( Order::getSession( 'errmsg.meishi_employees' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_employees' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf">
                <dt>お名前<span class="caution">※</span></dt>
                <dd>
                    <input type="text" class="input_001" id="meishi_name" name="meishi_name"
                           value="<?php echo h( $valueInstance->meishi_name ); ?>" placeholder="例：石野　紙男"/>
                    <?php if ( Order::getSession( 'errmsg.meishi_name' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_name' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf">
                <dt>ふりがな<span class="caution">※</span></dt>
                <dd>
                    <input type="text" class="input_001" id="meishi_kana" name="meishi_kana"
                           value="<?php echo h( $valueInstance->meishi_kana ); ?>" placeholder="例：いしの　かみお"/>
                    <?php if ( Order::getSession( 'errmsg.meishi_kana' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_kana' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf">
                <dt>住所<span class="caution">※</span></dt>
                <dd>
                    <p class="mb_sssss"><input type="text" class="order_post form_small" maxlength="3" id="meishi_post1"
                                               name="meishi_post1"
                                               value="<?php echo h( $valueInstance->meishi_post1 ); ?>" size="3"/> -
                        <input type="text" class="order_post form_small" maxlength="4" id="meishi_post2"
                               name="meishi_post2" value="<?php echo h( $valueInstance->meishi_post2 ); ?>" size="4"/>
                    </p>
                    <select class="mb_ssss" id="meishi_pref" name="meishi_pref">
                        <?php foreach ( Config::get( 'prefectures' ) as $key => $value ): ?>
                            <option
                                value="<?php echo $key ?>" <?php echo $valueInstance->meishi_pref == $key ? 'selected' : null ?> ><?php echo $value ?></option>
                        <?php endforeach; ?>
                    </select><br/>
                    <input type="text" class="input_001" id="meishi_addr" name="meishi_addr"
                           value="<?php echo h( $valueInstance->meishi_addr ); ?>"
                           placeholder="例：千代田区丸の内1-3-1　東京銀行協会ビル10階"/>
                    <?php if ( Order::getSession( 'errmsg.meishi_post1' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_post1' ) );
                    } ?>
                    <?php if ( Order::getSession( 'errmsg.meishi_post2' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_post2' ) );
                    } ?>
                    <?php if ( Order::getSession( 'errmsg.meishi_pref' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_pref' ) );
                    } ?>
                    <?php if ( Order::getSession( 'errmsg.meishi_addr' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_addr' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf">
                <dt>電話番号<span class="caution">※</span></dt>
                <dd>
                    <input type="text" class="input_001" id="meishi_tel" name="meishi_tel"
                           value="<?php echo h( $valueInstance->meishi_tel ); ?>" placeholder="例：0300000000"/>
                    <?php if ( Order::getSession( 'errmsg.meishi_tel' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_tel' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf mb40">
                <dt>メールアドレス<span class="caution">※</span></dt>
                <dd>
                    <input type="text" class="input_001" id="meishi_email" name="meishi_email"
                           value="<?php echo h( $valueInstance->meishi_email ); ?>" placeholder="test@test.com"/>
                    <?php if ( Order::getSession( 'errmsg.meishi_email' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_email' ) );
                    } ?>
                </dd>
            </dl>
            <div class="head">
                <div class="order_title">ご注文内容</div>
            </div>
            <dl class="cf mt30">
                <dt>ご注文人数<span class="caution">※</span><br>(名刺を作成される方の人数)</dt>
                <dd>
                    <input type="text" class="input_001" id="meishi_orders" name="meishi_orders"
                           value="<?php echo h( $valueInstance->meishi_orders ); ?>" placeholder="100"
                           onchange="setCount()"/>
                    <?php if ( Order::getSession( 'errmsg.meishi_orders' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_orders' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf">
                <dt>一人あたりのセット数<span class="caution">※</span><br/>(100枚 / 1セット)</dt>
                <dd>
                    <select id="meishi_per_person_set" name="meishi_per_person_set" onchange="setCount()">
                        <?php foreach ( Config::get( 'perPersonSetNumList' ) as $key => $value ) : ?>
                            <option
                                value="<?php echo $key ?>" <?php echo $valueInstance->meishi_per_person_set == $key ? 'selected="selected"' : null ?>>
                                &nbsp;<?php echo $value ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php if ( Order::getSession( 'errmsg.meishi_per_person_set' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_per_person_set' ) );
                    } ?>
                </dd>
                <dd class="file_caution" style="margin-top:8px;">
                    ＊「人によって異なる」を選択された場合は、備考欄<span style="color:red;">(ページ下)</span>にどなたの名刺を何セットご希望かご記入ください。
                </dd>
            </dl>
            <dl class="cf">
                <dt>合計セット数</dt>
                <dd>
                    <p id="setcount">-</p>
                </dd>
            </dl>
            <dl class="cf color_number">
                <dt>色数<span class="caution">※</span></dt>
                <div class="color_number_block">
                    <?php foreach ( Config::get( 'colorNumList' ) as $key => $value ): ?>
                        <?php echo ( $key == 1 ) ? '<div class="color_number_left" style="float:left;width:150px;">' : '' ?>
                        <?php echo ( $key == 3 ) ? '<div class="color_number_right" style="float:right;width:390px;">' : '' ?>

                        <dd>
                            <input type="hidden" name="meishi_colornum[<?php echo $key ?>]"
                                   id="color_num_<?php echo $key ?>"
                                   value="<?php echo ( $valueInstance->meishi_colornum[ $key ] == $key ) ? $key : '' ?>">
                            <label><input onchange="prmPriceClear();syncColorNumAndPromoCode();" class="color_num_list"
                                          type="checkbox" name="meishi_colornum_[<?php echo $key ?>]"
                                          value="<?php echo $key ?>"
                                    <?php echo ( $valueInstance->meishi_colornum[ $key ] == $key ) ? 'checked="checked"' : '' ?>>
                                <?php echo $value ?>
                            </label>
                        </dd>
                        <?php echo ( $key == 2 || $key == 5 ) ? '</div>' : '' ?>
                    <?php endforeach; ?>
                </div>
                <dd>
                    <?php if ( Order::getSession( 'errmsg.meishi_colornum' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_colornum' ) );
                    } ?>
                </dd>
                <!--
								<dd class="file_caution" style="margin-top:8px;">
								＊作成される名刺の色数のパターンを全てお答えください。
								</dd>
				-->
            </dl>

            <?php foreach ( Config::get( 'colorNumList' ) as $key => $value ): ?>
                <?php /* 色指定ごとの購入内容 */ ?>
                <?php
                $is_multi_side_print = ( strpos( $value, '両面' ) !== false ) ? true : false;
                ?>
                <div id="meishi_<?php echo $key ?>" class="meishi_color_div" data-color="<?php echo $key ?>">
                    <div class="head">
                        <div class="order_detail"><?php echo $value ?> のご指定</div>
                        <div class="order_detail_desc"></div>
                    </div>

                    <dl class="cf">
                        <dt>ご購入パターン<span class="caution">※</span></dt>
                        <dd>
                            <div id="buypatternlist" class="mb_sss">

                                <table class="form-pattern-table">
                                    <tbody>
                                    <tr>
                                        <label class="inline-block mb_sssss">
                                            <td>
                                                <input type="radio" name="meishi_buy_pattern[<?php echo $key ?>]"
                                                       class="v-top"
                                                       id="radio1" <?php echo $valueInstance->meishi_buy_pattern[ $key ] == 1 ? 'checked="checked"' : null ?>
                                                       value="1" onclick="setDesignPattern(<?php echo $key ?>)">
                                            </td>
                                            <td>
                                                ゼロベースから新規デザインを希望する
                                            </td>
                                            <td>要相談</td>
                                        </label>
                                    </tr>
                                    <tr>
                                        <label class="inline-block">
                                            <td>
                                                <input type="radio" name="meishi_buy_pattern[<?php echo $key ?>]"
                                                       class="v-top" id="radio2"
                                                       value="2" <?php echo $valueInstance->meishi_buy_pattern[ $key ] == 2 ? 'checked="checked"' : null ?>
                                                       onclick="setDesignPattern(<?php echo $key ?>)">
                                            </td>
                                            <td>
                                                ご希望のデザインに近い名刺のスキャンデータ等、 明確なイメージをアップロードする
                                            </td>
                                            <td>＋¥10,000~¥20,000</td>
                                        </label>
                                    </tr>
                                    <tr>
                                        <label class="inline-block">
                                            <td>
                                                <input type="radio" name="meishi_buy_pattern[<?php echo $key ?>]"
                                                       class="v-top" id="radio3"
                                                       value="3" <?php echo $valueInstance->meishi_buy_pattern[ $key ] == 3 ? 'checked="checked"' : null ?>
                                                       onclick="setDesignPattern(<?php echo $key ?>)">
                                            </td>
                                            <td>
                                                大体のデザインをこ自身てで制作し、アップロードする (Excel・PowerPoint・Word等)
                                            </td>
                                            <td>片面につき＋¥3,000~</td>
                                        </label>
                                    </tr>
                                    <tr>
                                        <label class="inline-block">
                                            <td>
                                                <input type="radio" name="meishi_buy_pattern[<?php echo $key ?>]"
                                                       class="v-top" id="radio4"
                                                       value="4" <?php echo $valueInstance->meishi_buy_pattern[ $key ] == 4 ? 'checked="checked"' : null ?>
                                                       onclick="setDesignPattern(<?php echo $key ?>)">
                                            </td>
                                            <td>
                                                入稿データがないため、ご希望の名刺デザインのスキャン(及びロゴデータ等)をアップロードする
                                            </td>
                                            <td>片面につき＋¥2,000~<br>＊トレース(データ起こし)料</td>
                                        </label>
                                    </tr>
                                    <tr>
                                        <label class="inline-block">
                                            <td>
                                                <input type="radio" name="meishi_buy_pattern[<?php echo $key ?>]"
                                                       class="v-top" id="radio5"
                                                       value="5" <?php echo $valueInstance->meishi_buy_pattern[ $key ] == 5 ? 'checked="checked"' : null ?>
                                                       onclick="setDesignPattern(<?php echo $key ?>)">
                                            </td>
                                            <td>
                                                入稿データをアップロードする (イラストレーター・Excel・PowerPoint・Word等)
                                            </td>
                                            <td>＋¥0<br>＊データによってはトレース(データ起こし)料が発生する場合があります</td>
                                        </label>
                                    </tr>
                                    <tr>
                                        <label class="inline-block">
                                            <td>
                                                <input type="radio" name="meishi_buy_pattern[<?php echo $key ?>]"
                                                       class="v-top" id="radio6"
                                                       value="6" <?php echo $valueInstance->meishi_buy_pattern[ $key ] == 6 ? 'checked="checked"' : null ?>
                                                       onclick="setDesignPattern(<?php echo $key ?>)">
                                            </td>
                                            <td>
                                                LIMEX名刺デザインテンプレートから選択する
                                            </td>
                                            <td>＋¥0</td>
                                        </label>
                                    </tr>
                                    </tbody>
                                </table>


                                <?php if ( Order::getSession( 'errmsg.meishi_buy_pattern' ) ) {
                                    printf( '<div class="errmsg">%s</div>',
                                        Order::getSession( 'errmsg.meishi_buy_pattern' ) );
                                } ?>
                            </div>
                        </dd>
                    </dl>
                    <dl class="cf card-template" style="display: none;">
                        <p class="title">LIMEX名刺デザインテンプレート</p>
                        <input id="meishi_card_type_<?php echo $key ?>" type="hidden"
                               name="meishi_card_type[<?php echo $key ?>]" class="meishi_card_type"
                               value="<?php if ( $valueInstance->meishi_card_type[ $key ] ) {
                                   echo $valueInstance->meishi_card_type[ $key ];
                               } else {
                                   echo "";
                               } ?>">
                        </dt>
                        <br>
                        <ul class="card-slider" style="position: relative;">
                            <li class="slide0">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE01_G-001.png" alt="TYPE01_G">
                                    <p>TYPE1 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE01_M-001.png" alt="TYPE01_M">
                                    <p>TYPE1 明朝体</p>
                                </div>
                            </li>
                            <li class="slide1">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE02_G-001.png" alt="TYPE02_G">
                                    <p>TYPE2 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE02_M-001.png" alt="TYPE02_M">
                                    <p>TYPE2 明朝体</p>
                                </div>
                            </li>
                            <li class="slide2">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE03_G-001.png" alt="TYPE03_G">
                                    <p>TYPE3 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE03_M-001.png" alt="TYPE03_M">
                                    <p>TYPE3 明朝体</p>
                                </div>
                            </li>
                            <li class="slide3">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE04_G-001.png" alt="TYPE04_G">
                                    <p>TYPE4 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE04_M-001.png" alt="TYPE04_M">
                                    <p>TYPE4 明朝体</p>
                                </div>
                            </li>
                            <li class="slide4">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE05_G-001.png" alt="TYPE05_G">
                                    <p>TYPE5 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE05_M-001.png" alt="TYPE05_M">
                                    <p>TYPE5 明朝体</p>
                                </div>
                            </li>
                            <li class="slide5">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE06_G-001.png" alt="TYPE06_G">
                                    <p>TYPE6 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE06_M-001.png" alt="TYPE06_M">
                                    <p>TYPE6 明朝体</p>
                                </div>
                            </li>
                            <li class="slide6">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE07_G-001.png" alt="TYPE07_G">
                                    <p>TYPE7 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE07_M-001.png" alt="TYPE07_M">
                                    <p>TYPE7 明朝体</p>
                                </div>
                            </li>
                            <li class="slide7">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE08_G-001.png" alt="TYPE08_G">
                                    <p>TYPE8 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE08_M-001.png" alt="TYPE08_M">
                                    <p>TYPE8 明朝体</p>
                                </div>
                            </li>
                            <li class="slide8">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE09_G-001.png" alt="TYPE09_G">
                                    <p>TYPE9 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE09_M-001.png" alt="TYPE09_M">
                                    <p>TYPE9 明朝体</p>
                                </div>
                            </li>
                            <li class="slide9">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE10_G-001.png" alt="TYPE10_G">
                                    <p>TYPE10 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE10_M-001.png" alt="TYPE10_M">
                                    <p>TYPE10 明朝体</p>
                                </div>
                            </li>
                            <li class="slide10">
                                <div class="gosick">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/gosick/TYPE11_G-001.png" alt="TYPE11_G">
                                    <p>TYPE11 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="selected"><span>選択中</span></div>
                                    <img src="../order/img/mintyou/TYPE11_M-001.png" alt="TYPE11_M">
                                    <p>TYPE11 明朝体</p>
                                </div>
                            </li>
                        </ul>
                        <div class="slider_caution">＊「LOGO」の箇所にロゴの挿入をご希望の場合は、ロゴデータのアップロードをお願いします。</div>
                    </dl>
                    <dl class="cf">
                        <dt>名刺の下部デザイン<span class="caution">※</span><br>
                            (無料でLIMEX使用表記を<br>挿入いたただけます。)
                        </dt>
                        <dd>
                            <div id="designpatternlist" class="mb_sss">
                                <table class="form-pattern-table" style="width:100%;">
                                    <tr>
                                        <td><span>表</span></td>
                                        <?php if ( $is_multi_side_print ): ?>
                                            <td><span>裏</span></td>
                                        <?php endif; ?>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <?php foreach ( Config::get( 'designPatternNumList' ) as $key2 => $value2 ): ?>
                                        <tr>
                                            <td>
                                                <input id="meishi_design_pattern<?php echo $key2 ?>" type="radio"
                                                       name="meishi_design_pattern[<?php echo $key ?>]"
                                                       value="<?php echo $value2 ?>" <?php echo $valueInstance->meishi_design_pattern[ $key ] == $value2 ? 'checked="checked"' : null ?>>
                                                <?php if ( ! $is_multi_side_print ): ?>
                                                    <input type="hidden"
                                                           name="meishi_design_pattern_back[<?php echo $key ?>]"
                                                           value="">
                                                <?php endif; ?>
                                            </td>
                                            <?php if ( $is_multi_side_print ): ?>
                                                <td>
                                                    <input id="meishi_design_pattern_back<?php echo $key2 ?>"
                                                           type="radio"
                                                           name="meishi_design_pattern_back[<?php echo $key ?>]"
                                                           value="<?php echo $value2 ?>" <?php echo $valueInstance->meishi_design_pattern_back[ $key ] == $value2 ? 'checked="checked"' : null ?>
                                                           class="uramen">
                                                </td>
                                            <?php endif; ?>

                                            <td><?php echo $value2 ?></td>

                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                                <!-- <a href="#js-meishi_design_popup" class="js-meishi_design_popup btn_sample">デザインサンプルはこちら</a> -->
                            </div>
                            <?php if ( Order::getSession( 'errmsg.meishi_design_pattern' ) ) {
                                printf( '<div class="errmsg">%s</div>',
                                    Order::getSession( 'errmsg.meishi_design_pattern' ) );
                            } ?>
                            <?php if ( Order::getSession( 'errmsg.meishi_design_pattern_back' ) ) {
                                printf( '<div class="errmsg">%s</div>',
                                    Order::getSession( 'errmsg.meishi_design_pattern_back' ) );
                            } ?>
                            <p class="select-design" style="margin-left: -230px;margin-bottom:20px;"><img
                                    src="img/order-select-design.png" alt="" width="780"></p>
                        </dd>
                    </dl>
                </div>
                <?php /* 色指定ごとの購入内容 ここまで */ ?>
            <?php endforeach; ?>

            <dl class="cf meishi-upload">
                <dt>入稿データ・ロゴデータ・<br/>
                    デザイン参考データ<br/>
                    のアップロード
                </dt>
                <dd>
                    <div id="filedeletebutton" style="display: none;">
                        <p><?php echo $valueInstance->meishi_file ?></p>
                        <input type="button" onclick="fileUpload.deleteButton()" value="削除する">
                    </div>
                    <input type="file" name="meishi_tmp_file"
                           onchange="fileUpload.postAjaxFile()" <?php echo $valueInstance->meishi_file ? 'style="display: none"' : null ?>
                           id="meishi_tmp_file">
                    <input type="hidden" value="<?php echo h( $valueInstance->meishi_file ) ?>" name="meishi_file"
                           id="meishi_file">
                    <input type="hidden" value="<?php echo h( $valueInstance->meishi_file_unique ) ?>"
                           name="meishi_file_unique" id="meishi_file_unique">
                    <div>
                        <p class="file_caution">
                            ＊イラストレータの場合、CS1バージョン、および確認用PDFの<br>　2種類をお願いします。<br>
                            ＊イラストレータの場合、可能であればフォント(書体)のアウトライン前、<br>　アウトライン後の2種類のアップロードをお願いいたします。<br>
                            ＊ Excel・PowerPoint・Wordの場合、 PDFに書き出したデータも<br>　あわせてアップロードをお願いいたします。<br>
                            ＊リンク画像等がある場合には、データに埋め込んでいただくか、<br>　別途画像データの提供をお願いいたします。<br>
                            ＊関連データは圧縮ソフト(ZIP、Lhazなど)で１つのファイルに圧縮し、<br>　アップロードをお願いいたします。
                        </p>
                    </div>
                    <div>
                        <?php if ( Order::getSession( 'errmsg.meishi_file' ) ) {
                            printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_file' ) );
                        } ?>
                    </div>
                </dd>
            </dl>

            <dl class="cf">
                <dt>プロモーションコード<br>(お持ちの方)</dt>
                <dd id="promotioninput">
                    <input onchange="prmPriceClear();checkPromotion();" type="text" class="input_001" id="meishi_promo"
                           name="meishi_promo" value="<?php echo $valueInstance->meishi_promo; ?>"
                           placeholder="1234567"/>
                    <?php if ( Order::getSession( 'errmsg.meishi_promo' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_promo' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf js-promo-code-price" style="display: none">
                <dt>プロモーション単価</dt>
                <?php foreach ( Config::get( 'colorNumList' ) as $key => $value ): ?>
                    <?php $meishi_promo_price = $valueInstance->meishi_promo_price; ?>
                    <dd class="js-promo-code-price-row" style="display: none" data-id="<?php echo $key ?>">
                        <span style="display: inline-block; width: 230px;"><?php echo $value ?></span><input
                            class="priceinput input_001" type="text" style="width: 200px"
                            name="meishi_promo_price[<?php echo base64_encode( $value ); ?>]"
                            value="<?php echo isset( $meishi_promo_price[ base64_encode( $value ) ] ) ? $meishi_promo_price[ base64_encode( $value ) ] : null ?>">&nbsp;円<br><br>
                    </dd>
                <?php endforeach; ?>
            </dl>
            <dl class="cf">
                <dt>どのようにしてLIMEXの名刺を<br/>お知りになりましたか？<span class="caution">※</span></dt>
                <dd>
                    <div class="mb_sss">
                        <select name="meishi_know_pattern" size="1" onchange="knowPatternManager()">
                            <?php foreach ( Config::get( 'knowPatternNumlist' ) as $key => $value ): ?>
                                <option
                                    value="<?php echo $key; ?>" <?php echo $valueInstance->meishi_know_pattern == $key ? 'selected="selected"' : null; ?>><?php echo $value; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php if ( Order::getSession( 'errmsg.meishi_know_pattern' ) ) {
                            printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_know_pattern' ) );
                        } ?>
                    </div>
                    <div class="js-noushin-input-area">
                        <div class="mb_sss js-noushin-sales"
                             style="display: <?php echo $valueInstance->meishi_know_pattern == 'sales' ? 'auto' : 'none' ?>">
                            <p class="mb_sssss">何という企業・担当者から紹介されましたか？</p>
                            <input type="text" class="input_001" id="nouhin_name" name="meishi_know_sales"
                                   value="<?php echo $valueInstance->meishi_know_sales; ?>" placeholder=""/>
                        </div>
                        <div class="mb_sss js-noushin-web"
                             style="display: <?php echo $valueInstance->meishi_know_pattern == 'web' ? 'auto' : 'none' ?>">
                            <p class="mb_sssss">何というサイト・記事ですか？</p>
                            <input type="text" class="input_001" id="nouhin_name" name="meishi_know_site"
                                   value="<?php echo $valueInstance->meishi_know_site; ?>" placeholder=""/>
                        </div>
                        <div class="mb_sss js-noushin-program"
                             style="display: <?php echo $valueInstance->meishi_know_pattern == 'program' ? 'auto' : 'none' ?>">
                            <p class="mb_sssss">何という番組ですか？</p>
                            <input type="text" class="input_001" id="nouhin_name" name="meishi_know_program"
                                   value="<?php echo $valueInstance->meishi_know_program; ?>" placeholder=""/>
                        </div>
                        <div class="mb_sss js-noushin-paper"
                             style="display: <?php echo $valueInstance->meishi_know_pattern == 'paper' ? 'auto' : 'none' ?>">
                            <p class="mb_sssss">何という新聞ですか？</p>
                            <input type="text" class="input_001" id="nouhin_name" name="meishi_know_paper"
                                   value="<?php echo $valueInstance->meishi_know_paper; ?>" placeholder=""/>
                        </div>
                        <div class="mb_sss js-noushin-seminar"
                             style="display: <?php echo $valueInstance->meishi_know_pattern == 'seminar' ? 'auto' : 'none' ?>">
                            <p class="mb_sssss">何というセミナーですか？</p>
                            <input type="text" class="input_001" id="nouhin_name" name="meishi_know_seminar"
                                   value="<?php echo $valueInstance->meishi_know_seminar; ?>" placeholder=""/>
                        </div>
                    </div>
                </dd>
            </dl>
            <dl class="cf mb40">
                <dt>備考欄</dt>
                <dd>
                    <textarea cols="60" rows="10"
                              name="meishi_note"><?php echo $valueInstance->meishi_note; ?></textarea>
                    <?php if ( Order::getSession( 'errmsg.meishi_note' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.meishi_note' ) );
                    } ?>
                    <p class="file_caution">
                        （400字以内）
                    </p>
                </dd>
            </dl>
            <!-- 納品先 -->
            <div class="head mb_sss">
                <div class="order_title">※ご注文者情報の住所と納品先が異なる場合</div>
            </div>
            <dl class="cf mt30">
                <dt>会社名</dt>
                <dd>
                    <input type="text" class="input_001" id="nouhin_company" name="nouhin_company"
                           value="<?php echo h( $valueInstance->nouhin_company ); ?>" placeholder="例：株式会社TBM"/>
                    <?php if ( Order::getSession( 'errmsg.nouhin_company' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.nouhin_company' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf">
                <dt>お名前</dt>
                <dd>
                    <input type="text" class="input_001" id="nouhin_name" name="nouhin_name"
                           value="<?php echo h( $valueInstance->nouhin_name ); ?>" placeholder="例：石野　紙男"/>
                    <?php if ( Order::getSession( 'errmsg.nouhin_name' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.nouhin_name' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf">
                <dt>ふりがな</dt>
                <dd>
                    <input type="text" class="input_001" id="nouhin_kana" name="nouhin_kana"
                           value="<?php echo h( $valueInstance->nouhin_kana ); ?>" placeholder="例：いしの　かみお"/>
                    <?php if ( Order::getSession( 'errmsg.nouhin_kana' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.nouhin_kana' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf">
                <dt>住所</dt>
                <dd>
                    <p class="mb_sssss"><input type="text" class="order_post form_small" maxlength="3" id="nouhin_post1"
                                               name="nouhin_post1"
                                               value="<?php echo h( $valueInstance->nouhin_post1 ); ?>" size="3"/> -
                        <input type="text" class="order_post form_small" maxlength="4" id="nouhin_post2"
                               name="nouhin_post2" value="<?php echo h( $valueInstance->nouhin_post2 ); ?>" size="4"/>
                    </p>
                    <select class="mb_ssss" id="nouhin_pref" name="nouhin_pref">
                        <?php foreach ( Config::get( 'prefectures' ) as $key => $value ): ?>
                            <option
                                value="<?php echo $key ?>" <?php echo $valueInstance->nouhin_pref == $key ? 'selected' : null ?>><?php echo $value ?></option>
                        <?php endforeach; ?>
                        ?>
                    </select><br/>
                    <input type="text" class="input_001" id="nouhin_addr" name="nouhin_addr"
                           value="<?php echo h( $valueInstance->nouhin_addr ); ?>"
                           placeholder="例：千代田区丸の内1-3-1 東京銀行協会ビル10階"/>
                    <?php if ( Order::getSession( 'errmsg.nouhin_post1' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.nouhin_post1' ) );
                    } ?>
                    <?php if ( Order::getSession( 'errmsg.nouhin_post2' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.nouhin_post2' ) );
                    } ?>
                    <?php if ( Order::getSession( 'errmsg.nouhin_pref' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.nouhin_pref' ) );
                    } ?>
                    <?php if ( Order::getSession( 'errmsg.nouhin_addr' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.nouhin_addr' ) );
                    } ?>
                </dd>
            </dl>
            <dl class="cf">
                <dt>電話番号</dt>
                <dd>
                    <input type="text" class="input_001" id="nouhin_tel" name="nouhin_tel"
                           value="<?php echo h( $valueInstance->nouhin_tel ); ?>" placeholder="例：0300000000"/>
                    <?php if ( Order::getSession( 'errmsg.nouhin_tel' ) ) {
                        printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.nouhin_tel' ) );
                    } ?>
                </dd>
            </dl>
            <div style="line-height: 1.5; text-align: center; padding-bottom: 20px; padding-top: 50px; ">
                <p><a href="http://tb-m.com/privacy/" target="_blank">個人情報の取得・利用について</a>をご確認いただき、同意するにチェックをつけてください。</p>
            </div>
            <div class="consent bottom_text">
                <label><input type="radio" name="agree"
                              value="1" <?php echo $valueInstance->agree == 1 ? 'checked="checked"' : null ?>/>&nbsp;同意する</label>
                <label><input type="radio" name="agree"
                              value="2" <?php echo $valueInstance->agree == 2 ? 'checked="checked"' : null ?>/>&nbsp;同意しない</label>
                <?php if ( Order::getSession( 'errmsg.agree' ) ) {
                    printf( '<div class="errmsg">%s</div>', Order::getSession( 'errmsg.agree' ) );
                } ?>
            </div>

            <div style="text-align:left ;">
                <div class="consent bottom_text">
                    <input type="hidden" name="related_news" value="2">
                    <label><input type="checkbox" name="related_news" id=""
                                  value="1" <?php echo $valueInstance->related_news != 2 ? 'checked="checked"' : null ?>/>&nbsp;LIMEXに関するニュースとお知らせ</label>
                    <div class="caution_text">※LIMEXに関するニュース、最新のアプリケーション情報をEメールで受け取ることができます。</div>
                    </label>
                </div>
                <div class="consent bottom_text">
                    <input type="hidden" name="pr_agree" value="2">
                    <label><input type="checkbox" name="pr_agree" id=""
                                  value="1" <?php echo $valueInstance->pr_agree === '' || $valueInstance->pr_agree == 1 ? 'checked="checked"' : null ?>/>&nbsp;名刺発注システムの導入を検討したい<br>
                        <div class="caution_text">従業員数30名以上(目安)対象の名刺発注業務の負荷を低減して頂けるシステムです。</div>
                    </label>
                </div>
            </div>
            <p class="btn_black">
                <input type="submit" name="btn_confirm" value="確認画面へ進む" id="btn_submit"/>
            </p>
        </form>
    </div>

    <!--キングソフトプロモーションコード自動入力-->    
    <div style="visibility:hidden;" id="param"></div> 


    <!-- モーダル埋め込み画像 -->
    <div style="display: none;">
        <div id="js-meishi_design_popup" class="order_modal">
            <div class="inner">
                <p class="title">- 名刺サンプル -</p>
                <p><img src="img/img-modal-01.png" alt=""></p>
                <p class="pt80"><img src="img/img-modal-02.png" alt=""></p>
            </div>
        </div>
    </div>
</section><!-- /#body -->
<script type="text/javascript">

    var tmp;

    var orderDetailPrint = function () {
        for (var key = 1; key <= 6; key++) {
            var str = '';
            var color_id = '#meishi_' + key;
            // get pattern
            var $pattern = $(color_id).find('input[name^=meishi_buy_pattern]:checked');

            if ($pattern.length > 0) {
                var pattern_text = $($pattern.parents('tr').find('td').get(1)).text();
                str += pattern_text + ' ';
            }

            $(color_id).find('.order_detail_desc').text(str);
        }
    };

    $(document).ready(function () {
        orderDetailPrint();
        $('.meishi_color_div').hide();
        $('.order_detail_desc').addClass('open');
        $('input[name^=meishi_buy_pattern]:checked').each(function () {
            if ($(this).val() == "6") {
                $(this).trigger('click');
            }

        });

        $('.color_num_list:checked').each(function () {
            // open accordion
            var color_id = "#meishi_" + $(this).val();
            $(color_id).slideDown('fast').addClass('open');
        });

        $('.color_num_list').on('click', function () {
            var color_id = "#meishi_" + $(this).val();

            if ($(this).is(':checked')) {
                $(color_id).slideDown('fast').addClass('open');
            } else {
                $(color_id).slideUp('fast').removeClass('open');
            }

            orderDetailPrint();
        });

        $('.meishi_color_div .head').on('click', function () {
            if ($(this).children('.order_detail_desc').hasClass('open')) {
                $(this).children('.order_detail_desc').removeClass('open');
                $(this).siblings('.cf').slideUp('fast');
            } else {
                $(this).children('.order_detail_desc').addClass('open');
                $(this).siblings('.cf').each(function() {
                    if ($(this).hasClass('card-template')) {
                        cardTemplateShowCheck($(this));
                    } else {
                        $(this).slideDown('fast');
                    }
                });
            }

        });

        $('#form').on('change', function () {
            orderDetailPrint();
        });
    });

    var cardTemplateShowCheck = function ($cardTemplate) {
        var colorNum = $cardTemplate.parent().data('color');
        var buyPatternNum = $('#meishi_' + colorNum + ' input[name^=meishi_buy_pattern]:checked').val();

        console.log(colorNum);
        console.log(buyPatternNum);

        if (buyPatternNum == "6") {
            $cardTemplate.slideDown('fast');
        } else {
            $cardTemplate.hide();
        }
    };

    // 名刺データの操作
    var aiNumClass = function () {
        // 持っているならtrue
        this.hasCheck = function () {
            var count = false;
            $.each($(".js-meishi_ai"), function (i, v) {
                if ($(v).val() == 1 && $(v).prop('checked')) {
                    count = true;
                }
            });
            if (count == true) {
                return true;
            }
        },
            this.uploadManager = function () {
                var $meishiUpload = $('.meishi-upload');
                if (this.hasCheck()) {
                    $('.meishi-upload').show();
                } else {
                    $('.meishi-upload').hide();
                }
            }
    };

    // 法人、個人の判別
    var useNumClass = function () {
        // 法人ならtrue
        this.hasCheck = function () {
            var count = false;
            $.each($(".meishi_use_list"), function (i, v) {
                if ($(v).val() == 2 && $(v).prop('checked')) {
                    count = true;
                }
            });
            if (count == true) {
                return true;
            }
        },
            this.cautionControl = function () {
                if (this.hasCheck()) {
                    $('.company_caution').show();
                    $('.employees_caution').show();
                } else {
                    $('.company_caution').hide();
                    $('.employees_caution').hide();
                }
            }
    };

    // 色数の操作
    var colorNumClass = function () {
        // チェックされているか
        this.hasCheck = function () {
            var count = false;
            $.each($(".color_num_list"), function (i, v) {
                if ($(v).prop('checked')) {
                    count = true;
                }
            });
            if (count == true) {
                return true;
            }
        },
            this.getCheckedValues = function () {
                var values = [];
                $.each($(".color_num_list"), function (i, v) {
                    if ($(v).prop('checked')) {
                        values.push($(v).val());
                    } else {
                        values = values.filter(function (item) {
                            return item !== v
                        });
                    }
                });
                $.unique(values);
                return values;
            }
    };

    // プロモーションコードの操作
    var promoCodeClass = function () {
        // チェックされているか
        this.hasCode = function () {
            if ($("input[name='meishi_promo']").val()) {
                return true;
            }
        }
    };

    // ファイルアップロード
    var fileUpload = function () {
        this.postAjaxFile = function () {
            var fd = new FormData();
            if ($("input[name='meishi_tmp_file']").val() !== '') {
                fd.append("file", $("input[name='meishi_tmp_file']").prop("files")[0]);
            }
            var postData = {
                type: "POST",
                dataType: "json",
                data: fd,
                processData: false,
                contentType: false
            };
            $.ajax(
                "<?php echo HOST ?>/order/ajax/upload", postData
            ).done(function (json) {
                $("#filedeletebutton").show();
                $("input[name='meishi_file']").val(json.origineFileName);
                $("input[name='meishi_file_unique']").val(json.deployFileName);
            });
        },
            this.deleteButton = function () {
                $("#filedeletebutton").hide();
                $("input[name='meishi_file']").val('');
                $("input[name='meishi_file_unique']").val('');
                $("input[name='meishi_tmp_file']").val('');
                $("input[name='meishi_tmp_file']").show();
            }
    }

    function syncColorNumAndPromoCode() {

        if($('.color_num_list:checked').length <= 0) {
            $('#promotioninput').append("<dt class=\"promotionerror\" style=\"color:red;\">色数を選んで下さい。</dt>");
            $('#meishi_promo').css("border", "1px solid #f00");
            return;
        } else {
            $('.promotionerror').remove();
            $('#meishi_promo').css("border", "none");
        }

        /* プロモ単価なし対応 ブロック表示 */
        var promotion = $('#meishi_promo').val();
        if (promotion == '130012') {
            $('.js-promo-code-price').hide();
        } else {
            $('.js-promo-code-price').show();
        }

        if (($(colorNum.getCheckedValues()).length === 0) || ($("#meishi_promo").val() === '')) {
            $('.js-promo-code-price').hide();
            $('.priceinput').each(function () {
                $(this).val("");
            });

            if ($(colorNum.getCheckedValues()).length === 0) {
                $('#promotioninput').append("<dt class=\"promotionerror\" style=\"color:red;\">色数を選んで下さい。</dt>");
                $('#meishi_promo').css("border", "1px solid #f00");
            }
        } else {
            $('.promotionerror').remove();
            $('#meishi_promo').css("border", "none");
        }

        // 一回消す
        $.each($('.js-promo-code-price-row'), function (_i, priceRow) {
            $(priceRow).hide();
        });

        // 選択されているリストを参照して表示
        $.each($(colorNum.getCheckedValues()), function (i, v) {
            $.each($('.js-promo-code-price-row'), function (_i, priceRow) {
                if ($(priceRow).data('id') == v) {
                    $(this).show();
                }
            });
        });
    }

    function knowPatternManager() {
        var patternValue = $("[name='meishi_know_pattern'] option:selected").val();
        $(".js-noushin-input-area input").val('');
        $(".js-noushin-input-area div").hide();
        if (patternValue) {
            var className = '.js-noushin-' + patternValue;
            $(className).show();
        }
    }
    function checkPromotion() {
        var promotion = $('#meishi_promo').val();
        if (promotion !== '') {
            $.ajax({
                method: "POST",
                url: "<?php echo $promotion_check_endpoint; ?>",
                data: {promotion: promotion}
            }).done(function (data) {
                if (data.exists === false) {
                    $('.promotionerror').remove();
                    $('.js-promo-code-price').hide();
                    $('.priceinput').each(function () {
                        $(this).val("");
                    });
                    // invalid promo code - red highlight textbox & warning message
                    $('#promotioninput').append("<dt class=\"promotionerror\" style=\"color:red;\">指定されたプロモーションコードは存在しません。</dt>");
                    $('#meishi_promo').css("border", "1px solid #f00");
                } else {
                    // valid promo code, show color settings
                    $('.promotionerror').remove();
                    $('#meishi_promo').css("border", "none");
                    syncColorNumAndPromoCode();
                }
            });
        } else {
            $('.js-promo-code-price').hide();
            $('.priceinput').each(function () {
                $(this).val("");
            });
            $('.promotionerror').remove();
            $('#meishi_promo').css("border", "none");
        }
    }

    function setCount() {
        var orders = $('#meishi_orders').val();
        var perperson = $('#meishi_per_person_set').val();
        var total = orders * perperson * 100;
        var settotal = orders * perperson;

        $('#setcount').text(settotal + "セット (" + total + "枚)");

        if (total == 0 || perperson == 12) {
            $('#setcount').text("-");
        }
        if (perperson == 11) {
            $('#setcount').text(orders * (perperson - 1) + "〜セット");
        }
    }

    function setDesignPattern(color_num) {
        var choice = $('#meishi_' + color_num + ' input[type=radio]:checked').val();
        var $card = $('#meishi_' + color_num + ' .card-template');

        if (choice === '6') {
            if (!$('#meishi_card_type_' + color_num).val()) {
                $('#meishi_card_type_' + color_num).val("TYPE01_G");
                $card.show(function () {
                    if (!$('#meishi_' + color_num + ' .bx-wrapper').length) {
                        $('#meishi_' + color_num + ' .card-slider').bxSlider({
                            touchEnabled: true
                        });
                    }
                });
                // has previous form pattern input
            } else {
                var selectedimg = $('#meishi_' + color_num + ' img[alt=' + $('#meishi_card_type_' + color_num).val() + ']');
                // hide current selected overlay
                $card.find('.selected').hide();
                // selected overlay over image
                selectedimg.parent().children('.selected').show();

                var slideid = selectedimg.closest('li').prop('id');
                var startpos = slideid.substring("slide".length);
                $card.show(function () {
                    if (!$('#meishi_' + color_num + ' .bx-wrapper').length) {
                        $('#meishi_' + color_num + ' .card-slider').bxSlider({
                            touchEnabled: true,
                            startSlide: startpos
                        });
                    }
                });

                //$('#designpatternlist :radio:not(:checked)').attr('disabled', true);
            }
        } else {
            if ($card.is(':visible')) {
                $card.hide();
            }
            $('#meishi_card_type_' + color_num).val("");
            //$('#designpatternlist :radio:not(:checked)').attr('disabled', false);
        }
    }

    function prmPriceClear() {
        $('.js-promo-code-price-row input').val("");
    }

    var checkUramen = function () {
        var $input = $('.color_num_list');
        $.each($input, function (i, elm) {
            if (elm.checked == true) {
                elm.click();
                return true;
            }
        });
    };


    $(function () {
        useNum = new useNumClass();
        colorNum = new colorNumClass();
        promoCode = new promoCodeClass();
        aiNum = new aiNumClass();
        fileUpload = new fileUpload();

        // 法人用を選択した場合
        if (useNum.hasCheck()) {
            $('.company_caution').show();
            $('.employees_caution').show();
        }

        // 色数にチェックがあり、プロモーションコードがああれば
        if (colorNum.hasCheck() && promoCode.hasCode()) {
            $('.js-promo-code-price').show();
            syncColorNumAndPromoCode();
        }

        // 名刺データを持っていた場合
        if (aiNum.hasCheck()) {
            // ファイルアップロードを表示させる処理
            $('.meishi-upload').show();
        }

        setCount();
        setDesignPattern(1);
        setDesignPattern(2);
        setDesignPattern(3);
        setDesignPattern(4);
        setDesignPattern(5);
//        checkPromotion();
//        checkUramen();

        $('.color_num_list').on('change', function () {
            var color_num = $(this).val();
            if ($(this).is(':checked')) {

                $('#color_num_' + color_num).val(color_num);
            } else {
                $('#color_num_' + color_num).val('');
            }
        });
    });

    var LIMEX = LIMEX || {};
    LIMEX.globalNav = (function ($) {
        var buttonMenu;
        var navGlobal;
        var _init = function () {
            buttonMenu = $("#button-menu");
            navGlobal = $("#nav-global");
            navGlobal.find(".bg").on("click", function () {
                close();
            })
            $("body").css({
                minHeight: $(window).height()
            });
            navGlobal.css({
                minHeight: $(window).height() - 50
            });
            buttonMenu.on("click", function () {
                if (buttonMenu.hasClass('active')) {
                    close();
                    if ($("div.story").css("display") == "block") {
                        $("div.story").fadeOut(200);
                    }
                } else {
                    open();
                }
            })
            navGlobal.find("li:has('li')").children('a')
                .addClass('close')
                .on("click", function () {
                    $(this).next().slideToggle(300, function () {
                        setBodyHeight();
                    });
                    $(this).toggleClass('open');
                    return false;
                })


            $("#key").on("click", ".show-story", function () {
                $("div.story").fadeIn(200);
                buttonMenu.addClass('active');
                $("body").css({
                    height: $("div.story").height() + 100,
                    overflowY: "hidden"
                })
                setBodyHeight();
                return false;
            })
        }
        var setBodyHeight = function () {
            $("body").css({
                //height: navGlobal.height() + 100,
                height: navGlobal.height() + 150,
                overflowY: "hidden"
            })
        }
        var open = function () {
            buttonMenu.addClass('active');
            navGlobal.fadeIn('200');
            setBodyHeight();
        }
        var close = function () {
            buttonMenu.removeClass('active');
            navGlobal.fadeOut('200');
            $("body").css({
                height: "auto",
                overflowY: ""
            })
        }
        var _self = {
            init: _init
        }
        return _self;
    }(jQuery));

    $(LIMEX.globalNav.init);

</script>
<?php get_footer(); ?>
