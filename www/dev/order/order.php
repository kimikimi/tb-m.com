<?php
/**
 * お問い合わせ送信処理ページ
 * 中間ファイルの為、htmkはありません。
 *
 * @package    limex
 * @subpackage order
 * @version    2015/10/15 1.0
 * @author     yutaka.sudo
 */

// お問い合わせクラスロード
require_once __DIR__ . "/include/Order.class.php";

// 会員情報も渡す
$order = new Order();
// 注文番号
$order->orderId = $order->createOrderId();

// 事前チェック
if (empty($_POST) || !$order->issetData() || Order::getSession('token') !== $_POST['token']) {
    Order::doSystemErr();
}

// トークン削除
unset($_SESSION[Order::$sessPagePrefix]['token']);


// メール変数
$mail_info = mb_encode_mimeheader(Order::SUPPORT_MAIL_FROM, 'ISO-2022-JP-MS') . ' <' . Order::USER_MAIL_FROM . '>';
$headers = implode("\n", array(
        'Content-Type: text/plain; charset=ISO-2022-JP',
        'From: ' . $mail_info,
    ));

// 管理送信
include(__DIR__ . '/include/mail_template_admin.php');    // 件名、本文
$admin_result =  mail(
    Order::SUPPORT_MAIL_TO,
    mb_encode_mimeheader($admin_subject, 'ISO-2022-JP-MS'),
    mb_convert_encoding($admin_body, 'ISO-2022-JP-MS'),
    $headers,
    '-f ' . Order::USER_MAIL_FROM
);

// ユーザー送信
include(__DIR__ . '/include/mail_template_client.php');    // 件名、本文
$user_result =  mail(
    $order->getData('meishi_email'),
    mb_encode_mimeheader($user_subject, 'ISO-2022-JP-MS'),
    mb_convert_encoding($user_body, 'ISO-2022-JP-MS'),
    $headers,
    '-f ' . Order::USER_MAIL_FROM
);

// エラー時
if (! $admin_result || ! $user_result)
{
    Order::setSession('errmsg', array('system' => Order::SYSTEM_ERR_MSG_01 . '(2)'));
    Order::location(Order::ORDER_DIR . Order::ORDER_TOP . '?status=err', true);
}

// 成功時
Order::deleteSession();
Order::location(Order::ORDER_DIR . '/complete.php?order_id=' . $order->getOrderId());