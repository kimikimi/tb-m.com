<?php
$IS_DEV = getenv('IS_DEV');

if (($_SERVER['SERVER_PORT']) == 80 && !$IS_DEV) {
    header("Location: https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
    exit;
}
?><?php
// WordPres用
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
require_once $_SERVER['DOCUMENT_ROOT']. '/wp-blog-header.php';
if(wp_is_mobile()){
    include('./sp_explain.php');
    exit();
}
?>
<?php get_header(); ?>

<script>
    $('body').addClass('explain');
    $('body').removeClass('explain.php');



</script>
<style type="text/css">
.contact_text{
text-align:center;
margin-top:25px;
font-size:18px;
line-height:1.8;
}
.bx-wrapper .img_pointer img{
cursor: default;
}

.bx-wrapper .bx-viewport {
border:none !important;
}
</style>
<link type="text/css" rel="stylesheet" href="/order/css/style.css" />
<script type="text/javascript" src="/order/js/order.js"></script>
<section id="body">
    <!-- <div class="pageTtl">
        <h2>ORDER</h2>
    </div> -->

     <div class="wrapper">
        <div class="keyv">
            <p class="img"></p>
            <h3></h3>
            <p class="des"></p>
        </div>

        <div class="about">
            <ul class="column_2 des">
                <li>
                     <p class="img"><img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/order/img/explain-img01.png" alt=""></p>
                </li>
                <li class="text">

                </li>
            </ul>
        </div>



         <p class="btn_black">
            <a href="/order">ご注文・お見積</a>
        </p>
        <div class="contact_text">
            <div>ご質問や不明な点がある方は、お電話にてお問い合わせ下さい</div>
            <div>TEL&nbsp;:&nbsp;03-3538-6777</div>
        </div>

        <div class="price contents">
            <h3>料金</h3>
            <p class="title pt0">直販価格(税抜)</p>
            <table class="sheets">
                <tr class="head">
                    <td></td>
                    <td>片面</td>
                    <td>片面</td>
                    <td>両面</td>
                    <td>両面</td>
                    <td>両面</td>
                </tr>
                <tr>
                    <th>ご注文枚数</th>
                    <td>カラー</td>
                    <td>黒</td>
                    <td>カラー/カラー</td>
                    <td>カラー/黒</td>
                    <td>黒/黒</td>
                </tr>
                <tr>
                    <th>100枚</th>
                    <td>¥1,750</td>
                    <td>¥1,250</td>
                    <td>¥2,500</td>
                    <td>¥2,250</td>
                    <td>¥1,500</td>
                </tr>
                <tr>
                    <th>200枚<span>※</span></th>
                    <td>¥3,360</td>
                    <td>¥2,400</td>
                    <td>¥4,800</td>
                    <td>¥4,320</td>
                    <td>¥2,880</td>
                </tr>
                <tr>
                    <th>300枚<span>※</span></th>
                    <td>¥4,830</td>
                    <td>¥3,450</td>
                    <td>¥6,900</td>
                    <td>¥6,210</td>
                    <td>¥4,140</td>
                </tr>
            </table>
            <p class="note"><span>※</span>当該料金は、同一データを複数セット(200枚以上)ご注文いただく場合の料金となります。400枚以上ご注文の場合、単価は300枚ご注文頂く場合の単価と同一となります。</p>
            <p class="title">送料</p>
            <table>
                <tr>
                    <td>¥500</td>
                </tr>
            </table>
            <p class="note"><span>※</span>一部の地域は異なります</p>


            <p class="title">オプション料金(税抜)</p>
            <table class="option">
                <tr>
                  <th colspan="2">サイズ</th>
                </tr>
                <tr>
                   <td class="head">イレギュラーサイズ対応料</td>
                   <td>+¥250～<br>
                   ※一般的な名刺サイズ(91mm×55mm)以外の場合</td>
                </tr>
                <tr>
                  <th colspan="2">デザイン</th>
                </tr>
                <tr>
                   <td class="head">ゼロベースから新規デザインをご希望の場合</td>
                   <td>要相談</td>
                </tr>
                <tr>
                   <td class="head">大体のデザインをご自身で制作し、アップロードする場合<br>(Excel・PowerPoint・Word等)</td>
                   <td>+¥10,000~¥20,000</td>
                </tr>
                <tr>
                   <td class="head">ご希望のデザインに近い名刺のスキャンデータ等、明確なイメージをアップロードする場合</td>
                   <td>片面につき+¥3,000～</td>
                </tr>
                <tr>
                   <td class="head">入稿データがないため、ご希望の名刺デザインのスキャンデータ(及びロゴデータ)等をアップロードする場合</td>
                   <td>片面につき＋¥2,000～<br>＊トレース(データ起こし)料</td>
                </tr>
                <tr>
                   <td class="head">入稿データをアップロードする場合<br>(イラストレーター・Excel・PowerPoint・Word 等)</td>
                   <td>+¥0<br>＊データによってはトレース(データ起こし)料が発生する場合があります</td>
                </tr>
                <tr>
                   <td class="head">LIMEX名刺デザインテンプレートから選択する場合</td>
                   <td>+¥0</td>
                </tr>
                <tr>
                  <th colspan="2">加工</th>
                </tr>
                <tr>
    							 <td class="head">加工をご希望の場合<br>（角丸、箔押し等）</td>
    							 <td>応相談<br>※別途見積もりとなります</td>
    						</tr>
            </table>

            <p class="title">LIMEX名刺デザインテンプレート</p>

            <div class="explainslider">
                <dl class="cf card-template" style="display:block; text-align:center;">
                    <dd>
                        <ul class="card-slider bxslider img_pointer">
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/gosick/TYPE01_G-001.png" alt="TYPE01_G">
                                    </div>
                                    <p>TYPE1 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/mintyou/TYPE01_M-001.png" alt="TYPE01_M">
                                    </div>
                                    <p>TYPE1 明朝体</p>
                                </div>
                            </li>
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/gosick/TYPE02_G-001.png" alt="TYPE02_G">
                                    </div>
                                    <p>TYPE2 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/mintyou/TYPE02_M-001.png" alt="TYPE02_M">
                                    </div>
                                    <p>TYPE2 明朝体</p>
                                </div>
                            </li>
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/gosick/TYPE03_G-001.png" alt="TYPE03_G">
                                    </div>
                                    <p>TYPE3 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/mintyou/TYPE03_M-001.png" alt="TYPE03_M">
                                    </div>
                                    <p>TYPE3 明朝体</p>
                                </div>
                            </li>
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/gosick/TYPE04_G-001.png" alt="TYPE04_G">
                                    </div>
                                    <p>TYPE4 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/mintyou/TYPE04_M-001.png" alt="TYPE04_M">
                                    </div>
                                    <p>TYPE4 明朝体</p>
                                </div>
                            </li>
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/gosick/TYPE05_G-001.png" alt="TYPE05_G">
                                    </div>
                                    <p>TYPE5 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/mintyou/TYPE05_M-001.png" alt="TYPE05_M">
                                    </div>
                                    <p>TYPE5 明朝体</p>
                                </div>
                            </li>
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/gosick/TYPE06_G-001.png" alt="TYPE06_G">
                                    </div>
                                    <p>TYPE6 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img class="small" src="../order/img/mintyou/TYPE06_M-001.png" alt="TYPE06_M">
                                    </div>
                                    <p>TYPE6 明朝体</p>
                                </div>
                            </li>
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img src="../order/img/gosick/TYPE07_G-001.png" alt="TYPE07_G">
                                    </div>
                                    <p>TYPE7 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img src="../order/img/mintyou/TYPE07_M-001.png" alt="TYPE07_M">
                                    </div>
                                    <p>TYPE7 明朝体</p>
                                </div>
                            </li>
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img src="../order/img/gosick/TYPE08_G-001.png" alt="TYPE08_G">
                                    </div>
                                    <p>TYPE8 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img src="../order/img/mintyou/TYPE08_M-001.png" alt="TYPE08_M">
                                    </div>
                                    <p>TYPE8 明朝体</p>
                                </div>
                            </li>
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img src="../order/img/gosick/TYPE09_G-001.png" alt="TYPE09_G">
                                    </div>
                                    <p>TYPE9 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img src="../order/img/mintyou/TYPE09_M-001.png" alt="TYPE09_M">
                                    </div>
                                    <p>TYPE9 明朝体</p>
                                </div>
                            </li>
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img src="../order/img/gosick/TYPE10_G-001.png" alt="TYPE10_G">
                                    </div>
                                    <p>TYPE10 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img src="../order/img/mintyou/TYPE10_M-001.png" alt="TYPE10_M">
                                    </div>
                                    <p>TYPE10 明朝体</p>
                                </div>
                            </li>
                            <li>
                                <div class="gosick">
                                    <div class="templateimage">
                                        <img src="../order/img/gosick/TYPE11_G-001.png" alt="TYPE11_G">
                                    </div>
                                    <p>TYPE11 ゴシック体</p>
                                </div>
                                <div class="mintyou">
                                    <div class="templateimage">
                                        <img src="../order/img/mintyou/TYPE11_M-001.png" alt="TYPE11_M">
                                    </div>
                                    <p>TYPE11 明朝体</p>
                                </div>
                            </li>
                        </ul>
                    </dd>
                </dl>
            </div>
            <p class="title">納品スケジュール</p>
            <div class="schedule">
                <ul class="disc">
                    <li>データ入稿日(※16時までの入稿)から3営業日後にご納品。</li>
                    <li>弊社にてデータ制作が必要なご注文については、内容により制作期間が異なりますので、お早めにお間い合わせください。</li>
                    <li>通常、土日祝日、年未年始を除く平日納品となりますが、ご希望により土曜日のお届けは可能です。</li>
                    <li>加工名刺などは、仕様により納期が異なります。</li>
                    <li>天災など予期せぬ事故によって、納期遅延が生じる場合があります。</li>
                </ul>
            </div>
        </div>
        <p class="btn_black">
            <a href="/order">ご注文・お見積</a>
        </p>
        <div class="contact_text">
            <div>ご質問や不明な点がある方は、お電話にてお問い合わせ下さい</div>
            <div>TEL&nbsp;:&nbsp;03-3538-6777</div>
        </div>


        <div class="contents points">
            <h3 class="title03">注意事項</h3>

            <ul>
                <li>
                    <p class="title">【お支払について】</p>
                    <p class="text">基本的に初回ご注文のお客様には事前の銀行お振り込みをお願いしています。<br />詳細についてはお申し込み後、メールにてご案内させていただきます。</p>
                </li>
                <li>
                    <p class="title">【印刷の仕上がりについて】</p>
                    <p class="text">
                        他業者様にて印刷されていた名刺と、仕上がりに若干違いが出る場合があります。<br />
                        特に広い塗り面積(ベタ色)・グラデーション等のデザインでは、色の仕上がりに差異が生じる場合があります。<br />
                        断裁の際、若干のズレが生じる場合があり、2ｍｍ以内の断裁ズレについては良品扱いとなります。<br />
                        その為、枠や左右対称になるデザインはズレが非常に目立ち易くなりますので、避けていただきますようお願いします。
                    </p>
                </li>
                <li>
                    <p class="title">【著作権、肖像権について】</p>
                    <p class="text">
                        著作権のあるもの（ロゴマーク、著名人やアニメキャラクター）は、権利元の許可がなければ製作いたしかねます。<br />
                        お客様より支給頂いた画像等は、権利処理がされたとみなしてお取り扱いいたします。<br />
                        賠償責任などの問題が発生した場合、弊社では責任を負いかねますので、十分ご注意の上支給いただきますようお願いたします。
                    </p>
                </li>
                <li>
                    <p class="title">【キャンセル、返品について】</p>
                    <p class="text">
                        入稿手配後、速やかに印刷・加工作業を行う為、入稿当日１６:００以降のキャンセルはお受けできません。内容をよくご確認の上、<br />ご注文をお願い致します。<br />
                        万一不良品が含まれていた場合は、商品到着後14日以内にご連絡をお願いします。<br />
                        商品の交換については未使用の場合の対応いたしますので、料金着払いにてご返送ください。<br />
                        いかなる場合におきましてもご注文代金を超える補償は弊社でお受けできません。あらかじめご了承ください。<br />
                        お客様からいただいた入稿データ、および校了後の不備等により発生した不良品につきましては弊社は責任を負いかねます。<br />
                        また運送会社による商品破損などの場合、弊社は免責とさせていただきます。
                    </p>
                </li>
            </ul>
        </div>

        <p class="btn_black">
            <a href="/order">ご注文・お見積</a>
        </p>
        <div class="contact_text">
            <div>ご質問や不明な点がある方は、お電話にてお問い合わせ下さい</div>
            <div>TEL&nbsp;:&nbsp;03-3538-6777</div>
        </div>
    </div>
</section><!-- /#body -->

<script>
$(document).ready(function(){
    $('.bxslider').bxSlider({
        slideWidth: 2000,
    });
});
</script>

<?php
get_footer();
