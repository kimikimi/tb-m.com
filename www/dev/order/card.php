<?php
// WordPres用
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
require_once $_SERVER['DOCUMENT_ROOT']. '/wp-blog-header.php';

?>
<?php get_header(); ?>

<script>
    $('body').addClass('explain');
    $('body').removeClass('explain.php');
</script>
<style type="text/css">
.contact_text{
text-align:center;
margin-top:25px;
font-size:18px;
line-height:1.8;
}
</style>
<link type="text/css" rel="stylesheet" href="./css/style.css" />
<script type="text/javascript" src="./js/order.js"></script>
<section id="body">
    <!-- <div class="pageTtl">
        <h2>ORDER</h2>
    </div> -->
     <div class="wrapper">
        <div class="keyv">
            <p class="img"></p>
            <h3></h3>
            <p class="des"></p>
        </div>

        <div class="about">
            <ul class="column_2 des">
                <li>
                     <p class="img"><img src="img/explain-img01.png" alt=""></p>
                </li>
                <li class="text">
                   
                </li>
            </ul>
        </div>

        

         <p class="btn_black">
            <a href="/order">ご注文・お見積</a>
        </p>
        <div class="contact_text">
            <div>ご質問や不明な点がある方は、お電話にてお問い合わせ下さい</div>
            <div>TEL&nbsp;:&nbsp;03-6212-7272</div>
        </div>

        <div class="price contents">
            <h3>料金</h3>
            <p class="title pt0">直販価格(税抜)</p>
            <table class="sheets">
                <tr class="head">
                    <td></td>
                    <td>片面</td>
                    <td>片面</td>
                    <td>両面</td>
                    <td>両面</td>
                    <td>両面</td>
                </tr>
                <tr>
                    <th>ご注文枚数</th>
                    <td>カラー</td>
                    <td>黒</td>
                    <td>カラー/カラー</td>
                    <td>カラー/黒</td>
                    <td>黒/黒</td>
                </tr>
                <tr>
                    <th>100枚</th>
                    <td>¥1,750</td>
                    <td>¥1,250</td>
                    <td>¥2,500</td>
                    <td>¥2,250</td>
                    <td>¥1,500</td>
                </tr>
                <tr>
                    <th>200枚<span>※</span></th>
                    <td>¥3,360</td>
                    <td>¥2,400</td>
                    <td>¥4,800</td>
                    <td>¥4,320</td>
                    <td>¥2,880</td>
                </tr>
                <tr>
                    <th>300枚<span>※</span></th>
                    <td>¥4,830</td>
                    <td>¥3,450</td>
                    <td>¥6,900</td>
                    <td>¥6,210</td>
                    <td>¥4,140</td>
                </tr>
            </table>
            <p class="note"><span>※</span>当該料金は、同一データを複数セット(200枚以上)ご注文いただく場合の料金となります。</p>

            <p class="title">オプション料金(税抜)</p>
            <table class="option">
                <tr>
                    <th>トレース料(片面)</th>
                    <td>¥2,000〜</td>
                    <th>イレギュラーサイズ対応料</th>
                    <td>¥250<br>＊サイズ&nbsp;91mm×55mm以下の場合</td>
                </tr>
                <tr>
                    <th>新規デザイン料</th>
                    <td>別途</td>
                    <th>送料</th>
                    <td>配達地域、郵送方法により異なる</td>
                </tr>
            </table>

            <p class="note">
                <span>※</span>1セット(名刺100枚)からご注文を承ります。以降も1セット（100枚）単位でのご注文となります。<br>
                <span>※</span>送料につきましては、納品先のご指定をいただいた後、ご連絡いたします。<br>
            </p>

            <p class="title">納品スケジュール</p>
            <div class="schedule">
                <ul class="disc">
                    <li>データ制作がある場合は内容により要する時間が異なりますので、お早めにご依頼頂ければと思います。</li>
                    <li>ご依頼をいただいた時点でご案内させていただきますが、データ作成に最低1~2日間は必要となりますので、ご了承ください。</li>
                    <li>土・日・祝日は、定休日の為、発送などの対応は出来ませんが、ご希望により土曜日のお届けは可能でございます。</li>
                    <li>加工名刺などの場合は、仕様により納期が異なります。</li>
                </ul>
            </div>
        </div>

        <p class="btn_black">
            <a href="/order">ご注文・お見積</a>
        </p>
        <div class="contact_text">
            <div>ご質問や不明な点がある方は、お電話にてお問い合わせ下さい</div>
            <div>TEL&nbsp;:&nbsp;03-6212-7272</div>
        </div>


        <div class="contents points">
            <h3 class="title03">注意事項</h3>

            <ul>
                <li>
                    <p class="title">【お支払について】</p>
                    <p class="text">基本的に事前の銀行お振り込みをお願いしております。<br />詳細についてはお申し込み後、メールにてご案内させていただきます。</p>
                </li>
                <li>
                    <p class="title">【印刷の仕上がりについて】</p>
                    <p class="text">
                        他業者様にて印刷されていたデータをご入稿いただいた場合、機械性能、紙の種類等により、仕上がりに若干違いが出る場合があります。<br />
                        特に広い塗り面積(ベタ色)・グラデーション等のデザインでは、色の仕上がりに誤差が生じる場合があります。<br />
                        断裁の際、若干のズレが生じる場合があり、2ｍｍ以内の断裁ズレについては良品扱いとなります。<br />
                        その為、枠や左右対称になるデザインはズレが非常に目立ち易くなりますので、避けていただきますようお願いします。
                    </p>
                </li>
                <li>
                    <p class="title">【著作権、肖像権について】</p>
                    <p class="text">
                        著作権のあるもの（ロゴマーク、タレントやアニメの画像等）は、権利元の許可がなければ製作できません。<br />
                        お客様より支給頂いた画像等は、権利処理がされたとみなして取り扱います。<br />
                        賠償責任などの問題が発生した場合、弊社では責任を負うことが出来ませんので、十分ご注意の上支給いただきますようお願いします。
                    </p>
                </li>
                <li>
                    <p class="title">【納期、配送について】</p>
                    <p class="text">
                        納品日については入稿手配後、納期算出の上、メールにてご連絡をさせていただきます。<br />
                        なお当日入稿の締め切り時間は１２:００まで(土日・祝日除く)となりますのでご考慮の上、ご連絡をお願い致します。<br />
                        また弊社を原因としない天災など予期せぬ事故によって、納期遅延が生じる場合があります。
                    </p>
                </li>
                <li>
                    <p class="title">【キャンセル、返品について】</p>
                    <p class="text">
                        入稿手配後、速やかに印刷・加工作業を行う為、入稿当日１６:００以降のキャンセルはお受けできませんので、注文内容をよくご確認の上、ご発注をお願い致します。<br />
                        弊社では発送前に厳重なチェック・検品の上、出荷いたしておりますが、商品がお手元に届きましたら、3日以内に商品の検品をお願い致します。<br />
                        万一不良品が含まれていた場合は、商品到着後14日以内にご連絡をお願いします。<br />
                        商品の交換については未使用の場合のみとし、料金着払いにてご返送をお願いします。<br />
                        弊社でいかなる場合におきましてもご注文代金を超える補償はお受けできません。あらかじめご了承ください。<br />
                        お客様から支給入稿データ、および校了後の不備等により発生いたしました不良品につきましては弊社は責任を負いかねます。<br />
                        また運送会社による商品破損などの場合、弊社は免責とさせていただきます。
                    </p>
                </li>
            </ul>
        </div>

        <p class="btn_black">
            <a href="/order">ご注文・お見積</a>
        </p>
        <div class="contact_text">
            <div>ご質問や不明な点がある方は、お電話にてお問い合わせ下さい</div>
            <div>TEL&nbsp;:&nbsp;03-6212-7272</div>
        </div>
    </div>
</section><!-- /#body -->
<?php 
get_footer();