<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$directory_name = get_directory_name();
$heder_js = get_head_js($directory_name);
$class_name = get_body_classname();

?><!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>LIMEX</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,user-scalable=no;">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="keywords" content="LIMEX,石灰石,リサイクル,エコノミー,エコロジー,新素材,ストーンペーパー,プラスチック,株式会社TBM">
	<meta name="description" content="株式会社TBMのLIMEXは、石灰石（炭酸カルシウム）を主原料に、紙やプラスチック商品の代替となり、エコロジーとエコノミーを実現する新素材です。">
		
	<meta property="og:title" content="次世代素材LIMEX 株式会社TBM" />
	<meta property="og:url" content="http://tb-m.com/" />
	<meta property="og:description" content="【株式会社TBM】LIMEXは、石灰石（炭酸カルシウム）を主原料に、紙やプラスチック商品の代替となり、エコロジーとエコノミーを実現する新素材です。" />
<?php if(preg_match('/order/', $_SERVER["REQUEST_URI"])):?>
		<meta property="og:image" content="http://tb-m.com/img/meisi_og.jpg" />
<?php else :?>
		<meta property="og:image" content="http://tb-m.com/img/ogp.jpg" />
<?php endif;?>	
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/reset.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/common.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/top.css">
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/vender.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
<script type="text/javascript">
$(function() {
	var topBtn = $('#pageTop');
	topBtn.hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
    topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 1000);
		return false;
    });
});

</script>
<style type="text/css">
#pageTop{
	size:11px;
	background:#A4A4A4;
	border-radius:5px;
	color:#FFF;
	padding:12px;
	position:fixed;
	bottom:5px;
	right:5px;
	text-decoration: none;
	}
#pageTop:hover{
/*	background:#EEE; */
	}
.fixed {
  position: fixed;
  width: 100%;
  height: 100%;
}
</style>
</head>
<body>
<body>
<header class="global">
	<h1><a href="/"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="LIMEX"></a></h1>

	<div class="button-menu toggle" id="button-menu">
		<div>
		<span></span>
		<span></span>
		<span></span>
		</div>
	</div>

	<nav class="nav-global menu" id="nav-global">
	<div class="bg"></div>
	<ul>
		<li><a href="/">TOP</a>
		<li><a href="/action/">LIMEX ACTION</a>
		<li><a href="/order/explain.php">名刺オーダー</a>
		<li><a href="http://recruit.tb-m.com/">RECRUIT</a>

		<li><a href="/about/">ABOUT</a>
			<ul>
				<li><a href="/about/">LIMEXとは</a></li>
				<li><a href="/about/paper/">LIMEXの紙</a></li>
				<li><a href="/about/plastic/">LIMEXのプラスチック</a></li>
				<li><a href="/about/future/">LIMEXの可能性</a></li>
			</ul>
		</li>
		<li><a href="/mission/">MISSION</a>
			<ul>
				<li><a href="/mission/">MISSION STORY</a></li>
				<li><a href="/mission/vision/">MISSION VISION</a></li>

			</ul>
		</li>

		<li><a href="/factory/">FACTORY</a>
			<ul>
				<li><a href="/factory/">白石蔵王</a></li>
				<li><a href="/factory/tagajo/">多賀城</a></li>
			</ul>
		</li>
		<li><a href="/company/">COMPANY</a>
			<ul>
				<li><a href="/company/">会社概要</a></li>
				<li><a href="/company/profile/">経営陣プロフィール</a></li>
				<li><a href="/company/identity/">企業理念体系</a></li>
				<li><a href="/company/history/">沿革</a></li>
				<li><a href="/company/message/">創設者メッセージ</a></li>
				<li><a href="/company/story/">創設ストーリー</a></li>
				<li><a href="/company/access/">アクセス</a></li>
				<li><a href="/company/award/">受賞歴</a></li>
			</ul>
		</li>
		<li><a href="/news/">NEWS</a>
			<ul>
				<li><a href="/news/?cat=press_release">プレスリリース</a></li>
				<!--<li><a href="#">LIMEXの紙</a></li>-->
				<li><a href="/news/?cat=topics">お知らせ</a></li>
				<li><a href="/news/?cat=media">メディア掲載</a></li>
			</ul>
		</li>
		<li><a href="/form/contact_lime-x/contact/">CONTACT</a>
	</ul>
	</nav>
</header>