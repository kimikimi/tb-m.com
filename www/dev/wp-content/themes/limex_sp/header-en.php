<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$directory_name = get_directory_name();
$heder_js = get_head_js($directory_name);
$class_name = get_body_classname();

?><!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>LIMEX</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,user-scalable=no;">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="keywords" content="LIMEX, limestone, recycle, economy, ecology, new material, stone paper, plastic TBM Co., Ltd">
	<meta name="description" content="LIMEX created by TBM Co. Ltd, is a new material made from limestone (calcium carbonate) serves to be a substitute of paper and plastic, realizing ecology and economy.">
		
	<meta property="og:title" content="a next generation material LIMEX, TBM Co., Ltd" />
	<meta property="og:url" content="http://tb-m.com/en/" />
	<meta property="og:description" content="LIMEX created by TBM Co. Ltd, is a new material made from limestone (calcium carbonate) serves to be a substitute of paper and plastic, realizing ecology and economy." />
<?php if(preg_match('/order/', $_SERVER["REQUEST_URI"])):?>
		<meta property="og:image" content="http://tb-m.com/img/meisi_og.jpg" />
<?php else :?>
		<meta property="og:image" content="http://tb-m.com/img/ogp.jpg" />
<?php endif;?>	
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/reset.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/common.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/top.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/shadowbox.css">
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/vender.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/shadowbox.js"></script>

<script type="text/javascript">
$(function() {
	var topBtn = $('#pageTop');
	topBtn.hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
    topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 1000);
		return false;
    });
});

</script>
<style type="text/css">
#pageTop{
	size:11px;
	background:#A4A4A4;
	border-radius:5px;
	color:#FFF;
	padding:12px;
	position:fixed;
	bottom:5px;
	right:5px;
	text-decoration: none;
	}
#pageTop:hover{
/*	background:#EEE; */
	}
.fixed {
  position: fixed;
  width: 100%;
  height: 100%;
}
#lang01 {
	width: 195px;
}
#lang02{
	position: absolute;
	text-align: center;
	width: 100%;
	margin-top: 20px;
}
#lang02 > a:nth-child(1) {
  margin-right: 10px;
}
</style>
</head>
<body>
<body>
<header class="global">
	<h1><a href="/en/"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="LIMEX"></a></h1>

	<div class="button-menu toggle" id="button-menu">
		<div>
		<span></span>
		<span></span>
		<span></span>
		</div>
	</div>

	<nav class="nav-global menu" id="nav-global">
	<div class="bg"></div>
	<ul>
		<li><a href="/en/">TOP</a>
		<!-- <li><a href="/order/en_explain.php">ORDER</a> -->
		<!-- <li><a href="http://recruit.tb-m.com/">RECRUIT</a> -->

		<li><a href="/en/about/">ABOUT</a>
			<ul>
				<li><a href="/en/about/">What is LIMEX</a></li>
				<li><a href="/en/about/paper/">LIMEX PAPER</a></li>
				<li><a href="/en/about/plastic/">LIMEX PLASTIC</a></li>
				<li><a href="/en/about/future/">LIMEX FUTURE</a></li>
			</ul>
		</li>
		<li><a href="/en/mission/">MISSION</a>
			<ul>
				<li><a href="/en/mission/">MISSION STORY</a></li>
				<li><a href="/en/mission/vision/">MISSION VISION</a></li>

			</ul>
		</li>

		<li><a href="/en/factory/">FACTORY</a>
			<ul>
				<li><a href="/en/factory/">SHIROISHI ZAO</a></li>
				<li><a href="/en/factory/tagajo/">TAGAJO</a></li>
			</ul>
		</li>
		<li><a href="/en/company/">COMPANY</a>
			<ul>
				<li><a href="/en/company/">ABOUT US</a></li>
				<li><a href="/en/company/profile/">PROFILE</a></li>
				<li><a href="/en/company/identity/">IDENTITY</a></li>
				<li><a href="/en/company/history/">HISTORY</a></li>
				<li><a href="/en/company/message/">MESSAGE</a></li>
				<li><a href="/en/company/story/story1/">STORY</a></li>
				<li><a href="/en/company/access/">ACCESS</a></li>
				<li><a href="/en/company/award/">AWARD</a></li>
			</ul>
		</li>
		<li><a href="/en/news/">NEWS</a>
			<ul>
				<li><a href="/en/news/?cat=press_release">PRESS RELEASE</a></li>
				<!--<li><a href="#">LIMEXの紙</a></li>-->
				<li><a href="/en/news/?cat=topics">TOPICS</a></li>
				<li><a href="/en/news/?cat=media">MEDIA</a></li>
			</ul>
		</li>
		<li><a href="mailto:infomail@tb-m.com">CONTACT</a>
	</ul>
	<div id="lang01">
		<div id="lang02">
			<?php if($_SERVER['REQUEST_URI']== '/order/en_explain.php'):?>
				<a href="/order/explain.php"><img src="<?php echo get_template_directory_uri(); ?>/images/lang_ja.png" class="fade01"></a>
			<?php else: ?>
				<a href="<?php echo preg_replace('/\/en/', '', $_SERVER['REQUEST_URI']) ;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/lang_ja.png" class="fade01"></a>
			<?php endif ;?>
			<a href="<?php echo $_SERVER['REQUEST_URI'] ;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/lang_en.png"></a>
		</div>
	</div>
	</nav>
</header>
