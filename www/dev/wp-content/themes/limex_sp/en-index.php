<?php
/**
 * Template Name: top
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

//ALL
//2016/06/28 メディア(id=2)以外を表示
$all_args = array(
//	'post_status'=>'publish',
	'post_type' => 'news-en',
	'category__not_in' => array(2),
	'posts_per_page' => 4,
);

//press release

$press_args = array(
//	'post_status'=>'publish',
	'category_name' => 'press_release',
	'post_type' => 'news-en',
	'posts_per_page' => 4,
);

  $top_media_args = array(
    'post_type'      => 'news-en',
    'posts_per_page' => 6,
  		'meta_query' => array(
			array(
				'key'=>'top_media',
				'value'=> '1'
			)
		)

  );
  //リンク用

$link_all_args = array(
//	'post_status'=>'publish',
	'post_type' => 'news-en',
	 'posts_per_page' => -1 
);

  $link_query = new WP_Query( $link_all_args );
get_header('en'); ?>
<script type="text/javascript">
Shadowbox.init({
  overlayColor:'#000',
  overlayOpacity:'0.8',
});</script>
<div class="story">
	<p>The earth is the planet of water. <br>
		The earth is the planet of life.<br>
		But it is not only that.<br>
		The earth is also the planet of stone.</p>
	<p>On the earth there is one material can be supplied unlimitedly.<br>
		That is stone.</p>
	<p>If paper can be made from stone,<br>
		 paper will be more durable and convenient.<br>
		But not only paper,<br>
		if more materials were to be made from stone then fossil fuels will bring to us without relying on water or trees a life style where things can be re-used over and over again.<br>
	</p>
	<p>Stone used to be tools for hunting or cooking.<br>
		They were also used as weapons.<br>
		And also they were used as a media for conveying messages.
		</p>
	<p> So far humanbeings has been very familir to stone.<br>
		It is now that we should re-examine the value of stone.<br>
		It is time we had bet on stone.
	</p>
	<p>Plenty of stone can be found on the earth.<br>
		To create possibility X from limestone, that is LIME, which still does not exist anywhere on the earth is the mission of LIMEX.
	</p>
	<p>The log symbol,<br>
		stands for that a world full of possibility will be created from stone, which was broken by the powerful X. <br>
	</p>
	<p>LIMEX will bring us a future starting from stone.</p>
</div>

<div class="key" id="key">
	<div class="key-slider" id="key-slider">
		<ul>
			<!--
			<li>
				<a href="/order/en_explain.php"><img src="<?php echo get_template_directory_uri(); ?>/images/en/slide/06.jpg"></a>
			</li>
		-->
			<li>
				<img class="show-story" src="<?php echo get_template_directory_uri(); ?>/images/en/slide/01.jpg">
			</li>
			<li>
				<img class="show-story" src="<?php echo get_template_directory_uri(); ?>/images/en/slide/02.jpg">
			</li>
			<li>
				<img class="show-story" src="<?php echo get_template_directory_uri(); ?>/images/en/slide/03.jpg">
			</li>
			<li>
				<a href="/en/about/#about_b2i4"><img src="<?php echo get_template_directory_uri(); ?>/images/en/slide/04.jpg"></a>
			</li>
		</ul>
	</div><!--/key-slider-->
	<div class="key-slider-ui" id="key-slider-ui">
		<ul class="circles">
		</ul>
	</div><!--/key-slider-ui-->
</div><!--/key-->

<div class="wrapper">


	<div class="banner">
		<a href="https://www.youtube.com/embed/ZQ0J8Ur9T-E?autoplay=1&rel=0&showinfo=0&autohide=1&loop=1&player=iframe&playlist=DJJ9zwqDL0c" rel="shadowbox;width=620;height=360"><img class="full" src="<?php echo get_template_directory_uri(); ?>/images/movie_banner.jpg"></a>
	</div>

	<div>
		<img style="width:100%; margin-top:30px;" src="<?php echo get_template_directory_uri(); ?>/images/en/top_text.jpg" alt="" >
	</div>
	<section class="news">
		<h2 class="top"><img src="<?php echo get_template_directory_uri(); ?>/images/title-news.png" alt="NEWS"></h2>

		<ul class="articles">
<?php
$the_query = new WP_Query( $all_args );
//var_dump($the_query);
// ループ
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();

$fields = get_fields(get_the_ID());

$cat = get_the_category();
$cat = $cat[0];
$cat_name = $cat->cat_name;
$category_nicename = $cat->category_nicename;
?>
			<li>
				<a href="<?php echo getLinkUrl($link_query,'en');?>">
					<div class="left">
						<p class="cat"><?php echo $cat_name; ?></p>
						<img src="<?php echo $fields['top_list_image']? $fields['top_list_image'] : '/wp-content/themes/limex/thm/news_i1.jpg';?>" alt="">
					</div>
					<div class="right">
						<p><?php the_title(); ?></p>
						<time><?php the_time('Y/m/d'); ?></time>
					</div>
				</a>
			</li>
<?php
endwhile;
endif;
?>

		</ul>

		<div class="button-more">
			<a href="/en/news/"><img src="<?php echo get_template_directory_uri(); ?>/images/button-more.png"></a>
		</div>

	</section>

	<nav class="nav-contents">
		<ul>
			<li>
				<a href="/en/about/"><img src="<?php echo get_template_directory_uri(); ?>/images/en/banner-about.jpg"></a>
			</li>
			<li>
				<a href="/en/mission/"><img src="<?php echo get_template_directory_uri(); ?>/images/en/banner-mission.jpg"></a>
			</li>
			<li>
				<a href="/en/factory/"><img src="<?php echo get_template_directory_uri(); ?>/images/en/banner-factory.jpg"></a>
			</li>
			<li>
				<a href="/en/company/"><img src="<?php echo get_template_directory_uri(); ?>/images/en/banner-company.jpg"></a>
			</li>
		</ul>
	</nav>

	<section class="media">
		<h2 class="top"><img src="<?php echo get_template_directory_uri(); ?>/images/title-media.png" alt="MEDIA"></h2>

		<ul class="articles">
<?php
$the_query = new WP_Query( $top_media_args );
//var_dump($the_query);
// ループ
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();

$fields = get_fields(get_the_ID());

$cat = get_the_category();
$cat = $cat[0];
$cat_name = $cat->cat_name;
$category_nicename = $cat->category_nicename;
?>
			<li>
				<a href="<?php echo getLinkUrl($link_query,'en');?>">
					<div class="left">
						<p class="cat"><?php echo $cat_name; ?></p>
						<img src="<?php echo $fields['top_list_image']? $fields['top_list_image'] : '/wp-content/themes/limex/thm/news_i1.jpg';?>" alt="">
					</div>
					<div class="right">
						<p><?php the_title(); ?></p>
						<time><?php the_time('Y/m/d'); ?></time>
					</div>
				</a>
			</li>
<?php
endwhile;
endif;
?>
<!--
			<li>
				<a href="/en/news/#625">
					<div class="left">
						<img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/08/news-i-160804-01-1.jpg" alt="">
					</div>
					<div class="right">
						<p>The NIKKEI broadcast about TBM -LIMEX by the  special TV program on 'The Cambria Palace by their 10 anniversary 500 times Memorial.</p>
						<time>2016/08/04</time>
					</div>
				</a>
			</li>
			<li>
				<a href="/en/news/#624">
					<div class="left">
						<img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/08/news-i-160803-01-2.jpg" alt="">
					</div>
					<div class="right">
						<p>Published in a magazine -Forbes 〔Create paper by stone without water〕　Start up ・TBM, From Japan to all over the world.</p>
						<time>2016/08/03</time>
					</div>
				</a>
			</li>
			<li>
				<a href="/en/news/#618">
					<div class="left">
						<img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/07/news-i-160715-01-2.jpg" alt="">
					</div>
					<div class="right">
						<p>Published in a International public relations magazine of [We are Tomodachi Summer 2016] issued by the government of Japan. Title : Cutting-Edge Technologies: Making Paper from Stone.</p>
						<time>2016/07/15</time>
					</div>
				</a>
			</li>
			<li>
				<a href="/en/news/#638">
					<div class="left">
						<img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/04/news-i-160428-01-1.jpg" alt="">
					</div>
					<div class="right">
						<p>LIMEX was introduced by public broadcasting in USA (PBS TV) 〜This Is America & The World〜</p>
						<time>2016/05/05</time>
					</div>
				</a>
			</li>
-->
	</ul>
		<div class="button-more">
			<a href="/en/news/?cat=media"><img src="<?php echo get_template_directory_uri(); ?>/images/button-more.png"></a>
		</div>

	</section>

</div><!--/wrapper-->
<?php
get_footer('en');
