<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<a id="pageTop" href="#contents">TOP</a>
<div class="wrapper">
	<footer class="footer-global">
	<nav class="nav-footer">
		<ul>
			<li><a href="/en/about/">ABOUT</a></li>
			<li><a href="/en/mission/">MISSION</a></li>
			<li><a href="/en/factory/">FACTORY</a></li>
			<li><a href="/en/company/">COMPANY</a></li>
			<li><a href="/en/news/">NEWS</a></li>
			<li><a href="/en/recruit/">RECRUIT</a></li>
			<li><a href="/en/privacy/">PRIVACY POLICY</a></li>
			<li><a href="mailto:infomail@tb-m.com">CONTACT</a></li>
		</ul>
	</nav>
	<ul class="sns">
		<li><a href="https://twitter.com/share?url=http://tb-m.com/&text=%e6%ac%a1%e4%b8%96%e4%bb%a3%e7%b4%a0%e6%9d%90LIMEX%20%e6%a0%aa%e5%bc%8f%e4%bc%9a%e7%a4%beTBM" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-twitter.png"></a></li>
		<li><a href="https://www.facebook.com/tbm.limex" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-facebook.png"></a></li>
		<li><a href="https://www.youtube.com/channel/UCIvfa-J0VGeLl71Aeg6sWRg" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-youtube.png"></a></li>
	</ul>
	<p class="cr"><small>Copyright TBM Co.,Ltd All Rights Reserved</small></p>
	</footer>
</div>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-46676692-1', 'auto');
			ga('send', 'pageview');
			
		</script>
		<!-- アクセス解析 -->
		<script type="text/javascript">
			
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-16758707-1']);
			_gaq.push(['_trackPageview']);
			
			(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
			
		</script>
		<!-- // アクセス解析 -->
</body>
</html>