var LIMEX = LIMEX || {};
LIMEX.key = (function($){
  var keySlider;
  var sliderUI;
  var sliderContainer;
  var circles;
  var slides;
  var total;
  var touchX;
  var touchY;
  var slideX;
  var canSlide = false;
  var isTouched = false;
  var currentSlide = 0;
  var isTweening = false;
  var timer;
  var windowWidth = $(window).width();
  var _init = function() {
    if(!$("#key-slider")) return false;
    keySlider = $("#key-slider");
    sliderContainer = keySlider.find("ul");
    total = sliderContainer.find('li').length;
    sliderUI = $("#key-slider-ui");

    for(var i = 0;i < total;i++){
      sliderUI.find(".circles").append('<li>')
    }
    slides = sliderContainer.find('li').clone();
    circles = sliderUI.find(".circles li");

    sliderContainer.empty();
    if(total == 1) {
      sliderContainer.append(slides[0]);
    } else {
      sliderContainer.append(slides[total-1]);
      sliderContainer.append(slides[0]);
      sliderContainer.append(slides[1]);
    }
    setCircle();
    setSliderSize();
    keySlider.css({
      visibility: "visible"
    })
    setTimer();
    addListeners();
  }
  var addListeners = function(){
    $(window).on("resize", function(){
      windowWidth = $(window).width();
      setSliderSize();
    })

    if(total == 1) return false; 
    keySlider.on({
      'touchstart': function(e){
        if(isTweening) return false;
        touchX = event.changedTouches[0].pageX;
        touchY = event.changedTouches[0].pageY;
        slideX = 0;
        isTouched = true;
        canSlide = true;
      },
      'touchmove': function(e){
        if(isTweening) return false;
        var moveX = event.changedTouches[0].pageX;
        var moveY = event.changedTouches[0].pageY;
        if(isTouched) {
          if(Math.abs(touchY - moveY) > 5 || Math.abs(touchX - moveX) < 3 ) canSlide = false;
          isTouched = false;
        }
        if(canSlide) {
          clearInterval(timer);
          slideX = touchX - moveX;
          sliderContainer.css({
            marginLeft: -slideX - windowWidth
          });
          e.preventDefault();
        }
      },
      'touchend': function(e){
        if(isTweening) return false;
        if(canSlide) {
          setTimer();
          var touchendX = event.changedTouches[0].pageX;
          if(touchendX > touchX) {
            toPrev();
          } else if(touchendX < touchX){
            toNext();
          }
        }
      },
    })
  }
  var setSlidePosition = function(isNext){
    var appendNum;
    if(isNext) {
      appendNum = (currentSlide + 1 > total - 1) ? 0 : currentSlide + 1;
      sliderContainer.find('li:first-child').remove();
      sliderContainer.append(slides[appendNum]);
      sliderContainer.css({
        marginLeft: -windowWidth
      })
    } else {
      appendNum = (currentSlide - 1 < 0) ? total - 1 : currentSlide - 1;
      sliderContainer.find('li:last-child').remove();
      sliderContainer.prepend(slides[appendNum]);
      sliderContainer.css({
        marginLeft: -windowWidth
      })
    }
  }
  var toNext = function(){
    if(isTweening) return false;
    setTimer();
    isTweening = true;
    TweenMax.to(sliderContainer,0.3,{marginLeft: -windowWidth * 2, onComplete: function(){
      setSlidePosition(true);
      setSliderSize();
      isTweening = false;
    }});
    currentSlide++;
    if(currentSlide > total - 1) currentSlide = 0;
    setCircle();
  }
  var toPrev = function(){
    if(isTweening) return false;
    setTimer();
    isTweening = true;
    TweenMax.to(sliderContainer,0.3,{marginLeft: 0,onComplete: function(){
      setSlidePosition(false);
      setSliderSize();
      isTweening = false;
    }});
    currentSlide--;
    if(currentSlide < 0) currentSlide = total - 1;
    setCircle();
  }
  var setCircle = function(){
    circles.each(function(index, el) {
      $(el).removeClass('current');
    });
    $(circles[currentSlide]).addClass('current');
  }
  var setSliderSize = function(){
    keySlider.find("li").css({
      width: windowWidth
    })
    if(total > 1) {
      sliderContainer.css({
        marginLeft: -windowWidth
      })
    }
  }
  var setTimer = function(){
    clearInterval(timer);
    timer = setInterval(function(){
      toNext();
    },5000);
  }
  var _self = {
    init: _init
  }
  return _self;
}(jQuery));

$(LIMEX.key.init);

LIMEX.globalNav = (function($){
  var buttonMenu;
  var navGlobal;
  var _init = function() {
    buttonMenu = $("#button-menu");
    navGlobal = $("#nav-global");
    navGlobal.find(".bg").on("click", function(){
      close();
    })
    $("body").css({
      minHeight: $(window).height()
    });
    navGlobal.css({
      minHeight: $(window).height() - 50
    });
    buttonMenu.on("click", function(){
      if(buttonMenu.hasClass('active')) {
        close();
        if($("div.story").css("display") == "block") {
          $("div.story").fadeOut(200);
        }
      } else {
        open();
      }
    })
    navGlobal.find("li:has('li')").children('a')
      .addClass('close')
      .on("click", function(){
        $(this).next().slideToggle(300, function(){
          setBodyHeight();
        });
        $(this).toggleClass('open');
        return false;
      })


    $("#key").on("click", ".show-story", function(){
      $("div.story").fadeIn(200);
      buttonMenu.addClass('active');
      $("body").css({
        height: $("div.story").height() + 100,
        overflowY: "hidden"
      })
      setBodyHeight();
      return false;
    })
  }
  var setBodyHeight = function(){
    $("body").css({
      //height: navGlobal.height() + 100,
      height: navGlobal.height() + 150,
      overflowY: "hidden"
    })
  }
  var open = function() {
    buttonMenu.addClass('active');
    navGlobal.fadeIn('200');
    setBodyHeight();
  }
  var close = function(){
    buttonMenu.removeClass('active');
    navGlobal.fadeOut('200');
    $("body").css({
      height: "auto",
      overflowY: ""
    })
  }
  var _self = {
    init: _init
  }
  return _self;
}(jQuery));

$(LIMEX.globalNav.init);