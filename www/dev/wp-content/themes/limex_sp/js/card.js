$(function () {
    var $card = $('.card-slider');
    $card.find('div:first').find('.selected').show();

    var $type = $('.select-type');

    $type.find('li').click(function () {
        var color_num = $($(this).parents('.meishi_color_div').get(0)).data('color');

        $('#meishi_' + color_num + ' .select-type').find('li').removeClass('now-type');
        $(this).addClass('now-type');
        var index = $('#meishi_' + color_num + ' .select-type').find('li').index(this);
        if (index === 0) {
            $('#meishi_' + color_num + ' .gosick').show();
            $('#meishi_' + color_num + ' .mintyou').hide();
        } else {
            $('#meishi_' + color_num + ' .gosick').hide();
            $('#meishi_' + color_num + ' .mintyou').show();
        }
    });


    $('.card-slider > li > div').on('click', function () {
        console.log($(this));
        var color_num = $($(this).parents('.meishi_color_div').get(0)).data('color');
        var selected = $(this).find('img').attr('alt');

        $('#meishi_card_type_' + color_num).val(selected);
        $('#meishi_' + color_num + ' .card-slider').find('.selected').hide();
        $(this).children('.selected').show();
    });
});