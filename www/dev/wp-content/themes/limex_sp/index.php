<?php
/**
 * Template Name: top
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

//ALL
//2016/06/28 メディア(id=2)以外を表示
$all_args = array(
//	'post_status'=>'publish',
	'post_type' => 'news',
	'category__not_in' => array(2),
	'posts_per_page' => 4,
);

//press release

$press_args = array(
//	'post_status'=>'publish',
	'category_name' => 'press_release',
	'post_type' => 'news',
	'posts_per_page' => 4,
);

  $top_media_args = array(
    'post_type'      => 'news',
    'posts_per_page' => 6,
  		'meta_query' => array(
			array(
				'key'=>'top_media',
				'value'=> '1'
			)
		)

  );

 //リンク用

$link_all_args = array(
//	'post_status'=>'publish',
	'post_type' => 'news',
	 'posts_per_page' => -1 
);

  $link_query = new WP_Query( $link_all_args );

get_header(); ?>
<script type="text/javascript">
Shadowbox.init({
  overlayColor:'#000',
  overlayOpacity:'0.8',
});</script>
<div class="story">
	<p>地球は水の星だ。 <br>
		地球は生命の星だ。 <br>
		でも、それだけじゃない。<br>
		地球は石の星でもあるのだ。</p>
	<p>地球に、ほぼ無限にあるもの、<br>
		それは石。 </p>
	<p>石から紙をつくれば、 <br>
		紙はもっと丈夫で便利になる。<br>
		紙だけじゃなく、<br>
		様々な素材をつくれば、 <br>
		化石燃料や、<br>
		木や水に頼ることのない<br>
		何度でも循環可能な生活が、 <br>
		世界中に広がってゆく。 </p>
	<p>かつて石は、<br>
		狩りや調理の道具だった。<br>
		闘うための武器であり、<br>
		伝えるためのメディアだった。</p>
	<p>長いあいだ、<br>
		人と石は親密な関係だった。<br>
		いまこそ石を、もっと見直そう。<br>
		石が持つ無限のチカラに、賭けてみよう。 </p>
	<p>石。 <br>
		それも、地球上の<br>
		どこにでも豊富にある<br>
		石灰石＝LIMEから、<br>
		まだどこにもない可能性＝Xをつくる。<br>
		それが、LIMEX /ライメックスの使命。</p>
	<p>そのシンボルロゴには、<br>
		Xのカタチに割れた「石」から、 <br>
		可能性に満ちた世界を生み出す <br>
		「意思」が込められている。</p>
	<p>ライメックス。<br>
		次の未来は、石からはじまる。</p>
</div>

<div class="key" id="key">
	<div class="key-slider" id="key-slider">
		<ul>
			<li>
				<img class="show-story" src="<?php echo get_template_directory_uri(); ?>/images/slide/01.jpg">
			</li>
			<li>
				<img class="show-story" src="<?php echo get_template_directory_uri(); ?>/images/slide/02.jpg">
			</li>
			<li>
				<img class="show-story" src="<?php echo get_template_directory_uri(); ?>/images/slide/03.jpg">
			</li>
			<li>
				<a href="http://recruit.tb-m.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/slide/07.jpg"></a>
			</li>
			<li>
				<a href="/about/#about_b2i4"><img src="<?php echo get_template_directory_uri(); ?>/images/slide/04.jpg"></a>
			</li>
			<li>
				<a href="/action/"><img src="<?php echo get_template_directory_uri(); ?>/images/slide/05.jpg"></a>
			</li>
			<li>
				<a href="/order/explain.php"><img src="<?php echo get_template_directory_uri(); ?>/images/slide/06.jpg"></a>
			</li>
		</ul>
	</div><!--/key-slider-->

	<div class="key-slider-ui" id="key-slider-ui">
		<ul class="circles">
		</ul>
	</div><!--/key-slider-ui-->
</div><!--/key-->

<div class="wrapper">


	<div class="banner">
		<a href="https://www.youtube.com/embed/ZQ0J8Ur9T-E?autoplay=1&rel=0&showinfo=0&autohide=1&loop=1&player=iframe&playlist=DJJ9zwqDL0c" rel="shadowbox;width=620;height=360"><img class="full" src="<?php echo get_template_directory_uri(); ?>/images/movie_banner.jpg"></a>
	</div>

	<div>
		<img style="width:100%; margin-top:30px;" src="<?php echo get_template_directory_uri(); ?>/images/top_text.jpg" alt="" >
	</div>
	<section class="news">
		<h2 class="top"><img src="<?php echo get_template_directory_uri(); ?>/images/title-news.png" alt="NEWS"></h2>

		<ul class="articles">
<?php
$the_query = new WP_Query( $all_args );
//var_dump($the_query);
// ループ
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();

$fields = get_fields(get_the_ID());

$cat = get_the_category();
$cat = $cat[0];
$cat_name = $cat->cat_name;
$category_nicename = $cat->category_nicename;
?>
			<li>
				<a href="<?php echo getLinkUrl($link_query);?>">
					<div class="left">
						<p class="cat"><?php echo $cat_name; ?></p>
						<img src="<?php echo $fields['top_list_image']? $fields['top_list_image'] : '/wp-content/themes/limex/thm/news_i1.jpg';?>" alt="">
					</div>
					<div class="right">
						<p><?php the_title(); ?></p>
						<time><?php the_time('Y/m/d'); ?></time>
					</div>
				</a>
			</li>
<?php
endwhile;
endif;
?>

		</ul>

		<div class="button-more">
			<a href="/news/"><img src="<?php echo get_template_directory_uri(); ?>/images/button-more.png"></a>
		</div>

	</section>

	<nav class="nav-contents">
		<ul>
			<li>
				<a href="/about/"><img src="<?php echo get_template_directory_uri(); ?>/images/banner-about.jpg"></a>
			</li>
			<li>
				<a href="/mission/"><img src="<?php echo get_template_directory_uri(); ?>/images/banner-mission.jpg"></a>
			</li>
			<li>
				<a href="/factory/"><img src="<?php echo get_template_directory_uri(); ?>/images/banner-factory.jpg"></a>
			</li>
			<li>
				<a href="/company/"><img src="<?php echo get_template_directory_uri(); ?>/images/banner-company.jpg"></a>
			</li>
		</ul>
	</nav>


	<section class="media">
		<h2 class="top"><img src="<?php echo get_template_directory_uri(); ?>/images/title-media.png" alt="MEDIA"></h2>

		<ul class="articles">
<?php
$the_query = new WP_Query( $top_media_args );
//var_dump($the_query);
// ループ
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();

$fields = get_fields(get_the_ID());

$cat = get_the_category();
$cat = $cat[0];
$cat_name = $cat->cat_name;
$category_nicename = $cat->category_nicename;
?>
			<li>
				<a href="<?php echo getLinkUrl($link_query);?>">
					<div class="left">
						<p class="cat"><?php echo $cat_name; ?></p>
						<img src="<?php echo $fields['top_list_image']? $fields['top_list_image'] : '/wp-content/themes/limex/thm/news_i1.jpg';?>" alt="">
					</div>
					<div class="right">
						<p><?php the_title(); ?></p>
						<time><?php the_time('Y/m/d'); ?></time>
					</div>
				</a>
			</li>
<?php
endwhile;
endif;
?>
<!--
			<li>
				<a href="/news/#661">
					<div class="left">
						<img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/08/news-i-160825-01-1.jpg" alt="">
					</div>
					<div class="right">
						<p>「Forbes」に掲載：<br>「水も木も使わず石から紙をつくる」スタートアップ、世界へ</p>
						<time>2016/08/25</time>
					</div>
				</a>
			</li>
			<li>
				<a href="/news/#603">
					<div class="left">
						<img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/08/news-i-160804-01-1.jpg" alt="">
					</div>
					<div class="right">
						<p>日経スペシャル「カンブリア宮殿」<br> 10周年500回記念番組で特集</p>
						<time>2016/08/04</time>
					</div>
				</a>
			</li>
			<li>
				<a href="/news/#562">
					<div class="left">
						<img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/07/news-i-160715-01-2.jpg" alt="">
					</div>
					<div class="right">
						<p>日本政府「We Are Tomodachi Summer 2016」に掲載</p>
						<time>2016/07/15</time>
					</div>
				</a>
			</li>
			<li>
				<a href="/news/page/2/#431">
					<div class="left">
						<img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/04/news-i-160428-01-1.jpg" alt="">
					</div>
					<div class="right">
						<p>「アメリカの公共放送PBSテレビ」でLIMEXが紹介 〜This Is America & The World〜</p>
						<time>2016/05/05</time>
					</div>
				</a>
			</li>
-->
	</ul>
		<div class="button-more">
			<a href="/news/?cat=media"><img src="<?php echo get_template_directory_uri(); ?>/images/button-more.png"></a>
		</div>

	</section>
</div><!--/wrapper-->
<?php
get_footer();
