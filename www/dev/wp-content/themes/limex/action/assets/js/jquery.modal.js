(function($){

  $.fn.grow_modal = function( method ) {
    var _this = this;

    current_num = 0;

    var md = {
      //ターゲット設定
      modal : _this,
      modal_contaner : _this.find('#modal-contaner'),
      modal_sections : _this.find('#modal-contaner').find('section'),
      show_btn_group: $('.mod-message-list'),
      show_btns: $('.mod-message-list').find('li'),

      //コントローラ設定
      btn_next : $('#modal-next'),
      btn_prev : $('#modal-prev'),
      btn_close : $('#modal-close'),
      modal_filter : $('#modal-filter'),

      //共通
      ww : $(window).width(),
      wh : $(window).height(),
      distance : 0,
      current_num : 0,
      next_num : 0,
      prev_num : 0,
      move_distance : 500,
      effect_speed : 600,
      effect_value : 'easeInOutQuint',
      vector : 'top',
      moving : 'fade',

      init : function() {
        md.centering();
        md.set_once_hide_content();
        md.set_once_show_content();
        $(window).resize(function(event) {
          md.horizontal_centering();
        });
        if(md.moving = 'fade'){md.fade_init();}
        md.btn_next.on('click', function(event) {
          if(md.moving = 'fade'){
            md.to_next_fade();
          } else { 
            md.to_next();
          }
        });
        md.btn_prev.on('click', function(event) {
          if(md.moving = 'fade'){
            md.to_prev_fade();
          } else { 
            md.to_prev();
          }
        });
      },

      //■モーダルウィンドウのセンタリング＃初期化
      centering : function() {
        md.ww = $(window).width();
        md.wh = $(window).height();
        var modal_w = md.modal_contaner.outerWidth();
        var modal_h = md.modal_contaner.outerHeight();

        md.modal_contaner.css({
          left: (md.ww - modal_w)/2,
          top: (md.wh - modal_h)/2 + $(window).scrollTop()
        });
      },
      horizontal_centering : function(){
        md.ww = $(window).width();
        var modal_w = md.modal_contaner.outerWidth();

        md.modal_contaner.css({
          left: (md.ww - modal_w)/2,
        });
      },
      body_fixed : function() {
        md.distance = $(window).scrollTop();
        $('body').css('top', -($(window).scrollTop()));
        $('body').addClass('noscroll');
      },
      body_unfixed : function() {
        $('body').removeClass('noscroll');
        $(window).scrollTop(md.distance);
        $('body').css('top', 'auto');
      },
      //モーダル枠内の高さを取得
      check_content_height : function() {
        var target_padding = parseInt(modal_contaner.css('padding-top'), 10) + parseInt(modal_contaner.css('padding-bottom'), 10);
        var content_height = modal_contaner.innerHeight() - target_padding;

      //各々のセクションの中において、もし内部の高さが枠の高さを超えていたら1を返す、そうでなければ0を返す。
      //スクロールでGOが出次第実装

      },

      //モーダルが出現する際に内容をセット
      set_show_content : function(target) {
        target_num = md.show_btns.index(target);
        md.check_num(target_num);
        md.init_scroll_content();
        // target_num = md.set_show_content(target);
        target_wraper = md.modal_sections.eq(target_num);
        if(md.moving = "fade"){
          md.move_distance = 0;
          md.modal_sections.find('.left').css('display', 'none');
          md.modal_sections.find('.right').css('display', 'none');
          target_wraper.find('.left').css('display', 'block');
          target_wraper.find('.right').css('display', 'block');
        } else {
          md.modal_sections.find('.left').css('top', - md.move_distance);
          md.modal_sections.find('.right').css('top', - md.move_distance);
          target_wraper.find('.left').css('top', 0);
          target_wraper.find('.right').css('top', 0);
        }
      },

      set_once_show_content : function(){
        $(".mod-message-list").find('li.mod-on').on('click', function(event) {
          _this = $(this);
          md.set_show_content(_this);
          md.centering();
          md.show_modal();
          md.body_fixed();
        });
      },
      set_once_hide_content : function() {
        md.modal_filter.on('click', function(event) {
          md.hide_modal();
        });
        md.btn_close.on('click', function(event) {
          md.hide_modal();
        });
        md.body_unfixed();
      },
      show_modal : function() {
        md.modal.fadeIn();
      },
      hide_modal : function() {
        md.modal.fadeOut();
        md.body_unfixed();
      },
      check_num : function(num) {
        var length = md.modal_sections.length -1;
        md.current_num = num;
        if(md.current_num < 0){
          md.current_num = length;
        } else if (md.current_num > length){
          md.current_num = 0;
        }
        if(md.current_num == length){
          md.next_num = 0;
        } else {
          md.next_num = md.current_num + 1;
        }
        if(md.current_num == 0) {
          md.prev_num = length;
        } else {
          md.prev_num = md.current_num - 1;
        }
      },
      //内部スクロールの位置を初期化
      init_scroll_content : function() {
        md.modal_sections.find('.tse-scroll-content').scrollTop(0);
      },

      //隣接ブロックの準備（次へ、前へボタンを押したときの準備）
      to_next_ready : function () {
        var target = md.modal_sections.eq(md.next_num);
        target.find('.left').css('top', -md.move_distance);
        target.find('.right').css('top', md.move_distance);
      },
      to_prev_ready : function () {
        var target = md.modal_sections.eq(md.prev_num);
        target.find('.left').css('top', md.move_distance);
        target.find('.right').css('top', -md.move_distance);
      },
      to_next_fade : function () {
        var target_current = md.modal_sections.eq(md.current_num);
        var target_next = md.modal_sections.eq(md.next_num);
        if(target_current.find('.right').is(':animated'))return false;

        target_current.find('.left').stop().fadeOut(md.effect_speed);
        target_current.find('.right').stop().fadeOut(md.effect_speed);

        target_next.find('.left').stop().fadeIn(md.effect_speed);
        target_next.find('.right').stop().fadeIn(md.effect_speed, function(){
          md.check_num(md.current_num+1);
          md.init_scroll_content();
        });
      },
      to_next : function() {
        md.to_next_ready();
        vector = md.vector;
        var target_current = md.modal_sections.eq(md.current_num);
        var target_next = md.modal_sections.eq(md.next_num);
        if(target_current.find('.right').is(':animated'))return false;

        var apLeft = {}
        var apRight = {}
        var apCurrentLeft = {}
        var apCurrentRight = {}
        apLeft[md.vector] = md.move_distance;
        apRight[md.vector] = -md.move_distance;
        apCurrentLeft[md.vector] = 0
        apCurrentRight[md.vector] = 0

        target_current.find('.left').stop().animate(apLeft, md.effect_speed,md.effect_value);
        target_current.find('.right').stop().animate(apRight, md.effect_speed,md.effect_value);

        target_next.find('.left').stop().animate(apCurrentLeft, md.effect_speed,md.effect_value);
        target_next.find('.right').stop().animate(apCurrentRight, md.effect_speed,md.effect_value, function(){
          md.check_num(md.current_num+1);
          md.init_scroll_content();
        });
      },
      to_prev_fade : function () {
        var target_current = md.modal_sections.eq(md.current_num);
        var target_prev = md.modal_sections.eq(md.prev_num);
        if(target_current.find('.right').is(':animated'))return false;

        target_current.find('.left').stop().fadeOut(md.effect_speed);
        target_current.find('.right').stop().fadeOut(md.effect_speed);

        target_prev.find('.left').stop().fadeIn(md.effect_speed);
        target_prev.find('.right').stop().fadeIn(md.effect_speed, function(){
          md.check_num(md.current_num-1);
          md.init_scroll_content();
        });
      },
      to_prev : function () {
        md.to_prev_ready();
        vector = md.vector;
        var target_current = md.modal_sections.eq(md.current_num);
        var target_prev = md.modal_sections.eq(md.prev_num);
        if(target_current.find('.right').is(':animated'))return false;

        var apLeft = {}
        var apRight = {}
        var apCurrentLeft = {}
        var apCurrentRight = {}
        apLeft[md.vector] = -md.move_distance;
        apRight[md.vector] = md.move_distance;
        apCurrentLeft[md.vector] = 0
        apCurrentRight[md.vector] = 0

        target_current.find('.left').stop().animate(apLeft, md.effect_speed,md.effect_value);
        target_current.find('.right').stop().animate(apRight, md.effect_speed,md.effect_value);

        target_prev.find('.left').stop().animate(apCurrentLeft, md.effect_speed,md.effect_value);
        target_prev.find('.right').stop().animate(apCurrentRight, md.effect_speed,md.effect_value, function(){
          md.check_num(md.current_num-1);
          md.init_scroll_content();
        });
      },
      fade_init : function() {
        md.modal_contaner.find('.left').css('top', 0);
        md.modal_contaner.find('.right').css('top', 0);
      }
    }

    md.init();

  }

})(jQuery);

$(function(){
  $('#action-modal').grow_modal();
});

