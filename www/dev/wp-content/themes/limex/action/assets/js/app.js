//alignHeight.JS

var action_digital_event=0;

(function($) {
    $.fn.alignHeight = function(column) {
        var self = this;
        var column = column ? column : 0;
        var height = 0;
        var row = 0;
        return self.each(function(index) {
            height = height < $(this).height() ? $(this).height() : height;
            if (!column || (index + 1) % column == 0 || index + 1 == self.length) {
                var start = row * column;
                self.slice(start, index + 1).css('height', height);
                height = 0;
                row ++;
            }
        });
    }
})(jQuery);


var init_set_num = function(){
    var set_target_list = ['project_number','warter_number','distribution_number','introduction_number','warter_number_500']
//    $.getJSON("/action/web/api/get_configs", function(data,json_target){
    $.getJSON("/api/", function(data,json_target){
    })
    .done(function(data){
        $.each(set_target_list,function(index,elem){
            var target_num = data[elem];
            $('#'+elem).attr('data-num',target_num);
        });
    })
    .done(function(){
        init_card_obj();
        init_bottole_obj();
    })
    .done(function(){
        $('body').removeClass('ready');
        setTimeout(function(){
            func_list();
            $(window).on('scroll',function(){
                func_list();
            });
        },2300);
        
    });
}

var init_rulet = function(target){
    //任意のターゲット指定
    var target_rulet = $('#'+target);
    //ターゲットに記述された６桁の数字を取得
    
    var score = target_rulet.attr("data-num");
    var target_num_length = score.length;
    var num_block = target_rulet.find('li');
    
    //各桁の数字ブロックごとに処理
    for (var i = 0; i < target_num_length; i++){
        var num = i;
        var target_block = num_block.eq(i);
        var target_num = target_block.find('p');
        //各桁の数字の順序入れ替え
        for (var m = 0; m <= score.charAt(i); m++){
            target_num.eq(m).appendTo(num_block.eq(i));
        }
    }
}
var digital_init_rulet = function(target){
    var target_id = $('#'+ target);
    var target_rulet = $('#'+target);
    var target_result = $('#result_'+target);
    //ターゲットに記述された６桁の数字を取得
    
    var score = target_rulet.attr("data-num");
    var target_num_length = score.length;
    var num_block = target_result.find('li');
    
    //各桁の数字ブロックごとに処理
    for (var i = 0; i < target_num_length; i++){
        var num = i;
        var target_num = num_block.eq(i).find('.result_num');
        target_num.attr('src', "/wp-content/themes/limex/action/assets/images/img-digital-num-" + score.charAt(i) + ".png");
    }
}

var add_random_class = function(target){
    var target_block = $('#'+target);
    var target_height = target_block.find('p').outerHeight()*9;
    var target_rulet = $('#'+target);
    var score = target_rulet.attr("data-num");
    var target_num_length = score.length;
    if(!target_block.hasClass('digital')){

        for (var m = 0; m < target_num_length; m++){
            target_block.find('li').eq(m).addClass('transition_'+m);
            target_block.find('li').css('top',-target_height);
        }
    }
}

var measurement_rulet = function(target,json_target){

    var target_block = $('#'+target);
    if(target_block.hasClass('done')){
        return false;
    } else {
        var distance = $(window).scrollTop();
        var target_block = $('#'+target);
        var window_h = $(window).height();
        var event_point_end = target_block.offset().top + target_block.find('.border').height()/2;
        var event_point_start = target_block.offset().top + target_block.find('.border').height()/2 - window_h;
        if(distance > event_point_start){
            if (distance < event_point_end){
            $.getJSON("/api/", function(data,json_target){
//           $.getJSON("http://limex.impv.net/action/web/api/get_configs", function(data,json_target){

            })
            .done(function(data){
                var target_num = data[json_target];
                target_block.attr('data-num',target_num);
                if(!target_block.hasClass('done')){
                    if(!target_block.hasClass('digital')){
                        init_rulet(target);
                    }
                }
                add_random_class(target);
                target_block.addClass('done');
            });
            }
        }
    }
}


var mesurement_obj = function(target){
    var target_block = $('#'+target);
    if(target_block.hasClass('done_obj')){
        return false;
    } else {
        var distance = $(window).scrollTop();
        var target_block = $('#'+target);
        var window_h = $(window).height();
        var event_point_end = target_block.offset().top + target_block.find('.border').height()/2;
        var event_point_start = target_block.offset().top + target_block.find('.border').height()/2 - window_h;
        if(distance > event_point_start){
            if (distance < event_point_end){
                apearance_turn(target);
                target_block.addClass('done_obj');
            }
        }
    }
}

var apearance_turn = function(target){
    var target_block = $('#'+target);
    var target_length = target_block.find('.on').length;
    var i = 0;
    setTimeout(function(){
        $('#card').addClass("done");
         setTimeout(function(){
            $('#bottle').addClass("done");
        },2500);
        $('.explanation').addClass("done");
    },300);
    setTimeout(function(){
        for (var u = 0; u < target_length; u++){
            var delay = 150*u - ((150*u)*u/target_length)*0.5;
            setTimeout(function(){
                target_block.find('.on').eq(i).addClass('done');
                i++;
            },delay);
        }
    },500);
}

var init_card_obj = function(){
    var setting_num_1 = 10000;
    var setting_num_2 = 1000;
    var int = Number($('#introduction_number').attr('data-num'));
    var length_mt = Math.floor(int/setting_num_1);
    var length_lt = Math.floor((int%setting_num_1)/setting_num_2);
    if(length_mt==0){
        $('#card').find(".large").css({"display":"none"});
    } else {
        $('#card').find(".value").text(length_mt);
    }    
    for (var i = 0; i < length_lt; i++){
        $('#card').find('li').eq(i).removeClass("off").addClass("on");
//        $('#card').find('ul').append('<li><img class="ready" src="/action/assets/images/icon_card_full.png" alt=""></li>');
    }
}
var init_bottole_obj = function(){
    var setting_num_1 = 1000;
    var setting_num_2 = 100;
    var int = Number($('#warter_number_500').attr('data-num'));
    var length_mt = Math.floor(int/setting_num_1);
    var length_lt = Math.floor((int%setting_num_1)/setting_num_2);

    if(length_mt==0){
        $('#bottle').find(".large").css({"display":"none"});
    } else {
        $('#bottle').find(".value").text(length_mt);
    }

    for (var i = 0; i < length_lt; i++){
        $('#bottle').find('li').eq(i).removeClass("off").addClass("on");
//        $('#bottle').find('ul').append('<li><img class="ready" src="/action/assets/images/icon_bottle_full.png" alt=""></li>');
    }
}

var showRuletResultLeft = function(){
    if($('#warter_number').hasClass('done')){
        var time= 400;
        var target = $("#warter_number").find('.result_num_box');
        for(var i=0;i<target.length;i++){
            setTimeout(function(){
                target = $("#warter_number").find('.result_num_box').filter(".ready");
                target.eq(0).removeClass("ready");
            },time*i);
        }
    }
}
var showRuletResultRight = function(){
    if($('#project_number').hasClass('done')){
        var time= 400;
        var target = $("#project_number").find('.result_num_box');
        for(var i=0;i<target.length;i++){
            setTimeout(function(){
                target = $("#project_number").find('.result_num_box').filter(".ready");
                target.eq(0).removeClass("ready");
            },time*i);
        }
    }
}

var func_list = function(){
    var distance = $(window).scrollTop();
    var target_block = $('#warter_number');
    var window_h = $(window).height();
    var event_point_end = target_block.offset().top + target_block.find('.border').height()/2;
    var event_point_start = target_block.offset().top + target_block.find('.border').height()/2 - window_h;
    if(distance > event_point_start){
        if (distance < event_point_end){
            if(action_digital_event==0){

                $('#warter_number').addClass('done');
                setTimeout(function(){
                    showRuletResultLeft();
                    
                    setTimeout(function(){
                        $('#project_number').addClass('done');
                        setTimeout(function(){
                            showRuletResultRight();
                        },1500);
                    },2700);
                },1500);

                // setTimeout(function(){
                //     showRuletResultLeft();
                // },2000);
                // setTimeout(function(){
                //     $('#project_number').addClass('done');
                //     // measurement_rulet('project_number');
                //     setTimeout(function(){
                //         showRuletResultRight();
                //     },4000);
                // },2500);
                setTimeout(function(){
                    $('.contents .resolve_block #project_number .dot').fadeIn(2000);
                },7000);

                action_digital_event = 1;
            }
        }
    }

    measurement_rulet('distribution_number');
    measurement_rulet('introduction_number');
    setTimeout(function(){
        measurement_rulet('warter_number_500');
    },2500);

    setTimeout(function(){
        mesurement_obj('card');
    },800);

    setTimeout(function(){
        mesurement_obj('bottle');
    },3200);
}

var random = function(min,max){
    return Math.floor( Math.random() * (max - min + 1) ) + min;
}
var digital_rulet = function(){
    $('.rulet_num').each(function(index, el) {
        $(this).attr('src', "/wp-content/themes/limex/action/assets/images/img-digital-num-" + random(0,9) + ".png");
    });
}

var set_result_num = function(){
    $('.result_num').each(function(index, el) {
        $(this).attr('src', "/wp-content/themes/limex/action/assets/images/img-digital-num-" + random(0,9) + ".png");
    });
}
// var loopDigitalNum = function(){
//     digital_rulet('#warter_number');
//     digital_rulet('#project_number');
// }

function stopTimer(){
    clearInterval(digitalTimer);
}

$(document).ready(function(){
    $('.bg_wrap').addClass('start');
    digital_init_rulet("project_number");
    digital_init_rulet("project_per_number");
    digital_init_rulet("warter_number");
    digital_init_rulet("warter_per_number");
});
$(window).on('load',function(){
    init_set_num();
    digitalTimer=setInterval(function(){
        digital_rulet();
    },100);
});