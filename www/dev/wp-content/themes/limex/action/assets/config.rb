http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "js"
line_comments = false

# output_style = :expanded or :nested or :compact or :compressed
output_style = :expanded