<?php
/**
 * Template Name: en-action-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$fields = get_fields(48);
$update_date = $fields['update_date'];
$special_fields = get_fields(53);

$args = array(
  'post_type'   => 'topics',
  'numberposts' => 3,
);

$topics = get_posts($args);

$args_special = array(
  'post_type'   => 'en-special',
  'order'   => 'ASC',
);

$specials = get_posts($args_special);

?><!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8" />
<title>LIMEX ACTION | 次世代素材LIMEX 株式会社TBM</title>
<meta name="description" content="日本の技術だからできる新素材ライメックス。変幻自在に生まれ変われるライメックスは、様々な製品になって、世界中の生活を支えます。">
<meta name="Keywords" content="LIMEX,石灰石,リサイクル,エコノミー,エコロジー,新素材,ストーンペーパー,プラスチック,株式会社TBM">
<link rel="SHORTCUT ICON" href="/img/favicon.ico" />

<meta property="og:title" content="次世代素材LIMEX 株式会社TBM" />
<meta property="og:url" content="http://tb-m.com/" />
<meta property="og:description" content="【株式会社TBM】LIMEXは、石灰石（炭酸カルシウム）を主原料に、紙やプラスチック商品の代替となり、エコロジーとエコノミーを実現する新素材です。" />
<meta property="og:image" content="http://tb-m.com/img/ogp.jpg" />

<meta name="viewport" content="width=1280px, maximum-scale=1.0, user-scalable=yes">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/action/assets/css/colorbox.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/action/assets/css/en-style.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/action/assets/css/modal.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/action/assets/css/trackpad-scroll-emulator.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/action/assets/css/slick.css" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/app.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/jquery.modal.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/jquery.trackpad-scroll-emulator.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/jquery.BlackAndWhite.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/slick.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/slick-app.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scrollstop.js"></script>
<script>

/*
$(function() {
    var topBtn = $('#order-menu');
    var deleted = false;
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {

        if($(this).scrollTop() >= 3300){
           topBtn.fadeOut();

        } else if ($(this).scrollTop() > 2500  && deleted == false) {
            topBtn.fadeIn();

        } else {
            topBtn.fadeOut(); 
        }
    });
$('.btn_delete').on('click', function() {
  deleted = true;
  topBtn.fadeOut();
});

});
*/
$(function(){
	var topBtn = $('#order-menu');
	var deleted = false;
	topBtn.hide();
	
	/* scrollstop */
  $(window).on("scrollstop",function(){
		if ( deleted == false ){
			topBtn.fadeIn();
		}
  });
	
	/* */
	$(window).scroll(function() {
			topBtn.fadeOut();	
  });
	
	$('.btn_delete').on('click', function() {
		deleted = true;
		topBtn.fadeOut();
	});

});

  $(document).ready(function() {
    $(".youtube").colorbox({
      iframe:true,
      innerWidth:1080,
      innerHeight:608
    });
    $(".companies").colorbox({
      inline:true,
      innerWidth:920,
      maxHeight:'90%'
    });
    $(document).on('cbox_open', function() {
      $('body').css('overflow', 'hidden');
    }).on('cbox_closed', function() {
      $('body').css('overflow', '');
    });

    //追記20160526
    $(".glay").BlackAndWhite({
      hoverEffect : true,
      webworkerPath : false,
      responsive:true,
      invertHoverEffect: false,
      speed: { //ホバーしたときのアニメーションの速度を設定
        fadeIn: 200, // 200ミリ秒でフェードイン
        fadeOut: 800 // 800ミリ秒でフェードアウト
      }
    });
    //追記ここまで
  });



</script>
<style>
.contents .resolve_block #project_number .dot  {
    display:none;
    position: absolute;
    top: 108px;
    right: 127px;
}
.glay {
    position:relative;
    display:block;
}
#cboxContent {
  padding: 40px;
  background-image:url(<?php echo get_template_directory_uri(); ?>/action/assets/images/bg_modal.png);
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center bottom;
}
#cboxTopLeft,
#cboxTopCenter,
#cboxTopRight,
#cboxBottomLeft,
#cboxBottomCenter,
#cboxBottomRight,
#cboxMiddleLeft,
#cboxMiddleRight {
  display:none;
}
#cboxClose {
  top:20px;
  right:20px;
  width:20px;
  height:20px;
  background-position:0 0;
}
#cboxClose:hover {
  background-position:0 0;
  opacity: 0.7;
}

/*
#order-menu {

}
*/
#order-menu a {

    text-decoration: none;
}
#order-menu a:hover {
    text-decoration: none;
}
header .bnr {
 position: fixed;
  bottom: 30px;
  right: 0;
  z-index: 10;
 display: none;
}
header .bnr_close {
  position: absolute;
  top: 7px;
  right: 7px;
  z-index: 1000;
}

</style>
</head>
<body class='top ready'>
   <div class="bg_wrap top">
    <header>
    
          <ul class="right clearfix">
            <li>
              <a href="/en/"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_limex_to.png" alt="LIMEXSITE"></a>
            </li>
            <li>
              <a href="/order/en_explain.php"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_meisi_to.png" alt="Business card order"></a>
            </li>
          </ul>
<!--
          <div class="bnr" id="order-menu">
            <div class="bnr_img">
              <a href="/order/explain.php"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/bnr_meisi.png" alt=""></a>
            </div>
            <div class="bnr_close btn_delete">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/bnr_meisi_close.png" alt="">
            </div> 
          </div>
-->
    </header>
    <div class="contents top">
       
        <div class="title_block center">
            <h2><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font_limex_action.png" alt="LIMEX ACTION"></h2>
            <p style="margin-bottom:135px;"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-action-subtitle-01.png" alt="A tomorrow we can save with limestones"></p>
            <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-action-description-01.png" alt="LIMEX paper made out of limestone will contribute to resolving water shortage  and deforestation."></p>
        </div>
        
        <div class="resolve_block">
            <div id="warter_number" class="titled_box transition_3 digital" data-num="<?php echo sprintf('%06d',$fields['warter_number'])?>">
                <!-- <img class="arrow abs" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_arrow_right_big.png" alt=""> -->
               <h3><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-txt-water-save.png" alt="LIMEX PRODUCT"></h3>
               <div class="border blue mb30">
                   <ul class="rulet_box clearfix" id="result_warter_number">
                       <li data-num = "1">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "2">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "3">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "4">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "5">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "6">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                   </ul>
               </div>

              <div class="water_saved_head"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-action-description-needwater.png" alt="If the necessary amountof water for living is 50 liters per person">
              </div>

               <div id="project_per_number" class="clearfix" data-num="<?php echo sprintf('%04d',$fields['project_per_number'])?>">
                  <div class="left">
                    <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-action-head-needwater.png" alt="Water saved for">
                  </div>
                  <div class="right float-r">
                     <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon-human.png" alt=""></div>
                     <div class="unit"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-txt-people.png" alt="people"></div>
                     <div class="border white">
                       <ul class="rulet_box digital mini clearfix" id="result_project_per_number">
                         <li data-num = "1">
                            <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                            <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                         </li>
                         <li data-num = "2">
                            <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                            <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                         </li>
                         <li data-num = "3">
                            <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                            <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                         </li>
                         <li data-num = "4">
                            <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                            <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                         </li>
                       </ul>
                     </div>
                   </div>
                 </div>
            </div>
            <div id="project_number" class="titled_box transition_3 digital" data-num="<?php echo sprintf('%06d',$fields['project_number'])?>">
            <!-- <div class="lt"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_arrow_right_big.png" alt=""></div> -->
            <div class="last-update"><p>As of<?php echo date('M d,Y',strtotime($update_date)) ?></p></div>
               <h3><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-txt-tree-save.png" alt="LIMEX PRODUCT"></h3>
               <div class="border blue mb30">
                   <ul class="rulet_box clearfix" id="result_project_number">
                       <li data-num = "1">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "2">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "3">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "4">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "5">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "6">
                           <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                           <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                   </ul>
                   <div class="dot">
                       <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_dot.png" alt=""></p>
                   </div>

               </div>

               <div class="trees_saved_head"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-action-description-chopstick.png" alt="If we convert the trees into pairs of wooden chopsticks"></div>

               <div id="warter_per_number" class="clearfix" data-num="<?php echo sprintf('%07d',$fields['water_per_number'])?>">
                
                 <div class="right float-r">
                   <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon-chopstick.png" alt=""></div>
                   <div class="unit"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/text-pairs.png" alt="pairs"></div>
                   <div class="border white">
                     <ul class="rulet_box digital mini clearfix" id="result_warter_per_number">
                       <li data-num = "1">
                          <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                          <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "2">
                          <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                          <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "3">
                          <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                          <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "4">
                          <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                          <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "5">
                          <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                          <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "6">
                          <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                          <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                       <li data-num = "7">
                          <p><img class="rulet_num" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-digital-num-8.png" alt="0"></p>
                          <p class="result_num_box ready"><img class="result_num" src="" alt=""></p>
                       </li>
                     </ul>
                   </div>
                 </div>
               </div>
            </div>
        </div>
        <!-- 
        <div class="resolve_block">
            <div id="warter_number" class="titled_box transition_3" data-num="000001">
               <h3><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/txt-water-save.png" alt="LIMEX PRODUCT"></h3>
               <div class="border blue" style="margin-bottom: 10px;">
                   <ul class="rulet_box clearfix">
                       <li data-num = "1">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "2">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "3">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "4">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "5">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "6">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                   </ul>
               </div>
            </div>
            <div id="project_number" class="titled_box transition_3" data-num="001233">
               <h3><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/txt-tree-save.png" alt="LIMEX PRODUCT"></h3>
               <div class="border blue">
                   <ul class="rulet_box clearfix">
                       <li data-num = "1">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "2">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "3">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "4">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "5">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "6">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                   </ul>
                   <div class="dot">
                       <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_dot.png" alt=""></p>
                   </div>
               </div>
            </div>
        </div> -->
      </div>
<!--
        <div class="country">
            <h3><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font_country.png" alt="country"></h3>
            <ul>
<?php foreach ($fields['language'] as $key => $value):?>
                <li><img src="<?php echo $value['language_image'];?>" alt="<?php echo $value['language_name'];?>"></li>
<?php endforeach;?>
            </ul>
        </div>
-->
      <div class="contents middle">
        <div class="water_block">
          <div class="head"><img img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-water-head-2.png" alt="Our dreams through LIMEX"></div>
          <div class="water_contents">
           <!--  <div class="text-area">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/water-contents-title.png" alt="">
              <p class="title"><img img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/water-title.png" alt=""></p>
              <p class="text">2015年、ダボス会議で発表された｢今後10年間で環境に影響が大きなリスク｣は、｢水危機｣が気候変動や森林伐採を抑えて1位になりました。さらなる人口増加にともなう水の使用量の増加は、世界の水不足を深刻化させるはず。普通紙は約1tつくるために、約100tもの水を利用するのに対し、ライメックスペーパーは水を使用せずにつくれます。世界の水資源の問題に、ライメックスなら貢献できます。</p>
            </div> -->
            <div class="slider">
              <div class="slider-water">
                <p class="slider-text water en-w"><img img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-slider01-text.png" alt="Protecting our Water Resources. At Davos Forum 2015, it was announced that 'Water Crisis' moved to the top position beyond climate change and deforestation for 'risks that impact the environment the most in the next ten years'. Population increase will lead to increase in total water consumption, deepening the water-shortage problems. Making 1 ton of paper requires 100 tons of water, but for making 1 ton of LIMEX we can make it without using any water. We can protect the water resources by using LIMEX."></p>
                <p class="slider-bg"></p>
              </div>
              <div class="slider-tree">
                <p class="slider-text tree"><img img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-slider02-text.png" alt="Leaving Fertile Forests. Japan imports nearly all of the wood for the paper we produce. Deforestation of the size of a baseball field is said to be occurring every 10 minutes in the world. By using LIMEX –paper made of limestone- we can protect our valuable forests."></p>
                <p class="slider-bg"></p>
              </div>
             
          </div>
          </div>
        </div>
        <div class="about_block">
            <h2 class="center"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-about-title.png" alt="What is LIMEX ACTION?"></h2>
            <div class="about_contents_wrap">
              <div class="about_contents">
                <h3><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-about-title-sub.png" alt="Driving the Planet to the right direction,with the new Japanese technology."></h3>
                <p class="text">
                  Through LIMEX ACTION we will visualize the savings of
                  water, trees and petroleum that are our planet's valuable
                  resources so that we can see the social impact and
                  the value of our business activities.  We will transmit
                  how our company members are striving daily to better
                  he resource issues and our enthusiasm to our LIMEX products. 
                </p>
              </div>
              <div class="about_movie cf">
                <div class="left">
                  <a href="https://www.youtube.com/embed/ZQ0J8Ur9T-E?autoplay=1&rel=0&showinfo=0&autohide=1&loop=1&playlist=DJJ9zwqDL0c" class="youtube">
                      <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/movie.png" width="371" height="213">
                  </a>
                </div>
                <div class="right">
                  <p>LIMEX a new material born from Japanese technology. <br>
                  If LIMEX replaces paper and plastic, the two widely used materials, many things will be saved.</p>
                </div>
              </div>
          </div>
        </div>
    </div>

    <div style="display: none;">
      <div id="companies" class="participant_more">
        <h3><img src="<?php echo get_template_directory_uri() ?>/action/assets/images/en/en-font_campany_title.png" alt="Companies using LIMEX business cards"></h3>
        <ul class="detail_list">
<?php foreach ($fields['en_iIntroduction_company_modal'] as $key => $value): ?>
            <li>
<?php if ($value['link']): ?>
              <a href="<?php echo $value['link'] ?>" target="_blank">
<?php endif ?>
                <div class="company_name<?php echo ($value['company_logo']['url']) ? '' : ' no_image' ?>">
                  <?php echo $value['company_name'] ?>
                </div>
<?php if ($value['link']): ?>
              </a>
<?php endif ?>
            </li>
<?php endforeach ?>
        </ul>
        <p class="annotation">
          * Including companies (organizations) that<br>
          are partially or newly using LIMEX business cards.
        </p>
      </div>
    </div>

    <div class="participant">
      <h3><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_campany_title.png" alt="Companies using LIMEX business cards"></h3>
      <ul>
<?php foreach ($fields['en_iIntroduction_company'] as $key => $value):?>
        <li><?php echo ($value['link'])? '<a href="' . $value['link']  . '" target="_blank">': '' ?><img src="<?php echo $value['company_logo']['url'];?>" alt="<?php echo $value['company_name'];?>"><?php echo ($value['link'])? '</a>' : ''?>></li>
<?php endforeach;?>
      </ul>
      <div class="center participant_btn">
        <a href="#companies" class="companies"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-btn_campany_list.png" alt="more"></a>
      </div>
      <div class="explanation">
        <p><span>* Including companies (organizations) that<br>
          are partially or newly using LIMEX business cards.</span></p>
      </div>
    </div>

    <div class="contents_bottom">
        <div class="wrap">
            <img class="description_2" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_limex_resource.png" alt="With 100 LIMEX business cards, you can save the same amount of water in twenty 500 ml PET bottles.">
        </div>
        <div class="rulet_small">
            <div id="distribution_number" class="titled_box small transition_0" data-num="000233">
              <img class="font-sya abs" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_companies.png" alt="companies">
               <h3><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_card_campany.png" alt="Number of Companies introducing LIMEX business cards"></h3>
               <div class="border">
                   <ul class="rulet_box clearfix">
                       <li data-num = "1">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "2">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "3">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "4">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "5">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "6">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                   </ul>
               </div>
            </div>
        </div>
        <div class="resolve_block medium">
            <div id="introduction_number" class="titled_box medium transition_0" data-num="000411">
              <img class="font-mai abs" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/font_cards.png" alt="LIMEX business cards">
              <img class="arrow middle abs" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_arrow_right_big.png" alt="">
               <h3><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_card_figure.png" alt="Number of LIMEX business cards in circulation"></h3>
               <div class="border">
                   <ul class="rulet_box clearfix">
                       <li data-num = "1">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "2">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "3">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "4">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "5">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "6">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "7">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                   </ul>
               </div>
            </div>
            <div id="warter_number_500" class="titled_box medium transition_0" data-num="000833">
              <img class="font-hon abs" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_500ml.png" alt="500ml PET Bottles">
               <h3><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_save_water.png" style="padding:1px;" alt="SAVED WATER (in numbers of 500 ml PET Bottles)"></h3>
               <div class="border">
                   <ul class="rulet_box clearfix">
                       <li data-num = "1">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "2">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "3">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "4">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "5">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "6">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                       <li data-num = "7">
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_0.png" alt="0"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_1.png" alt="1"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_2.png" alt="2"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_3.png" alt="3"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_4.png" alt="4"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_5.png" alt="5"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_6.png" alt="6"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_7.png" alt="7"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_8.png" alt="8"></p>
                           <p><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/num_9.png" alt="9"></p>
                       </li>
                   </ul>
               </div>
            </div>
        </div>
        <div class="shape clearfix">
            <div id="card" class="ready">
                <div class="large">
                    <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_large.png" alt="">
                    <div class="wrap"><span class="value">1</span>0,000</div>
                </div>
                <ul class="clearfix">
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_card_full.png" alt=""></li>
                </ul>
                <div class="center"><img class="explanation ready" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_attention_card.png" alt="Thousand LIMEX business cards"></div>
            </div>
            <div id="bottle" class="ready">
               <div class="large">
                   <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_large.png" alt="">
                   <div class="wrap"><span class="value">2</span>,000</div>
                </div>
                <ul class="clearfix">
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_full.png" alt=""></li>
                    <li class="off"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_bottle_full.png" alt=""></li>
                </ul>
                <div class="center"><img class="explanation ready" src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-font_attention_bottle.png" alt="Hundred 500 ml PET Bottles"></div>
            </div>
        </div>
    </div>
    <div class="special_block">
    <section class="mod contents">
        <h2 class="center"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font-message.png" alt="MESSAGE"></h2>
        <p class="center txt-description">Messages received from people from various industries</p>
        <div class="mod-message">
          <ul class="mod-message-list clearfix">
            <li class="mod-on fleft bottom"><a class="glay" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-message-0-on.png" width="305" height="205" alt=""></a></li>
            <li class="mod-on fleft left bottom"><a class="glay" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-message-1-on.png" width="305" height="205" alt=""></a></li>
            <li class="mod-on fleft left bottom"><a class="glay" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-message-2-on.png" width="305" height="205" alt=""></a></li>
            <li class="mod-on fleft bottom"><a class="glay" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-message-3-on.png" width="305" height="205" alt=""></a></li>
            <li class="mod-on fleft left bottom"><a class="glay" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-message-4-on.png" width="305" height="205" alt=""></a></li>
            <li class="mod-on fleft left bottom"><a class="glay" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-message-5-on.png" width="305" height="205" alt=""></a></li>
            <li class="mod-on fleft bottom"><a class="glay" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-message-6-on.png" width="305" height="205" alt=""></a></li>
            <li class="mod-on fleft left bottom"><a class="glay" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-message-8-off.png" width="305" height="205" alt=""></a></li>
            <li class="mod-on fleft left bottom"><a class="glay" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/en/en-img-message-9-off.png" width="305" height="205" alt=""></a></li>
        </div>
    </section>
    <section class="special-story">
        <h2 class="center"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font_special_story.png" alt="SPECIAL STORY"></h2>
        <p class="center txt-description">Messages from TBM members spreading LIMEX worldwide</p>
        <ul class="special-story-list clearfix">
<?php if ($specials): ?>
<?php foreach ($specials as $special): ?>
<?php
$fields = get_fields($special->ID);
$post_name = get_page($special->ID)->post_name;
?> 
          <li>
            <a class="glay" href="/en-special/<?php echo $post_name ?>"><img src="<?php echo ($fields['list_image']) ? $fields[ 'list_image'] : $fields['main_visual'];?>" alt=""></a>
          </li>
<?php endforeach ;?>
<?php endif;?>
        </ul>
    </section>
<!--
    <section class="mod contents last">
        <h2 class="center"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font-topics.png" alt="TOPICS"></h2>
        <p class="center txt-description">世界各国の環境問題やLIMEXの導入事例を紹介します。</p>
        <div class="mod-topics center">
          <ul class="mod-topics-list clearfix">
<?php if ($topics): ?>
<?php foreach ($topics as $topic): ?>
<?php
$image_url = wp_get_attachment_image_src($topic->list_image, true);
$custom_fields = get_post_custom($topic->ID);
$tag_id = $custom_fields['category'];
$tag = get_term_by('id', $tag_id[0], 'topics');
$post_name = get_page($topic->ID)->post_name;
?>
            <li>
            <div class="mod-topics-block">
              <a href="/topics/<?php echo $post_name ?>">
                <div class="img-block"><img src="<?php echo $image_url[0]; ?>" alt=""></div>
                <h4><?php echo $tag->name ?></h4>
                <div class="future">
                  <?php echo $topic->list_description ?>
                </div>
                <div class="readmore">
                  <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/detail-btn.png" alt="詳細を見る">
                </div>
              </a>
            </div>
            </li>
<?php endforeach ?>
<?php endif ?>
          </ul>
        </div>
    </section>
-->
    </div>
    <footer>
      <div class="footArea">
        <ul>
            <li><a target="_blank" href="https://twitter.com/intent/tweet?text=%E6%AC%A1%E4%B8%96%E4%BB%A3%E7%B4%A0%E6%9D%90LIMEX%20%E6%A0%AA%E5%BC%8F%E4%BC%9A%E7%A4%BETBM&url=http%3A%2F%2Ftb-m.com%2F&original_referer=http%3A%2F%2Fwww.tb-m.com%2F"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_tw.png" alt="Twitter"></a></li>
            <li><a target="_blank" href="https://www.facebook.com/tbm.limex"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_fb.png" alt="Facebook"></a></li>
            <li><a target="_blank" href="https://www.youtube.com/channel/UCIvfa-J0VGeLl71Aeg6sWRg"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_yt.png" alt="youtube"></a></li>
        </ul>
      </div>
      <p class="copy"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font_copy.png" alt="Copyright TBM Co.,LTD All Rights Reserved"></p>
    </footer>
    </div>
    <div id="action-modal">
      <div id="modal-filter"></div>
      <div id="modal-contaner">
        <div id="modal-controller">
          <a href="javascript:void(0)" id="modal-next"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/top_story_btn_next.png"></a>
          <a href="javascript:void(0)" id="modal-prev"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/top_story_btn_prev.png"></a>
          <a href="javascript:void(0)" id="modal-close"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/top_story_btn_close.png"></a>
        </div>
        <div class="section-wrap">
          <section id="article0" num="0" >
            <div class="separate left">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-message-face-0.png" alt="">
            </div>
            <div class="separate right tse-scrollable wrapper">
              <div class="tse-content">
              <p class="title">LIMEXの名づけ親として、一言</p>
              <p>木や草を原料とし生産過程で大量の水を必要とする“紙”は、国によりその生産方式の違いはあれ、古くから人間生活にとって不可欠な素材だった。<br />
              しかし、木や草も地球上の生物の生存にとって不可欠な有限の資源だ。したがって、工業化の進展に伴う人類の生活水準の向上がもたらす“紙”需要の急増を“深刻な地球的危機”とする声は、すでに前世紀後半から各国の識者の間で次第に高まってきていた。<br />
・・・それだけに、石灰（lime）を原料とし水を使わず、しかも安価に大量生産できる“限りなく紙”に近い新素材LIMEXの開発は、今世紀の行く末に対する画期的貢献として世界から高く評価されるだけの価値があるに違いない。</p>
              <p class="author">野田 一夫</p>
              </div>
            </div>
          </section>
          <section id="article1" num="1" >
            <div class="separate left">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-message-face-1.png" alt="">
            </div>
            <div class="separate right tse-scrollable wrapper">
              <div class="tse-content">
              <p>炭酸カルシウム粒子60％とポリプロピレン40％から比重１．０、厚さ100μｍのLIMEXが製造されている。TBMは世界に類を見ない合成紙の高い目標と生産可能性が評価され経済産業省から重要先端技術実証・評価制度に認定され補助金をいただいたベンチャー企業である。2015．2.7に白石工場の全設備が竣工した。混錬、Ｔダイ押し出し、延伸、シート化、スリット、機能性膜コーティング、断裁の各工程を完璧に進めなければならない。生産現場には多くの技術的課題・問題が存在し、それらは掛け算的に継続して製造されていく。掛け算とは一つの工程が十分でない品質になると、出来上がったLIMEXの品質と性能は、その一つの工程の品質に劣化してしまう。すなわち一つの工程の不十分な品質に影響され、固体微粒子を６０％含有し熱可塑性ポリプロピレン４０％から、比重１．０、厚さ100μｍのLIMEXを製造できなくなってしまう。つまり多くの生産工程の課題を解決し最高レベルの品質を実現することが必要である。<br /><br />

              　竣工後、生産・開発中心に、それら多数の問題・課題の解決努力がなされ、2ヶ月後2015年3月には、ミラノ万博向けポスター等の他の生産・開発・営業・管理部門のベクトルを合わせた力の結集により具体化できた。また1年後2016．３にまた一段高レベルの品質・性能であるミラノ万博総集編用LIMEX製造でも生産・開発・営業の力の結集がなされた。これらLIMEX製品づくりは、大袈裟に言えば危機管理的経営により実現できた。この実績から関係者による業務の迅速な推進と、今何が最重要であるかを体で理解することができ、一人一人の役割と使命をやり遂げることの大切さを学び取った。しかしながらLIMEXは未だ富士山やエベレストの世界一の頂上に立ってはいない。<br /><br />

              日本や世界では、年間4億トンもの紙が製造され、パルプの抄紙プロセスでは100倍もの水を必要とする。日本にも世界にも無尽蔵に埋蔵されている鉱物炭酸カルシウム微粉60％以上とオレフイン残分でほとんど水を使わずにLIMEX紙は製造できる。今後世界一を目指すには、新しい価値機能の創造と用途開発、また水の価値を徹底して告知することである。<br /><br />

              　２つの材料の真比重から、LIMEXの体積分率は炭カル33％、ポリプロ66％であり、炭カル比率の向上とともに、ＴＢＭの高い理念の具現化できるさらなる薄膜化の可能性が広がっている。そのためには、多くの生産工程の課題を解決し最高レベルの品質・性能を実現することが必要である。これには炭カルの微粒子化と表面改質、ポリプロをはじめ材料の検討などにより、薄膜LIMEX製品化に挑戦していく。<br /><br />

              　そのためには物質科学、高分子科学、複合化科学の異質な科学知識の取得と加工工学、精密機械工学、制御工学等必要な生産技術を修めた研究技術者の叡智を結集することが必要である。生産サイドは歩留まりを含めた変動費と固定費の製造全部原価削減に挑戦しLIMEXの競争力を上げていく。世界一の目標を達成するまで、仮説・実行・検証サイクルを粘り強く回し続け、100ｍ走のスピードで42㎞のマラソンを走りきらなければならない。<br /><br />
              　
              　その結果、差別化独自性ある夢のLIMEX製品と生産確立でき、高い品質と性能のLIMEXの具現化されることで、世界中の大切な水資源を保護する類稀な新規合成紙技術の創造ができる。この挑戦を明るく楽しくやることで実現できると信じている。</p>
              <p class="author">今村 哲也</p>
              </div>
            </div>
          </section>
          <section id="article2" num="2">
            <div class="separate left">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-message-face-2.png" alt="">
            </div>
            <div class="separate right tse-scrollable wrapper">
              <div class="tse-content">
              <p>
              I met LIMEX President Yamazaki-san in Singapore about 2 years ago.  I am excited about this stone paper technology, their new potential business applications and benefits to society. <br /><br />

              Having arrived in Tokyo 6 months ago, I was happy to learn that LIMEX opened their factory in Japan in 2015 and was one of the recipients in the recent 2016 Japan Venture Award. <br /><br />

              I am interested to learn more about this new technology and explore potential business alliances between Singapore companies and LIMEX to proliferate this technology globally and develop new business applications using LIMEX technology.
              </p>
              <p class="author">Sean ONG</p>
            </div>
            </div>
          </section>
          <section id="article3" num="3">
            <div class="separate left">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-message-face-3.png" alt="">
            </div>
            <div class="separate right tse-scrollable wrapper">
             <div class="tse-content">
              <p>
             原始時代、洞窟に壁画を描いていた人類は、その後竹や椰子の葉、動物の皮などに文字を書き、古代エジプトでパピルスが発明されて以来、植物繊維を原材料に使う紙の本質は、数千年もの間変わる事はありませんでした。 <br /><br />

              十年近く前、石で紙を作るという、途方 もない夢を追いかけている企業に出会いました。当時はまだ技術やコストの面で大変なご苦労があったようですが、「地球の環境を守るためにも、必ずこの仕事 はやり遂げる」と言っていた、社長をはじめ社員たちの希望に満ちた眼差しは、未だに忘れられません。 <br /><br />

              LIMEXは、地球上に豊富に存在する石灰石を使用し、水源を汚染せずに紙を作る新たな技術です。革命的なこの新素材が世界に広まっていく事を心より願い、応援の言葉とさせていただきます。
              </p>
              <p class="author">Balint Rei KOSA/バーリン・レイ　コーシャ</p>
            </div>
            </div>
          </section>
          <section id="article4" num="4">
            <div class="separate left">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-message-face-4.png" alt="">
            </div>
            <div class="separate right tse-scrollable wrapper">
            <div class="tse-content">
              <p>もしも地球の直径が１ｍだったら？<br />
                実は、存在する全ての水を集めても約600mlにしかなりません。その大半は海水で、人類が利用できる淡水は約5ml、小さじ1杯だけです。<br />
                この僅かな水をめぐって人類は紛争を繰り返してきました。今も10億人以上が水不足に悩まされていると言われていますし、今後の人口増加や経済発展・都市化の進展で状況はさらに悪化すると予想されています。<br />
                その一方で、我々は飲み水や農業に加えて、工業製品の生産にも膨大な量の水を利用してきました。この状況を劇的に改善できる新素材として生まれたのが、水を全く使わず生産できるLIMEXです。<br />
                地球のため、人類のため、次の世代のため、水資源の保護は喫緊の課題。今まで使っていた物をLIMEX製品に切り替えるだけで、世界の水問題解決の一助になるとしたら、こんな素晴らしい事はありません。<br />
                ここにこそ、LIMEXを使い、広める意義があると確信しています。
              </p>
              <p class="author">加藤 公一</p>
              </div>
            </div>
          </section>
          <section id="article5" num="5">
            <div class="separate left">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-message-face-5.png" alt="">
            </div>
            <div class="separate right tse-scrollable wrapper">
              <div class="tse-content">
                <p>生活のシーンでＬＩＭＥＸがアチコチで目に付く時代は何時だろうか？<br /><br />

                  新素材の宿命として、製品化に苦労を重ねているのはやむを得ないが、一日も早くその日の来ることを望んでいる。<br />
                  凡そこの世に存在する物質は、長所と短所を併せ持つ。<br />
                  ＬＩＭＥＸは、耐候性に優れ、印刷すると美しい。<br />けれどもちょっと重いかもしれない。何しろ石灰石が原料だから。<br />
                  でも、手にとると、しっとりとして馴染みやすい。<br /><br />

                  ただそれだけではお客様には買って頂ける商品になるとは限らない。<br />そこに、製品開発・商品開発という猛烈な研究が求められる。<br />続けよ、研究を。<br />がんばれ、開発を。<br />限りない苦労とは言え、お客様が笑顔で評価して下さる日が来る日は遠くない。
                </p>
                <p class="author">水野 勝</p>
            </div>
            </div>
          </section>
          <section id="article6" num="6">
            <div class="separate left">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-message-face-6.png" alt="">
            </div>
            <div class="separate right tse-scrollable wrapper">
              <div class="tse-content">
                <p>水を使用しない紙の代替品？？<br />商社勤務の後、静岡で段ボール原紙製造会社の仕事に携わり、製造工程における水の重要性と使用量の多さを目の当たりにしておりましたので、‘その’水を使用せずに石灰石を主原料として紙の代替となる‘LIMEX’には、今後の新規用途開発・展開に大いなる可能性を感じています。<br />又、副原料とて容器リサイクル委託費用の支払い対象となる、石油化学の代表的製品である合成樹脂（ポレエチレン、ポリプロピレン）が使用されますが、配合比率面で石灰石が過半を占めていることから、この処理費用の支払いが不要となる。これは、環境面への低負荷と並ぶ価格競合面での差別化に繋がります。<br /><br />
                今後の事業拡大には、ユーザーとの共同開発による新規需要の開拓、独自の市場の形成、と同時に海外展開も視野に入れた活動が必須となりますが、その将来性に夢が膨らみます。
                </p>
                <p class="author">八田 賢一</p>
            </div>
            </div>
          </section>
          <section id="article7" num="7">
            <div class="separate left">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-message-face-9.png" alt="">
            </div>
            <div class="separate right tse-scrollable wrapper">
              <div class="tse-content">
                <p>人間の身体は約60%%が水から成り立っており、毎日 2〜3ℓの水分の接種が必要です。<br />人間にとってだけでなく、地球上のあらゆる動物、植物などの生物が水を 必要としています。<br />地球には大きな海がありますが、ほとんどの生物にとって海水は使える水ではなく、地球全体では人間が使える 水はほんのわずかです。<br /> その大事な水を人間はいろいろな形で消費しています。<br />例えば紙。木材から紙を作るには製品の100倍の水が 必要で、1kgの紙を作るには約100ℓの水を消費します。<br />日本は比較的水は豊富な国ですが、水を消費している あらゆる物を輸入しています。<br /> LIMEX。石灰石を使い、水を原料としない紙代替素材ができました。 <br />名刺1箱をLIMEXに切り替えるだけで、人類が使える 水を10ℓ削減できます。 <br />自分達の利益だけを考えるのはなく、将来や世界のためにLIMEXを広げていきたい。
                </p>
                <p class="author">坂本 孝治</p>
            </div>
            </div>
          </section>
          <section id="article8" num="8">
            <div class="separate left">
              <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/img-message-face-8.png" alt="">
            </div>
            <div class="separate right tse-scrollable wrapper">
              <div class="tse-content">
                <p>水と緑は豊かな自然環境の象徴であると同時に、我々の健康で文化的な生活を直接・間接に支えています。<br />水は多すぎても洪水をもたらし、経済のグローバル化によってその影響は地域を越えて世界中に波及しますし、旱魃による食料不足が社会不安をもたらし、テロを引き起こすという説まであります。<br />ミレニアム開発目標の実現に向けた国際的な取り組みによって飲む水にも困窮するような人々の数は、1990年から2010年に半減しましたが、それでも2015年の時点で7億人弱の人々が毎日何時間もかけて水汲みをしなければならない状況に置かれています。<br />また、森林は温室効果ガスである二酸化炭素を吸収し、土壌表面の侵食を抑え、森林土壌は水質を浄化したり水循環を穏やかにしたりしてくれています。<br />一方で、人類の文明の勃興を智の持続性を支えてきた紙は、その製造に大量の水と木材を必要とします。<br />水や森林が不足しがちな地域で紙を持続的に自給するためには、新たな発想に基づく、紙に代わる新たな素材の開発が必要でしょう。
                </p>
                <p class="author">沖 大幹</p>
            </div>
            </div>
          </section>
        </div>
      </div>
    </div>
		<!-- アクセス解析 -->
		<script type="text/javascript">
			
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-16758707-1']);
			_gaq.push(['_trackPageview']);
			
			(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
			
		</script>
		<!-- // アクセス解析 -->
</body>
<script>
  $(".future").alignHeight(3);
  $('.wrapper').TrackpadScrollEmulator();
</script>
</html>