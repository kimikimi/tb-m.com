<?php

/*
 * This file is part of the CRUD Admin Generator project.
 *
 * Author: Jon Segador <jonseg@gmail.com>
 * Web: http://crud-admin-generator.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../src/app.php';
require_once __DIR__.'/configs/index.php';

// api
$app->match('/api/get_configs', function (Symfony\Component\HttpFoundation\Request $request) use ($app) { 
    $find_sql = "SELECT * FROM `configs` ORDER BY id DESC LIMIT 1";
    $rows_sql = $app['db']->fetchAll($find_sql, array());
    $configs = array();
    if (isset($rows_sql[0]))
    {
        foreach ($rows_sql[0] as $key => $row)
        {
            if ($key == 'introduction_number' || $key == 'warter_number_500')
            {
                $digit = '%07d';
            }
            else
            {
                $digit = '%06d';
            }

            $configs[$key] = sprintf($digit, $row);
        }
    }
    $json = json_encode($configs);
    header('Content-Type: text/javascript; charset=utf-8');
    echo $json;
    exit();
});

$app->run();