<?php

/*
 * This file is part of the CRUD Admin Generator project.
 *
 * Author: Jon Segador <jonseg@gmail.com>
 * Web: http://crud-admin-generator.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


require_once __DIR__.'/../../vendor/autoload.php';
require_once __DIR__.'/../../src/app.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;


$http_auth = function (){
    switch (true) {
    case !isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']):
    case $_SERVER['PHP_AUTH_USER'] !== 'admin':
    case $_SERVER['PHP_AUTH_PW']   !== 'test':
        header('WWW-Authenticate: Basic realm="Enter username and password."');
        header('Content-Type: text/plain; charset=utf-8');
        die('このページを見るにはログインが必要です');
    }
};

// dashborad
$app->match('/', function () use ($app, $http_auth) {
    $http_auth();
    return $app['twig']->render('ag_dashboard.html.twig', array());

})->bind('dashboard');

$app->match('/configs/list', function (Symfony\Component\HttpFoundation\Request $request) use ($app, $http_auth) {  
    $http_auth();
    $start = 0;
    $vars = $request->query->all();
    $qsStart = (int)$vars["start"];
    $search = $vars["search"];
    $order = $vars["order"];
    $columns = $vars["columns"];
    $qsLength = (int)$vars["length"];
    
    if($qsStart) {
        $start = $qsStart;
    }    

    $index = $start;   
    $rowsPerPage = $qsLength;
       
    $rows = array();
    
    $searchValue = $search['value'];
    $orderValue = $order[0];
    
    $orderClause = "";
    if($orderValue) {
        $orderClause = " ORDER BY ". $columns[(int)$orderValue['column']]['data'] . " " . $orderValue['dir'];
    }

    $table_columns = array(
		'id', 
		'project_number', 
		'warter_number', 
		'distribution_number', 
		'introduction_number', 
		'warter_number_500', 

    );
    
    $table_columns_type = array(
		'int(11)', 
		'int(11)', 
		'int(11)', 
		'int(11)', 
		'int(11)', 
		'int(11)', 

    );    
    
    $whereClause = "";
    
    $i = 0;
    foreach($table_columns as $col){
        
        if ($i == 0) {
           $whereClause = " WHERE";
        }
        
        if ($i > 0) {
            $whereClause =  $whereClause . " OR"; 
        }
        
        $whereClause =  $whereClause . " " . $col . " LIKE '%". $searchValue ."%'";
        
        $i = $i + 1;
    }
    
    $recordsTotal = $app['db']->executeQuery("SELECT * FROM `configs`" . $whereClause . $orderClause)->rowCount();
    
    $find_sql = "SELECT * FROM `configs`". $whereClause . "ORDER BY id desc LIMIT 1";

    $rows_sql = $app['db']->fetchAll($find_sql, array());

    foreach($rows_sql as $row_key => $row_sql){
        for($i = 0; $i < count($table_columns); $i++){

		if( $table_columns_type[$i] != "blob") {
				$rows[$row_key][$table_columns[$i]] = $row_sql[$table_columns[$i]];
		} else {				if( !$row_sql[$table_columns[$i]] ) {
						$rows[$row_key][$table_columns[$i]] = "0 Kb.";
				} else {
						$rows[$row_key][$table_columns[$i]] = " <a target='__blank' href='menu/download?id=" . $row_sql[$table_columns[0]];
						$rows[$row_key][$table_columns[$i]] .= "&fldname=" . $table_columns[$i];
						$rows[$row_key][$table_columns[$i]] .= "&idfld=" . $table_columns[0];
						$rows[$row_key][$table_columns[$i]] .= "'>";
						$rows[$row_key][$table_columns[$i]] .= number_format(strlen($row_sql[$table_columns[$i]]) / 1024, 2) . " Kb.";
						$rows[$row_key][$table_columns[$i]] .= "</a>";
				}
		}

        }
    }    
    
    $queryData = new queryData();
    $queryData->start = $start;
    $queryData->recordsTotal = $recordsTotal;
    $queryData->recordsFiltered = $recordsTotal;
    $queryData->data = $rows;

    return new Symfony\Component\HttpFoundation\Response(json_encode($queryData), 200);
});

$app->match('/configs', function () use ($app, $http_auth) {
    $http_auth();
	$table_columns = array(
		'id', 
		'project_number', 
		'warter_number', 
		'distribution_number', 
		'introduction_number', 
		'warter_number_500', 

    );

    $primary_key = "id";	

    return $app['twig']->render('admin/configs/list.html.twig', array(
    	"table_columns" => $table_columns,
        "primary_key" => $primary_key
    ));
        
})
->bind('configs_list');


$app->match('/configs/edit/{id}', function ($id) use ($app, $http_auth) {
    $http_auth();
    $find_sql = "SELECT * FROM `configs` WHERE `id` = ?";
    $row_sql = $app['db']->fetchAssoc($find_sql, array($id));

    if(!$row_sql){
        $app['session']->getFlashBag()->add(
            'danger',
            array(
                'message' => 'Row not found!',
            )
        );        
        return $app->redirect($app['url_generator']->generate('configs_list'));
    }

    
    $initial_data = array(
		'project_number' => $row_sql['project_number'], 
		'warter_number' => $row_sql['warter_number'], 
		'distribution_number' => $row_sql['distribution_number'], 
		'introduction_number' => $row_sql['introduction_number'], 
		'warter_number_500' => $row_sql['warter_number_500'], 

    );


    $form = $app['form.factory']->createBuilder('form', $initial_data);


	$form = $form->add('project_number', 'text', array('required' => true));
	$form = $form->add('warter_number', 'text', array('required' => true));
	$form = $form->add('distribution_number', 'text', array('required' => true));
	$form = $form->add('introduction_number', 'text', array('required' => true));
	$form = $form->add('warter_number_500', 'text', array('required' => true));


    $form = $form->getForm();

    if("POST" == $app['request']->getMethod()){

        $form->handleRequest($app["request"]);

        if ($form->isValid()) {
            $data = $form->getData();

            $update_query = "UPDATE `configs` SET `project_number` = ?, `warter_number` = ?, `distribution_number` = ?, `introduction_number` = ?, `warter_number_500` = ? WHERE `id` = ?";
            $app['db']->executeUpdate($update_query, array($data['project_number'], $data['warter_number'], $data['distribution_number'], $data['introduction_number'], $data['warter_number_500'], $id));            


            $app['session']->getFlashBag()->add(
                'success',
                array(
                    'message' => 'configs edited!',
                )
            );
            return $app->redirect($app['url_generator']->generate('configs_edit', array("id" => $id)));

        }
    }

    return $app['twig']->render('admin/configs/edit.html.twig', array(
        "form" => $form->createView(),
        "id" => $id
    ));
        
})
->bind('configs_edit');
