<?php
add_theme_support( 'title-tag' );

add_filter( 'wp_title', 'my_extend_title' );
function my_extend_title( $title ){

	preg_match('/\/(.*?)\//', $_SERVER['REQUEST_URI'],$matches);
	$page_array = array(
		'form' => 'CONTACT お問い合わせ | 次世代素材LIMEX 株式会社TBM',
		'order' => 'ORDER ご注文 | 次世代素材LIMEX 株式会社TBM',

		);
	if(isset($matches[1]) && array_key_exists($matches[1],$page_array)){
		$title  = $page_array["$matches[1]"];
		return $title;
	}
	return $title;
}

function get_directory_name($lang = null){
	$request_uri = explode('/', $_SERVER['REQUEST_URI']);
if($lang == 'en'){
	$directory_name = $request_uri[2];
}else{
	$directory_name = $request_uri[1];
}
	//親階層ディレクトリ名取得
	if($directory_name == ''){
		$directory_name = 'top';
	}
	return $directory_name;
}

function get_head_js($directory_name){
$request_uri = explode('/', $_SERVER['REQUEST_URI']);
	//body のclass名種等
	$heder_js = '';
	if($request_uri[1] == 'en'){//enテンプレート時
        if($directory_name == 'form'){
                $heder_js .= '		<script type="text/javascript" src="'. get_template_directory_uri() .'/js/en/common.js" charset="utf-8"></script>"' . "\r\n";
                return  $heder_js;
        }
        if(file_exists(TEMPLATEPATH .'/js/'. $directory_name)){
            $files = scandir(TEMPLATEPATH .'/js/'. $directory_name);
            foreach ($files as $key => $value) {
                if(preg_match('/\.js$/', $value)){
                    $heder_js .= '		<script type="text/javascript" src="'. get_template_directory_uri() .'/js/en/' . $directory_name . '/'. $value . ' " charset="utf-8"></script>
            ';
                }
            }
        }
    } else {
        if($directory_name == 'form'){
                $heder_js .= '		<script type="text/javascript" src="'. get_template_directory_uri() .'/js/common.js" charset="utf-8"></script>' . "\r\n";
                return  $heder_js;
        }
		if ($directory_name == 'order') {
			$heder_js .= '		<link rel="stylesheet" href="'.get_template_directory_uri().'/css/card.css">'. "\r\n";
			$heder_js .= '		<script type="text/javascript" src="' . get_template_directory_uri() . '/js/jquery.bxslider.min.js" charset="utf-8"></script>' . "\r\n";
			$heder_js .= '		<script type="text/javascript" src="' . get_template_directory_uri() . '/js/card.js" charset="utf-8"></script>' . "\r\n";
			return $heder_js;
		}
        if(file_exists(TEMPLATEPATH .'/js/'. $directory_name)){
            $files = scandir(TEMPLATEPATH .'/js/'. $directory_name);
            foreach ($files as $key => $value) {
                if(preg_match('/\.js$/', $value)){
                    $heder_js .= '		<script type="text/javascript" src="'. get_template_directory_uri() .'/js/' . $directory_name . '/'. $value . ' " charset="utf-8"></script>
            ';
                }
            }
        }
    }
	return $heder_js;
}

function get_body_classname(){
global $template;
$template_name = basename($template, '.php');
	$request_uri = explode('/', $_SERVER['REQUEST_URI']);
	//body のclass名種等
	if($request_uri[1] == 'en'){//enテンプレート時
		if($request_uri[1] == 'contact_lime-x'){//contactの時の例外処理
			$class_name = 'contact';
		}elseif($template_name == 'en-company-story-list'){
			$class_name = 'story-list';
		}elseif(isset($request_uri[3]) && $request_uri[3] != '' && !preg_match('/\?/', $request_uri[3]) && $request_uri[3] != 'page') {
			$class_name = $request_uri[3];
		}elseif(isset($request_uri[2])){
			$class_name = $request_uri[2];
		}elseif($request_uri[2] == 'contact_lime-x'){
			$class_name = 'contact';
		}else{
			$class_name = '';
		}
	}else{

		if(isset($request_uri[2]) && $request_uri[2] == 'contact_lime-x'){//contactの時の例外処理
			$class_name = 'contact';
		}elseif($template_name == 'company-story-index'){
			$class_name = 'story-list';
		}elseif(isset($request_uri[2]) && $request_uri[2] != '' && !preg_match('/\?/', $request_uri[2]) && $request_uri[2] != 'page') {
			$class_name = $request_uri[2];
		}elseif(isset($request_uri[1])){
			$class_name = $request_uri[1];
		}else{
			$class_name = '';
		}
	}
	return $class_name;
 }

 //グロナビcurrent判定
function is_current($nav_name,$directory_name){
	if($nav_name == $directory_name){
		return  true;
	}
}
 //ニュースcurrent判定

function is_news_current($nav_name,$category_name){
	if($nav_name == $category_name){
		return  true;
	}
}

function get_archives_by_fiscal_year( $args = '' ,$post_type = 'post') {
    global $wpdb, $wp_locale;
    $defaults = array (
        'post_type' => $post_type,
        'limit' => '',
        'format' => 'html',
        'before' => '',
        'after' => '',
        'show_post_count' => false,
        'echo' => 1
    );
    $r = wp_parse_args( $args, $defaults );
    extract ( $r, EXTR_SKIP );
    if ( '' != $limit ) {
        $limit = absint( $limit );
        $limit = ' LIMIT ' . $limit;
    }


    if($args["category_name"]){
    	$term_taxonomy = get_term_by('slug',$args["category_name"],'category');
    	$args_query = " AND wp_term_relationships.term_taxonomy_id = $term_taxonomy->term_id";
    }

    $arcresults = (array) $wpdb->get_results(
        "SELECT YEAR(post_date) AS `year`, COUNT(ID) AS `posts`
		FROM wp_posts
		inner join wp_term_relationships
		on wp_term_relationships.object_id = wp_posts.id
        WHERE wp_posts.post_type = '$post_type' AND
        wp_posts.post_status = 'publish'
        $args_query
        GROUP BY YEAR(post_date)
        ORDER BY wp_posts.post_date DESC
        $limit"
    );
    return $arcresults;
}

function get_category_titile($tag,$lang = 'jp'){
	if($lang == 'en'){
		$category_array = array(
			'press_release' => 'Press Release',
			'topics' => 'Topics',
			'media' => 'Media',
			);
	}else{
		$category_array = array(
			'press_release' => 'プレスリリース',
			'topics' => 'お知らせ',
			'media' => 'メディア掲載',
			);
	}
	if($category_array[$tag]){
		return $category_array[$tag];

	}else{
		return false;
	}
}

function no_en_page(){
global $template;
$template_file_name = basename($template, '.php');
preg_match('/\/(.*?)\//', $_SERVER['REQUEST_URI'],$matches);
	$no_en_page = array(
		'company-profile',
		'company-story',

		);
	if(in_array($template_file_name,$no_en_page) || (isset($matches[1]) && $matches[1] == 'form')  || (isset($matches[1]) && $matches[1] == 'order')){
		return true;
	}else{
		return false;
	}

}

/**
 * NEWSページへのリンクを返す
 *
 * @param  integer $perPage NEWSページの1ページあたりの表示件数
 * @return string $linkUrl リンクURL
 */
function getLinkUrl($query,$type = '',$perPage = 20) {
	$currentId = get_the_ID();
	if($type == ''){
		$linkUrl = '/news/';
	}else{
		$linkUrl = '/' . $type . '/news/';
	}
	foreach ($query->posts as $key => $post) {
		if ($post->ID == $currentId) {
			$page = floor($key / $perPage);
			if ($page == 0) {
				$linkUrl .= '#' . $currentId;
			} else {
				$page += 1;
				$linkUrl .= 'page/' . $page . '/#' . $currentId;
			}
		}
	}
	return $linkUrl;
}

/* 存在しないページを指定された場合は 404 ページを表示する */
/*メインページ・シングルページ・アーカイブ（月別）・固定ページ 以外の指定の場合 404 ページを表示する */
/*function redirect_404() {
  if(is_home() || is_single() || is_month() || is_page() ) return; include(TEMPLATEPATH . '/404.php'); exit;
}
add_action('template_redirect', 'redirect_404');*/
