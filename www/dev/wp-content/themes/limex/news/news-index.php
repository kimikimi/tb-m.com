<?php
/**
 * Template Name: news-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$class_name = 'news';
get_header(); ?>
<?php
$paged = get_query_var('paged');

$args = array( 
	'post_status'=>'publish',
	'paged' => $paged,
	'posts_per_page' => 1, 
);
$category = isset($_GET['cat'])? $_GET['cat'] : '';

if(isset($_GET['cat'])){
	$args['category_name']  = $_GET['cat'];
}
if(isset($_GET['y'])){
	$args['year']  = $_GET['y'];
}
?>
<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/company/aboutus/">会社概要</a></li>
				<li><a href="/company/profile/">経営陣プロフィール</a></li>
				<li><a href="/company/identity/">企業理念体系</a></li>
				<li><a href="/company/history/">沿革</a></li>
				<li><a href="/company/message/" class="cur">創設者メッセージ</a></li>
				<li><a href="/company/story/">創業ストーリー</a></li>
				<li><a href="/company/access/">アクセス</a></li>
				<li><a href="/company/award/">受賞歴</a></li>
			</ul>
		</div>
	</div>
	<h3>創設者メッセージ</h3>
	<div class="wrapper">
<!-- 		<div class="tabWrap">
			<ul id="tab">
				<li><a href="#data1" class="cur">全て</a></li>
				<li><a href="#data2">2015</a></li>
				<li class="last"><a href="#data3">2014以前</a></li>
			</ul>
		</div>
 -->

		<div id="data1" class="data">
<?php
$the_query = new WP_Query( $args );
// ループ
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();
?>
        <article><a name="news_160223-01"></a>
				<p class="date"><?php the_time('Y/m/d'); ?></p>
				<h3><?php the_title(); ?></h3>
				<p class="text01"><?php the_content(); ?></p>
			</article>
<?php
endwhile;
endif;
?>
		</div><!-- /#data1.data -->

			<?php if(function_exists('wp_pagenavi')) {
				wp_pagenavi(array('query'=>$the_query));
			} ?>
	</div>


</section><!-- /#body -->

<?php
get_footer();
