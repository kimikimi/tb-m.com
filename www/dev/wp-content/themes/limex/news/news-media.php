<?php
/**
 * Template Name: news-media
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$class_name = 'news';
get_header(); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>NEWS</h2>
		</div>
	</div>
	<div id="subNav02">
	<div id="subNav02-01" class="pr01">
	<ul>
		<li class="nav1"><a href="index.html">ALL</a></li>
		<li class="nav2"><a href="topics.html">TOPICS</a></li>
		<li class="nav3"><a href="media.html" class="cur">MEDIA</a></li>
	</ul>
	</div>
	</div>
	<div class="wrapper">
		<div id="data1" class="data">
<article><a name="news_160103-01"></a>
				<p class="date">2016/01/03</p>
				<h3>「日本経済新聞」に掲載：<br>大震災から5年　再建への工程</h3>
				<p class="text01">2016年1月3日 「新年に考える（被災地産業芽吹きの時）」のコーナーにて、山﨑が多賀城市役所で開かれた立地協定式で述べた抱負が記事として掲載されました。 <br>ご覧いただけますと幸いです。</p>
<p><a href="http://www.nikkei.com/article/DGXLASFB21HC8_S5A221C1TZG000/" target="_blank">詳細はこちら＞＞</a></p>
			</article>

			<article><a name="news_150225-01"></a>
				<p class="date">2015/02/25</p>
				<h3>経済情報誌「仙台経済界」に掲載：<br>「市場を拓く在仙12社　突破力」で特集</h3>
				<p class="text01">仙台圏の経済情報誌「仙台経済界」にて、弊社の新しい工場とLIMEXの取材記事が掲載されました。取締役会長でLIMEX開発者の角と、工場長 細谷のインタビューも掲載されております。<br>ぜひご覧いただけますと幸いです。</p>
				<p><a href="http://www.senkey.co.jp/senkei.htm" target="_blank">「仙台経済界」バックナンバーのご購入はこちら＞＞</a></p>
			</article>
            
			<article><a name="news_150413-01"></a>
				<p class="date">2015/04/13</p>
				<h3>「日本経済新聞」に掲載：<br>石灰石で紙代替素材  宮城に工場、年6000トン</h3>
				<p class="text01">2015年4月13日 新興・中小企業コーナーにて、弊社の記事が掲載されました。
LIMEXをたくさんの方々に知って頂きたく、ぜひご覧頂けますと幸いです。</p>
			</article>

			<article><a name="news_150225-01"></a>
				<p class="date">2015/02/25</p>
				<h3>経済情報誌「仙台経済界」に掲載：<br>「市場を拓く在仙12社　突破力」で特集</h3>
				<p class="text01">仙台圏の経済情報誌「仙台経済界」にて、弊社の新しい工場とLIMEXの取材記事が掲載されました。取締役会長でLIMEX開発者の角と、工場長 細谷のインタビューも掲載されております。<br>ぜひご覧いただけますと幸いです。</p>
				<p><a href="http://www.senkey.co.jp/senkei.htm" target="_blank">「仙台経済界」バックナンバーのご購入はこちら＞＞</a></p>
			</article>



			<article><a name="news_1408-01"></a>
				<p class="date">2014/08</p>
				<h3>「情熱社長～情熱的なベンチャー企業社長のメッセージ～」<br>に掲載しました</h3>
				<p class="text01">日本最大級の経営者インタビューサイト「情熱社長」に、ストーンペーパーの可能性や、LIMEXにかける想いなど、山﨑からのメッセージを掲載しております。ご覧いただけますと幸いです。</p>
				<a href="http://www.jonetu-ceo.com/2014/08/22762/" target="_blank">「情熱社長」詳細はこちら＞＞</a>
			</article>

			<article><a name="news_1404-01"></a>
				<p class="date">2014/04</p>
				<h3>「広報しろいし」4月号に掲載：<br>白石工場建設を主とした白石市の雇用確保と製造業の振興について</h3>
				<p class="text01">宮城県白石市が発行する「広報しろいし」4月号8ページに掲載されました。ぜひご覧いただけますと幸いです。</p>
				<a href="http://www.city.shiroishi.miyagi.jp/info/magazine/PDF/14_04pdf/08-09.pdf" target="_blank">「広報しろいし 4月号」掲載ページはこちら＞＞</a>
			</article>
			<article><a name="news_1403-01"></a>
				<p class="date">2014/03</p>
				<h3>「広報しろいし」3月号に掲載：<br>立地協定の締結・建設する工場とLIMEXの紹介</h3>
				<p class="text01">宮城県白石市が発行する「広報しろいし」3月号の背表紙に掲載されました。ぜひご覧いただけますと幸いです。</p>
				<a href="http://www.city.shiroishi.miyagi.jp/info/magazine/PDF/14_03pdf/36.pdf" target="_blank">「広報しろいし 3月号」掲載ページはこちら＞＞</a>
			</article>

			<article><a name="news_140301-01"></a>
				<p class="date">2014/03/01</p>
				<h3>「合成紙新聞」に掲載：<br>宮城に工場を建設・LIMEXの素材紹介・経産省補助金について</h3>
				<p class="text01">2014年3月1日発行の「合成紙新聞」１面に掲載されました。掲載記事は、メールもしくはFAXにてお送りさせて頂きます。ご入用の際は、遠慮なくお申し付け下さい。</p>
			</article>
			<article><a name="news_140207-02"></a>
				<p class="date">2014/02/07</p>
				<h3>「建設新聞」に掲載：<br>白石工場建設・隈研吾氏設計・経産省補助金採択・工場紹介</h3>
				<p class="text01">2014年2月7日発行の「建設新聞」１面に掲載されました。掲載記事は、メールもしくはFAXにてお送りさせて頂きます。ご入用の際は、遠慮なくお申し付け下さい。</p>
			</article>
			<article><a name="news_140207-01"></a>
				<p class="date">2014/02/07</p>
				<h3>「河北新聞」に掲載：<br>新工場について・新規採用・協定締結式</h3>
				<p class="text01">2014年2月7日発行の「河北新聞」経済面に掲載されました。掲載記事は、メールもしくはFAXにてお送りさせて頂きます。ご入用の際は、遠慮なくお申し付け下さい。</p>
			</article>


			<article><a name="news_140110-01"></a>
				<p class="date">2014/01/10</p>
				<h3>遂に発見！企業成功の秘密Yahooビジネス「キワマル」に<br>動画が掲載されました</h3>
				<div class="youtube01"><iframe width="560" height="315" src="https://www.youtube.com/embed/KchJZKoT16w?rel=0" frameborder="0" allowfullscreen></iframe></div>
			</article>


			<article><a name="news_1307-01"></a>
				<p class="date">2013/07</p>
				<h3>経済情報誌「仙台経済界」7・8月号に掲載：<br>「紙革命を宮城から世界へ」</h3>
				<p class="text01">仙台圏の経済情報誌「仙台経済界」にて、事業構想大学院大学学長の野田一夫先生・元経済産業事務次官の北畑隆生様・宮城県知事の村井嘉浩様と、山﨑が対談させていただいた記事が掲載されました。ご覧いただけますと幸いです。</p>
				<p><a href="http://www.senkey.co.jp/senkei.htm" target="_blank">「仙台経済界」バックナンバーのご購入はこちら＞＞</a></p>
			</article>

		</div><!-- /#data1.data -->
	</div>


</section><!-- /#body -->

<?php
get_footer();
