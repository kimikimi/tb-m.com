<?php
/**
 * Template Name: news-topics
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0v
 */
$class_name = 'news';
get_header(); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>NEWS</h2>
		</div>
	</div>
	<div id="subNav02">
	<div id="subNav02-01" class="pr01">
	<ul>
		<li class="nav1"><a href="index.html">ALL</a></li>
		<li class="nav2"><a href="topics.html" class="cur">TOPICS</a></li>
		<li class="nav3"><a href="media.html">MEDIA</a></li>
	</ul>
	</div>
	</div>
	<div class="wrapper">
		<div id="data1" class="data">
        <article><a name="news_160223-01"></a>
				<p class="date">2016/02/22</p>
				<h3>スイスUBSグループによるビジネスマッチングを通じて総額15億円の資金調達を実施</h3>
				<p class="text01">株式会社TBMは、スイスで生まれ、150年余の歴史を持つグローバルな金融グループであるUBSグループのUBS証券株式会社およびUBS銀行東京支店のウェルス・マネジメントが提供するビジネス・マッチング・サービスにより紹介を受けた事業会社らを引受先とする第三者割当増資を実施し、総額15億円の資金調達を実施いたしました。<br>
これにより資本金は40億6,920万円（資本準備金を含む）となりました。<br>
この調達により、LIMEXの技術開発・生産の強化、人材採用などを通じたさらなる営業体制の強化、およびグローバル展開の推進に取り組んでまいります。<br>
※UBSウェルス・マネジメントについては、<a href="https://www.ubs.com/jp/ja/wm.html" target="_blank">https://www.ubs.com/jp/ja/wm.html </a>を参照ください。</p>
<p class="text01"><a href="<?php echo get_template_directory_uri(); ?>/pdf/pressrelease_160223.pdf" target="_blank">プレスリリースはこちら＞＞</a></p>
			</article>
        <article><a name="news_160210-01"></a>
				<p class="date">2016/02/10</p>
				<h3>Japan Venture Awards 2016「東日本大震災復興賞」を受賞しました</h3>
				<img src="<?php echo get_template_directory_uri(); ?>/img/news/image160210-01.jpg" width="293" height="220" alt="" />
				<p class="text01">2月10日（水）虎ノ門ヒルズファーラムにて、独立行政法人中小企業基盤整備機構主催、革新的かつ潜在成長力の高い事業や、地域の活性化に資する事業を行う、志の高いベンチャー企業の経営者を称える、Japan Venture Awards 2016の表彰式が行われました。<br>
弊社はJapan Venture Awards 2016「東日本大震災復興賞」を受賞しました。<br>
今後とも役職員一同邁進致します。詳細につきましては、以下のプレスリリースをご覧下さい<br>
<a href="<?php echo get_template_directory_uri(); ?>/pdf/pressrelease_160210.pdf" target="_blank">プレスリリースはこちら＞＞</a></p>
			</article>
        <article><a name="news_151217-01"></a>
				<p class="date">2015/12/17</p>
				<h3>LIMEX工場第二号立地協定締結式（宮城県多賀城市）が行われました</h3>
				<img src="<?php echo get_template_directory_uri(); ?>/img/news/image151227-01.jpg" width="293" height="220" alt="" />
				<p class="text01">株式会社TBMは12月17日（木）に宮城県多賀城市役所にてLIMEX工場第二号立地協定締結式を開催致しましたことをお知らせさせていただきます。</p><p class="text01">本年11月27日、経済産業省の「津波・原子力災害被災地域雇用創出企業立地補助金（製造業等立地支援事業）」に採択され、2017年12月までに、総投資額66億5000万円の量産工場を建設いたします。本工場は年産30,000トンになる見込みで、稼働時までに新規 地元雇用を約60人とし、数年内に約80人以上を雇用する計画です。</p><p class="text01">建設に先立ち、多賀城市と立地協定を行い、同市の復興牽引拠点と位置付けられているさんみらい多賀城・復興 団地(一本柳工業団地)内に、敷地面積約27,500m²の用地を確保する予定です。将来的にはこの量産工場をモデル工場とし、水資源の不足している地域を中心として世界中へ技術輸出をすすめます。日本のイノベーション技術で世界に雇用と産業を創出し、同時に世界の水・石油資源問題に貢献していきます。詳細につきましては、以下のプレスリリースをご覧下さい。</p><p><a href="<?php echo get_template_directory_uri(); ?>/pdf/pressrelease_151217.pdf" target="_blank">プレスリリースはこちら＞＞</a></p>
			</article>
        <article><a name="news_151127-01"></a>
				<p class="date">2015/11/27</p>
				<h3>経済産業省より「津波・原子力災害被災地域雇用創出企業立地補助金<br>
（製造業等立地支援事業）」の採択を受けました</h3>
				<!--<img src="<?php echo get_template_directory_uri(); ?>/img/news/image150827-01.jpg" width="220" height="220" alt="" />-->
				<p class="text01">担当：経済産業政策局 地域経済産業グループ 産業施設課<br>
公表日：平成27年11月27日（金）
</p><p>公募概要：本補助金は、東日本大震災で特に大きな被害を受けた津波浸水地域（青森県、岩手県、宮城県、福島県、茨城県）及び原子力災害により甚大な被害を受けた避難指示区域（避難指示が解除された地域）等の復興を加速するため、これらの地域において工場等を新増設する企業に対し、その経費の一部を補助することにより、企業の立地を円滑に進め、雇用を創出することを目的とします。
			</article>
            <article><a name="news_151106-01"></a>
				<p class="date">2015/11/06</p>
				<h3>Job Creation 2015「特別賞」を受賞しました</h3>
				<img src="<?php echo get_template_directory_uri(); ?>/img/news/image151106-01.jpg" width="220" height="220" alt="" />
				<p class="text01">11月6日（金）新日本有限責任監査法人主催、雇用創出効果の高い企業を表彰するJob Creation 2015の表彰式が行われました。弊社はJob Creation 2015「特別賞」を受賞しました。今後とも役職員一同邁進致します。</p>
                <p><a href="http://www.shinnihon.or.jp/about-us/news-releases/2015/2015-11-09.html" target="_blank">詳細はこちら＞＞</a><br>
</p>
			</article>
        <article><a name="news_150827-01"></a>
				<p class="date">2015/08/27・28</p>
				<h3>第40回中東協力現地会議（ドイツ連邦共和国・ミュンヘン）へ出席</h3>
				<img src="<?php echo get_template_directory_uri(); ?>/img/news/image150827-01.jpg" width="220" height="220" alt="" />
				<p class="text01">【第40回中東協力現地会議】<br>
テーマ：中東・北アフリカの混迷、原油市場と日本のビジネス機会<br>
■ 開催日：2015年8月27日（木）、28日（金）<br>
■ 開催地：ドイツ連邦共和国・ミュンヘン<br>
■ 会　場：Hilton Munich Park</p>
			</article>
			<article><a name="news_150531-01"></a>
				<p class="date">2015/05/31</p>
				<h3>資本金が25億3,920万円となりました<br>（資本準備金を含みます）</h3>
				<p class="text01">2015年5月31日払込期日にて、総額2億8,020万円の第三者割当増資を実施致しました。</p>
			</article>
			<article><a name="news_150501-01"></a>
				<p class="date">2015/05/01</p>
				<h3>2015年ミラノ国際博覧会（ EXPO Milano 2015 ）<br>日本館内でLIMEX 製品が使用されます</h3>
				<img src="<?php echo get_template_directory_uri(); ?>/img/news/image150501-01.jpg" width="220" height="220" alt="" />
				<p class="text01">日本発の技術で新しくエコロジーな新素材であるLIMEXが、2015年ミラノ国際博覧会（ EXPO Milano 2015 ）日本館にてお子様向けスタンプラリーのカード、紙袋、ポスターとして、日本館で使用されます。<br>
				これらの印刷物は大日本印刷株式会社さまにご協力頂き製品化しております。</p>
				<p>◆2015年ミラノ国際博覧会<br>
				<a href="http://expo2015.jp/" target="_blank">日本館詳細はこちら＞＞</a></p>
			</article>

			<article><a name="news_150406-01"></a>
				<p class="date">2015/04/06</p>
				<h3>本社移転のお知らせ</h3>
				<img src="<?php echo get_template_directory_uri(); ?>/img/news/image150406-01.jpg" width="220" height="420" alt="" />
				<p class="text01">平素は格別のご高配を賜り　厚く御礼申し上げます。<br>
				この度弊社は新工場が稼働し、さらなる業務拡大を目的に下記へ本社を移転いたしました。これを機に、旧に倍しまして社業に励み、皆様のご期待にお応えできますよう努力を尽くして参りますので今後とも一層のご支援ご指導のほどお願い申し上げます。</p>

				<p class="text01">株式会社TBM<br>
				代表取締役　山﨑敦義</p>

				<p class="text01">■移転先<br>
				〒100-0005<br>
				東京都千代田区丸の内1-3-1 東京銀行協会ビル10階<br>
				TEL: 03-6212-7270（代表）<br>
				FAX: 03-6212-7271</p>

				<p class="text01">■業務開始日<br>
				2015年4月6日（月）</p>
			</article>
			<article><a name="news_150331-01"></a>
				<p class="date">2015/03/31</p>
				<h3>資本金が22億5,900万円となりました<br>（資本準備金を含みます）</h3>
				<p class="text01">2015年3月31日払込期日にて、総額1億6,920万円の第三者割当増資を実施致しました。</p>
			</article>
			<article><a name="news_150228-01"></a>
				<p class="date">2015/02/28</p>
				<h3>資本金が20億8,980万円となりました<br>（資本準備金を含みます）</h3>
				<p class="text01">2015年2月28日払込期日にて、総額5億8,980万円の第三者割当増資を実施致しました。</p>
			</article>

			<article><a name="news_150206-01"></a>
				<p class="date">2015/02/06</p>
				<h3>隈研吾氏設計のLIMEX第一号工場が完成しました</h3>
				<p class="text01">2月6日、弊社新工場の起動式を行い、社内役職員をはじめ、機械メーカーの方々、建設にご尽力いただいた方々、長年に渡り事業を応援頂いた方々にお越し頂きました。</p>
				<p class="text01">代表の山崎、最高技術責任者で取締役会長の角が、2人で同時にラインの起動スイッチを押し、工場に命が吹き込まれ、機械が一斉に動き始めました。</p>
				<p class="text01">総投資額 累計19億円を超える大きなプロジェクトとなった新工場は、隈研吾建築都市設計事務所が設計。多くの皆様の応援とご支援を頂き、ようやく世界へ向けて新素材"LIMEX"を広めるためのスタート地点に立つことが出来ました。</p>
				<p class="text01">工場の職員は全員、宮城県白石市や近郊の地元在住で、今後は本社と密に連携し、製品の開発や改良など、様々な課題解決に努めて参ります。</p>
				<p class="text01">この新工場からスタートし、今後第二、第三の工場、また、世界中に"LIMEX"の工場が増えるよう、社内役職員一同新たな決意を胸に、より一層精進していきます。</p>
			</article>
			<article><a name="news_150202-01"></a>
				<p class="date">2015/02/02</p>
				<h3>2015年ミラノ国際博覧会（EXPO Milano 2015）の<br>日本館への協賛企業に決定致しました</h3>
				<img src="<?php echo get_template_directory_uri(); ?>/img/news/image150130-01.jpg" width="136" height="177" alt="" />
				<p class="text01">日本発の技術で新しくエコロジーな素材であるLIMEXのコンセプトを評価され、この度正式に経済産業省よりお声掛け頂き、2015年ミラノ国際博覧会（EXPO Milano 2015）日本館の協賛企業に決定しました。多岐に渡ってLIMEXが日本館で使用されます。<br>
				LIMEXが世界中に普及する第一歩となる大きな舞台です。</p>
				<p>◆2015年ミラノ国際博覧会<br>
				<a href="http://expo2015.jp/" target="_blank">日本館詳細はこちら＞＞</a></p>
			</article>
			<article><a name="news_141121-02"></a>
				<p class="date">2014/11/21</p>
				<h3>資本金が15億円となりました<br>（資本準備金を含みます）</h3>
				<p class="text01">2014年11月21日払込期日にて、総額2億7,000万円の第三者割当増資を実施致しました。</p>
			</article>
			<article><a name="news_141121-01"></a>
				<p class="date">2014/11/21</p>
				<h3>ニッポン新事業創出大賞「復興賞」を受賞しました</h3>
				<img src="<?php echo get_template_directory_uri(); ?>/img/news/image141121-01.jpg" width="293" height="220" alt="" />
				<p>11月20日（木）静岡にて独立行政法人中小企業基盤整備機構関東本部、公益社団法人日本ニュービジネス協議会連合会主催、第10回新事業創出全国フォーラムin静岡にて第9回ニッポン新事業創出大賞表彰式が行われました。弊社は公益社団法人日本ニュービジネス協議会連合会会長表彰「復興賞」を受賞しました。「復興賞」を頂いた企業として、まずは宮城県白石市の工場を成功させるべく今後とも役職員一同邁進致します。</p>
			</article>
			<article><a name="news_1410-01"></a>
				<p class="date">2014/10</p>
				<h3>東京都江東区の東京都立産業技術研究センターに<br>東京ラボを設置しました</h3>
			</article>
			<article><a name="news_140915-01"></a>
				<p class="date">2014/09/15</p>
				<h3>資本金が12億3,000万円となりました<br>（資本準備金を含みます）</h3>
				<p class="text01">2014年9月15日払込期日にて、総額3億1,500万円の第三者割当増資を実施致しました。</p>
			</article>

			<article><a name="news_14082728-01"></a>
				<p class="date">2014/08/27・28</p>
				<h3>第39回中東協力現地会議（トルコ・イスタンブール）<br>へ出席しました</h3>
				<img src="<?php echo get_template_directory_uri(); ?>/img/news/image14082728-01.jpg" width="293" height="220" alt="" />
				<p class="text01">【第39回中東協力現地会議】<br>
テーマ：混迷する中東・北アフリカ情勢と日本のビジネス<br>
■ 開催日：2014年8月27日（水）、28日（木）<br>
■ 開催地：トルコ共和国・イスタンブール<br>
■ 会　場：Hilton Istanbul Bomonti Hotel</p>
				<p>本年度は正会員として出席。雇用をはじめとする現地の経済情勢と、水資源の枯渇を危惧する環境問題に関するテーマが多く、我々へ寄せられる期待を改めて実感いたしました。</p>
			</article>
			<article><a name="news_140707-01"></a>
				<p class="date">2014/07/07</p>
				<h3>新工場建設工事着工のお知らせ</h3>
				<p class="text01">宮城県白石市にて計画しております国内第一号のストーンペーパーパイロットプラントの工事が着工致しました。年内完成、年明け稼働を予定しております。国内第一号となりますパイロットプラントはストーンペーパー"LIMEX"の製造と共に、研究開発や人材育成の中核拠点となります。</p>
				<p class="text02">■工場概要　建設予定地 : 宮城県白石市大鷹沢三沢、敷地面積 : 10,510㎡、建物床面積 : 2,884㎡、生産量 : 年6,000t (月500t)、従業員 : 約30名</p>
			</article>

			<article><a name="news_140704-01"></a>
				<p class="date">2014/07/04</p>
				<h3>資本金が9億1,500万円となりました<br>（資本準備金を含みます）</h3>
				<p class="text01">2014年7月4日払込期日にて、総額3億2,600万円の第三者割当増資を実施致しました。</p>
			</article>
			<article><a name="news_140508-01"></a>
				<p class="date">2014/05/08</p>
				<h3>資本金が5億8,900万円となりました<br>（資本準備金を含みます）</h3>
				<p class="text01">2014年5月8日払込期日にて、総額9,000万円の第三者割当増資を実施致しました。</p>
			</article>
			<article><a name="news_140331-01"></a>
				<p class="date">2014/03/31</p>
				<h3>資本金が4億9,900万円となりました<br>（資本準備金を含みます）</h3>
				<p class="text01">2014年3月31日払込期日にて、総額2億2,400万円の第三者割当増資を実施致しました。</p>
			</article>
			<article><a name="news_140206-01"></a>
				<p class="date">2014/02/06</p>
				<h3>LIMEX工場第一号立地協定式が行われました</h3>
				<img src="<?php echo get_template_directory_uri(); ?>/img/news/image140206-01.jpg" width="220" height="293" alt="" />
				<p>LIMEX工場第一号立地協定式が行われました。株式会社TBMは2月6日（木）に宮城県庁舎にてLIMEX工場第一号立地協定式を開催致しましたことをお知らせさせていただきます。<br>今回締結した協定は、宮城県と弊社による活力ある地域社会の創造、人材育成および両者の発展に資することを目的として、弊社の開発した次世代ストーンペーパー「LIMEX」の工場建設に関する立地協定となっております。従来のストーンペーパーより大幅に軽く、品質も高く、安価で製造することが可能になり2013年、弊社製造技術が経済産業省のイノベーション補助金事業の認定を受け、2014年に宮城県白石市にて次世代ストーンペーパー「LIMEX」工場第一号が建設予定となっております。今後ともどうぞよろしくお願い申し上げます。</p>
			</article>
			<article><a name="news_140124-01"></a>
				<p class="date">2014/01/24</p>
				<h3>自社ストーンペーパーLIMEXに関する特許が承認されました</h3>
				<p class="text01">無機物質粉末高配薄膜シートの製造方法(登録番号5461614)</p>
				<p class="text01">弊社の特許は、高フィラー含有のポリオレフィンフィルムの製造において、一段階工程での混練を特徴としております。通常、同様の製品を作る場合は樹脂とフィラーを予め混ぜたマスターペレットを作成してからシート化しますが、弊社の製造法では樹脂とフィラーを混ぜながらシート化します。そのため、将来的に様々なフィラーや樹脂の組み合わせを実現できます。</p>
			</article>

			<article><a name="news_13082526-01"></a>
				<p class="date">2013/08/25・26</p>
				<h3>第38回中東協力現地会議（アラブ首長国連邦・ドバイ首長国）<br>へ出席しました</h3>
			<p class="text01">【第38回中東協力現地会議】<br>
テーマ：我が国の成長戦略と中東・北アフリカにおけるビジネスチャンス<br>
■ 開催日：2013年8月25日（日）、26日（月）<br>
■ 開催地：アラブ首長国連邦・ドバイ首長国<br>
■ 会　場：Intercontinental Dubai Festival City</p></article>
			<article><a name="news_1308-01"></a>
				<p class="date">2013/08</p>
				<h3>本社移転のお知らせ</h3>
				<p class="text01">東京都港区赤坂二丁目に移転しました</p>
			</article>
			<article><a name="news_1302-01"></a>
				<p class="date">2013/02</p>
				<h3>経済産業省よりイノベーション拠点立地推進事業<br>（先端技術実証・評価設備整備費等補助金）として<br>当社のストーンペーパー製造事業が採択を受けました</h3>
                <img src="<?php echo get_template_directory_uri(); ?>/img/news/image1302-01.jpg" width="293" height="220" alt="" />
                <p class="text01">担当 : 産業技術環境局　研究開発課　公表日:平成25年2月6日（水）</p>
                <p>公募概要 : 本事業は、企業等による先端技術の実証・評価等のための設備の整備又は開発に対する補助を行うことにより、東日本大震災や円高等の影響による企業の事業開発の悪化等を原因として急速に縮小している研究開発投資の促進を図り、新技術の実用化を加速することで、“震災からの復興”と“新たな成長”を実現することを目的とするものです。
当社助成金対象設備の2/3が、経済産業省助成金として支給されます。</p>
			</article>
			<article><a name="news_120725-01"></a>
				<p class="date">2012/07/25</p>
				<h3>資本金が2億7,500万円となりました</h3>
			</article>
			<article><a name="news_1206-01"></a>
				<p class="date">2012/06</p>
				<h3>日立造船株式会社と成形技術の共同開発を開始しました</h3>
			</article>
			<article><a name="news_111031-01"></a>
				<p class="date">2011/10/31</p>
				<h3>資本金が2,000万円となりました</h3>
			</article>
			<article><a name="news_1105-01"></a>
				<p class="date">2011/05</p>
				<h3>自社の開発によるストーンペーパーの特許を申請しました</h3>
			</article>
		</div><!-- /#data1.data -->
	</div>


</section><!-- /#body -->

<?php
get_footer();
