<?php
/**
 * Template Name: en-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>
<?php
//ALL

$all_args = array( 
//	'post_status'=>'publish',
	'post_type' => 'news-en',
	'posts_per_page' => 6, 
);

//press release

$press_args = array( 
//	'post_status'=>'publish',
	'category_name' => 'press_release',
	'post_type' => 'news-en',
	'posts_per_page' => 6, 
);
?>
<style type="text/css">
	.slide.smp img {
		opacity: 1 !important;
		width: 100%;
	}
	.kv_link{
		cursor:pointer;
	}
</style>
<script type="text/javascript">
$(function(){
	$('.kv_link.n01').on('click',function(){
		$(location).attr("href", "/en/action/");
	});
	$('.kv_link.n02').on('click',function(){
		$(location).attr("href", "/order/en_explain.php");
	});
	$('.kv_link.n03').on('click',function(){
		$(location).attr("href", "/en/about/#about_b2i4");
	});
});
</script>
		<section id="top">
			<div class="fv">
				<div class="fv_main">
                    <div class="fv_img kv_slider">
                                    <div class="first slide" data-current="1">
                                        <div class="fv_logo">
                                            <div>
                                                <img src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_stone.png">
                                                <img src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_logo.png">
                                            </div>
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_c1.png" alt="A Revolutionary New Limestone Based Material">
                                        </div>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_img1.jpg">
                                    </div>
                                    <div class="slide" data-current="0"><img src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_img2.jpg"></div>
                                    <div class="slide" data-current="0"><img src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_img3.jpg"></div>
                                    <div class="slide kv_link n03" data-current="0"><img src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_img6.jpg"></div>
                                    <div class="slide kv_link n02" data-current="0"><img src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_img5.jpg"></div>
                                    <!-- <div class="slide kv_link n01" data-current="0"><img src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_img4.jpg"></div> -->
 
                    <?php if(wp_is_mobile()):?>
                                    <div class="slide smp" data-current="0"><a href="https://www.youtube.com/embed/ZQ0J8Ur9T-E?autoplay=1&rel=0&showinfo=0&autohide=1&loop=1&playlist=DJJ9zwqDL0c" class="youtube"><img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_movie_img.jpg"></a></div>
                    <?php else:?>
                                    <div class="slide" data-current="0" id="youtube-movie-content">
                                        <a href="https://www.youtube.com/embed/ZQ0J8Ur9T-E?autoplay=1&rel=0&showinfo=0&autohide=1&loop=1&playlist=DJJ9zwqDL0c" class="youtube overlay_link"></a>
                                        <a id="bgndVideo" class="player" data-property="{
                                        videoURL:'http://youtu.be/ZQ0J8Ur9T-E',
                                        containment:'.video_wrap',
                                        ratio:'4/3',
                                        optimizeDisplay:false,
                                        autoPlay:true,
                                        stopMovieOnBlur:false,
                                        showControls:false,
                                        mute:true,
                                        startAt:0,
                                        opacity:1
                                        }">background movie</a>
                                        <div class="section-top-main video_wrap"></div>
                                    </div>
                    <?php endif ;?>
                    </div>
					<img class="btn_open btn" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_btn_story.png" alt="LIMEX STORY">
				</div>
				<div class="fv_story">
					<div class="story_bk"></div>
					<div class="story_img">
						<img class="stone" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_story_img.png">
						<img class="story_txt" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_story_text.png" alt="The earth is the planet of water. The earth is the planet of life. But it is not only that. The earth is also the planet of stone. On the earth there is one material can be supplied unlimitedly. That is stone. If paper can be made from stone, paper will be more durable and convenient. But not only paper, if more materials were to be made from stone then fossil fuels will bring to us without relying on water or trees a life style where things can be re-used over and over again. Stone used to be tools for hunting or cooking. They were also used as weapons. And also they were used as a media for conveying messages. So far humanbeings has been very familir to stone. It is now that we should re-examine the value of stone. It is time we had bet on stone. Plenty of stone can be found on the earth. To create possibility X from limestone, that is LIME, which still does not exist anywhere on the earth is the mission of LIMEX. The log symbol, stands for that a world full of possibility will be created from stone, which was broken by the powerful X. LIMEX will bring us a future starting from stone.">

						<img class="mask_left" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_story_mask_l.png">
						<img class="mask_right" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_story_mask_r.png">

					</div>
					<div class="story_control">
						<img class="btn_prev btn" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_story_btn_prev.png">
						<img class="btn_next btn" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_story_btn_next.png">
						<img class="btn_close btn" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_story_btn_close.png">
					</div>
				</div>
			</div>
            <div class="fv_control cf01" style="opacity: 1; display: block;">
                <img class="btn_slide" data-id="1" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_btn_slide.png" style="opacity: 1;">
                <img class="btn_slide" data-id="2" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_btn_slide.png" style="opacity: 0.5;">
                <img class="btn_slide" data-id="3" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_btn_slide.png" style="opacity: 0.5;">
                <img class="btn_slide" data-id="4" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_btn_slide.png" style="opacity: 0.5;">
                <img class="btn_slide" data-id="5" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_btn_slide.png" style="opacity: 0.5;">
                <img class="btn_slide" data-id="6" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_btn_slide.png" style="opacity: 0.5;">
                <!-- <img class="btn_slide" data-id="7" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_fv_btn_slide.png" style="opacity: 0.5;"> -->
 
            </div>
			<div class="news">
				<h2 class="over" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/en/top/top_news_ttl.jpg);">NEWS</h2>
				<a href="https://www.facebook.com/tbm.limex" target="_blank"><img class="fb btn" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_news_btn_fb.png" alt="LIMEX in Facebook"></a>
				<hr>
				<ul class="category">
					<li class="cate_pr">PRESS RELEASE</li>
					<li class="cate_all cur">ALL</li>
				</ul>
				<div class="news_thumb all">
					<ul>
<?php
$the_query = new WP_Query( $all_args );
//var_dump($the_query);
// ループ
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();

$fields = get_fields(get_the_ID());

$cat = get_the_category();
$cat = $cat[0];
$cat_name = $cat->cat_name;
$category_nicename = $cat->category_nicename;
?>
						<li>
							<a href="/en/news/#<?php echo get_the_ID();?>">
								<div class="img"><img src="<?php echo $fields['top_list_image']? $fields['top_list_image'] : '/wp-content/themes/limex/thm/news_i1.jpg';?>" alt=""></div>
								<div class="thumb">
									<p class="cate top_<?php echo mb_strtolower($category_nicename);?>"><?php echo $cat_name ?></p>
									<p class="txt"><?php the_title(); ?></p>
									<p class="date"><?php the_time('Y/m/d'); ?></p>
								</div>
							</a>
						</li>
<?php
endwhile;
endif;
?>
					</ul>
				</div>

				<div class="news_thumb press-release">
					<ul>
<?php
$the_query = new WP_Query( $press_args );
//var_dump($the_query);
// ループ
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();

$fields = get_fields(get_the_ID());
$cat = get_the_category();
$cat = $cat[0];
$cat_name = $cat->cat_name;
$category_nicename = $cat->category_nicename;
?>
						<li>
							<a href="/en/news/#<?php echo get_the_ID();?>">
								<div class="img"><img src="<?php echo $fields['top_list_image']? $fields['top_list_image'] : '/wp-content/themes/limex/thm/news_i1.jpg';?>" alt=""></div>
								<div class="thumb">
									<p class="cate top_<?php echo mb_strtolower($category_nicename);?>"><?php echo $cat_name ?></p>
									<p class="txt"><?php the_title(); ?></p>
									<p class="date"><?php the_time('Y/m/d'); ?></p>
								</div>
							</a>
						</li>
<?php
endwhile;
endif;
?>
					</ul>
				</div>

				<a href="/en/news/"><img class="more btn" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_news_btn_more.png" alt="MORE"></a>
			</div>
			<div class="link">
				<a href="/en/about/"><img class="btn" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_about_btn_link.png" alt="ABOUT - The new material made from the almost unlimited  limestone , which save the future is  LIMEX."></a>
				<a href="/en/mission/"><img class="btn" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_mission_btn_link.png" alt="MISSION - The mission of LIMEX - Materials made from limestones are increasing, which will lead to the changes of enviroment, sociaty and the future."></a>
				<a href="/en/factory/"><img class="btn" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_factory_btn_link.png" alt="FACTORY"></a>
				<a href="/en/company/"><img class="btn" src="<?php echo get_template_directory_uri(); ?>/img/en/top/top_company_btn_link.png" alt="COMPANY"></a>
			</div>
		</section>
<script>
    $(document).ready(function() {
        $(".youtube").colorbox({
            iframe:true,
            innerWidth:1080,
            innerHeight:608
        });
    });
</script>

<?php
get_footer('en');
