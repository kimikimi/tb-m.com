<?php
/*
Template Name: API Page
*/

$fields = get_fields(48);

$json = array();
foreach ($fields as $key => $value) {
	if($key == 'project_number' || $key == 'warter_number' || $key == 'introduction_number' ||$key == 'distribution_number' || $key == 'warter_number_500'){
		if($key == 'project_number' || $key == 'warter_number' || $key == 'distribution_number'){
			$json[$key] = sprintf('%06d',$value);
		}else{
			$json[$key] = sprintf('%07d',$value);
		}
	}
}

header("Content-Type: application/json; charset=utf-8");
echo json_encode($json);
exit();