(function(){

/*--------------------
add mouse events
--------------------*/

var addMouseEvents = function(obj, enFc, leFc, clFc){
	$(document).on('mouseenter', obj, enFc).on('mouseleave', obj, leFc).on('click', obj, clFc);
};

/*--------------------
add news
--------------------*/

var initNews = function(){
	$.ajax({url:'./thm/news.xml', dataType:'xml', cache:false, async:false, success:function(xml){
		var dom = '';
		$(xml).find('news').each(function(){
			var cat = ($(this).find('cat').text() === 'news') ? 'top_news_icon_news.jpg' : 'top_news_icon_media.jpg';
			var date = $(this).find('date').text();
			var body = $(this).find('body').text();
			var pict = $(this).find('pict').text();
			var link = $(this).find('link').text();
			dom = dom + '<div class="thumb"><a href="' + link + '"><img class="pict btn" src="./thm/' + pict + '"></a><p class="date"><img class="cat" src="./top/img/' + cat + '">' + date + '</p><p class="body">' + body + '</p></div>';
		});
		$('#top .news_thumb').append(dom);
	}});
};

/*--------------------
ie filter err fix
--------------------*/

function ieRemoveFilter($dom){
	var stl = $dom.attr('style').toString();
	(0 <= stl.indexOf('alpha(opacity=100); ')) && $dom.attr('style', stl.split('alpha(opacity=100); ').join(''));
}

/*--------------------
fv obj
--------------------*/

var fv = new function(){
	this.enable = false; //enable
	var now = 1;
	// this.slideAnim = function(index){
	// 	fv.enable = false; //*enable

	// 	if(!index){
	// 		var chs = $('#top .fv_logo > img'), bks = $('#top .fv_img img'), next = (now === bks.length) ? 1 : (now + 1);
	// 		chs.eq(now - 1).animate({'opacity':'0.0'}, {duration:500, complete:function(){
	// 			$('#top .btn_slide').css('opacity', '0.5').eq(next - 1).css('opacity', '1.0');
	// 			// ieRemoveFilter($('#top .btn_slide').eq(next - 1));
	// 			for(var k=1, lk=bks.length; k<=lk; k++){
	// 				((k !== now) && (k !== next)) && bks.eq(k - 1).css('opacity', '0.0');
	// 			}
	// 			bks.eq(next - 1).css({'opacity':'1.0', 'left':'100%', 'z-index':'1'}).animate({'left':'0%'}, {duration:1000, complete:function(){
	// 				// ieRemoveFilter($(this));
	// 			}});
	// 			bks.eq(now - 1).css('z-index', '0').animate({'left':'-100%'}, {duration:1000, complete:function(){
	// 				chs.eq(next - 1).css({'left':'-50px', 'opacity':'0.0'}).animate({'left':'0px', 'opacity':'1.0'}, {duration:500, complete:function(){
	// 					// ieRemoveFilter($(this));
	// 					now = next;

	// 					fv.enable = true; //*enable
	// 				}});
	// 			}});
	// 		}});
	// 	}else if(now !== index){
	// 		this.timerStop(); //*timer

	// 		var chs = $('#top .fv_logo > img'), bks = $('#top .fv_img img');
	// 		for(var i=1, li=bks.length; i<=li; i++){
	// 			if(now !== i){
	// 				bks.eq(i - 1).css({'opacity':'1.0', 'z-index':'1', 'left':100 * (i - now) + '%'});
	// 				ieRemoveFilter(bks.eq(i - 1));
	// 			}
	// 		}
	// 		chs.eq(now - 1).animate({'opacity':'0.0'}, {duration:500, complete:function(){
	// 			$('#top .btn_slide').css('opacity', '0.5').eq(index - 1).css('opacity', '1.0');
	// 			ieRemoveFilter($('#top .btn_slide').eq(index - 1));
	// 			for(var j=1, lj=bks.length; j<=lj; j++){
	// 				bks.eq(j - 1).animate({'left':100 * (j - index) + '%'}, {duration:500});
	// 			}
	// 			chs.eq(index - 1).css({'left':'-50px', 'opacity':'0.0'}).animate({'left':'0px', 'opacity':'1.0'}, {duration:500, complete:function(){
	// 				ieRemoveFilter($(this));
	// 				now = index;

	// 				fv.enable = true; //*enable
	// 				fv.timerStart(); //*timer
	// 			}});
	// 		}});
	// 	}
	// };
	//add timer

	this.timerStart = function(){
		if(!this.timer){
			// this.timer = setInterval(function(){
			// 	fv.enable && fv.slideAnim();
			// }, 5000);
		}
	};

	//remove timer
	this.timerStop = function(){
		clearInterval(this.timer);
		this.timer = void(0);
	};

	//init & opening
	this.init_limited = function(){
		$('#top .fv_main .btn').css({
			display: 'none',
			opacity: 1
		});
		$('#top .fv_control').css('display', 'block').animate({'opacity':'1.0'}, {duration:250, complete:function(){
			ieRemoveFilter($(this));

			// fv.enable = true; //*enable
			// fv.timerStart(); //*timer
		}});
	}
	this.init = function(){
		var stone = $('#top .fv_logo img:eq(0)').css('right', '350px').animate({'opacity':'0.5'}, {duration:2000,  easing:'linear', complete:function(){
			$(this).delay(1000).animate({'right':'-15px'}, {duration:1000, complete:function(){
				$('#top .fv_logo img:eq(1)').delay(500).animate({'opacity':'1.0'}, {duration:1000, complete:function(){
					ieRemoveFilter($(this));
					$('#top .fv_logo > img:eq(0)').css('margin-left', '-50px').delay(500).animate({'opacity':'1.0', 'margin-left':'0px'}, {duration:500, complete:function(){
						$('#top .btn_slide:gt(0)').css('opacity', '0.5');
						$('#top .fv_main .btn, #top .fv_control').css('display', 'block').animate({'opacity':'1.0'}, {duration:250, complete:function(){
							ieRemoveFilter($(this));

							// fv.enable = true; //*enable
							// fv.timerStart(); //*timer
						}});
					}});
				}});
			}}).delay(500).animate({'opacity':'1.0'}, {duration:1000, complete:function(){
				ieRemoveFilter($(this));
			}
			});
		}});
		$({blurR:10.0}).animate({blurR:0.0}, {duration:1500, step:function(){
			stone.css({'filter':'blur(' + this.blurR + 'px)', '-webkit-filter':'blur(' + this.blurR + 'px)', '-moz-filter':'blur(' + this.blurR + 'px)'});
		}, complete:function(){
			stone.css({'filter':'', '-webkit-filter':'', '-moz-filter':''}); //filter err fix
		}});
	};
};

/*--------------------
story obj
--------------------*/

var story = new function(){
	this.main = void(0);
	this.dst = 0;
	this.prev = this.next = true;

	//open
	this.open = function(){
		// fv.timerStop(); //*timer
		fv.enable = false; //*enable

		$('#top .fv_story').css('display', 'block');
		$('#top .story_img').css('display', 'none');
		$('#top .story_bk').css({'opacity':'0.0'}).animate({'opacity':'1.0'}, {duration:1000, complete:function(){
			// ieRemoveFilter($(this));
			$('#top .story_img').css('display', 'block');
			var styImgs = story.main.animate({'opacity':'1.0'}, {duration:1500, complete:function(){
				// ieRemoveFilter($(this));
				$('#top .stone').delay(500).animate({'opacity':'1.0'}, {duration:1000, complete:function(){
					// ieRemoveFilter($(this));
				}});
				$('#top .story_control').delay(1000).animate({'opacity':'1.0'}, {duration:1000, complete:function(){
					// ieRemoveFilter($(this));
				}});

				//get width *indispensable to visibled
				story.dst = parseInt(story.main.width()) - 940;
				scrollCheck();
			}});
			$({blurR:10.0}).animate({blurR:0.0}, {duration:1500, step:function(){
				styImgs.css({'filter':'blur(' + this.blurR + 'px)', '-webkit-filter':'blur(' + this.blurR + 'px)', '-moz-filter':'blur(' + this.blurR + 'px)'});
			}, complete:function(){
				styImgs.css({'filter':'', '-webkit-filter':'', '-moz-filter':''}); //filter err fix
			}});
		}});
	};

	//close
	this.close = function(){$('#top .story_control, #top .stone, #top .story_txt').animate({'opacity':'0.0'}, {duration:1000, complete:function(){
			$('#top .story_img').css('display', 'none');
			$('#top .story_bk').animate({'opacity':'0.0'}, {duration:1000, complete:function(){
				$('#top .story_img').css('display', 'block');
				$('#top .fv_story').css('display', 'none');

				fv.enable = true; //*enable
				// fv.timerStart(); //*timer
			}});
		}});
	};

	//scroll
	var scrollCheck = function(){
		var rt = parseInt(story.main.css('right'));
		story.prev = story.next = true;
		$('#top .btn_prev, #top .btn_next').css('display', 'block');

		if(0 <= rt){
			story.next = false;
			$('#top .btn_next').css('display', 'none');
			story.main.css('right', '0px'); //err fix
		}
		if((story.dst <= -rt)){
			story.prev = false;
			$('#top .btn_prev').css('display', 'none');
			story.main.css('right', (-story.dst).toString() + 'px'); //err fix
		}
	};
	this.scroll = function(prev, skip){
		story.prev = story.next = false;
		var nowDst = parseInt(story.main.css('right'));
		if(prev && skip){
			story.main.animate({'right':(-story.dst).toString() + 'px'}, {duration:1000, complete:function(){
				scrollCheck();
			}});
		}else if(!prev && skip){
			story.main.animate({'right':'0px'}, {duration:1000, complete:function(){
				scrollCheck();
			}});
		}else if(prev && !skip){
			story.main.css('right', (nowDst - 1).toString() + 'px');
			scrollCheck();
		}else if(!prev && !skip){
			story.main.css('right', (nowDst + 1).toString() + 'px');
			scrollCheck();
		}
	};

	//add timer
	this.timerStart = function(prev, skip){
		if(!this.timer){
			// this.timer = setInterval(function(){story.scroll(prev, skip);}, 10);
		}
	};

	//remove timer
	this.timerStop = function(){
		this.timer && clearInterval(this.timer);
		this.timer = void(0);
	};
};

/*--------------------
init
--------------------*/

$(document).ready(function(){
	initNews();

	//story init
	story.main = $('#top .story_txt');

	//css reset (ie8 deal)
	$('#top .fv_main .btn').css('display', 'none')
	$('#top .fv_img img, #top .fv_logo img').css({'visibility':'visible', 'opacity':'0.0'});
	$('#top .story_bk, #top .stone, #top .story_txt, #top .story_control, #top .fv_main .btn, #top .fv_control').css({'opacity':'0.0'});
	$('.base-scale-img img').css({'opacity':'1'});

	//.btn
	addMouseEvents('.btn', function(){
		if(!ua.sp){
			var btnName = $(this).attr('class');
			if(btnName === 'btn_prev btn'){
				story.prev && story.timerStart(true, false);
			}else if(btnName === 'btn_next btn'){
				story.next && story.timerStart(false, false);
			}else{
				$(this).stop().css('opacity', '0.5');
			}
		}
		return false;
	}, function(){
		if(!ua.sp){
			var btnName = $(this).attr('class');
			if(btnName.match(/(btn_prev|btn_next)/)){
				story.timerStop();
			}else{
				$(this).animate({'opacity':'1.0'}, {duration:250, complete:function(){}});
			}
		}
		return false;
	}, function(){
		var btnName = $(this).attr('class');
		if(btnName.match(/(btn_open|btn_close|btn_prev|btn_next)/)){
			if(btnName === 'btn_open btn'){
				story.open();
			}else if(btnName === 'btn_close btn'){
				story.close();
			}else if(btnName === 'btn_prev btn'){
				story.timerStop();
				story.prev && story.scroll(true, true);
			}else if(btnName === 'btn_next btn'){
				story.timerStop();
				story.next && story.scroll(false, true);
			}
			return false;
		}
	});

	//.btn_slide
	addMouseEvents('#top .btn_slide', function(){
		return false;
	}, function(){
		return false;
	}, function(){
		fv.enable && fv.slideAnim(parseInt($(this).attr('data-id')));
		return false;
	});
});

$(window).load(function(){
	addMouseEvents = initNews = void(0); //delete ref
	 fv.init(); //１枚目がカンブリアの時
	//fv.init_limited();
});

})();

var kvSlider = {
	current_index :0,
	next_index :0,
	prev_index : 0,
	//KV数チェック
	checkLength: function() {
		var length = $(".kv_slider").find('.slide').length;
		return length;
	},
	//表示中のKVチェック
	checkCurrent: function() {
		$(".kv_slider").find('.slide').each(function(index, el) {
			if($(this).attr('data-current') == 1){
				kvSlider.current_index = $(".kv_slider").find('.slide').index($(this));
				return kvSlider.current_index;
			}
		});
	},
	//次に表示する順番をチェック
	checkNext: function(){
		if (kvSlider.current_index == kvSlider.checkLength()-1) {
			kvSlider.next_index = 0;
			return kvSlider.next_index;
		} else {
			kvSlider.next_index = kvSlider.current_index+1;
			return kvSlider.next_index;
		}

	},
	checkPrev: function(){
		if (kvSlider.current_index == 0) {
			kvSlider.prev_index = kvSlider.checkLength()-1;
			return kvSlider.prev_index;
		} else {
			kvSlider.prev_index = kvSlider.current_index-1;
			return kvSlider.prev_index;
		}

	},
	//次の画像をセット
	setNext: function(event) {
		kvSlider.checkCurrent();
		kvSlider.checkNext();
		$(".kv_slider").find('.slide').eq(kvSlider.current_index).attr('data-current', 0).css({
			zIndex: 1
		});
		$(".kv_slider").find('.slide').eq(kvSlider.next_index).attr('data-current', 1).css({
			left: '100%',
			zIndex: 2,
			opacity: 1
		});
	},
	//カレント、ネクストを移動
	toNext: function() {
		var current = kvSlider.current_index;
		$(".kv_slider").find('.slide').eq(current).animate({
			left: '-100%'
		},1000);
		$(".kv_slider").find('.slide').eq(current-1).animate({
			left: '-200%'
		},1000);
		$(".kv_slider").find('.slide').eq(current-2).animate({
			left: '-300%'
		},1000);
		$(".kv_slider").find('.slide').eq(current-3).animate({
			left: '-400%'
		},1000);

		var next = kvSlider.next_index;
		$(".kv_slider").find('.slide').eq(next).animate({
			left: '0%'
		},1000,function(){
			$(".kv_slider").find('.slide').attr('data-current', 0).eq(next).attr('data-current', 1).css('z-index', 2);
			kvSlider.checkPagenation();
			if ( kvSlider.current_index == kvSlider.checkLength()-1 || kvSlider.current_index == kvSlider.checkLength()-2 || kvSlider.current_index == kvSlider.checkLength()-3 || kvSlider.current_index == kvSlider.checkLength()-4 || kvSlider.current_index == kvSlider.checkLength()-5){
				$('.fv_main').find('.btn_open').fadeOut('200');
				clearInterval(setSlide);
				if (kvSlider.current_index == kvSlider.checkLength()-1){
					setSlide= setInterval(kvSlider.eventNext,10000);
				} else {
					setSlide= setInterval(kvSlider.eventNext,5000);
				}

				// setTimeout(function(){
				// 	setSlide= setInterval(kvSlider.eventNext,8000);
				// },5000);
			} else if (kvSlider.current_index == 0){
				clearInterval(setSlide);
				setSlide= setInterval(kvSlider.eventNext,5000);
				$('.fv_main').find('.btn_open').fadeIn('200');
			}
		});
	},
	//次の画像をセット
	setPrev: function(event) {
		kvSlider.checkCurrent();
		kvSlider.checkPrev();
		$(".kv_slider").find('.slide').eq(kvSlider.current_index).attr('data-current', 0).css({
			zIndex: 1
		});
		$(".kv_slider").find('.slide').eq(kvSlider.prev_index).attr('data-current', 1).css({
			left: '100%',
			zIndex: 2,
			opacity: 1
		});
	},
	//カレント、ネクストを移動
	toPrev: function() {
		var current = kvSlider.current_index;
		$(".kv_slider").find('.slide').eq(current).animate({
			left: '100%'
		},1000);
		$(".kv_slider").find('.slide').eq(current+1).animate({
			left: '200%'
		},1000);
		$(".kv_slider").find('.slide').eq(current+2).animate({
			left: '300%'
		},1000);
		$(".kv_slider").find('.slide').eq(current+3).animate({
			left: '400%'
		},1000);

		var prev = kvSlider.prev_index;
		$(".kv_slider").find('.slide').eq(prev).animate({
			left: '0%'
		},1000,function(){
			$(".kv_slider").find('.slide').attr('data-current', 0).eq(prev).attr('data-current', 1).css('z-index', 2);
			kvSlider.checkPagenation();
			if (kvSlider.current_index == kvSlider.checkLength()-1 || kvSlider.current_index == kvSlider.checkLength()-2 || kvSlider.current_index == kvSlider.checkLength()-3 || kvSlider.current_index == kvSlider.checkLength()-4 || kvSlider.current_index == kvSlider.checkLength()-5){
				$('.fv_main').find('.btn_open').fadeOut('200');
				clearInterval(setSlide);
				// setTimeout(function(){
				// 	setSlide= setInterval(kvSlider.eventprev,8000);
				// },5000);
			} else if (kvSlider.current_index == 0){
				$('.fv_main').find('.btn_open').fadeIn('200');
			}
		});
	},
	eventNext: function(){
		kvSlider.setNext();
		kvSlider.toNext();
	},
	eventPrev: function(){
		kvSlider.setPrev();
		kvSlider.toPrev();
	},
	checkPagenation: function() {
		kvSlider.checkCurrent();
		kvSlider.current_index;
		$(".btn_slide").css('opacity', 0.5);
		$(".btn_slide").eq(kvSlider.current_index).css('opacity', 1);
	},
	init: function(target_num) {
		$(".slide").css({
			zindex: 1,
			property2: 'value2'
		});
	},
	gotoClicknum: function() {
		$(".btn_slide").on('click', function(event) {
			if($(".slide").is(':animated')){
				return false;
			} else {
				clearInterval(setSlide);
				//現在のページを取得
				var current = kvSlider.current_index;
				//クリックされた番号を取得
				var target_num = $(this).attr("data-id")-1;
				//飛び先までの距離を取得
				var distance = (target_num - current);
				//もし飛び先がマイナス方向だったら
				if(distance < 0){
					$(".slide").eq(current).animate({left: "100%"}, 1000);
					$(".slide").eq(current + distance).css({
						left: '-100%',
						zIndex: '2'
					}).animate({left:"0%"}, 1000,function(){
						$(".btn_slide").attr('data_id', 0);
						$(".slide").attr('data-current', 0).eq(current + distance).attr('data-current', 1);
						kvSlider.checkCurrent();
						kvSlider.checkPagenation();
						var new_current = kvSlider.current_index;
						if(new_current==3 || new_current==4 || new_current==5 || new_current == 6 || new_current == 7){
							if(new_current == 7){
								setSlide= setInterval(kvSlider.eventNext,10000);
							} else {
								setSlide= setInterval(kvSlider.eventNext,5000);
							}
							$('.fv_main').find('.btn_open').fadeOut('200');

						} else {
							$('.fv_main').find('.btn_open').fadeIn('200');
							setSlide= setInterval(kvSlider.eventNext,5000);
						}
					});
					$(".slide").eq(current).attr('data_current', 0).css('z-index', '1');
				}
				//もし飛び先がプラス方向だったら
				if(distance > 0){
					$(".slide").eq(current).animate({left: "-100%"}, 1000);
					$(".slide").eq(current + distance).css({
						left: '100%',
						zIndex: '2'
					}).animate({left:"0%"}, 1000,function(){
						$(".btn_slide").attr('data_id', 0);
						$(".slide").attr('data-current', 0).eq(current + distance).attr('data-current', 1);
						kvSlider.checkCurrent();
						kvSlider.checkPagenation();
						var new_current = kvSlider.current_index;
						if(new_current==3 || new_current==4 || new_current==5 || new_current == 6 || new_current == 7){
							$('.fv_main').find('.btn_open').fadeOut('200');
							if(new_current == 7){
								setSlide= setInterval(kvSlider.eventNext,10000);
							} else {
								setSlide= setInterval(kvSlider.eventNext,5000);
							}
						} else {
							$('.fv_main').find('.btn_open').fadeIn('200');
							setSlide= setInterval(kvSlider.eventNext,5000);
						}
					});
					$(".slide").eq(current).attr('data_current', 0).css('z-index', '1');
				}
			}
		});
	},
}


$(document).ready(function() {
	$(".fv_img .slide").css('width', $(window).width());
});

$(window).resize(function() {
	$(".fv_img .slide").css('width', $(window).width());
});
$(window).load(function() {
	setTimeout(function(){
		setSlide= setInterval(kvSlider.eventNext,5000);
	},4000);
	kvSlider.gotoClicknum();
});
