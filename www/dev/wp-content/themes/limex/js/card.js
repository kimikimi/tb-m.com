$(function () {
    var $card = $('.card-slider');
    $card.find('div:first').find('.selected').show();

    // $card.find('div').click(function () {
        // var selected = $(this).find('img').attr('alt');
        // $('.meishi_card_type').val(selected);
        // $card.find('.selected').hide();
        // $(this).children('.selected').show();
        // $('#meishi_card_type').val(selected);
    // });

    $('.card-slider div').on('click', function () {
        var color_num = $($(this).parents('.meishi_color_div').get(0)).data('color');
        var selected = $(this).find('img').attr('alt');

        $('#meishi_card_type_' + color_num).val(selected);
        $('#meishi_' + color_num + ' .card-slider').find('.selected').hide();
        $(this).children('.selected').show();
    });
});