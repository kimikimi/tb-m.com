(function(){

/*--------------------
scroll object
--------------------*/

var scroll = new function(){
	var tgts = [], dly = false, old = -1, first = true;
	
	//check items
	var check = function(){
		if(!dly){
			dly = true;
			var now = $(document).scrollTop(), count = 0, hitArea;
			if(first){
				first = false;
				hitArea = [now, now + ua.h];
				for(var k=0, len=tgts.length; k<len; k++){
					((tgts[k][1] <= hitArea[0]) || (hitArea[1] <= tgts[k][0])) && tgts[k][2].css('opacity', '0.0');
				}
			}else if(old < now){
				hitArea = [now, now + ua.h - 50];
				for(var i=0, len=tgts.length; i<len; i++){
					var topd = tgts[i][0], btmd = tgts[i][1], tgtd = tgts[i][2];
					clearTimeout(tgts[i][3]); //timer stop
					if((hitArea[0] < btmd) && (topd < hitArea[1]) && (tgtd.css('opacity') == 0)){
						var dly = (tgtd.attr('class') === 'icon') ? 10 : 1; //icon delay
						(function(tgtd){
							tgts[i][3] = setTimeout(function(){tgtd.animate({'opacity':'1.0'}, {duration:500, easing:'linear', complete:function(){
								//ie png err fix
								var stl = $(this).attr('style').toString();
								(0 <= stl.indexOf('alpha(opacity=100); ')) && $(this).attr('style', stl.split('alpha(opacity=100); ').join(''));
							}});}, 20 * count * dly); //start timer
							count++;
						})(tgtd);
					}else if(btmd <= hitArea[0]){
						tgtd.stop().css('opacity', '0.0');
					}
				}
			}else if (now < old){
				hitArea = [now + 25, now + ua.h];
				for(var j=tgts.length-1; 0<=j; j--){
					var topu = tgts[j][0], btmu = tgts[j][1], tgtu = tgts[j][2];
					clearTimeout(tgts[j][3]); //timer stop
					if((hitArea[0] < btmu) && (topu < hitArea[1]) && (tgtu.css('opacity') == 0)){
						(function(tgtu){
							tgts[j][3] = setTimeout(function(){tgtu.animate({'opacity':'1.0'}, {duration:500, easing:'linear', complete:function(){
								//ie png err fix
								var stl = $(this).attr('style').toString();
								(0 <= stl.indexOf('alpha(opacity=100); ')) && $(this).attr('style', stl.split('alpha(opacity=100); ').join(''));
							}});}, 20 * count); //start timer
							count++;
						})(tgtu);
					}else if(hitArea[1] <= topu){
						tgtu.stop().css('opacity', '0.0');
					}
				}
			}
			//timeout
			setTimeout(function(){
				dly = false;
				if(old !== now){
					old = now;
					check(); //finish deal
				}
			}, 25 * (count + 1));
		}
	};
	
	//init & first view
	this.init = function(){
		old = $(document).scrollTop();
		$('#about .img_layer > *').each(function(){
			var tgt = $(this);
			var top = parseInt(tgt.offset().top), btm = top + parseInt(tgt.css('height'));
			tgts.push([top, btm, tgt, void(0)]); //item 4 is timer obj
		});
		setInterval(function(){
			if(old !== $(document).scrollTop()){
				check();
			}
		}, 200);
		check();
	};
};

/*--------------------
init
--------------------*/

$(window).load(function(){
	scroll.init();
});

})();