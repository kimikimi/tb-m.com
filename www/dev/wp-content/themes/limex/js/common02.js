/*--------------------
user agent object
--------------------*/

var ua = new function(){
	//browser & machine
	try{
		var uaStr = window.navigator.userAgent.toLowerCase();
		if(0 <= uaStr.indexOf('opera')){
			this.opera = true;
		}else if(0 <= uaStr.indexOf('chrome')){
			this.chrome = true;
		}else if(0 <= uaStr.indexOf('safari')){
			this.safari = true;
		}else if(0 <= uaStr.indexOf('firefox')){
			this.firefox = true;
		}else if(0 <= uaStr.indexOf('msie')){
			this.ie = true;
		}else if(0 <= uaStr.indexOf('trident')){
			this.trident = true;
		}
		uaStr.match(/(iphone|ipod|ipad|android|mobile|tablet|windows phone|symbian)/i) && (this.sp = true);
	}catch(e){}
	
	//window resize init
	this.resize = function(){
		this.h = this.sp ? window.innerHeight : $(window).height();
		this.w = $(window).width();
	};
};
$(window).on('resize', function(){setTimeout(function(){ua.resize();}, 100)}); //IE fix delay 100
ua.resize();

/*--------------------
fix
--------------------*/

$(function(){
	if(ua.ie){
		//img filter for ie
		$('#top img, #about img, #mission img').each(function(){
			($(this).attr('src').match(/(.png)/i)) && $(this).css('filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + $(this).attr('src') + '", sizingMethod="scale");');
		});
	}
	
	//*old common.js
	$(".logo img,.skip,#pdfDl img,.fade01").hover(function(){
		$(this).fadeTo(0, 0.6);
	},function(){
		$(this).fadeTo(300, 1.0);
	});
});

/*--------------------
sub-navi
--------------------*/

var currentTargetId = null;

$(window).on('mousemove', function(e) {
	currentTargetId = $(e.target).data('id');
});

$(function(){
	$("#gnav > ul > li").on('mouseenter', function(e) {
		var $this = $(this);
		var hoverTargetId = $(e.target).data('id');
		
		setTimeout(function() {
			if (currentTargetId == hoverTargetId) {
				$this.children('.g-sub-navi').stop().fadeIn();
				$this.siblings().children('.g-sub-navi').stop().fadeOut();
			}
		}, 200);
	});

	$('#gnav > ul, #gnav .sub-navi').on('mouseleave', function() {
		$(this).find('.g-sub-navi').stop().fadeOut();
	});
});



/*--------------------
gnavi fixed
--------------------*/

$(function(){
    var box    = $("header");
    var boxTop = box.offset().top;
    $(window).scroll(function () {
        if($(window).scrollTop() >= boxTop) {
            box.addClass("fixed");
        } else {
            box.removeClass("fixed");
        }
    });
});


/*--------------------
banner
--------------------*/
$(function(){
	var topBtn = $('#order-menu');
	var deleted = false;
	topBtn.hide();
	
	/* scrollstop */
  $(window).on("scrollstop",function(){
		if ( deleted == false ){
			topBtn.fadeIn();
		}
  });
	
	/* */
	$(window).scroll(function() {
			topBtn.fadeOut();	
  });
	
	$('.btn_delete').on('click', function() {
		deleted = true;
		topBtn.fadeOut();
	});

});
