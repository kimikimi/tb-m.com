//////////////////////////////////////////
//	jQuery common
//////////////////////////////////////////
var w = 0;	//width
var h = 0;	//height
$(function(){

//// fade ////
$(".logo img,.skip,#pdfDl img,.face01").hover(function(){
	$(this).fadeTo(0, 0.6);
	},function(){
	$(this).fadeTo(300, 1.0);
});

//// IE PNG ////
if(navigator.userAgent.indexOf("MSIE") != -1) {
	$('img').each(function() {
		if($(this).attr('src').indexOf('.png') != -1) {
			$(this).css({
				'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + jQuery(this).attr('src') + '", sizingMethod="scale");'
			});
		}
	});
}

//// pageScroll ////
$('.scroll[href^="#"]').click(function(e) {
	var id = $(this).attr("href");
	var offset = 60;
	var target = $(id).offset().top - offset;
	$('html, body').animate({
		scrollTop:target
	}, 500);
	e.preventDefault();
});


});

//// news tab ////
$(function(){
	$("#tab a").on("click", function() {
		$("body.news .data").hide();
		$($(this).attr("href")).fadeToggle();
		$("#tab a").removeClass('cur');
		$(this).addClass('cur');
		return false;
	});
});

//// form check ////
function form_check01(){
	var val01=$("input[name='form-agree']:checked").val();
	if(val01=='ok'){
		$("input[name='form-agree']").remove();
		document.frm_main.submit();
	}else if(val01=='ng'){
		window.location = "http://tb-m.com/";
	}else{
		alert('個人情報の取得・利用についてに同意するにチェックをつけてください');
	}
}


//// ie link border fix ////
$(function(){$("a").bind("focus",function(){if(this.blur)this.blur();});});


$(window).load(function(){

})
