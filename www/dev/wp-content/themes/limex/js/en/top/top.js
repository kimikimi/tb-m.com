//----------------------------------------
//	top
//----------------------------------------

$(function(){
	$(".cate_pr").on("click", function() {
		changeTab($(".all"), $(".press-release"), $(this));
	});
	$(".cate_all").on("click", function() {
		changeTab($(".press-release"), $(".all"), $(this));
	});

	var changeTab = function(currentList, hideList, currentTab) {
		currentList.css({ 
			opacity: 0
		}).hide();

		hideList.css({ 
			opacity: 1
		}).fadeIn(700);

		currentTab.addClass('cur');
		currentTab.siblings('li').removeClass('cur');
	}
	$("#bgndVideo").mb_YTPlayer();
});
 // var watchBgPos = function(val){
 //    if(device === 'sp') return;
 //    for (var i = 0 ; i < $_prlx.obj.length ; i++){
 //      $_prlx.obj[i].css({'background-position': '0px ' + (val - $_prlx.pos[i]) + 'px'});
 //    }
 //  }
