(function(){

/*--------------------
scroll object
--------------------*/

var scroll = new function(){
	var bks, dly = false, old = -1, first = true, state;
	
	//check items
	var check = function(){
		if(!dly){
			dly = true;
			var now = $(document).scrollTop(), count = 0, hitArea;
			if(first){ //first
				first = false;
				hitArea = [now, now + ua.h];
				for(var i=0, ilen=bks.length; i<ilen; i++){
					((bks[i][1] <= hitArea[0]) || (hitArea[1] <= bks[i][0])) && bks[i][2].css('opacity', '0.0').next('div').find('img').css('opacity', '0.0');
				}
			}else if(old < now){ //scroll dowu
				hitArea = [now, now + ua.h - 200];
				for(var j=0, jlen=bks.length; j<jlen; j++){
					if((hitArea[0] < bks[j][1]) && (bks[j][0] < hitArea[1]) && (bks[j][2].css('opacity') == 0)){
						(function(tgt){
							setTimeout(function(){
								tgt.animate({'opacity':'1.0'}, {duration:500, easing:'linear', complete:function(){
									var delay = 0;
									$(this).next('div').find('img').each(function(){
										var tgt = $(this);
										setTimeout(function(){
											(tgt.css('opacity') == 0) && tgt.animate({'opacity':'1.0'}, {duration:750, complete:function(){
												//ie png err fix
												var stl = $(this).attr('style').toString();
												(0 <= stl.indexOf('alpha(opacity=100); ')) && $(this).attr('style', stl.split('alpha(opacity=100); ').join(''));
											}});
										}, 100 * delay);
										delay++;
									});
								}});
							}, 100 * count);
							count++;
						}(bks[j][2]));
					}else if(bks[j][1] <= hitArea[0]){
						bks[j][2].stop().css('opacity', '0.0').next('div').find('img').css('opacity', '0.0');
					}
				}
			}else if (now < old){ //scroll up
				hitArea = [now + 200, now + ua.h];
				for(var k=bks.length-1; 0<=k; k--){
					if((hitArea[0] < bks[k][1]) && (bks[k][0] < hitArea[1]) && (bks[k][2].css('opacity') == 0)){
						(function(tgt){
							setTimeout(function(){
								tgt.animate({'opacity':'1.0'}, {duration:500, easing:'linear', complete:function(){
									var delay = 0;
									$(this).next('div').find('img').each(function(){
										var tgt = $(this);
										setTimeout(function(){
											(tgt.css('opacity') == 0) && tgt.animate({'opacity':'1.0'}, {duration:750, complete:function(){
												//ie png err fix
												var stl = $(this).attr('style').toString();
												(0 <= stl.indexOf('alpha(opacity=100); ')) && $(this).attr('style', stl.split('alpha(opacity=100); ').join(''));
											}});
										}, 100 * delay);
										delay++;
									});
								}});
							}, 100 * count);
							count++;
						}(bks[k][2]));
					}else if(hitArea[1] <= bks[k][0]){
						bks[k][2].stop().css('opacity', '0.0').next('div').find('img').css('opacity', '0.0');
					}
				}
			}
			//timeout
			setTimeout(function(){
				dly = false;
				if(old !== now){
					old = now;
					check(); //finish deal
				}
			}, 200 * (count + 1));
		}
	};
	
	//resise init
	var bksInit = function(){
		bks = [];
		old = $(document).scrollTop();
		$('#mission .bk').each(function(){
			var bk = $(this);
			if(ua.w < 1440){
				bk.css('margin-left', ((ua.w - parseInt(bk.css('width'))) / 2).toString() + 'px'); //img center
			}else{
				bk.css('margin-left','0px');
			}
			var top = parseInt(bk.offset().top), btm = top + parseInt(bk.css('height'));
			bks.push([top, btm, bk]);
		});
		
		state = [ua.h, ua.w];
	}
	
	//init & first view
	this.init = function(){
		bksInit();
		
		setInterval(function(){
			if(state.toString() !== [ua.h, ua.w].toString()){ //changed window size
				bksInit();
			}else if(old !== $(document).scrollTop()){ //scrolled
				check();
			}
		}, 200);
		
		check();
	};
};

/*--------------------
init
--------------------*/

$(window).load(function(){
	scroll.init();
});

})();