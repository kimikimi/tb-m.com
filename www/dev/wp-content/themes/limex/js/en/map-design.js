/////// googlemap API /////
var map;
var map1;
var cent = new google.maps.LatLng(35.6731335,139.7674044); // 中心の座標
var MY_MAPTYPE_ID = 'cool';
var marker;
var marker01;
var infowindow1;
var windowOpenFlag = 0;
var window1OpenFlag = 0;
var handler1="";
var isSmall = 0;
var bounceFlag = 0;
function initialize() {
	var stylez = [
	{
		"stylers": [
			{ "hue": "#c3c3c3" },
      		{ "lightness": -1 },
      		{ "gamma": 0.92 },
      		{ "visibility": "simplified" },
      		{ "saturation": -100 }
		]
	}
	];
	//***** デザインのカスタマイズ部分 *****//
	//MAP1-----------------------------
	var mapOptions01 = {
		zoom: 17,                  // マップの拡大値
		center: cent,
		navigationControl:false,  // マップの拡大縮小等のナビの表示・非表示を行います。
		mapTypeControl:false,     // 右上マップタイプ名を表示し、通常マップとの切替を可能にします。
		scaleControl:false,       // 左下にスケールを表示します。
		scrollwheel:false,
		draggable: false,
		mapTypeId: MY_MAPTYPE_ID
	};

	map1 = new google.maps.Map(document.getElementById("map"),mapOptions01);

	var styledMapOptions01 = {name: "sample"};
	var jayzMapType = new google.maps.StyledMapType(stylez, styledMapOptions01);map1.mapTypes.set(MY_MAPTYPE_ID, jayzMapType);
	var icon = new google.maps.MarkerImage('/wp-content/themes/limex/img/company/aboutus/pin01@2x.png', null, null, null, new google.maps.Size(60, 72), new google.maps.Point(0, 0));
	var markerOpts01 = {
			position: new google.maps.LatLng(35.6731335,139.7674044), // マーカの座標
			map: map1,
			icon:icon,	// マーカーアイコンを画像に変更
			draggable:false
	};
	latlng = new google.maps.LatLng(35.6731335,139.7674044);//35.673993, 139.738423
	marker01 = new google.maps.Marker(markerOpts01);

}
var markerEventBind = function(){
	marker01.setClickable(true);
}
var markerEventUnBined = function(){
	marker01.setClickable(false);
}
$(window).load(function(){

	$("#map").css("height","600px");
	//setTimeout(function(){$(".gmnoprint").remove();},800);


	initialize();
});
