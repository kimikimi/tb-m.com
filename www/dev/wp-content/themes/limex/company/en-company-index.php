<?php
/**
 * Template Name: en-company-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>


<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/company/" class="cur">ABOUT US</a></li>
				<li><a href="/en/company/profile/">PROFILE</a></li>
				<li><a href="/en/company/identity/">IDENTITY</a></li>
				<li><a href="/en/company/history/">HISTORY</a></li>
				<li><a href="/en/company/message/">MESSAGE</a></li>
				<li><a href="/en/company/story/story1/">STORY</a></li>
				<li><a href="/en/company/access/">ACCESS</a></li>
				<li><a href="/en/company/award/">AWARD</a></li>
			</ul>
		</div>
	</div>
	<h3>ABOUT US</h3>

	<div class="wrapper">
		<div class="data" style="line-height:1.5;">
			<table>
				<tr>
					<th>Company name</th>
					<td>TBM Co., Ltd.</td>
				</tr>
				<tr>
					<th>Chainman of Boards of Directors</th>
					<td>Yuichiro Sumi (Former senior managing director of Nippon Paper Industries CO., LTD.)</td>
				</tr>
				<tr>
					<th>Chief Executive Officer</th>
					<td>Nobuyoshi Yamasaki</td>
				</tr>
				<tr>
					<th>Date of Foundation</th>
					<td>August 30, 2011</td>
				</tr>
				<tr>
					<th>Tokyo Headquarters</th>
					<td>
					The 6th Floor, 2-7-17 ,Ginza, Chuou-ku, Tokyo 104-0061
					<a class="link_access" href="/en/company/access/"><span></span>ACCESS</a>
					</td>
				</tr>
				<tr>
					<th>Telephone number</th>
					<td>+81 (0)3 3538 6777</td>
				</tr>
				<tr>
					<th>FAX number</th>
					<td>+81 (0)3 3538 6778</td>
				</tr>
				<tr>
					<th>Capital</th>
					<td>2.54 billion yen (Including legal capital surplus)</td>
				</tr>
				<tr>
					<th>Law Advising Firm</th>
					<td>Nishimura Asahi Law office</td>
				</tr>
				<tr>
					<th>Business Division</th>
					<td>Research&Devleopment,production and sales of LIMEX</td>
				</tr>
			</table>
		</div>
	</div>

</section><!-- /#body -->

<?php
get_footer('en');
