<?php
/**
 * Template Name: en-company-profile
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>


<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/company/">ABOUT US</a></li>
				<li><a href="/en/company/profile/" class="cur">PROFILE</a></li>
				<li><a href="/en/company/identity/">IDENTITY</a></li>
				<li><a href="/en/company/history/">HISTORY</a></li>
				<li><a href="/en/company/message/">MESSAGE</a></li>
				<li><a href="/en/company/story/story1/">STORY</a></li>
				<li><a href="/en/company/access/">ACCESS</a></li>
				<li><a href="/en/company/award/">AWARD</a></li>
			</ul>
		</div>
	</div>
	<h3>Leadership Team</h3>
	<div class="wrapper">
		<section>
			<h4>Directors & Officers</h4>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-01.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>Chief Executive Officer</span>Nobuyoshi Yamasaki </p>
					<p>Brief Summary of Personal History</p>
					<table>
						<!-- <tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所朝日監査法人（現 有限責任あずさ監査法人）入所朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr> -->
						<tr>
							<td colspan="2" class="career_text">
<p>Founded a Car Dealing Company at 20 , which was the start of his career as an entrepreneur and successfully set up several businesses then after. In his 30s he set up TBM (Times Bridge Management) as he rose to challenge to set up a trillion yen size global company that contributes to the happiness of men even after 100 years, and to build a bridge to the future generations.</p>
<p>Received 'Reconstruction Award' of Japan New Business Awards in 2014, "Special Award" of Job Creation Awards in 2015 and 'Great East Japan Earthquake Reconstruction Award' of Japan Venture Awards in 2016.</p>
 							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<div class="img_block">
						<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-02.png" alt="">
					</div>
					<div class="author">
						<p>His Writings</p>
						<p>“Science of Paper”
						</p>
					</div>
				</div>
				<div class="txt">
					<p class="name"><span>Chairman</span>Dr.&nbsp;Yuichiro&nbsp;Sumi </p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year_month">1989</td>
							<td>Managing Director of Sanyo Kokusaku Pulp Industry Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year_month">1993</td>
							<td>Managing Director of Nippon Paper Industries Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year_month">1999</td>
							<td>CEO of VENTURE SUPPORT Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year_month">2011</td>
							<td>Director of TBM Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year_month">2014</td>
							<td>Chairman of Board of Directors of TBM Co., Ltd.</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-08.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>Director</span>Kenji&nbsp;Fukahori</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year">1993</td>
							<td>Joined The Dai-Ichi Kangyo Bank,Ltd. （current Mizuho Bank, Ltd. )</td>
						</tr>
						<tr>
							<td class="year">2001</td>
							<td>Joined Mizuho Securities Co., Ltd. Investment Bank </td>
						</tr>
						<tr>
							<td class="year">2010</td>
							<td>Registered Lawyer, Japan Federation of Bar Associations</td>
						</tr>
						<tr>
							<td class="year">2010</td>
							<td>Joined Yaesu Sogo Law Office</td>
						</tr>
						<tr>
							<td class="year">2011</td>
							<td>Auditor of Sanei Securities co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2013</td>
							<td>Auditor of Daiya Tsusho Co.,Ltd.</td>
						</tr>
						<tr>
							<td class="year">2014</td>
							<td>Director of TBM Co., Ltd Director</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-04.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>Director</span>Takashi Kobayashi</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year">1994</td>
							<td>Joined KYOCERA Corp.</td>
						</tr>
						<tr>
							<td class="year">1996</td>
							<td>Joined Akiya Accounting firm</td>
						</tr>
						<tr>
							<td class="year">1999</td>
							<td>President of BLUE GIRAFFE Inc.</td>
						</tr>
						<tr>
							<td class="year">2012</td>
							<td>Director of TBM Co., Ltd.</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-05.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>Director</span>Tadato&nbsp;Kataji</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year">1993</td>
							<td>Joined Nomura Securities Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2000</td>
							<td>Director of HTC Co., Ltd.  Investing Manager of Venture Capital</td>
						</tr>
						<tr>
							<td class="year">2004</td>
							<td>Joined Japan Lep Co., Ltd (Goodman Japan Co., Ltd) and started Asset Management Business</td>
						</tr>
						<tr>
							<td class="year">2006</td>
							<td>IPO of Japan Lep Cp., Ltd as CEO of the company</td>
						</tr>
						<tr>
							<td class="year">2009</td>
							<td>Inauguration of CEO of RIC Management Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2010</td>
							<td>President of Eco-Hai Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2013</td>
							<td>Director of TBM Co., Ltd.</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-06.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>Director</span>Minoru&nbsp;Sugimori</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year">1996</td>
							<td>President of ISHIZUE Corp.</td>
						</tr>
						<tr>
							<td class="year">201年</td>
							<td>Director of TBM Co., Ltd.</td>
						</tr>
						<tr>
							<td class="career_text" colspan="2">
<p>Before setting up the ISHIZUE Corp. experienced a wide and deep career by working at an advertising agency, construction company  and real estate company for many years. Offering the best solutions to his clients, ranging from real estate matters to market research, product planning,  concept making,  designing, advertising, opening and marketing strategies. Headquarter in Kyoto and holds branches in Tokyo and Osaka. </p>
<p> Ranked 11 of the 'TOP 150 Vigorous Local Company Ranking' in DIAMOND, a famous weekly business magazine.   Elected from about 3 million local companies,  excluding companies from Tokyo, Osaka, Chiba, Saitama and Kanagawa. (DIAMOND No.104 (2015.12.26/2016.01.02 new year's big issue).</p>
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-17.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>Director</span>Koji&nbsp;Sakamoto</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year">1990</td>
							<td>Joined  ITOCHU Corporation</td>
						</tr>
						<tr>
							<td class="year">2007</td>
							<td>President and Representative Director of Excite Japan Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2012</td>
							<td>Corporate Officer (Consumer Business) of Yahoo Japan Corporation</td>
						</tr>
						<tr>
							<td class="year">2013</td>
							<td>President of YAHUOKU! Company of Yahoo Japan Corporation</td>
						</tr>
						<tr>
							<td class="year">2014</td>
							<td>President of YJ America, Inc.</td>
						</tr>
						<tr>
							<td class="year">2016</td>
							<td> Outside Director of TBM Co., Ltd.</td>
						</tr>
					</table>
				</div>
			</article>
			<!-- <article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-03.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>松木&nbsp;貴志</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="career_text">
							
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-04.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>小林&nbsp;孝至</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td colspan="2" class="career_text">
							
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-05.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>片地&nbsp;格人</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td colspan="2" class="career_text">
							
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-06.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>杉森&nbsp;実</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td colspan="2" class="career_text">
							
							</td>
						</tr>
					</table>
				</div>
			</article> -->
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-10.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>Corporate Officer<br>Sales</span>Kouji Aonuma</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="career_text">
							After joining Nissho Iwai Corporation (current Sojitz Corporation), responsible for developing and launching new projects in the New Business Development Group. Seconded to the new business company launched and worked as the planning sales General Manager for sales and business operation. Then after, joined the Motor Vehicle Division and was in charge of  motor vehicle business in Latin America and new business set up in Southeast Asia. Stationed in Lain America and managed the vehicle fabrication plant operation and the sales. Joined TBM in February 2016.							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-11.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>Corporate Officer<br>Corporate Planning</span>Taichi&nbsp;Yamaguchi</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="career_text">
<p>Joined Fuji Xerox Co., Ltd. and was responsible for new businesses in the Production Service Sales Division. Then after joined PricewaterhouseCoopers co., Ltd.,  carrying out due diligence, planning and executing business recovery plans,  supporting negotiation with financial institutions, forming and executing growth strategies in the Business Recovery Services Group. </p>
<p>After moving to the Deal Advisory Group, was project manager for various cross-border M&A transactions and PMI( Post Merger Integration) projects.</p>
<p>Joined TBM Co., Ltd in September 2015. Currently engaged in drawing up business strategies, business plans and capital policy and project managing the new plant in the Corporate Planning Division.</p>
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<div class="img_block">
						<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-12.png" alt="">
					</div>
					<div class="author">
						<p>主な著書</p>
						<p>「自分ゴト化　社員の行動をブランディングする」(ファーストプレス、共著)<br>「広告のやりかたで就活をやってみた」(宣伝会議、企画・制作)<br>「なぜ君たちは就活になるとみんな同じようなことばかりしゃべりだすのか」（宣伝会議、共著）<br>一般社団法人日本広告業協会第４０回懸賞論文「次代のフロントライン」入選。
						</p>
					</div>
				</div>
				<div class="txt">
					<p class="name"><span>Corporate Officer<br>Corporate Communication</span>Takayuki&nbsp;Sasaki</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="career_text">
<p>Joined Dentsu Inc. and worked in the Creating the Future Group,. striving to breakthrough and reviving management's and company's business matters with 'ideas'. Worked as a Chief Planner to develop new business and new products, to plan and produce new stores with marketing as a starting point.  Also a member of Dentsu Communication Institute Inc. B Team. </p>

<p>Won MVP Award and semi-MVP Award at the  Dentsu  Inc. Solution Division's  Best Practices Forum. Joined TBM Co., Ltd in April 2016.  Senior Researcher of Keio Research Institute at SFC.</p>
							</td>
						</tr>
					</table>
				</div>
			</article>

			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-16.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>Full Time Auditor</span>Koichi&nbsp;Kato</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year">1988</td>
							<td>Joined Recruit Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2000</td>
							<td>First time elected as a representative in the general election of the members of the House of Representatives</td>
						</tr>
						<tr>
							<td class="year">2009</td>
							<td>Appointed as Senior Vice-Minister of Justice</td>
						</tr>
						<tr>
							<td class="year">2010</td>
							<td>Appointed as Special Adviser to the Prime Minister</td>
						</tr>
						<tr>
							<td class="year">2011</td>
							<td>Appointed as Member of the Japanese National Commission for UNESCO</td>
						</tr>
						<tr>
							<td class="year">2016</td>
							<td>Auditor of TBM Co., Ltd.</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-07.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>Part Time Auditor</span>Masaru&nbsp;Mizuno</span></p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year">1961</td>
							<td>Joineding Marubeni Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">1999</td>
							<td>Vice President of Marubeni Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2007</td>
							<td>Chairman of the Board of Pasona Co., Ltd.( until 2011）</td>
						</tr>
						<tr>
							<td class="year">2013</td>
							<td>Director of TBM Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2014</td>
							<td>Auditor of TBM Co., Ltd.</td>
						</tr>
					</table>
				</div>
			</article>
		</section>

		<section>
			<h4>Advisers</h4>
			<article>
				<div class="img">
					<div class="img_block">
						<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-13.png" alt="">
					</div>
					<div class="author">
						<p>His Writings</p>
						<p> “Directors in Japan”  “Japan Company History” <br>
						“Great Japanese Economy after WWII”<br>
						“Kounosuke Matsushita ~the person and his business”<br>
						 “Modern Management”
						</p>
					</div>
				</div>
				<div class="txt">
					<p class="name"><span>Supreme Advisor</span>Kazuo&nbsp;Noda</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year">1952</td>
							<td>Graduated from the University of Tokyo, Sociology Department (Industrial Sociology)</td>
						</tr>
						<tr>
							<td class="year">1953</td>
							<td>Fellowship Researcher, Graduate School, University of Tokyo (Business Management)</td>
						</tr>
						<tr>
							<td class="year">1960</td>
							<td>Post Doctoral Fellow at Massachusetts Institute of Technology</td>
						</tr>
						<tr>
							<td class="year">1975</td>
							<td>Fellow at Harvard University</td>
						</tr>
						<tr>
							<td class="year">1981</td>
							<td>President of Japan Research Institute (until 2001)</td>
						</tr>
						<tr>
							<td class="year">1985</td>
							<td>President and Founder of New Business Conference, a non-profit organization of  entrepreneurs</td>
						</tr>
						<tr>
							<td class="year">1989</td>
							<td>President and Founder of Tama University [private university]</td>
						</tr>
						<tr>
							<td class="year">1993</td>
							<td>Chairman of Japan Management School (to present)</td>
						</tr>
						<tr>
							<td class="year">1995</td>
							<td>Honorary President of Tama University upon resignation as President (to present)</td>
						</tr>
						<tr>
							<td class="year">1997</td>
							<td>President and Founder of Miyagi University [Prefectural university] (until 2001)</td>
						</tr>
						<tr>
							<td class="year">2001</td>
							<td>Chairman of Japan Research Institute</td>
						</tr>
						<tr>
							<td class="year">2006</td>
							<td>President of Japan Research Institute (to present)</td>
						</tr>
						<tr>
							<td class="year">2007</td>
							<td>Trustee of the Graduate School of Management, Globis University</td>
						</tr>
						<tr>
							<td class="year">2013</td>
							<td>Supreme Advisor of TBM Co., Ltd.</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<div class="img_block">
						<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-14.png" alt="">
					</div>
					<div class="author">
						<p>His Writings</p>
						<p>“Klop and Make Business” (Seisansei Shuppan)<br>
						“Spirit of Kao ~Importance of Accomplishing  Business~” (Seisansei Shuppan)
						</p>
					</div>
				</div>
				<div class="txt">
					<p class="name"><span>Technical Advisor</span>Tetsuya Imamura</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year">1967</td>
							<td>Joined Kao Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">1992</td>
							<td>
Director of Technical Development Division  in Kao Co., Ltd.<br>
Responsible for product development and commercialization of Floppy Disk. Market share grew to the top (15%) in 1992.
Directing the R&D group , and after becoming Director of  Kao Co., Ltd , directing the product development of “Econa Healthy Oil” and “Helthia Green Tea” as general manager of the Healthcare Division. Well managing the new product and intellectual property rights strategy and both products gained the largest market share.
							</td>
						</tr>
						<tr>
							<td class="year">2005</td>
							<td>Leave Kao CO, Ltd.</td>
						</tr>
						<tr>
							<td class="year">2005</td>
							<td>Fulltime Advisor of Kikkoman Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2010</td>
							<td>Advisor of Mitsui Chemical Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2013</td>
							<td>Technical Advisor of TBM Co., Ltd.</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
				</div>
				<div class="txt">
					<p class="name"><span>Advisor</span> Daisuke&nbsp;Tanida</p>
					<p>Brief Summary of Personal History</p>
					<table>
						<tr>
							<td class="year">1966</td>
							<td>Joined Tanita Factory Co., Ltd. (Tanita Corporation)</td>
						</tr>
						<tr>
							<td class="year">1987</td>
							<td>
President of Tanita Corporation<br>
"Tanita" became the top brand of health measuring machine in Japan
							 </td>
						</tr>
						<tr>
							<td class="year">2010</td>
							<td>Chairman of Board of Directors in Eco-Hai Co., Ltd.</td>
						</tr>
						<tr>
							<td class="year">2013</td>
							<td>Director of TBM Co., Ltd.</td>
						</tr>
					</table>
				</div>
			</article>
		</section>
	</div>

</section><!-- /#body -->

<?php
get_footer('en');
