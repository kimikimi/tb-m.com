<?php
/**
 * Template Name: en-company-story-list
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

 	// story1にリダイレクト
 	header( "Location: https://tb-m.com/en/company/story/story1/" ) ;

 	// 終了
 	exit ;

get_header('en'); ?>


<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/company/">ABOUT US</a></li>
				<li><a href="/en/company/profile/">PROFILE</a></li>
				<li><a href="/en/company/identity/">IDENTITY</a></li>
				<li><a href="/en/company/history/">HISTORY</a></li>
				<li><a href="/en/company/message/">MESSAGE</a></li>
				<li><a href="/en/company/story/"  class="cur">STORY</a></li>
				<li><a href="/en/company/access/">ACCESS</a></li>
				<li><a href="/en/company/award/">AWARD</a></li>
			</ul>
		</div>
	</div>
	

	<div class="wrapper">
		<ul class="link-list">
			<li><a href="/en/company/story/story1/">
				<p class="img"><img src="<?php echo get_template_directory_uri(); ?>/img/en/company/story-list/img-story-list-01.png" alt=""></p>
				<div class="title">
					<p class="title_top">
						Chairman Dr.Yuichiro Sumi
					</p>
					<span></span>
					<p class="title_bottom">
						Chief Executive Officer Nobuyoshi Yamasaki
					</p>
				</div>
				<p class="link-text">
					<span></span>READ MORE
				</p>
			</a></li>

			<li><a href="">
				<p class="img"><img src="<?php echo get_template_directory_uri(); ?>/img/en/company/story-list/img-story-list-02.png" alt=""></p>
				<p class="title">
					角会長<span></span>山﨑社長
				</p>
				<p class="link-text">
					<span></span>READ MORE
				</p>
			</a></li>
		</ul>
	</div>


</section><!-- /#body -->

<?php
get_footer('en');
