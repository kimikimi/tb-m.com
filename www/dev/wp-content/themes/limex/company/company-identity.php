<?php
/**
 * Template Name: company-identity
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
			<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/company/">会社概要</a></li>
				<li><a href="/company/profile/">経営陣プロフィール</a></li>
				<li><a href="/company/identity/" class="cur">企業理念体系</a></li>
				<li><a href="/company/history/">沿革</a></li>
				<li><a href="/company/message/">創設者メッセージ</a></li>
				<li><a href="/company/story/">創設ストーリー</a></li>
				<li><a href="/company/access/">アクセス</a></li>
				<li><a href="/company/award/">受賞歴</a></li>
			</ul>
		</div>
	</div>
	<div class="wrapper">
		<h3>企業理念体系</h3>
		<div class="sections">
			<section class="sec sec1">
				<h4>TBM IDENTITY</h4>
				<h5>MISSION</h5>
				<p>今までにない笑顔が、人と人をつなぐ世界をつくる。</p>
			</section>
			<section class="sec sec2">
				<h5>VISION</h5>
				<p>過去を活かして未来を創る。100年後でも持続可能な循環型イノベーション。</p>
			</section>
			<section class="sec sec3">
				<h5>CREDO</h5>
				<ol>
					<li class="li1">橋を架ける人は、過去を知り未来を想い、新たな価値を創造する。<br />そのために今日すべきことを具体的に見極め、速やかに実行する。</li>
					<li class="li2">橋を架ける人は、すべての汗と涙の先に、世界と自分の幸せがある。<br />そのための知識を吸収し、自らを鍛え、目の前の仕事にあらゆる手を尽くす。</li>
					<li class="li3">橋を架ける人は、自分の能力より大きな仕事に全力でぶつかる。<br />自らの欠点を謙虚に見つめ、しかし生みの苦しみから決して逃げず成長してゆく。</li>
					<li class="li4">橋を架ける人は、理想と現実のバランス感覚を磨いている。 <br />仕事相手の願い、自分や仲間たちの願い、そのどちらも両立させる。</li>
					<li class="li5">橋を架ける人は、人と人との間に絆をつくる。 <br />つらい時こそ仲間を信じ、前向きに笑い、いつも感動と感謝を忘れない。</li>
				</ol>
			</section>
		</div>
		<h3>社名の由来</h3>
		<div class="sections">
			<section class="sec sec4">
				<h5>Times Bridge Management</h5>
				<p align="center"><br />当社の社名TBMの由来は、Times Bridge Management の頭文字を取り<br />
				「100年後も残る技術や事業を創り、時代の架け橋となる会社にしたい」<br />
				という想いが込められています。</p>
			</section>
		</div>
	</div>
</section><!-- /#body -->

<?php
get_footer();
