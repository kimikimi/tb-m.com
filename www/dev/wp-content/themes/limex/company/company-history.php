<?php
/**
 * Template Name: company-history
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/company/">会社概要</a></li>
				<li><a href="/company/profile/">経営陣プロフィール</a></li>
				<li><a href="/company/identity/">企業理念体系</a></li>
				<li><a href="/company/history/" class="cur">沿革</a></li>
				<li><a href="/company/message/">創設者メッセージ</a></li>
				<li><a href="/company/story/">創設ストーリー</a></li>
				<li><a href="/company/access/">アクセス</a></li>
				<li><a href="/company/award/">受賞歴</a></li>
			</ul>
		</div>
	</div>
	<div class="wrapper">
			<h3>沿革</h3>
			<div class="data" style="line-height:1.5;">
			<table>
				<tr>
					<th>2008年11月</th>
					<td>台湾製ストーンペーパーの輸入販売事業を開始</td>
				</tr>
				<tr>
					<th>2010年8月</th>
					<td>自社にて技術開発を開始</td>
				</tr>
				<tr>
					<th>2011年8月</th>
					<td>事業譲渡にて、東京都港区芝大門に新生株式会社TBMを設立</td>
				</tr>
                <tr>
					<th>2012年3月</th>
					<td>資本金：2000万円</td>
				</tr>
				<tr>
					<th>2012年5月</th>
					<td>自社開発によるLIMEXの特許を出願</td>
				</tr>
				<tr>
					<th>2012年6月</th>
					<td>日立造船株式会社と成形技術の共同開発を開始</td>
				</tr>
				<tr>
					<th>2013年2月</th>
					<td>経済産業省より、イノベーション拠点立地推進事業「先端技術実証・評価設備整備費等補助金」として採択</td>
				</tr>
                <tr>
					<th>2013年3月</th>
					<td>資本金：2億7500万円</td>
				</tr>
                <tr>
					<th>2013年8月</th>
					<td>所在地を東京都港区赤坂二丁目に移転</td>
				</tr>
				<tr>
					<th>2014年1月</th>
					<td>自社開発によるLIMEXが特許を承認</td>
				</tr>
				<tr>
					<th>2014年2月</th>
					<td>宮城県庁にて当社白石工場の立地協定を締結（宮城県白石市）</td>
				</tr>
                <tr>
					<th>2014年3月</th>
					<td>資本金：4億9,900万円（資本準備金を含む）</td>
				</tr>
                <tr>
					<th>2014年4月</th>
					<td>所在地を東京都港区赤坂三丁目に移転</td>
				</tr>
				<tr>
					<th>2014年7月</th>
					<td>当社白石工場　建設工事着工（宮城県白石市）</td>
				</tr>
                 <tr>
					<th>2014年9月</th>
					<td>資本金：12億3,000万円（資本準備金を含む）</td>
				</tr>
                <tr>
					<th>2014年10月</th>
					<td>東京都江東区の東京都立産業技術研究センターに東京ラボを設置</td>
				</tr>
                <tr>
					<th>2014年11月</th>
					<td>第9回ニッポン新事業創出大賞（主催：公益社団法人 日本ニュービジネス協議会連合会）にて「復興賞」を受賞</td>
				</tr>
				<tr>
                <tr>
					<th>2015年2月</th>
					<td>当社白石工場　完成（宮城県白石市）</td>
				</tr>
				<tr>
                <tr>
					<th>2015年3月</th>
					<td>資本金：22億5,900万円（資本準備金を含む）</td>
				</tr>
                <tr>
					<th>2015年4月</th>
					<td>所在地を東京都千代田区丸の内に移転</td>
				</tr>
                <tr>
					<th>2015年5月</th>
					<td>2015年ミラノ国際博覧会開催　協賛企業として当社LIMEX製品を提供</td>
				</tr>
                <tr>
					<th>2015年11月</th>
					<td>経済産業省より、「津波・原子力災害被災地域雇用創出企業立地補助金（製造業等立地支援事業）」として採択</td>
				</tr>
                <tr>
					<th>2015年11月</th>
					<td>Job Creation 2015（主催：新日本有限責任監査法人）にて「特別賞」を受賞</td>
				</tr>
                <tr>
					<th>2015年12月</th>
					<td>多賀城市役所にて当社第二号工場の立地協定を締結（宮城県多賀城市）</td>
				</tr>
                <tr>
					<th>2016年2月</th>
					<td>Japan Venture Awards 2016（主催：独立行政法人中小企業基盤整備機構）にて「東日本大震災復興賞」を受賞</td>
				</tr>
                <tr>
					<th>2016年2月</th>
					<td>資本金：40億6,920万円（資本準備金を含む）<br>スイスUBSグループによるビジネスマッチングを通じて総額15億円の資金調達を実施</td>
				</tr>
				<tr>
					<th>2016年5月</th>
					<td>山形大学グリーンマテリアル成形加工研究センター センター長 伊藤浩志教授と新規機能性材料を研究開発</td>
				</tr>
				<tr>
					<th>2016年6月</th>
					<td>LIMEX製品の名刺販売を本格的に開始</td>
				</tr>
				<tr>
					<th>2016年6月</th>
					<td>東京大学 生産技術研究所 沖研究室とLCA（ライフサイクルアセスメント）に関する共同研究を開始</td>
				</tr>
				<tr>
					<th>2016年6月</th>
					<td>LIMEXによる水や森林資源保護への貢献度を数値で可視化し、地球の環境問題を啓発する「LIMEX ACTION」サイトを開設</td>
				</tr>
				<tr>
					<th>2016年7月</th>
					<td>米国サンフランシスコに初の海外子会社、Times Bridge Management Global, Inc.を設立</td>
				</tr>
				<tr>
					<th>2016年8月</th>
					<td>電通と販売代理業務契約を締結</td>
				</tr>
				<tr>
					<th>2016年8月</th>
					<td>米国シリコンバレーの「Plug and Play」 新素材・梱包部門のピッチイベントで1位を獲得</td>
				</tr>
				<tr>
					<th>2016年10月</th>
					<td>米国シリコンバレーの「Plug and Play」で初の 『世の中に最も社会的影響を与える企業-ソーシャルインパクトアワード』を受賞</td>
				</tr>
				<tr>
					<th>2016年11月</th>
					<td>TBMと凸版印刷、新素材LIMEXの活用で共同開発</td>
				</tr>                
				<tr>
				<td colspan="2" style="border-bottom:0px;text-align:right;font-size:12px;">※2016年11月25日現在</td>
				</tr>
			</table>
			<br>
		</div>
	</div>

</section><!-- /#body -->

<?php
get_footer();
