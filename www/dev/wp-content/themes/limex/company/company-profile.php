<?php
/**
 * Template Name: company-profile
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/company/">会社概要</a></li>
				<li><a href="/company/profile/" class="cur">経営陣プロフィール</a></li>
				<li><a href="/company/identity/">企業理念体系</a></li>
				<li><a href="/company/history/">沿革</a></li>
				<li><a href="/company/message/">創設者メッセージ</a></li>
				<li><a href="/company/story/">創設ストーリー</a></li>
				<li><a href="/company/access/">アクセス</a></li>
				<li><a href="/company/award/">受賞歴</a></li>
			</ul>
		</div>
	</div>
	<h3>役員</h3>
	<div class="wrapper">
		<section>
			<h4>取締役</h4>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-01.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>代表取締役</span>山﨑 敦義</p>
					<p>経歴</p>
					<table>
						<!-- <tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所朝日監査法人（現 有限責任あずさ監査法人）入所朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr> -->
						<tr>
							<td colspan="2" class="career_text">
							20歳に中古車販売業を起業。複数の事業立ち上げを行う。30代になり、グローバルで勝負が出来て100年後も継承される人類の幸せに貢献できる1兆円事業を興したいと奮起。時代の架け橋となる株式会社TBMを立ち上げる。2014年ニッポン新事業創出大賞「復興賞」受賞。Job Creation 2015「特別賞」受賞。Japan Venture Awards 2016「東日本大震災復興賞」受賞。
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<div class="img_block">
						<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-02.png" alt="">
					</div>
					<div class="author">
						<p>主な著書</p>
						<p>紙の科学（中外産業調査会）
						</p>
					</div>
				</div>
				<div class="txt">
					<p class="name"><span>取締役会長</span>角&nbsp;祐一郎</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year_month">1989年6月</td>
							<td>山陽国策パルプ株式会社専務取締役</td>
						</tr>
						<tr>
							<td class="year_month">1993年6月</td>
							<td>日本製紙株式会社専務取締役</td>
						</tr>
						<tr>
							<td class="year_month">1999年6月</td>
							<td>株式会社ベンチャーサポート代表取締役社長</td>
						</tr>
						<tr>
							<td class="year_month">2011年8月</td>
							<td>株式会社TBM取締役</td>
						</tr>
						<tr>
							<td class="year_month">2014年1月</td>
							<td>株式会社TBM取締役会長就任&nbsp;現在に至る</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-08.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>深堀&nbsp;健二<span>（八重洲総合法律事務所&nbsp;弁護士）</span></p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">1993年</td>
							<td>株式会社第一勧業銀行（現みずほ銀行）&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">2001年</td>
							<td>みずほ証券㈱投資銀行部門&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">2010年</td>
							<td>日本弁護士連合会&nbsp;弁護士登録</td>
						</tr>
						<tr>
							<td class="year">2010年</td>
							<td>八重洲総合法律事務所&nbsp;入所（現任）</td>
						</tr>
						<tr>
							<td class="year">2011年</td>
							<td>三栄証券株式会社&nbsp;監査役就任（現任）</td>
						</tr>
						<tr>
							<td class="year">2013年</td>
							<td>ダイヤ通商株式会社&nbsp;監査役就任（現任）</td>
						</tr>
						<tr>
							<td class="year">2014年</td>
							<td>株式会社TBM&nbsp;監査役就任</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-04.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>小林&nbsp;孝至</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">1994年</td>
							<td>京セラ株式会社入社&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">1997年</td>
							<td>会計事務所&nbsp;入所</td>
						</tr>
						<tr>
							<td class="year">1998年</td>
							<td>デジタルクリエイターのフリーランスとして独立</td>
						</tr>
						<tr>
							<td class="year">1999年</td>
							<td>ブルージラフ株式会社設立（現任）&nbsp;社長就任</td>
						</tr>
						<tr>
							<td class="year">2013年</td>
							<td>株式会社TBM&nbsp;取締役就任（現任）</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-05.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>片地&nbsp;格人<span>※会社法で定める社外取締役</span></p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">1993年</td>
							<td>野村證券株式会社&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">2000年</td>
							<td>株式会社エイチ・ティ・シー&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">2003年</td>
							<td>株式会社びしゃもん&nbsp;代表取締役就任（現任）</td>
						</tr>
						<tr>
							<td class="year">2006年</td>
							<td>株式会社日本レップ&nbsp;代表取締役就任</td>
						</tr>
						<tr>
							<td class="year">2009年</td>
							<td>アール・アイ・シー・マネジメント株式会社&nbsp;代表取締役就任（現任）</td>
						</tr>
						<tr>
							<td class="year">2010年</td>
							<td>株式会社エコ配&nbsp;代表取締役就任（現任）</td>
						</tr>
						<tr>
							<td class="year">2013年</td>
							<td>株式会社TBM&nbsp;取締役就任（現任）</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-06.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>杉森&nbsp;実<span>※会社法で定める社外取締役</span></p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">1995年</td>
							<td>株式会社　礎&nbsp;設立（現任）</td>
						</tr>
						<tr>
							<td class="year">2014年</td>
							<td>株式会社TBM&nbsp;取締役就任（現任）</td>
						</tr>
						<tr>
							<td class="career_text" colspan="2">
								起業するまでの間、広告代理店を経て建築・不動産会社で長年勤務。様々な経験を基に独立する。
								土地・建物から付随するマーケティングリサーチから商品企画、コンセプト作りからデザイン設計、広告宣伝・開業・販売戦略に至るまで、すべての局面においてクライアントにベストな解決策を提供している。現在、京都を本社とし、東京・大阪にも支店を展開。週刊ダイヤモンド第104号（2015.12.26／2016.01.02新年合併特大号）において、地方「元気」企業ランキングトップ150（約300万社のデータから東京、大阪、千葉、埼玉、神奈川を除く42道府県代表を選出）選抜の11位と掲載される。
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-17.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>坂本&nbsp;孝治<span>※会社法で定める社外取締役</span></p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">1990年</td>
							<td>伊藤忠商事株式会社&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">2007年</td>
							<td>エキサイト株式会社&nbsp;代表取締役、常務取締役就任</td>
						</tr>
						<tr>
							<td class="year">2012年</td>
							<td>ヤフー株式会社&nbsp;コンシューマ事業担当執行役員就任</td>
						</tr>
						<tr>
							<td class="year">2013年</td>
							<td>ヤフー株式会社&nbsp;ヤフオク！カンパニー長就任</td>
						</tr>
						<tr>
							<td class="year">2014年</td>
							<td>YJ America, Inc.&nbsp;社長就任（現任）</td>
						</tr>
						<tr>
							<td class="year">2016年</td>
							<td>株式会社TBM&nbsp;社外取締役就任（現任）</td>
						</tr>
					</table>
				</div>
			</article>
			<!-- <article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-03.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>松木&nbsp;貴志</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="career_text">
							
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-04.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>小林&nbsp;孝至</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td colspan="2" class="career_text">
							
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-05.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>片地&nbsp;格人</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td colspan="2" class="career_text">
							
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-06.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>取締役</span>杉森&nbsp;実</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">2001年</td>
							<td>朝日監査法人（現 有限責任あずさ監査法人）入所</td>
						</tr>
						<tr>
							<td colspan="2" class="career_text">
							
							</td>
						</tr>
					</table>
				</div>
			</article> -->

			<h4>監査役</h4>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-16.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>常勤監査役</span>加藤&nbsp;公一<span>※会社法で定める社外監査役</span></p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">1988年</td>
							<td>株式会社リクルート&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">2000年</td>
							<td>衆議院議員&nbsp;初当選</td>
						</tr>
						<tr>
							<td class="year">2009年</td>
							<td>法務副大臣&nbsp;就任</td>
						</tr>
						<tr>
							<td class="year">2010年</td>
							<td>内閣総理大臣補佐官&nbsp;就任</td>
						</tr>
						<tr>
							<td class="year">2011年</td>
							<td>日本ユネスコ&nbsp;国内委員会委員就任</td>
						</tr>
						<tr>
							<td class="year">2016年</td>
							<td>株式会社TBM&nbsp;監査役就任</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-07.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>非常勤監査役</span>水野&nbsp;勝<span>（元&nbsp;丸紅株式会社&nbsp;取締役&nbsp;副社長）</span><span>※会社法で定める社外監査役</span></p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">1961年</td>
							<td>丸紅株式会社&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">1999年</td>
							<td>丸紅株式会社&nbsp;取締役副社長就任</td>
						</tr>
						<tr>
							<td class="year">2007年</td>
							<td>株式会社パソナ&nbsp;取締役会長就任</td>
						</tr>
						<tr>
							<td class="year">2013年</td>
							<td>株式会社TBM&nbsp;顧問就任</td>
						</tr>
						<tr>
							<td class="year">2014年</td>
							<td>株式会社TBM&nbsp;監査役就任</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
				</div>
				<div class="txt">
					<p class="name"><span>非常勤監査役</span>髙田&nbsp;大記<span>※会社法で定める社外監査役</span></p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">2000年</td>
							<td>株式会社コメリ&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">2007年</td>
							<td>監査法人トーマツ（現有限責任監査法人トーマツ）&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">2011年</td>
							<td>公認会計士終了試験&nbsp;合格</td>
						</tr>
						<tr>
							<td class="year">2011年</td>
							<td>税理士法人ファースト会計事務所&nbsp;入所</td>
						</tr>
						<tr>
							<td class="year">2011年</td>
							<td>髙田大記公認会計事務所開設（現任）</td>
						</tr>
					</table>
				</div>
			</article>

			<h4>執行役員</h4>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-10.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>執行役員（営業担当）</span>青沼&nbsp;紘史</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="career_text">
								日商岩井株式会社(現双日)入社後、新規事業開発グループで新規案件の開拓、 立ち上げを担当。新規事業を立ち上げ事業会社に出向、企画営業GMとして 営業活動・事業運営に従事。自動車本部に異動後、中南米での自動車事業、 東南アジアでの新規事業立ち上げ等を担当。中南米に駐在し自動車組み立て 工場の運営、販売事業を管轄。2016年2月にTBMに入社。
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-11.png" alt="">
				</div>
				<div class="txt">
					<p class="name"><span>執行役員（経営企画担当）</span>山口&nbsp;太一</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="career_text">
								富士ゼロックス株式会社に入社後、プロダクションサービス営業本部にて 新規ビジネス開発を担当。 その後、プライスウォーターハウスクーパース株式会社にて、 事業再生支援業務に従事し、ビジネスデューデリジェンス、事業再生計画策定・ 実行支援、金融機関との折衝支援、成長戦略の立案支援等を担当。ディールアドバイザリーグループに異動後、マネージャーとしてクロスボーダーM&A案件におけるM&A戦略検討から実行支援、さらにはPMI(M&A後の企業統合)まで、各種のプロジェクトマネジメントを実施。
								2015年9月株式会社TBMに入社。経営企画室にて事業戦略、事業計画、 および資本政策の策定、量産工場に関するプロジェクトを推進。
							</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<div class="img_block">
						<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-12.png" alt="">
					</div>
					<div class="author">
						<p>主な著書</p>
						<p>「自分ゴト化　社員の行動をブランディングする」(ファーストプレス、共著)<br>「広告のやりかたで就活をやってみた」(宣伝会議、企画・制作)<br>「なぜ君たちは就活になるとみんな同じようなことばかりしゃべりだすのか」（宣伝会議、共著）<br>一般社団法人日本広告業協会第４０回懸賞論文「次代のフロントライン」入選。
						</p>
					</div>
				</div>
				<div class="txt">
					<p class="name"><span>執行役員（コーポーレート・コミュニケーション担当）</span>笹木&nbsp;隆之</p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="career_text">
								株式会社電通に入社後、経営者と企業のあらゆる事業活動を“アイデア”で活性化させる未来創造グループに所属。マーケティングを起点にした 新規事業開発、新商品開発、店舗開発のプランニング及びプロデュースなど、 チーフプランナーとして活動。電通総研Bチームのメンバーも兼務。<br>電通のソリューション部門におけるべストプラクティスフォーラムにおいて、 MVP賞、準MVP賞などを受賞。2016年4月株式会社TBMに入社。慶應義塾大学SFC研究所 上席所員。
							</td>
						</tr>
					</table>
				</div>
			</article>

			<h4>顧問</h4>
			<article>
				<div class="img">
					<div class="img_block">
						<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-13.png" alt="">
					</div>
					<div class="author">
						<p>主な著書</p>
						<p>「日本の重役(ダイヤモンド社)<br>日本会社史(文藝春秋社)<br>戦後日本の経済成長(共著、岩波書店、日経・経済図書文化賞受賞)<br>松下幸之助-その人と事業-(実業之日本社)<br>現代の経営（ピーター・ドラッカー著の監訳書、ダイヤモンド社)
						</p>
					</div>
				</div>
				<div class="txt">
					<p class="name"><span>最高顧問</span>野田&nbsp;一夫<span>（日本総合研究所 会長）</span></p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">1952年</td>
							<td>東京大学社会学科卒業</td>
						</tr>
						<tr>
							<td class="year">1960年</td>
							<td>マサチューセッツ工科大学ポストドクトラル・フェロー</td>
						</tr>
						<tr>
							<td class="year">1965年</td>
							<td>立教大学教授</td>
						</tr>
						<tr>
							<td class="year">1975年</td>
							<td>ハーバード大学イェンチン・フェロー</td>
						</tr>
						<tr>
							<td class="year">1981年</td>
							<td>財団法人&nbsp;日本総合研究所&nbsp;理事長</td>
						</tr>
						<tr>
							<td class="year">1985年</td>
							<td>社団法人&nbsp;ニュービジネス協議会&nbsp;理事長</td>
						</tr>
						<tr>
							<td class="year">1989年</td>
							<td>多摩大学&nbsp;学長</td>
						</tr>
						<tr>
							<td class="year">1993年</td>
							<td>社団法人&nbsp;日本マネジメントスクール&nbsp;会長</td>
						</tr>
						<tr>
							<td class="year">1997年</td>
							<td>宮城大学&nbsp;学長</td>
						</tr>
						<tr>
							<td class="year">1998年</td>
							<td>財団法人&nbsp;宮城総合研究所&nbsp;所長</td>
						</tr>
						<tr>
							<td class="year">2002年</td>
							<td>財団法人&nbsp;社会開発研究センター&nbsp;理事長</td>
						</tr>
						<tr>
							<td class="year">2007年</td>
							<td>学校法人&nbsp;グロービス経営大学院&nbsp;理事</td>
						</tr>
						<tr>
							<td class="year">2013年</td>
							<td>株式会社TBM&nbsp;最高顧問就任</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
					<div class="img_block">
						<img src="<?php echo get_template_directory_uri(); ?>/img/company/profile/img-profile-14.png" alt="">
					</div>
					<div class="author">
						<p>主な著書</p>
						<p>「ガツンと事業をつくれ」（生産性出版）<br>「花王魂、事業をやり遂げる事の大切さ」（生産性出版）
						</p>
					</div>
				</div>
				<div class="txt">
					<p class="name"><span>技術顧問</span>今村&nbsp;哲也<span>（元 花王(株)取締役 研究開発部門統括 ／ 三井化学(株)顧問）</span></p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">1967年</td>
							<td>花王㈱&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">1992年</td>
							<td>同社&nbsp;取締役&nbsp;研究開発部門統括<br>フロッピーディスクの製品開発から事業化を手がけ、1992年シェア15％世界1位を達成する。<br>研究部門を統括し取締役就任後、ヘルスケア事業本部長としてエコナ健康油とヘルシア緑茶飲料の開発を指揮。<br>事業立ち上げ・知財戦略を行い両商品共にシェアトップを達成する。
							</td>
						</tr>
						<tr>
							<td class="year">2005年</td>
							<td>花王㈱&nbsp;退社</td>
						</tr>
						<tr>
							<td class="year">2005年</td>
							<td>キッコーマン常勤顧問&nbsp;就任</td>
						</tr>
						<tr>
							<td class="year">2010年</td>
							<td>三井化学㈱顧問&nbsp;就任</td>
						</tr>
						<tr>
							<td class="year">2013年</td>
							<td>(株)TBM&nbsp;技術顧問&nbsp;就任</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
				</div>
				<div class="txt">
					<p class="name"><span>顧問</span>谷田&nbsp;大輔<span>（元 (株)タニタ代表取締役社長）</span></p>
					<p>経歴</p>
					<table>
						<tr>
							<td class="year">1966年</td>
							<td>株式会社タニタ製作所（現株式会社タニタ）&nbsp;入社</td>
						</tr>
						<tr>
							<td class="year">1987年</td>
							<td> 同社代表取締役社長就任。<br>長年にわたり、同社の発展に尽力し、健康機器のトップブランドに育て上げた。</td>
						</tr>
						<tr>
							<td class="year">2010年</td>
							<td>株式会社エコ配取締役会長&nbsp;就任</td>
						</tr>
						<tr>
							<td class="year">2013年</td>
							<td>㈱TBM&nbsp;顧問就任</td>
						</tr>
					</table>
				</div>
			</article>
			<article>
				<div class="img">
				</div>
				<div class="txt">
					<p class="name"><span>会計顧問</span>茂木&nbsp;亮一<span>（公認会計士・東京第一監査法人 統括代表）</span></p>
				</div>
			</article>
		</section>
	</div>

</section><!-- /#body -->

<?php
get_footer();
