<?php
/**
 * Template Name: company-award
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/company/">会社概要</a></li>
				<li><a href="/company/profile/">経営陣プロフィール</a></li>
				<li><a href="/company/identity/">企業理念体系</a></li>
				<li><a href="/company/history/">沿革</a></li>
				<li><a href="/company/message/">創設者メッセージ</a></li>
				<li><a href="/company/story/">創設ストーリー</a></li>
				<li><a href="/company/access/">アクセス</a></li>
				<li><a href="/company/award/" class="cur">受賞歴</a></li>
			</ul>
		</div>
	</div>
	<h3>受賞歴</h3>
	<div class="wrapper">
		<article>
			<div class="title">
				Japan Venture Awards 2016<br>
				<span>「東日本大震災復興賞」受賞</span>
			</div>
			<div class="contents">
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/award/img-award-00.jpg" alt="Japan Venture Awards 2016 「東日本大震災復興賞」受賞">
				</div>
				<div class="txt">
2月10日（水）虎ノ門ヒルズファーラムにて、独立行政法人中小企業基盤整備機構主催、革新的かつ潜在成長力の高い事業や、地域の活性化に資する事業を行う、志の高いベンチャー企業の経営者を称える、Japan Venture Awards 2016の表彰式が行われました。
弊社はJapan Venture Awards 2016「東日本大震災復興賞」を受賞しました。<br />
今後とも役職員一同邁進致します。詳細につきましては、以下のプレスリリースをご覧下さい<br />
					<a href="/pdf/pressrelease_160210.pdf" target="_blank">プレスリリースはこちら>></a>
				</div>
			</div>
		</article>
		<article>
			<div class="title">
				Job Creation 2015<br>
				<span>「特別賞」受賞</span>
			</div>
			<div class="contents">
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/award/img-award-02.jpg" alt="Job Creation 2015「特別賞」受賞">
				</div>
				<div class="txt">
11月6日（金）新日本有限責任監査法人主催、雇用創出効果の高い企業を表彰するJob Creation 2015の表彰式が行われました。<br />
弊社はJob Creation 2015「特別賞」を受賞しました。今後とも役職員一同邁進致します。
				<br>
					<a href="http://www.shinnihon.or.jp/about-us/news-releases/2015/2015-11-09.html" target="_blank">詳細はこちら>></a>
				</div>
			</div>
		</article>
		<article>
			<div class="title">
			第９回（2014年） ニッポン新事業創出大賞 <br>
				<span>「復興賞」受賞</span>
			</div>
			<div class="contents">
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/award/img-award-03.jpg" alt="ニッポン新事業創出大賞「復興賞」受賞">
				</div>
				<div class="txt">
11月20日（木）静岡にて独立行政法人中小企業基盤整備機構関東本部、公益社団法人日本ニュービジネス協議会連合会主催、第10回新事業創出全国フォーラムin静岡にて第9回ニッポン新事業創出大賞表彰式が行われました。<br />
弊社は公益社団法人日本ニュービジネス協議会連合会会長表彰「復興賞」を受賞しました。<br />
「復興賞」を頂いた企業として、まずは宮城県白石市の工場を成功させるべく今後とも役職員一同邁進致します。
				</div>
			</div>
		</article>
	</div>

</section><!-- /#body -->


<?php
get_footer();
