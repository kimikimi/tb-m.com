<?php
/**
 * Template Name: company-story-story1
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style_story.css" />

<section id="body"  class="story1">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/company/">会社概要</a></li>
				<li><a href="/company/profile/">経営陣プロフィール</a></li>
				<li><a href="/company/identity/">企業理念体系</a></li>
				<li><a href="/company/history/">沿革</a></li>
				<li><a href="/company/message/">創設者メッセージ</a></li>
				<li><a href="/company/story/" class="cur">創設ストーリー</a></li>
				<li><a href="/company/access/">アクセス</a></li>
				<li><a href="/company/award/">受賞歴</a></li>
			</ul>
		</div>
	</div>
	<h3>創設ストーリー</h3>
	<section class="sec01">
		<p class="main-visual"></p>
		<div class="wrapper">
			<div class="contents">
				<p class="title">角会長<span></span>山﨑社長</p>
			</div>
		</div>
	</section>
	<section class="sec02 column02">
		<div class="left">
		</div>
		<div class="right">
			<div class="inner">
				<div class="title">製品を見た当時、ビジネスになる確信はなかった</div>
				<div class="txt">
                    <p class="question">山﨑社長は株式会社TBMを設立する前にストーンペーパーと出会っていますが、当時は何を想いましたか？</p>
					<p class="name">山﨑</p>
					<p class="comment">2008年に知人が台湾製のストーンペーパーを見せてくれたのが最初の出会いです。当時はちょうどエコが世の中的に注目され始めた時期で、ストーンペーパーの環境にやさしい点や、石灰石からつくる技術に面白味を感じ、日本での輸入元になるために台湾に行きました。<br>
当時、製品を見たときには、「これはすごいイノベーションだ！」、「これなら世界に通用するビジネスになる！」といった確信はありませんでした。

					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="sec03">
		<div class="wrapper">
			<div class="contents">
				<div class="title">
				課題さえ解決できれば、大きなビジネスになると感じた
				</div>
				<div class="txt">
                    <p class="question">台湾製のストーンペーパーには課題があったと思いますが、その対策としてどう活動したのですか？</p>
					<p class="name">山﨑</p>
					<p class="comment">日本でストーンペーパーの営業活動をすると、大手企業などとにかく多くの会社が興味を示してくれました。素材のコンセプトやポテンシャルについてはものすごく評価が高かったです。でも、売れなかった。その理由は普通紙より比重が重く、価格が高く、品質が不安定だったからです。<br>
私はこの課題さえ解決できれば、大きなビジネスになると感じ、毎月台湾のメーカーに改良すべきことと、日本での期待の声を届けに行くことにしたのです。
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="sec04 column02">
		<div class="left">
			<div class="inner">
				<div class="title">
				その可能性に興味が湧いた
				</div>
				<div class="txt">
                    <p class="question">そうした状況下で、現在の角会長と出会ったのですね。当時どのような話をしていたのですか？また、角会長はストーンペーパーにどのような印象を持ちましたか？</p>
					<p class="name">山﨑</p>
					<p class="comment">知人から「紙の神様」のような人がいると紹介されたのが角会長です。何とかしたい、力を貸してほしいと相当お願いをし、会長から台湾のストーンペーパーの品質向上のためのアドバイスを頂くことになりました。
					</p>
				</div>
				<div class="txt mt30">
					<p class="name">角</p>
					<p class="comment">合成紙は業界でもかなり研究されていたのですが、シートになっても中々ビジネスとしてうまくいかない素材です。山﨑さんからストーンペーパーの話を聞いたとき、その可能性に非常に興味が沸いてきました。
					</p>
				</div>
			</div>
		</div>
		<div class="right">
		</div>
	</section>

	<section class="sec03">
		<div class="wrapper">
			<div class="contents">
				<div class="title">
				こんなに夢のある事業はない！
				</div>
				<div class="txt">
                    <p class="question">2010年の秋から石灰石を主原料にした新素材、LIMEX（ライメックス）の自社開発に踏み切りました。当時のエピソードを教えてください。</p>
					<p class="name">山﨑</p>
					<p class="comment">角会長のアドバイスを元に私は必死になって交渉しましたが、メーカー側はそれをなかなか受け入れようとしてくれませんでした。けれど、そのころには既にストーンペーパーのビジネスとしての可能性や社会的な意義も実感していて、「こんなに夢のある事業はない！」と、あきらめきれなかったです。このまま彼らと交渉を続けても仕方がない、それで自社開発に踏み切ったのです。
					</p>
				</div>
				<div class="txt mt30">
					<p class="name">角</p>
					<p class="comment">自社開発について不安もありましたが、山﨑さんの「やりましょう」という言葉で決断しました。当時は自社工場もなく、実験は他企業やラボで装置を借りて行います。多くの会社から断られ、私も困りました。最終的に日立造船に協力して頂き、2011年には特許を申請。少しずつ明かりが見えてきたのです。その後、経済産業省の支援対象となり、LIMEXの開発の歯車が回り始め、前進した時期でした。
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="sec05">
	</section>

	<section class="sec03 last">
		<div class="wrapper">
			<div class="contents">
				<div class="txt">
                    <div class="title">
                    完成したときは震えた
                    </div>
                    <div class="txt">
                        <p class="question">2015年、宮城県白石市に工場が完成しました。どんな想いでこのときを迎えましたか？</p>
                        <p class="name">山﨑</p>
                        <p class="comment">本当に完成直前まで安心できない状況でした。これまで多くの方々の協力があり、大きな責任を感じていたので、完成したときは震えるほど感激しました。そして、そこで働く若い人を見たらこれまでの苦労が吹き飛び、ここまでやってきて本当によかったなという思いでいっぱいになりました。
                        </p>
                    </div>
                 </div>
				<div class="txt mt30">
					<p class="name">角</p>
					<p class="comment">山﨑さんは本当に感激していましたね。私もこれまでの苦労を思い出すと、よくここまでたどり着いたと、夢のような感覚でした。同時にこれからが大変だと身が引き締まる思いもありました。
					</p>
				</div>
				<div class="txt mt70">
                    <p class="question">LIMEXの未来に寄せる想いを聞かせてください。</p>
					<p class="name">角</p>
					<p class="comment">まず日本発の技術として確立し、将来的には特に水のない途上国に工場を設立し、子供たちのノートや教科書をつくれるようにしたいという目標があります。そして、LIMEXは新素材として非常に期待値の高い素材です。たとえば今、プラスチック製造においては複合材が流行りですが、LIMEXのように炭酸カルシウムを50%以上と高充填していて、しかもシートになっているものは珍しく、これがTBMの強みになっていきます。その他、充填する素材次第でいろいろな特性を出すことも可能で、数え出したらキリがないくらい発展性の高い素材です。苦労してもゴールをめがけて行くしかないという気持ちです。
					</p>
				</div>
				<div class="txt mt30">
					<p class="name">山﨑</p>
					<p class="comment">これまで悲しい思い、悔しい思いはキリがないほどありました。ただ、ミラノ万博でLIMEXでつくったポスターが使用されたり、LIMEXの製品を手にする人がリアルにいると、自分の行いにプライドを持って前進できます。LIMEXに対する想いは語りきれないほどあります。将来的にはLIMEXで産業をつくり、日本の技術で世界中に雇用を生むことを目指しますが、まずは周りにいる人を笑顔にして、幸せにできる会社にしていきたいですね。
					</p>
				</div>
			</div>
		</div>
	</section>


</section><!-- /#body -->

<?php
get_footer();
