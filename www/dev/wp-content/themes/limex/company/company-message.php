<?php
/**
 * Template Name: company-message
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/company/">会社概要</a></li>
				<li><a href="/company/profile/">経営陣プロフィール</a></li>
				<li><a href="/company/identity/">企業理念体系</a></li>
				<li><a href="/company/history/">沿革</a></li>
				<li><a href="/company/message/" class="cur">創設者メッセージ</a></li>
				<li><a href="/company/story/">創設ストーリー</a></li>
				<li><a href="/company/access/">アクセス</a></li>
				<li><a href="/company/award/">受賞歴</a></li>
			</ul>
		</div>
	</div>
	<h3>創設者メッセージ</h3>

	<div id="kv01" class="pr01">
		<div id="kv02">
			<img src="<?php echo get_template_directory_uri(); ?>/img/company/message/block01.gif" alt="">
		</div>
		<div id="kv03">
			<div><img src="<?php echo get_template_directory_uri(); ?>/img/company/message/copy01.png" alt=""></div>
			<h3>進みたい未来へ、橋を架けよう。</h3>
			<p>目の前を今、巨大な河が流れている。黒く濁った水が激しくぶつかりあい、行く手を阻んでいる。人々は立ち止まり、あきらめ、そこで日々を過ごす。河を渡れば素晴らしい世界にたどりつくことを、知らないまま。</p>
			<p>それが、我々が生きている時代であり、河の向こうに待つのが、目指すべき理想の未来ならその河に橋を架けるのが、私たちの仕事だ。</p>
			<p>途方もない時間が、かかるだろう。簡単には、理解してもらえないかもしれない。それでも、橋を架け終えることができたなら10年、いや100年後も、人類の幸せに貢献できる。それが、私たちの仕事なんだ。</p>
		</div>
	</div>

</section><!-- /#body -->

<?php
get_footer();
