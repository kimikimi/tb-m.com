<?php
/**
 * Template Name: en-company-identity
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/company/">ABOUT US</a></li>
				<li><a href="/en/company/profile/">PROFILE</a></li>
				<li><a href="/en/company/identity/" class="cur">IDENTITY</a></li>
				<li><a href="/en/company/history/">HISTORY</a></li>
				<li><a href="/en/company/message/">MESSAGE</a></li>
				<li><a href="/en/company/story/story1/">STORY</a></li>
				<li><a href="/en/company/access/">ACCESS</a></li>
				<li><a href="/en/company/award/">AWARD</a></li>
			</ul>
		</div>
	</div>
	<h3>IDENTITY</h3>
	<div class="sections">
		<section class="sec sec1">
			<h4>TBM IDENTITY</h4>
			<h5>MISSION</h5>
			<p>Create a world where people are smiling and connected.</p>
		</section>
		<section class="sec sec2">
			<h5>VISION</h5>
			<p>Learn from the past and build the future. A cycloid innovation that is still sustainable after 100 years.
</p>
		</section>
		<section class="sec sec3">
			<h5>CREDO</h5>
			<ol style="margin-left:35px;">
				<li class="li1">People building a bridge to the future<br> Value the past, dream the future and strive to create a new value.<br>Focus on what to do today, and do it right away.</li>
				<li class="li2">Peoble building a bridge to the future<br> Know that the world's happiness and that of your own lies behind all the devotion and tears.<br>Thus absorb knowledge, strengthen yourself and strive to complete your mission.</li>
				<li class="li3">Peoble building a bridge to the future<br> Challenge and devort on the task beyond one's ability.<br>Face your weakness, do not run away from the birthpang that will prosper you.</li>
				<li class="li4">Peoble building a bridge to the future<br> Stay well balanced between dream and reality.<br>Listen well and achieve client's hopes, your party's hopes and that of your own.</li>
				<li class="li5">Peoble building a bridge to the future<br> Create bonds between people.<br>Believe in your party especially when things are tough, look forward and smile<br>With deep emotion and appreciation in your heart.</li>
			</ol>
		</section>
	</div>
</section><!-- /#body -->

<?php
get_footer('en');
