<?php
/**
 * Template Name: en-company-award
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/company/">ABOUT US</a></li>
				<li><a href="/en/company/profile/">PROFILE</a></li>
				<li><a href="/en/company/identity/">IDENTITY</a></li>
				<li><a href="/en/company/history/">HISTORY</a></li>
				<li><a href="/en/company/message/">MESSAGE</a></li>
				<li><a href="/en/company/story/story1/">STORY</a></li>
				<li><a href="/en/company/access/">ACCESS</a></li>
				<li><a href="/en/company/award/" class="cur">AWARD</a></li>
			</ul>
		</div>
	</div>
	<h3>AWARD</h3>
	<div class="wrapper">
		<article>
			<div class="title">
				New Business Creation Award<br>
				<span>Won "Restoration Award" of Japa</span>
			</div>
			<div class="contents">
				<div class="img">
					<img src="<?php echo get_template_directory_uri(); ?>/img/company/award/img-award-01.png" alt="">
				</div>
				<div class="txt">
					On the November 20, the Kanto Headquarters of the Organization for Small and Medium Enterprises and Regional Innovation (SMRJ), together with Japan New Business Conferences held the 10th National New business Forum in Shizuoka.<br><br> 
					The 9th Japan New Business Award ceremony was given at the forum. We were awarded the “Restoration Award” by the chairman of Japan New Business Conferences. As one of the awarded enterprises of the “Restoration Award”, We will continue to work even harder to succeed in the development of the plant constructed in Shiraishi city, Miyagi.
				</div>
			</div>
		</article>

	</div>

</section><!-- /#body -->

<?php
get_footer('en');
