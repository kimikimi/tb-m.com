<?php
/**
 * Template Name: company-story-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<style>
#body {
	min-height:700px;
}

</style>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/company/">会社概要</a></li>
				<li><a href="/company/profile/">経営陣プロフィール</a></li>
				<li><a href="/company/identity/">企業理念体系</a></li>
				<li><a href="/company/history/">沿革</a></li>
				<li><a href="/company/message/">創設者メッセージ</a></li>
				<li><a href="/company/story/">創設ストーリー</a></li>
				<li><a href="/company/access/">アクセス</a></li>
				<li><a href="/company/award/">受賞歴</a></li>
			</ul>
		</div>
	</div>
	<!-- <h3>創設ストーリー</h3>
	<section class="sec01">
		<p class="main-visual"></p>
		<div class="wrapper">
			<div class="contents">
				<p class="title">角会長<span></span>山﨑社長</p>
			</div>
		</div>
	</section>
	 -->

	<div class="wrapper">
		<ul class="link-list">
			<li><a href="/company/story/story1/">
				<p class="img"><img src="<?php echo get_template_directory_uri(); ?>/img/company/story-list/img-story-list-01.png" alt=""></p>
				<p class="title">
					角会長<span></span>山﨑社長
				</p>
				<p class="link-text">
					<span></span>詳細はこちら
				</p>
			</a></li>

			<li><a href="/company/story/story2/">
				<p class="img"><img src="<?php echo get_template_directory_uri(); ?>/img/company/story-list/img-story-list-02.png" alt=""></p>
				<p class="title">
					野田先生<span></span>山﨑社長
				</p>
				<p class="link-text">
					<span></span>詳細はこちら
				</p>
			</a></li>
		</ul>
	</div>

</section><!-- /#body -->

<?php
get_footer();
