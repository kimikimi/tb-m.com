<?php
/**
 * Template Name: en-company-story-story1
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('en'); ?>

<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/en/style_story.css" />

<section id="body"  class="story1">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/company/">ABOUT US</a></li>
				<li><a href="/en/company/profile/">PROFILE</a></li>
				<li><a href="/en/company/identity/">IDENTITY</a></li>
				<li><a href="/en/company/history/">HISTORY</a></li>
				<li><a href="/en/company/message/">MESSAGE</a></li>
				<li><a href="/en/company/story/" class="cur">STORY</a></li>
				<li><a href="/en/company/access/">ACCESS</a></li>
				<li><a href="/en/company/award/">AWARD</a></li>
			</ul>
		</div>
	</div>
	<h3>Start Up History</h3>
	<section class="sec01">
		<p class="main-visual"></p>
		<div class="wrapper">
			<div class="contents">
				<div class="title">
					<p class="title_top">
						Chairman  Dr.Yuichiro Sumi
					</p>
					<span></span>
					<p class="title_bottom">
					Chief Executive Officer  Nobuyoshi Yamasaki
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="sec02 column02">
		<div class="left">
		</div>
		<div class="right">
			<div class="inner">
				<div class="title">When I first saw the product, I was not sure whether this could become a business.</div>
				<div class="txt">
                    <p class="question">Mr. Yamasaki, before setting up TBM Co. Ltd, you knew about the Stone Paper, what did you think then?</p>
					<p class="name">Yamasaki</p>
					<p class="comment">I first met this material when an acquaintance showed me the Stone Paper made in Taiwan in 2008. The world was getting more and more eco-friendly at that time. An eco-friendly material made out of lime , that hit me! So I went to Taiwan to become the Japanese importer. When I first saw the product, I was not at all sure that this could be an innovation or a global business.
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="sec03">
		<div class="wrapper">
			<div class="contents">
				<div class="title">
				But I felt that if we could clear the issues , it could become a huge business.
				</div>
				<div class="txt">
                    <p class="question">There were issues with the Taiwanese stone paper, what activities did you do to solve the problems?</p>
					<p class="name">Yamasaki</p>
					<p class="comment">When I went for presentations to large companies in Japan, many showed interest in the stone paper.  The concept and the potential of the material was very much valued. But, it did not sell well at all. Because the paper was heavy, expensive and the quality was unstable.  I was confident if these problems were cleared it could become a big business, so I went every month to Taiwan in order to try to better the quality.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="sec04 column02">
		<div class="left">
			<div class="inner">
				<div class="title">
				I was interested in the possibility
				</div>
				<div class="txt">
                    <p class="question">Under that situation you met Mr. Sumi. What did you talk about? And Mr. Sumi what was your impression about the stone paper?</p>
					<p class="name">Yamasaki</p>
					<p class="comment">I was introduced Mr. Sumi as the 'God of the papers' by an acquaintance. I begged him to help me out and to give me advice to improve the stone paper's quality.
					</p>
				</div>
				<div class="txt mt30">
					<p class="name">Sumi</p>
					<p class="comment">Synthetic paper has been investigated in the industry for a long time, but it has been a difficult material to make business out of it. When I heard about the stone paper story from Mr. Yamasaki, I was very much interested.
					</p>
				</div>
			</div>
		</div>
		<div class="right">
		</div>
	</section>

	<section class="sec03">
		<div class="wrapper">
			<div class="contents">
				<div class="title">
				It is a business with a gigantic dream!
				</div>
				<div class="txt">
                    <p class="question">Fall 2010, you started developing LIMEX, new material made out of lime stone, in-house. Please let me know about the episodes.
										</p>
					<p class="name">Yamasaki</p>
					<p class="comment">Receiving Mr. Sumi's advice, I toughly negotiated with the Taiwanese maker, but it was not at all accepted. But then at that time, I already believed in the possibilities of this stone paper business and the social meaning of this business, and I could not give up as I was sure that this business is filled with dreams. That is why I changed in to in-house development. 
					</p>
				</div>
				<div class="txt mt30">
					<p class="name">Sumi</p>
					<p class="comment">I of course was a bit worried about in-house development , but with the strong words of Mr. Yamasaki ' Let's do it!' I decided to go forward. Of course we did not have our own plant, so we had to do our experiments in other companies and laboratories lending their machines. Of course we were denied by many companies and I was in difficulties. At last Hitachi Zosen Corporation supported us , and we could apply to file the patent in 2011. Then we were nominated by the Ministry of Economy, Trade and Industry  to receive support, and then the LIMEX development finally started  going forward.
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="sec05">
	</section>

	<section class="sec03 last">
		<div class="wrapper">
			<div class="contents">
				<div class="txt">
                    <div class="title">
                    I was thrilled when it was completed!
                    </div>
                    <div class="txt">
                        <p class="question">2015, the plant in Shiroishi City,  Miyagi Prefecture was completed. How did you feel at this moment?</p>
                        <p class="name">Yamasaki</p>
                        <p class="comment">Until the moment that the plant was  completed we could not feel comfortable at all. We have received support from various people and I felt a huge responsibility, I was thrilled when the plant was finally completed. Also when I saw many young people working there, I was filled with excitement that we could come this far.
                        </p>
                    </div>
                 </div>
				<div class="txt mt30">
					<p class="name">Sumi</p>
					<p class="comment">Yes, Mr. Yamasaki was really moved. I felt like dreaming as remembering all the sufferings we went through. At the same time, I was tensed as I knew that it is going to get even tough.
					</p>
				</div>
				<div class="txt mt70">
                    <p class="question">Please tell us about your feelings for LIMEX</p>
					<p class="name">Sumi</p>
					<p class="comment">First of all I would like to make it a revolutionary Japanese technology , and I dream to set up these plants in developing countries facing water shortage, and make notebooks and textbooks for the children there. And LIMEX is a material with a huge potential. As an example, when making plastic it is mainly a composite material. LIMEX is mainly made out of calcium carbonate, and it can be made into sheets , which is rare and makes the LIMEX a special material. Also by combining various material we can make different materials , and the variety we can make is uncountable. Even though there are many challenges, we have to achieve our goals. 
					</p>
				</div>
				<div class="txt mt30">
					<p class="name">Yamasaki</p>
					<p class="comment">In the past we had many sad and bitter experience. But when LIMEX was used for the MILANO EXPO posters or when I see people in real using our products, I feel very proud of it and things like that make me go forward. My feelings and dreams for LIMEX is overflowing. In the future I would like to make LIMEX an industry, and make new employment in the world. But before that , first I want to make my surroundings smile and the company a happy place.
					</p>
				</div>
			</div>
		</div>
	</section>


</section><!-- /#body -->

<?php
get_footer('en');
