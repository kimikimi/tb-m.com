<?php
/**
 * Template Name: company-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/company/" class="cur">会社概要</a></li>
				<li><a href="/company/profile/">経営陣プロフィール</a></li>
				<li><a href="/company/identity/">企業理念体系</a></li>
				<li><a href="/company/history/">沿革</a></li>
				<li><a href="/company/message/">創設者メッセージ</a></li>
				<li><a href="/company/story/">創設ストーリー</a></li>
				<li><a href="/company/access/">アクセス</a></li>
				<li><a href="/company/award/">受賞歴</a></li>
			</ul>
		</div>
	</div>
	<div class="wrapper">
		<h3>会社概要</h3>
		<div class="data" style="line-height:1.5;">
			<table>
				<tr>
					<th>社名</th>
					<td>株式会社TBM</td>
				</tr>
				<tr>
					<th>取締役会長</th>
					<td>角 祐一郎（日本製紙株式会社 元専務取締役）著書に「紙の科学」</td>
				</tr>
				<tr>
					<th>代表取締役</th>
					<td>山﨑 敦義</td>
				</tr>
				<tr>
					<th>設立年月日</th>
					<td>平成23年8月30日</td>
				</tr>
				<tr>
					<th>東京本社</th>
					<td>
						〒104-0061 東京都中央区銀座2-7-17-6F
						<a class="link_access" href="/company/access/"><span></span>アクセス</a>
					</td>
				</tr>
				<tr>
					<th>海外子会社</th>
					<td>
						Times Bridge Management Global, Inc.<br />
						717 Market Street, San Francisco, CA, USA
					</td>
				</tr>				<tr>
					<th>電話番号</th>
					<td>03-3538-6777（代表）</td>
				</tr>
				<tr>
					<th>FAX番号</th>
					<td>03-3538-6778</td>
				</tr>
				<tr>
					<th>資本金</th>
					<td>40億6,920万円（資本準備金を含みます）</td>
				</tr>
				<tr>
					<th>従業員数</th>
					<td>60人（株式会社TBM単体・2016年9月末）</td>
				</tr>				<tr>
					<th>顧問法律事務所</th>
					<td>西村あさひ法律事務所</td>
				</tr>
				<tr>
					<th>事業内容</th>
					<td>LIMEX及びLIMEX製品の開発、製造、販売</td>
				</tr>
			</table>
		</div>
	</div>

</section><!-- /#body -->



<?php
get_footer();
