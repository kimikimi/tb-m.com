<?php
/**
 * Template Name: company-access
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/company/">会社概要</a></li>
				<li><a href="/company/profile/">経営陣プロフィール</a></li>
				<li><a href="/company/identity/">企業理念体系</a></li>
				<li><a href="/company/history/">沿革</a></li>
				<li><a href="/company/message/">創設者メッセージ</a></li>
				<li><a href="/company/story/">創設ストーリー</a></li>
				<li><a href="/company/access/" class="cur">アクセス</a></li>
				<li><a href="/company/award/">受賞歴</a></li>
			</ul>
		</div>
	</div>
	<div class="wrapper">
		<h3>アクセス</h3>
	</div>
	<div id="map">
	</div>
	<div class="wrapper">
		<h4 class="">本社</h4>
		<div class="data">
			<p>〒104-0061 東京都中央区銀座2-7-17-6F<br>電話番号：03-3538-6777（代表）</p>
			<h5>駅からのアクセス方法</h5>
			<p>JR山手線「有楽町駅」中央口より徒歩6分<br />
				東京メトロ有楽町線「銀座一丁目駅」9番出口より徒歩1分<br />
				東京メトロ銀座線・丸ノ内線・日比谷線「銀座駅」A13出口より徒歩2分</p>
		</div>
	</div>
	<div class="wrapper">
		<h4 class="">白石蔵王<span>(国内第一プラント)</span></h4>
		<div class="data">
			<p>〒989-0213　宮城県白石市大鷹沢三沢字前輪55<br>電話番号：0224-24-5600</p>
			<h5>駅からのアクセス方法</h5>
			<p>JR新幹線 白石蔵王駅よりタクシーで4分</p>
		</div>
	</div>

</section><!-- /#body -->

<?php
get_footer();
