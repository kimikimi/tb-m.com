<?php
/**
 * Template Name: en-company-message
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/company/">ABOUT US</a></li>
				<li><a href="/en/company/profile/">PROFILE</a></li>
				<li><a href="/en/company/identity/">IDENTITY</a></li>
				<li><a href="/en/company/history/">HISTORY</a></li>
				<li><a href="/en/company/message/" class="cur">MESSAGE</a></li>
				<li><a href="/en/company/story/story1/">STORY</a></li>
				<li><a href="/en/company/access/">ACCESS</a></li>
				<li><a href="/en/company/award/">AWARD</a></li>
			</ul>
		</div>
	</div>
	<h3>MESSAGE</h3>

	<div id="kv01" class="pr01">
		<div id="kv02">
			<img src="<?php echo get_template_directory_uri(); ?>/img/en/company/message/block01.gif" alt="">
		</div>
		<div id="kv03">
			<div><img src="<?php echo get_template_directory_uri(); ?>/img/en/company/message/copy01.png" alt=""></div>
			<h3>Build a Bridge to the Future</h3>
			<p>Giant river flowing in front of us. Black and muddy water hitting heavily blocking our way. People stand still, give up and pass their lives there. Without knowing that just crossing the river will bring them to a marvelous world...</p>
			<p>That is the era we are living in and. If the other side is the ideal future. It is our mission to build the bridge over the river.</p>
			<p>Enormous amount of time will be needed. We might not easily be understood. But if we do succeed to build the bridge over the river, After 10 years... or even 100 years we can contribute to people's happiness. Yes, that is our mission.</p>
		</div>
	</div>



</section><!-- /#body -->

<?php
get_footer('en');
