<?php
/**
 * Template Name: en-company-access
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/company/">ABOUT US</a></li>
				<li><a href="/en/company/profile/">PROFILE</a></li>
				<li><a href="/en/company/identity/">IDENTITY</a></li>
				<li><a href="/en/company/history/">HISTORY</a></li>
				<li><a href="/en/company/message/">MESSAGE</a></li>
				<li><a href="/en/company/story/story1/">STORY</a></li>
				<li><a href="/en/company/access/" class="cur">ACCESS</a></li>
				<li><a href="/en/company/award/">AWARD</a></li>
			</ul>
		</div>
	</div>
	<h3>ACCESS</h3>

	<div id="map">
	</div>
	<div class="wrapper">
		<div class="data">
			<p>The 6th Floor, 2-7-17 ,Ginza, Chuou-ku, Tokyo 104-0061<br>Telephone number: +81 (0)3 3538 6777</p>
			<h4>Access to the office from the nearby stations</h4>
			<p>6-minutes walk from the JR Yurakucho station, Central exit<br>
1-minute walk from the Ginza-itchome station, Tokyo Metro Yurakucho Line, Exit  No 9<br>
2-minutes walk from Tokyo Metro Ginza line, Hibuya line, Marunouchi line "Ginza station" Exit A13</p>
		</div>
	</div>


	<div class="wrapper">
		<h4 class="">Shiroishi Zao <span>( Jpan first plant )</span></h4>
		<div class="data">
			<p>Otakasawamisawamaewa 55,Shiroishi, Miyagi 989-0213<br>
			Telephone number: +81 0224-24-5600</p>
			<h4>Access to the office from the nearby stations</h4>
			<p>4-minutes by taxi from the JR Shinkansen Shiroishi Zao Station</p>
		</div>
	</div>
</section><!-- /#body -->

<?php
get_footer('en');
