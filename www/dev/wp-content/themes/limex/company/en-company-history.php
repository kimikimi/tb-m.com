<?php
/**
 * Template Name: en-company-history
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>


<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/company/">ABOUT US</a></li>
				<li><a href="/en/company/profile/">PROFILE</a></li>
				<li><a href="/en/company/identity/">IDENTITY</a></li>
				<li><a href="/en/company/history/" class="cur">HISTORY</a></li>
				<li><a href="/en/company/message/">MESSAGE</a></li>
				<li><a href="/en/company/story/story1/">STORY</a></li>
				<li><a href="/en/company/access/">ACCESS</a></li>
				<li><a href="/en/company/award/">AWARD</a></li>
			</ul>
		</div>
	</div>
	<h3>COMPANY HISTORY</h3>

	<div class="wrapper">
		<div class="data" style="line-height:1.5;">
		<table>
			<tr>
				<th>2008-11</th>
				<td>Started Import and sale Taiwan-made stone paper</td>
			</tr>
			<tr>
				<th>2010-8</th>
				<td>Started technology development at the company</td>
			</tr>
	        	<tr>
				<th>2011-5</th>
				<td>Applied for patent for self-made stone paper (LIMEX)</td>
			</tr>
			<tr>
				<th>2011-8</th>
				<td>The new TBM Co., Ltd was founded at is founded in Shibadaimon, Minato, Tokyo by business transference</td>
			</tr>
	        <tr>
				<th>2012-3</th>
				<td>Capital: 20 million yen</td>
			</tr>
			<tr>
				<th>2012-6</th>
				<td>Started co-development of Molding technology with Hitachi Zosen Corporation</td>
			</tr>
			<tr>
				<th>2013-2</th>
				<td>Chosen by the Ministry of Economy, Trade and Industry to be one of the Innovation Base Promotion Businesses to be aided by "Subsidy for Advanced Technology Demonstration and Evaluation Facility Devlopment."</td>
			</tr>
	        <tr>
				<th>2013-3</th>
				<td>Capital: 2 hundred 75 million yen</td>
			</tr>
	        <tr>
				<th>2013-8</th>
				<td>Company moved to Akasaka Ni-chou-me, Minato-ku, Tokyo</td>
			</tr>
			<tr>
				<th>2014-1</th>
				<td>Patent Approval for self-made stone paper (LIMEX) </td>
			</tr>
			<tr>
				<th>2014-2</th>
				<td>Shiraishi Plant location agreement made at Miyagi prefecture government building (Shiroisi city, Miyagi)</td>
			</tr>
	        <tr>
				<th>2014-3</th>
				<td>Capital: 499 million yen (Including legal capital surplus)</td>
			</tr>
	        <tr>
				<th>2014-4</th>
				<td>Company moved to Akasaka san-Chou-me , Minato-ku,Tokyo</td>
			</tr>
			<tr>
				<th>2014-7</th>
				<td>Started construction of Shiraishi Plant (Shiroishi city, Miyagi)</td>
			</tr>
	         <tr>
				<th>2014-9</th>
				<td>Capital: 1 billion 230 million yen (Including legal capital surplus)</td>
			</tr>
	        <tr>
				<th>2014-10</th>
				<td>Tokyo lab was set up at Tokyo Metropolitan Industrial Technology Research Institute in Koutou-ku, Tokyo</td>
			</tr>
	        <tr>
				<th>2014-11</th>
				<td>Awarded Reconstruction Award at the Ninth Nippon New Business Creation Awards opened by the chairman of Japan New Business Conferences</td>
			</tr>
			<tr>
	        <tr>
				<th>2015-2</th>
				<td>Construction of Shiraishi Plant was completed (Shiroishi city, Miyagi)</td>
			</tr>
			<tr>
	        <tr>
				<th>2015-3</th>
				<td>Capital: 2 billion 259 million yen (Including legal capital surplus)</td>
			</tr>
	        <tr>
				<th>2015-4</th>
				<td>Company moved to Marunouchi, Chiyoda-ku, Tokyo</td>
			</tr>
	        <tr>
				<th>2015-5</th>
				<td>Provided LIMEX products for EXPO Milano 2015 as a sponsor</td>
			</tr>
			<tr>
			<td colspan="2" style="border-bottom:0px;text-align:right;font-size:12px;">As of August 1, 2015</td>
			</tr>
		</table>
		<br>
		
		</div>
	</div>

</section><!-- /#body -->

<?php
get_footer('en');
