<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<footer>

			<ul class="cf01">
				<li><a href="https://twitter.com/share?url=http://tb-m.com/en/&amp;text=a next generation material LIMEX, TBM Co., Ltd" target="_blank" class="icon01">twitter</a></li>
				<li><a href="https://www.facebook.com/tbm.limex" target="_blank" class="icon02">facebook</a></li>
				<li><a href="https://www.youtube.com/channel/UCIvfa-J0VGeLl71Aeg6sWRg" target="_blank" class="icon03">youtube</a></li>
			</ul>
			<div class="txtLink01"><a href="/en/company/">ABOUT US</a><a href="/en/privacy/">PRIVACY POLICY</a><a href="/en/company/access/">ACCESS</a><a href="mailto:infomail@tb-m.com">CONTACT</a></div>
			<p class="copyright">Copyright TBM Co.,Ltd All Rights Reserved</p>
		</footer>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-46676692-1', 'auto');
			ga('send', 'pageview');
			
		</script>
		<!-- access log -->
		<script type="text/javascript">
			
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-16758707-1']);
			_gaq.push(['_trackPageview']);
			
			(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
			
		</script>
		<!-- // access log -->
	</body>
</html>