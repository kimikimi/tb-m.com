    <?php
/**
 * Template Name: action-topics
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

$fields = get_fields($post->ID);

?><!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8" />
<title>LIMEX ACTION | TOPICS</title>
<meta name="description" content="日本の技術だからできる新素材ライメックス。LIMEXに携わる人々の想いを伝えます。">
<meta name="Keywords" content="LIMEX,石灰石,リサイクル,エコノミー,エコロジー,新素材,ストーンペーパー,プラスチック,株式会社TBM">
<link rel="SHORTCUT ICON" href="/img/favicon.ico" />

<meta property="og:title" content="次世代素材LIMEX 株式会社TBM" />
<meta property="og:url" content="http://tb-m.com/" />
<meta property="og:description" content="【株式会社TBM】LIMEXは、石灰石（炭酸カルシウム）を主原料に、紙やプラスチック商品の代替となり、エコロジーとエコノミーを実現する新素材です。" />
<meta property="og:image" content="http://tb-m.com/img/ogp.jpg" />


<meta name="viewport" content="width=1280px, maximum-scale=1.0, user-scalable=yes">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/action/assets/css/style.css" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/jquery-1.12.0.min.js"></script>

</head>
<body>
    <header>
        <div class="left"><a href="/action/"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/logo_limex.png" alt="LIMEX ACTION"></a></div>
        <div class="right"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font_limex_to.png" alt="LIMEXサイト"></a></div>
    </header>
    <div class="contents special">
        <article class="submod">
            <h2 class="center"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font-topics.png" alt="TOPICS"></h2>
<?php if ($fields['top_text']): ?>
            <p class="center description"><?php echo $fields['top_text'] ?></p>
<?php endif ?>
            <div class="center img_large">
                <img src="<?php echo $fields['key_visual'] ?>">
            </div>
<?php if ($fields['middle_text']): ?>
            <p class="center subtitle"><?php echo $fields['middle_text'] ?></p>
<?php endif ?>
            <section class="mod-content">
<?php if ($fields['bottom_text']): ?>
                <h4 class="center"><?php echo $fields['bottom_text'] ?></h4>
<?php endif ?>
                <div class="mod-content-text">
<?php foreach ($fields['contents_block'] as $field): ?>
<?php if ($field['title']): ?>
                    <h3><?php echo $field['title'] ?></h3>
<?php endif ?>
<?php if ($field['contents']): ?>
                    <div class="topics_paragrah"><?php echo $field['contents'] ?></div>
<?php endif ?>
<?php if ($field['image']): ?>
                    <div class="img_large"><img src="<?php echo $field['image'] ?>"></div>
<?php endif ?>
<?php endforeach ?>
                </div>
            </section>

        </article>
    </div>

    <footer>
      <div class="footArea">
        <ul>
            <li><a target="_blank" href="https://twitter.com/intent/tweet?text=%E6%AC%A1%E4%B8%96%E4%BB%A3%E7%B4%A0%E6%9D%90LIMEX%20%E6%A0%AA%E5%BC%8F%E4%BC%9A%E7%A4%BETBM&url=http%3A%2F%2Ftb-m.com%2F&original_referer=http%3A%2F%2Fwww.tb-m.com%2F"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_tw.png" alt="Twitter"></a></li>
            <li><a target="_blank" href="https://www.facebook.com/tbm.limex"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_fb.png" alt="Facebook"></a></li>
            <li><a target="_blank" href="https://www.youtube.com/channel/UCIvfa-J0VGeLl71Aeg6sWRg"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_yt.png" alt="youtube"></a></li>
        </ul>
      </div>
      <p class="copy"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font_copy.png" alt="Copyright TBM Co.,LTD All Rights Reserved"></p>
    </footer>
		<!-- アクセス解析 -->
		<script type="text/javascript">
			
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-16758707-1']);
			_gaq.push(['_trackPageview']);
			
			(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
			
		</script>
		<!-- // アクセス解析 -->
</body>
</html>