<?php
/**
 * Template Name: about-paper
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="pageTtl">
		<h2>LIMEX FACTORY</h2>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/about/">LIMEXとは</a></li>
				<li><a href="/about/paper/" class="cur">LIMEXの紙</a></li>
				<li><a href="/about/plastic/">LIMEXのプラスチック</a></li>
				<li><a href="/about/future/">LIMEXの可能性</a></li>
			</ul>
		</div>
	</div>

	<h3>LIMEXの紙</h3>

		<section id="about">
			<div class="cnt">
				<div class="img_layer">
					<!-- <img id="about_cnt2" src="<?php echo get_template_directory_uri(); ?>/img/about/about_cnt2.png"> -->
					<img id="about_c1t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1t.png" alt="LIME X PAPER">
					<img class="icon" id="about_c1i1" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1i1.png" alt="">
					<img class="icon" id="about_c1i2" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1i2.png" alt="">
					<img class="icon" id="about_c1i3" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1i3.png" alt="">
					<img class="icon" id="about_c1i4" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1i4.png" alt="">
					<img class="icon" id="about_c1i5" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1i5.png" alt="">
					<img class="icon" id="about_c1i6" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1i6.png" alt="">
					<img class="icon" id="about_c1i7" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1i7.png" alt="">
					<img class="icon" id="about_c1i8" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1i8.png" alt="">
					<img class="icon" id="about_c1i9" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1i9.png" alt="">
					<img class="icon" id="about_c1i10" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1i10.png" alt="">
					<p id="about_c1r" class="fs-16">ライメックスペーパーは、石灰石からつくる紙。水も木も使わずにつくれて、使いやすさは普通紙以上。印刷も可能ですから、本やノートや名刺、ポスターなど様々な用途で使えます。</p>
					<img id="about_c1bt" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1bt.png" alt="LIMEXでつくる紙の特徴">
					<img id="about_c1b1t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1b1t.png" alt="辿りついた軽さ">
					<p id="txt1" class="ts14">新しい製造技術を開発することにより、従来のストーンペーパーより大幅に軽く、品質も高く、しかも安くつくることができます。</p>
					<img id="about_c1b1i" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1b1i.png" alt="">
					<hr id="line11">
					<img id="about_c1b2t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1b2t.png" alt="高い耐水性">
					<p id="txt2" class="ts14">非常に高い耐水性の為、浴室や水回り、屋外や水中での利用が可能。水中での筆記も可能です。</p>
					<img id="about_c1b2i" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1b2i.png" alt="">
					<hr id="line12">
					<img id="about_c1b3t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1b3t.png" alt="半永久的にリサイクル">
					<p id="txt3" class="ts14">LIMEXは原料が石だから、経年変化にも強く、半永久的にリサイクル可能という画期的な新素材です。</p>
					<img id="about_c1b3i" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c1b3i.png" alt="">
					
				</div>
			</div>
			<div class="sub-nav img_layer">
				<a href="/about/" class="about-index"><p><span></span>LIMEXとは</p></a>
				<ul>
					<li><a href="/about/paper/"><p><span></span>LIMEXの紙</p></a></li>
					<li><a href="/about/plastic/"><p><span></span>LIMEXの<br>プラスチック</p></a></li>
					<li><a href="/about/future/"><p><span></span>LIMEXの可能性</p></a></li>
				</ul>
			</div>
		</section>

<?php
get_footer();
