<?php
/**
 * Template Name: en-about-plastic
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>

<section id="about">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/about/">LIMEX</a></li>
				<li><a href="/en/about/paper/">LIMEX PAPER</a></li>
				<li><a href="/en/about/plastic/" class="cur">LIMEX PLASTIC</a></li>
				<li><a href="/en/about/future/">LIMEX FUTURE</a></li>
			</ul>
		</div>
	</div>
	
	<h3>LIMEX PLASTIC</h3>

	<div class="cnt">
		<div class="img_layer">
			<img id="about_c2t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2t.png" alt="LIME X PLASTIC">
			<img class="icon" id="about_c2i1" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i1.png">
			<img class="icon" id="about_c2i2" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i2.png">
			<img class="icon" id="about_c2i3" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i3.png">
			<img class="icon" id="about_c2i4" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i4.png">
			<img class="icon" id="about_c2i5" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i5.png">
			<img class="icon" id="about_c2i6" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i6.png">
			<img class="icon" id="about_c2i7" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i7.png">
			<img class="icon" id="about_c2i8" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i8.png">
			<img class="icon" id="about_c2i9" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i9.png">
			<img class="icon" id="about_c2i10" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i10.png">
			<img id="about_c2r" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2r.png" alt="LIMEX plastic - plastic made from limestone-It can be used for films and sheets currently made from 100% petroleum-based plastic materials, and thus reduces the consumption of petroleum.">
			<img id="about_c2bt" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2bt.png" alt="Characteristics of LIMEX Plastics">
			<img id="about_c2b1t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2b1t.png" class="Extreme Cost Reduction">
			<p id="txt4" class="ts14">High quality but cost reducing as Limestone is much cheaper than petroleum.</p>
			<hr id="line21">
			<img id="about_c2b2t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2b2t.png" alt="No duty to pay Recycling Charge">
			<p id="txt5" class="ts14">By using LIMEX plastics, recycling charge under the Japan Containers and Packaging Recycling Law can be avoided. Thus reduces costs even more.</p>
			<hr id="line22">
			<img id="about_c2b3t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2b3t.png" alt="Contribute to Company's Image Improvement">
			<p id="txt6" class="ts14">By using LIMEX plastics, it will lead to company's environment friendly image.</p>
			<img id="about_c2bi" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2bi.png">
			<p id="txt7" class="ts14">LIMEX paper can contribute to water shortage and deforestation matters and LIMEX plastic to conserving petroleum resources. LIMEX will grow in each market in order to change the world.</p>
		</div>
	</div>
	<div class="sub-nav img_layer">
				<a href="/en/about/" class="about-index"><p><span></span>What is LIMEX</p></a>
				<ul>
					<li><a href="/en/about/paper/"><p><span></span>LIMEX PAPER</p></a></li>
					<li><a href="/en/about/plastic/"><p><span></span>LIMEX PLASTIC</p></a></li>
					<li><a href="/en/about/future/"><p><span></span>LIMEX FUTURE</p></a></li>
				</ul>
			</div>
</section>

<?php
get_footer('en');
