<?php
/**
 * Template Name: en-about-paper
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>

<section id="about">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/about/">LIMEX</a></li>
				<li><a href="/en/about/paper/" class="cur">LIMEX PAPER</a></li>
				<li><a href="/en/about/plastic/">LIMEX PLASTIC</a></li>
				<li><a href="/en/about/future/">LIMEX FUTURE</a></li>
			</ul>
		</div>
	</div>

	<h3>LIMEX PAPER</h3>

	<div class="cnt">
		<div class="img_layer">
			<img id="about_c1t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1t.png" alt="LIME X PAPER">
			<img class="icon" id="about_c1i1" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i1.png">
			<img class="icon" id="about_c1i2" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i2.png">
			<img class="icon" id="about_c1i3" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i3.png">
			<img class="icon" id="about_c1i4" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i4.png">
			<img class="icon" id="about_c1i5" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i5.png">
			<img class="icon" id="about_c1i6" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i6.png">
			<img class="icon" id="about_c1i7" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i7.png">
			<img class="icon" id="about_c1i8" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i8.png">
			<img class="icon" id="about_c1i9" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i9.png">
			<img class="icon" id="about_c1i10" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i10.png">
			<img id="about_c1r" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1r.png" alt="LIMEX paper - paper made from limestone - without using pulp nor water, easy to use. Printable, can be used for books, notebooks, name-cards, posters and for many other purposes.">
			<img id="about_c1bt" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1bt.png" alt="Characteristics of LIMEX paper">
			<img id="about_c1b1t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b1t.png" alt="Finally achieved competitive lightness">
			<p id="txt1" class="ts14">With the new manufacturing technology, LIMEX is now much lighter, cheaper and higher in quality.</p>
			<img id="about_c1b1i" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b1i.png">
			<hr id="line11">
			<img id="about_c1b2t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b2t.png" alt="Excellent water resistance">
			<p id="txt2" class="ts14">Because of its excellent water resistance, can be used out-door, in bathrooms or any other wet areas including under water. You can even write under water.</p>
			<img id="about_c1b2i" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b2i.png">
			<hr id="line12">
			<img id="about_c1b3t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b3t.png" alt="Semipermanent Recycling Capability">
			<p id="txt3" class="ts14">LIMEX's main material is stone, thus durable and can be recycled semi-permanently</p>
			<img id="about_c1b3i" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b3i.png">
		</div>
	</div>
	<div class="sub-nav img_layer">
				<a href="/en/about/" class="about-index"><p><span></span>What is LIMEX</p></a>
				<ul>
					<li><a href="/en/about/paper/"><p><span></span>LIMEX PAPER</p></a></li>
					<li><a href="/en/about/plastic/"><p><span></span>LIMEX PLASTIC</p></a></li>
					<li><a href="/en/about/future/"><p><span></span>LIMEX FUTURE</p></a></li>
				</ul>
			</div>
</section>

<?php
get_footer('en');
