<?php
/**
 * Template Name: about-future
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


	<div class="pageTtl">
		<h2>LIMEX FACTORY</h2>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/about/">LIMEXとは</a></li>
				<li><a href="/about/paper/">LIMEXの紙</a></li>
				<li><a href="/about/plastic/">LIMEXのプラスチック</a></li>
				<li><a href="/about/future/" class="cur">LIMEXの可能性</a></li>
			</ul>
		</div>
	</div>

	<h3>LIMEXの可能性</h3>

		<section id="about">
			<div class="cnt">
				<div class="img_layer">
					<img id="about_c3t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3t.png" alt="LIME X PLASTIC">
					<img class="icon" id="about_c3i1" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3i1.png">
					<img class="icon" id="about_c3i2" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3i2.png">
					<img class="icon" id="about_c3i3" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3i3.png">
					<img class="icon" id="about_c3i4" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3i4.png">
					<img class="icon" id="about_c3i5" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3i5.png">
					<img class="icon" id="about_c3i6" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3i6.png">
					<img class="icon" id="about_c3i7" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3i7.png">
					<img class="icon" id="about_c3i8" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3i8.png">
					<img class="icon" id="about_c3i9" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3i9.png">
					<img class="icon" id="about_c3i10" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c3i10.png">
					<p id="about_c3r" class="fs-16">紙やプラスチックにとどまらず、ライメックスは建材や服飾、クルマや医療、<br>
					ロボットなど、あらゆる分野の素材になることを目指しています。</p>
				</div>
			</div>
			<div class="sub-nav img_layer">
				<a href="/about/" class="about-index"><p><span></span>LIMEXとは</p></a>
				<ul>
					<li><a href="/about/paper/"><p><span></span>LIMEXの紙</p></a></li>
					<li><a href="/about/plastic/"><p><span></span>LIMEXの<br>プラスチック</p></a></li>
					<li><a href="/about/future/"><p><span></span>LIMEXの可能性</p></a></li>
				</ul>
			</div>
		</section>

<?php
get_footer();
