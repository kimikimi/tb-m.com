<?php
/**
 * Template Name: about-lca
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="pageTtl">
		<h2>LIMEX FACTORY</h2>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/about/">LIMEXとは</a></li>
				<li><a href="/about/paper/">LIMEXの紙</a></li>
				<li><a href="/about/plastic/">LIMEXのプラスチック</a></li>
				<li><a href="/about/future/">LIMEXの可能性</a></li>
				<li><a href="/about/lca/"class="cur">LIMEXの環境影響評価</a></li>
			</ul>
		</div>
	</div>

	<h3><span style="font-size:39px; padding-left:0px; letter-spacing:4px;">LIMEX</span>の環境影響評価</h3>

		<section id="about">
			<div class="cnt lca2" id="lca2">
            	<div class="img_layer">
                <h4 class="lca-h4" ><span style="font-size:31px;">TBM</span>の持続的な事業発展を目指して</h4>
                <p class="lca-p">TBMが掲げる企業理念の実現および持続的な事業発展を目指すには、事業の中核となるLIMEX製品の環境影響の科学的評価 および管理が重要であり、LCA(ライフサイクルアセスメント)を用いた定量的な検証が必須であると考えます。今年度 (平成28年度)は、白石工場で製造される当社製品と既存の紙やプラスチックの環境負荷を比較し、LIMEX 製品が水資源や 森林資源などにもたらす環境負荷や貢献度の詳細数値を理解するため調査しました。また、今後、LIMEX製品で循環型社会 へ貢献するため、今回の数値結果を事業活動に考慮し、高い目標を掲げ更なる改善に向けて取り組みます。</p>
 				<img src="http://www.placehold.it/615x164/" id="lca-img1a">
                <img src="http://www.placehold.it/295x164/" id="lca-img1b">
                </div>
                
				<div class="img_layer">
					<h4 class="lca-h4" id="lca-h42a">東京大学生産技術研究所 沖研究室との連携</h4>
                    <p class="lca-p" id="lca-p2a">TBMは、2016年4月から国立大学法人東京大学 生産技術研究所 沖研究室と共同研究を実施し、LIMEXペーパー及びLIMEX プラスチックのライフサイクル全体における環境影響の数値を算出いたしました。沖研究室は、水循環や水資源の保全、またウォーターフットプリントやLCAに関し豊富な研究実績を有されており、今回の測定結果に対する正確性の担保、また、 外部機関として連携することで測定結果の透明性を図る目的があります。</p>
 					<h4 class="lca-h4" id="lca-h43a">LCA算定における基本条件</h4>
 					<p class="lca-p" id="lca-p3a">影響及び報告範囲</p>
					<p class="lca-p" id="lca-p3c">TBMは、2016年4月から国立大学法人東京大学 生産技術研究所 沖研究室と共同研究を実施し、LIMEXペーパー及びLIMEX プラスチックのライフサイクル全体における環境影響の数値を算出いたしました。沖研究室は、水循環や水資源の保全、ま たウォーターフットプリントやLCAに関し豊富な研究実績を有されており、今回の測定結果に対する正確性の担保、また、 外部機関として連携することで測定結果の透明性を図る目的があります。</p>
 					<hr class="lca-centerline">
 					<p class="lca-p" id="lca-p3b">評価物質および環境負荷原単位データベース</p>
 					<p class="lca-p" id="lca-p3d">調査では、LIMEXペーパーおよびLIMEXプラスチック各1トン当たりの「水消費量（ウォーターフットプリント）」および「温室効果ガス排出量注１」、を評価しました。評価に必要となる原単位として、温室効果ガス排出量には「３EID（2005年）」を、また、水消費量には、「Ono et al（2016年）」を採用しています。「３EID」は、国立環境研究所によって開発され日本における代表的なデータベースであり、また、「Ono et al」においては、日本における唯一の水の原単位データベース注２として、あらゆる分野の企業や研究において活用されていることから、本調査に適するデータベースとして採用しています。</p>
 					<p id="p-cert" style="font-size:10px;  line-height: 15px;;  letter-spacing: 0.9px;
 text-align: left; padding-bottom:83px; top:1252px; width:940px; height:15px; left:0px; position:absolute;">						<br>注１　二酸化炭素（CO2）、メタン(CH4)、一酸化二窒素（N2O）を含む。<br>
注２　厳密には国立研究開発法人産業技術総合研究所が開発した水原単位データベースがありますが、「Ono et al」の技術提供を受けたことから基本的同じデータベースとして考慮する。
</p>
     				<h4 class="lca-h4" id="lca-h44a">従来の紙・プラスチックとLIMEXの比較</h4>
 <!--<p style="font-size:20px;  line-height: 30px;;  letter-spacing: 0.9px;
 text-align: left; padding-bottom:83px; top:1451px; width:440px; height:40px; left:0px; position:absolute;">従来の紙・プラスチックとLIMEXの比較</p>-->
                		<img src="http://www.placehold.it/360x40/" id="lca-img4a">
                	<img src="http://www.placehold.it/360x40/" id="lca-img4b">
                	<img src="http://www.placehold.it/440x212/" id="lca-img4c">
                	<img src="http://www.placehold.it/440x212/" id="lca-img4d">
                    <img src="http://www.placehold.it/440x350/" id="lca-img4e">
                	<img src="http://www.placehold.it/440x350/" id="lca-img4f">
				</div>
               
                
			</div>
            <!--<div class="sub-nav img_layer" style="top:2400px; left:0px; ">
                <div class="lca_footer_l" style="width:440px; float:left; margin:auto 0;"><a href="/about/" class="about-index"><p><span></span>LIMEXとは</p></a></div>
                <div class="lca_footer_r" style="width:440px; float:right; margin:auto 0;"><a href="/about/" class="about-index"><p><span></span>LIMEXとは</p></a></div>
				<!--<a style="left:0px;postion:absolute;" href="/about/" class="about-index"><p><span></span>LIMEXとは</p></a>
				<ul>
					<li><a href="/about/paper/"><p><span></span>LIMEXの紙</p></a></li>
					<li><a href="/about/plastic/"><p><span></span>LIMEXの<br>プラスチック</p></a></li>
					<li><a href="/about/future/"><p><span></span>LIMEXの可能性</p></a></li>
				</ul>-->
			<!--</div>-->
            <!---
			<span class="sub-nav img_layer lca-ul" style="top:2400px; 
	left:0px; ">
				
				<ul>
					<li><a href="/about/paper/"><p><span></span>LIMEX PAPERの<br>可能性</p></a></li>
                </ul>
            </span>
            <div class="sub-nav img_layer lca-ul">
            		<ul>
					<li><a href="/about/plastic/"><p><span></span>LIMEX PLASTICの<br>可能性</p></a></li>
                    
					<!--<li><a href="/about/future/"><p><span></span>LIMEXの可能性</p></a></li>
					</ul>-->
            <div class="sub-nav sub-navd img_layer" style="width:940px; padding-bottom:80px;">
            	<div style="width:440px; float:left;">
				<a href="/about/" class="about-index"><p style="width:200px; line-height:1.5"><span></span>LIMEX PAPERの<br />LCA結果</p></a>
                </div>
                <div style="width:440px; float:right;">
				<a href="/about/" class="about-index"><p><span></span>LIMEX PLASTICの<br />LCA結果</p></a>
                </div>
				<!--<ul>
					<li><a href="/about/paper/"><p><span></span>LIMEXの紙</p></a></li>
					<li><a href="/about/plastic/"><p><span></span>LIMEXの<br>プラスチック</p></a></li>
					<li><a href="/about/future/"><p><span></span>LIMEXの可能性</p></a></li>
				</ul>-->
			</div>
		</section>

<?php
get_footer();
