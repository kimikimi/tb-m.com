<?php
/**
 * Template Name: about
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<div class="pageTtl">
		<h2>LIMEX FACTORY</h2>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/about/" class="cur">LIMEXとは</a></li>
				<li><a href="/about/paper/">LIMEXの紙</a></li>
				<li><a href="/about/plastic/">LIMEXのプラスチック</a></li>
				<li><a href="/about/future/">LIMEXの可能性</a></li>
			</ul>
		</div>
	</div>

	<h3>LIMEXとは</h3>

		<section id="about">
		<div class="movie">
			<iframe width="940" height="528" src="https://www.youtube.com/embed/ZQ0J8Ur9T-E?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
		</div>
			<div class="cnt">
				<img id="about_bk" src="<?php echo get_template_directory_uri(); ?>/img/about/about_stone.jpg">
				<div class="img_layer">
					<img id="about_top1" src="<?php echo get_template_directory_uri(); ?>/img/about/about_top1.png" alt="LIME">
					<p id="about_top2" class="fs-18">石灰石。英語でLimestone。地球上にほぼ無尽蔵にあり、<br>人類が長く見過ごしてきたこの素材が大活躍する。そんな未来が、もう始まっています。</p>
					<img id="about_b1t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b1t.png" alt="地球は石の星でもある。">
					<p id="about_b11">ライメックスの原料である石灰石は埋蔵量も多く、日本でも100％自給自足できる資源です。また世界各地の埋蔵量も豊富で、かつ高効率でリサイクルも可能なため、ほぼ無尽蔵と言っても過言ではありません。どこにでも豊富にある、決して貴重ではない資源だからこそ、未来を救える。私たちはそう考えています。</p>
					<img id="about_b1i" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b1i.png">
					<img id="about_b2t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b2t.png" alt="紙を減らす石灰石。">
					<p id="about_b21">全世界の紙の消費量は、2030年に現在の約2倍になると予測されています。消費量の一部を、木を使わず、水を汚さずにつくるライメックスの紙に置き換えることで、地球環境の保護につながります。</p>
					<img id="about_b2i1" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b2i1.png">
					<img id="about_b2i2" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b2i2.png">
					<img id="about_b2i3" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b2i3.png">
					<img id="about_b3t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b3t.png" alt="木と水を守る石灰石。">
					<p id="about_b31">普通紙を１ｔつくるのに使用する樹木はおよそ20本。そして約100tもの水を必要とします。ライメックスなら石灰石約0.6t〜0.8t、ポリオレフィン樹脂(ポリプロピレンなど)約0.2tで、1tの紙をつくれます。木も水も全く使いません。さらにライメックスの紙は普通紙と比べ、リサイクル効率が高いのが特徴です。</p>
					<img id="about_b3i1" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b3i1.png">
					<img id="about_b3i2" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b3i2.png">
					<img id="about_b3i3" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b3i3.png">
					<img id="about_b2i4" src="<?php echo get_template_directory_uri(); ?>/img/about/about_b3i4.png">
					<img id="about_cnt1" src="<?php echo get_template_directory_uri(); ?>/img/about/about_cnt1.png">
					<p id="about_cnt2" class="fs-18">LIMEXは、紙にも、プラスチックにもなる。<br>変幻自在に生まれ変われる新素材は、<br>様々な製品になって、世界中の生活を支えてゆく。</p>
				</div>
			</div>
			<div class="sub-nav img_layer">
				<!-- <a href="/about/" class="about-index"><p><span></span>LIMEXとは</p></a> -->
				<ul>
					<li><a href="/about/paper/"><p><span></span>LIMEXの紙</p></a></li>
					<li><a href="/about/plastic/"><p><span></span>LIMEXの<br>プラスチック</p></a></li>
					<li><a href="/about/future/"><p><span></span>LIMEXの可能性</p></a></li>
				</ul>
			</div>
		</section>



<?php
get_footer();
