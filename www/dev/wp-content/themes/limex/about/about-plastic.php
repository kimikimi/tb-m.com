<?php
/**
 * Template Name: about-plastic
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="pageTtl">
		<h2>LIMEX FACTORY</h2>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/about/">LIMEXとは</a></li>
				<li><a href="/about/paper/">LIMEXの紙</a></li>
				<li><a href="/about/plastic/" class="cur">LIMEXのプラスチック</a></li>
				<li><a href="/about/future/">LIMEXの可能性</a></li>
			</ul>
		</div>
	</div>

	<h3>LIMEXのプラスチック</h3>

		<section id="about">
			<div class="cnt">
				<div class="img_layer">
					<img id="about_c2t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2t.png" alt="LIME X PLASTIC">
					<img class="icon" id="about_c2i1" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2i1.png">
					<img class="icon" id="about_c2i2" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2i2.png">
					<img class="icon" id="about_c2i3" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2i3.png">
					<img class="icon" id="about_c2i4" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2i4.png">
					<img class="icon" id="about_c2i5" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2i5.png">
					<img class="icon" id="about_c2i6" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2i6.png">
					<img class="icon" id="about_c2i7" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2i7.png">
					<img class="icon" id="about_c2i8" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2i8.png">
					<img class="icon" id="about_c2i9" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2i9.png">
					<img class="icon" id="about_c2i10" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2i10.png">

					<p id="about_c2r" class="fs-16">ライメックスプラスチックは、石灰石からつくるプラスチック。<br>ポリエチレンやポリプロピレンのフィルムやシートにも代替可能な素材で、石油の使用量を減らすことができます。</p>
					<img id="about_c2bt" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2bt.png" alt="LIMEXでつくるプラスチックの特徴">
					<img id="about_c2b1t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2b1t.png" alt="コストを大幅に削減">
					<p id="txt4" class="ts14">石灰石は石油に比べて安価なため、高品質なライメックスプラスチックを低コストでつくれます。</p>
					<hr id="line21">
					<img id="about_c2b2t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2b2t.png" alt="リサイクル委託金がなくなる">
					<p id="txt5" class="ts14">ライメックスプラスチックは容器リサイクル法に基づくリサイクル委託金の義務がなく、さらなるコスト削減を可能にします。</p>
					<hr id="line22">
					<img id="about_c2b3t" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2b3t.png" alt="企業のイメージアップに貢献">
					<p id="txt6" class="ts14">ライメックスプラスチックを導入することで、企業の環境への貢献イメージの向上につながります。</p>
					<img id="about_c2bi" src="<?php echo get_template_directory_uri(); ?>/img/about/about_c2bi.png">
					<p id="txt7" class="ts14">ライメックスペーパーでは水資源と森林資源、ライメックスプラスチックでは石油資源の保全に貢献しながら、未来を変えるために、それぞれの領域へと市場を広げていきます。</p>
				</div>
			</div>
			<div class="sub-nav img_layer">
				<a href="/about/" class="about-index"><p><span></span>LIMEXとは</p></a>
				<ul>
					<li><a href="/about/paper/"><p><span></span>LIMEXの紙</p></a></li>
					<li><a href="/about/plastic/"><p><span></span>LIMEXの<br>プラスチック</p></a></li>
					<li><a href="/about/future/"><p><span></span>LIMEXの可能性</p></a></li>
				</ul>
			</div>
		</section>

<?php
get_footer();
