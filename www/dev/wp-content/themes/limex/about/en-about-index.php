<?php
/**
 * Template Name: en-about-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>

<section id="about">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/about/" class="cur">LIMEX</a></li>
				<li><a href="/en/about/paper/">LIMEX PAPER</a></li>
				<li><a href="/en/about/plastic/">LIMEX PLASTIC</a></li>
				<li><a href="/en/about/future/">LIMEX FUTURE</a></li>
			</ul>
		</div>
	</div>

	<h3>LIMEX PAPER</h3>
	<div class="movie">
		<iframe width="940" height="528" src="https://www.youtube.com/embed/ZQ0J8Ur9T-E?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
	</div>

	<div class="cnt">
	<img id="about_bk" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_stone.jpg">
		<div class="img_layer">
			<img id="about_top1" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_top1.png" alt="LIME">
			<img id="about_top2" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_top2.png" alt="Limestone, almost inexhaustible, a materialoverlooked for a long time is now making splendid success.Yes, the future has already begun.">
			<img id="about_b1t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b1t.png" alt="Our Earth, the planet of stones.">
			<img id="about_b11" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b11.png" alt="LIMEX made from Limestone.<br>Limestone reserve is abundant, and is a natural resource 100% self-sufficient in Japan.In the world the limestone reserves are abundant as well, and due to its high recycling capability, it is nearly inexhaustible.Abundant, common, not precious natural resource makes it possible to save our future. That is what we believe.">
			<img id="about_b1i" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b1i.png">
			<img id="about_b2t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b2t.png" alt="Limestone reducing the consumption of paper">
			<img id="about_b21" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b21.png" alt="Paper consumption is estimated to double in 2030.<br>If LIMEX partially replaces paper, it will lead to saving forests and reducing water usage.">
			<img id="about_b2i1" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b2i1.png">
			<img id="about_b2i2" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b2i2.png">
			<img id="about_b2i3" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b2i3.png">
			<img id="about_b3t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b3t.png" alt="Limestone, protecting forests and water">
			<img id="about_b31" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b31.png" alt="1 ton of paper is made from ca. 20 trees and uses 100 tons of water.1 ton of LIMEX is made from 0.8 ton of Limestone and 0.2 ton of polyethylene, with high recycling capability compared to paper. ">
			<img id="about_b3i1" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b3i1.png">
			<img id="about_b3i2" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b3i2.png">
			<img id="about_b3i3" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b3i3.png">
			<img id="about_b2i4" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_b3i4.png">

			<img id="about_cnt1" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_cnt1.png" alt="X">
			<img id="about_cnt2" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_cnt2.png" alt="LIMEX can be used as paper and plastic, a versatile new material that will support our daily life world-wide.">
		</div>
	</div>

	<!-- <div class="cnt">
			<img id="about_c1t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1t.png" alt="LIME X PAPER">
			<img class="icon" id="about_c1i1" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i1.png">
			<img class="icon" id="about_c1i2" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i2.png">
			<img class="icon" id="about_c1i3" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i3.png">
			<img class="icon" id="about_c1i4" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i4.png">
			<img class="icon" id="about_c1i5" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i5.png">
			<img class="icon" id="about_c1i6" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i6.png">
			<img class="icon" id="about_c1i7" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i7.png">
			<img class="icon" id="about_c1i8" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i8.png">
			<img class="icon" id="about_c1i9" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i9.png">
			<img class="icon" id="about_c1i10" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1i10.png">
			<img id="about_c1r" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1r.png" alt="LIMEX paper - paper made from limestone - without using pulp nor water, easy to use. Printable, can be used for books, notebooks, name-cards, posters and for many other purposes.">
			<img id="about_c1bt" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1bt.png" alt="Characteristics of LIMEX paper">
			<img id="about_c1b1t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b1t.png" alt="Finally achieved competitive lightness">
			<p id="txt1" class="ts14">With the new manufacturing technology, LIMEX is now much lighter, cheaper and higher in quality.</p>
			<img id="about_c1b1i" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b1i.png">
			<hr id="line11">
			<img id="about_c1b2t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b2t.png" alt="Excellent water resistance">
			<p id="txt2" class="ts14">Because of its excellent water resistance, can be used out-door, in bathrooms or any other wet areas including under water. You can even write under water.</p>
			<img id="about_c1b2i" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b2i.png">
			<hr id="line12">
			<img id="about_c1b3t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b3t.png" alt="Semipermanent Recycling Capability">
			<p id="txt3" class="ts14">LIMEX's main material is stone, thus durable and can be recycled semi-permanently</p>
			<img id="about_c1b3i" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c1b3i.png">
			<img id="about_c2t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2t.png" alt="LIME X PLASTIC">
			<img class="icon" id="about_c2i1" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i1.png">
			<img class="icon" id="about_c2i2" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i2.png">
			<img class="icon" id="about_c2i3" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i3.png">
			<img class="icon" id="about_c2i4" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i4.png">
			<img class="icon" id="about_c2i5" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i5.png">
			<img class="icon" id="about_c2i6" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i6.png">
			<img class="icon" id="about_c2i7" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i7.png">
			<img class="icon" id="about_c2i8" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i8.png">
			<img class="icon" id="about_c2i9" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i9.png">
			<img class="icon" id="about_c2i10" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2i10.png">
			<img id="about_c2r" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2r.png" alt="LIMEX plastic - plastic made from limestone-It can be used for films and sheets currently made from 100% petroleum-based plastic materials, and thus reduces the consumption of petroleum.">
			<img id="about_c2bt" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2bt.png" alt="Characteristics of LIMEX Plastics">
			<img id="about_c2b1t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2b1t.png" class="Extreme Cost Reduction">
			<p id="txt4" class="ts14">High quality but cost reducing as Limestone is much cheaper than petroleum.</p>
			<hr id="line21">
			<img id="about_c2b2t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2b2t.png" alt="No duty to pay Recycling Charge">
			<p id="txt5" class="ts14">By using LIMEX plastics, recycling charge under the Japan Containers and Packaging Recycling Law can be avoided. Thus reduces costs even more.</p>
			<hr id="line22">
			<img id="about_c2b3t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2b3t.png" alt="Contribute to Company's Image Improvement">
			<p id="txt6" class="ts14">By using LIMEX plastics, it will lead to company's environment friendly image.</p>
			<img id="about_c2bi" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c2bi.png">
			<p id="txt7" class="ts14">LIMEX paper can contribute to water shortage and deforestation matters and LIMEX plastic to conserving petroleum resources. LIMEX will grow in each market in order to change the world.</p>
			<img id="about_c3t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3t.png" alt="LIMEX X FUTURE">
			<img class="icon" id="about_c3i1" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i1.png">
			<img class="icon" id="about_c3i2" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i2.png">
			<img class="icon" id="about_c3i3" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i3.png">
			<img class="icon" id="about_c3i4" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i4.png">
			<img class="icon" id="about_c3i5" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i5.png">
			<img class="icon" id="about_c3i6" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i6.png">
			<img class="icon" id="about_c3i7" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i7.png">
			<img class="icon" id="about_c3i8" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i8.png">
			<img class="icon" id="about_c3i9" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i9.png">
			<img class="icon" id="about_c3i10" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i10.png">
			<img id="about_c3r" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3r.png" alt="LIMEX not only to replace paper and plastic, but aiming to become material for building materials, clothing, cars, medical appliances , robots and etc.">
		</div>
	</div> -->
	<div class="sub-nav img_layer">
				<!-- <a href="/about/" class="about-index"><p><span></span>What is LIMEX</p></a> -->
				<ul>
					<li><a href="/en/about/paper/"><p><span></span>LIMEX PAPER</p></a></li>
					<li><a href="/en/about/plastic/"><p><span></span>LIMEX PLASTIC</p></a></li>
					<li><a href="/en/about/future/"><p><span></span>LIMEX FUTURE</p></a></li>
				</ul>
			</div>
</section>


<?php
get_footer('en');
