<?php
/**
 * Template Name: en-about-future
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>

<section id="about">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>COMPANY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/about/">LIMEX</a></li>
				<li><a href="/en/about/paper/">LIMEX PAPER</a></li>
				<li><a href="/en/about/plastic/">LIMEX PLASTIC</a></li>
				<li><a href="/en/about/future/" class="cur">LIMEX FUTURE</a></li>
			</ul>
		</div>
	</div>
	<h3>LIMEX FUTURE</h3>

	<div class="cnt">
		<div class="img_layer">
			<img id="about_c3t" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3t.png" alt="LIMEX X FUTURE">
			<img class="icon" id="about_c3i1" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i1.png">
			<img class="icon" id="about_c3i2" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i2.png">
			<img class="icon" id="about_c3i3" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i3.png">
			<img class="icon" id="about_c3i4" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i4.png">
			<img class="icon" id="about_c3i5" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i5.png">
			<img class="icon" id="about_c3i6" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i6.png">
			<img class="icon" id="about_c3i7" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i7.png">
			<img class="icon" id="about_c3i8" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i8.png">
			<img class="icon" id="about_c3i9" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i9.png">
			<img class="icon" id="about_c3i10" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3i10.png">
			<img id="about_c3r" src="<?php echo get_template_directory_uri(); ?>/img/en/about/about_c3r.png" alt="LIMEX not only to replace paper and plastic, but aiming to become material for building materials, clothing, cars, medical appliances , robots and etc.">
		</div>
	</div>
	<div class="sub-nav img_layer">
				<a href="/en/about/" class="about-index"><p><span></span>What is LIMEX</p></a>
				<ul>
					<li><a href="/en/about/paper/"><p><span></span>LIMEX PAPER</p></a></li>
					<li><a href="/en/about/plastic/"><p><span></span>LIMEX PLASTIC</p></a></li>
					<li><a href="/en/about/future/"><p><span></span>LIMEX FUTURE</p></a></li>
				</ul>
			</div>
</section>

<?php
get_footer('en');
