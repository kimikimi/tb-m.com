<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$directory_name = get_directory_name('en');
$heder_js = get_head_js($directory_name);
$class_name = get_body_classname();

?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html>
<!--<![endif]-->
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<!--[if lt IE 9]>
		<script type="text/javascript" src="./js/ie/html5shiv.js"></script>
		<![endif]-->
		<title>a next generation material LIMEX, TBM Co., Ltd</title>
		<meta name="keywords" content="LIMEX, limestone, recycle, economy, ecology, new material, stone paper, plastic TBM Co., Ltd">
		<meta name="description" content="LIMEX created by TBM Co. Ltd, is a new material made from limestone (calcium carbonate) serves to be a substitute of paper and plastic, realizing ecology and economy. ">
		
		<meta property="og:title" content="a next generation material LIMEX, TBM Co., Ltd" />
		<meta property="og:url" content="http://tb-m.com/en/" />
		<meta property="og:description" content="LIMEX created by TBM Co. Ltd, is a new material made from limestone (calcium carbonate) serves to be a substitute of paper and plastic, realizing ecology and economy. " />
		<meta property="og:image" content="http://tb-m.com/img/ogp.jpg" />
		
		<meta name="viewport" content="width=1280px, maximum-scale=1.0, user-scalable=yes">

		<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/en/style.css" />

<?php if(file_exists( TEMPLATEPATH. '/css/style_'. $directory_name . '.css')):?>
		<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/en/style_<?php echo $directory_name ;?>.css" />
<?php endif;?>
<?php if(is_current('/',$class_name)):?>
		<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/en/style_top.css" />
<?php endif;?>
		<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/en/custom_style.css" />

		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/en/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/en/common02.js" charset="utf-8"></script>
<?php echo ($heder_js) ? $heder_js : '' ;?>
		<LINK REL="SHORTCUT ICON" HREF="<?php echo get_template_directory_uri(); ?>/img/common/favicon.ico">
<?php if(is_current('access',$class_name)):?>
		<script src="https://maps.google.com/maps/api/js?sensor=false&v=3.21"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/map-design.js"></script>
<?php endif;?>

	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/en/selectivizr-min.js" charset="UTF-8"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/en/ie/html5shiv.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/en/ie/jquery.backgroundSize.js"></script>
		<script>
			$(function() {
				$(".header-facebook a").css({backgroundSize: "cover"});
			});
		</script>
	<![endif]-->
	<?php if(is_current('story',$class_name)):?>
	<!--[if lt IE 9]>
	<script>
		$(function() {
			$(".story .main-visual, sec02 .left, sec04").css({backgroundSize: "cover"});
		});
	</script>

	<![endif]-->
<?php endif;?>

</head>
<body<?php echo $class_name ?' class="'.$class_name . '"' : '' ;?>>
	<div id="lang01">
		<div id="lang02">
<?php if($_SERVER['REQUEST_URI']== '/order/en_explain.php'):?>
			<a href="/order/explain.php"><img src="<?php echo get_template_directory_uri(); ?>/img/en/common/lang_ja_off.png" class="fade01"></a>
<?php else: ?>
			<a href="<?php echo preg_replace('/\/en/', '', $_SERVER['REQUEST_URI']) ;?>"><img src="<?php echo get_template_directory_uri(); ?>/img/en/common/lang_ja_off.png" class="fade01"></a>
<?php endif ;?>
			<a href="<?php echo $_SERVER['REQUEST_URI'] ;?>"><img src="<?php echo get_template_directory_uri(); ?>/img/en/common/lang_en_on.png"></a>
		</div>
		<p class="header-facebook">
			<a href="https://www.facebook.com/tbm.limex" target="_blank" class="icon02">facebook</a>
		</p>
		<!-- <div id="ext01-01"><a href="/en/action/"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav_header_ext_action.gif" class="fade01" alt="TBM ACTION"></a></div> -->
		<!-- <div id="ext01-02"><a href="/order/en_explain.php"><img src="<?php echo get_template_directory_uri(); ?>/img/en/common/nav_header_ext_order.png" class="fade01" alt="TBM ACTION"></a></div> -->
		<div id="ext01-03"><a href="mailto:infomail@tb-m.com"><img src="<?php echo get_template_directory_uri(); ?>/img/en/common/nav_header_ext_contact.png" class="fade01" alt="TBM ACTION"></a></div>
	</div>

	<header>	
		<div class="wrapper">
			
			<h1 class="logo"><a href="/en/"><img src="<?php echo get_template_directory_uri(); ?>/img/en/common/logo_header.png" height="44" width="192" alt="LIMEX"></a></h1>
			<div id="gnav">
				<ul>
					<li class="nav1">
						<a data-id="about" href="/en/about/"<?php echo is_current('about',$directory_name) ? ' class="cur"':'';?>>ABOUT</a>
						<div class="g-sub-navi">
							<ul>
								<li><a href="/en/about/">What is LIMEX</a></li>
								<li><a href="/en/about/paper/">LIMEX PAPER</a></li>
								<li><a href="/en/about/plastic/">LIMEX PLASTIC</a></li>
								<li><a href="/en/about/future/">LIMEX FUTURE</a></li>
							</ul>
						</div>
					</li>
					<li class="nav2">
						<a data-id="mission" href="/en/mission/"<?php echo is_current('mission',$directory_name) ? ' class="cur"':'';?>>MISSION</a>
						<div class="g-sub-navi">
							<ul>
								<li><a href="/en/mission/">MISSION STORY</a></li>
								<li><a href="/en/mission/vision/">VISION STORY</a></li>
							</ul>
						</div>
					</li>
					<li class="nav3">
						<a data-id="factory" href="/en/factory/"<?php echo is_current('factory',$directory_name) ? ' class="cur"':'';?>>FACTORY</a>
						<div class="g-sub-navi">
								<ul>
									<li><a href="/en/factory/">SHIROISHI ZAO</a></li>
									<li><a href="/en/factory/tagajo/">TAGAJO</a></li>
								</ul>
							</div>
					</li>
					<li class="nav4">
						<a data-id="company" href="/en/company/"<?php echo is_current('company',$directory_name) ? ' class="cur"':'';?>>COMPANY</a>
						<div class="g-sub-navi">
								<ul>
									<li><a href="/en/company/">ABOUT US</a></li>
									<li><a href="/en/company/profile/">PROFILE</a></li>
									<li><a href="/en/company/identity/">IDENTITY</a></li>
									<li><a href="/en/company/history/">HISTORY</a></li>
									<li><a href="/en/company/message/">MESSAGE</a></li>
									<li><a href="/en/company/story/story1/">STORY</a></li>
									<li><a href="/en/company/access/">ACCESS</a></li>
									<li><a href="/en/company/award/">AWARD</a></li>
								</ul>
							</div>
					</li>
					<li class="nav5">
						<a data-id="news" href="/en/news/"<?php echo is_current('news',$directory_name) ? ' class="cur"':'';?>>NEWS</a>
						<div class="g-sub-navi">
							<ul>
								<li><a href="/en/news/?cat=press_release">PRESS RELEASE</a></li>
								<li><a href="/en/news/?cat=topics">TOPICS</a></li>
								<li><a href="/en/news/?cat=media">MEDIA</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</header>