<?php
/**
 * Template Name: privacy-disclose
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<style>
body.disclose .content01 {
    width: 608px;
    margin: 0 auto 50px;
    text-align: left;
}
body.disclose p {
  font-size: 13.7px;
  line-height: 25px;
  margin-bottom: 30px;
}
body.disclose .content01 ol li {
  font-size: 13.7px;
  line-height: 25px;
  margin-bottom: 30px;
}
body.disclose .pageTtl {
  padding: 0;
}
body.disclose p.mb0 {
  margin-bottom: 0px;
}
</style>

<section id="body">
<div class="pageTtl">
<div class="wrapper">
<h2>PRIVACY POLICY</h2>
</div>
</div>

<div class="sec1">
<h3>開示等の求めに応じる手続きに関する事項</h3>
</div>


<div class="content01">
<p>当社は、保有する個人情報の本人またはその代理人（法定代理人または委任に基づく代理人）から「利用目的の通知、個人情報の開示、訂正・追加・削除、利用の停止・消去、第三者提供の停止の求め」（以下「開示等の求め」という。）につき、以下の手続きにより対応いたします。</p>

<p>（開示等の求めの申し出先）<br>
開示等の求めは、下記の窓口宛に、所定の書式（「開示対象個人情報の開示申請書」）に必要書類を添付の上、郵送にてお送りください。なお、郵送以外でのお申し出は、お受けいたしかねますので、その旨ご了承の程お願いいたします。</p>

<p>申し出窓口<br>
〒100-0005 東京都千代田区丸の内1-3-1　東京銀行協会ビル10階<br>
株式会社ＴＢＭ　個人情報保護担当
</p>

<p>（開示等の求めを行うための書式）<br>
ご本人がご自身を識別できる開示対象個人情報について、開示等を求める場合は、（書面様式）<span class="bd01"><a href="/pdf/disclose_privacy.pdf" target="_blank">「個人情報の開示等の請求書」（ＰＤＦ形式）</a></span>をダウンロードして使用し、必要事項をご記入の上、必要書類を添付して、所定のいずれか提出してください
</p>

<p class="mb0">（開示等の求めに際し、「本人の知り得る状態に置くべき事項」）
<ol>
<li>1. 事業者の名称<br>
株式会社TBM</li>
<li>2. 個人情報保護管理者の氏名、所属及び連絡先<br>
土屋　好弘　（開発本部）<br>
TEL 03-6212-7270　FAX 03-6212-7271</li>
<li>3. 開示対象個人情報の利用目的<br>
取引先情報｜商談その他各種相談・お問い合わせへの対応<br>
社員情報｜当社従業者の人事労務管理、業務管理、健康管理、セキュリティ管理のため<br>
採用応募者に関する個人情報｜採用業務管理、応募者との連絡のため
</li>
<li>4. 苦情及び相談先<br>
個人情報に関する苦情の申し出およびご相談につきましては、下記窓口で受付けております。<br>
〒100-0005 東京都千代田区丸の内1-3-1 東京銀行協会ビル10階<br>
株式会社TBM　個人情報保護担当
</li>
</ol></p>
</div>

</section><!-- /#body -->

<?php
get_footer();
