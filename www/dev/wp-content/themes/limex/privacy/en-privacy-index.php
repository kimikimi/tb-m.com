<?php
/**
 * Template Name: en-privacy-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('en'); ?>

<section id="body">
<div class="pageTtl">
<div class="wrapper">
<h2>PRIVACY POLICY</h2>
</div>
</div>

<div class="sec1">
<h3>Personal Information Protection Policy</h3>
</div>


<div class="content01">
<p class="eoc01">Enacted in April 1,2015<br>
TBM Co., Ltd<br>
Chief Executive Officer, Nobuyoshi Yamasaki
</p>
<p>This company started with selling LIMEX, ecofriendly stone paper made from limestone, as name cards and other products. Because this company sells to various people and handle a large amount of personal information, we think it is our social responsibility to protect our customers' information regarding their rights and profits. For the sake of the security of our customers' information we have laid down the following rules.</p>

<p class="mb0">1. Regarding the usage, gain and provision of personal information.
<ol>
<li>1)<!--.--> In the case of gaining Personal information. We will make sure that the customer is in full understanding of what we are going to use the information for and the laws regarding the handling of personal information and only after we gained the customer consent will we gain the personal information.</li>
<li>2)<!--.--> In the case of personal information usage. Usage is limited based on our customers wishes.</li>
<li>3)<!--.--> In the case of sharing personal information with a third-party. Only information that the customer has agreed on to share will be used.</li>
</ol></p>

<p>2. Regarding security procedures<br>
To ensure the accuracy, safe usage of the information, security plans against leakage, destruction or damage to personal information are put in place.</p>

<p>3. Regarding complaints or consulting.<br>
If there are any complaints, or consultations, from the customer we will take immediate action. If a customer wants to delete, correct or stop the distribution of the personal information, we will respect the customers wishes and take proper measurements without a delay.</p>

<p>4. Regarding observations of the  law, guides or normative.<br>
We follows the rules defined by the government .regarding the handling and the accuracy of personal information.</p>

<p>5. Regarding the intermittent improvement of the personal information management system<br>
The Personal information protection system's operation circumstances are regular inspected and improved to ensure the security of the customers personal information.</p>

<p class="mb20">Regarding questions about personal safety.<br>
TBM Co., Ltd Person in charge of  Personal information security<br>
TEL +81 03-6212-7270<br>FAX +81 03-6212-7271<br>
e-mail : <a href="mailto:info@tb-m.com">info@tb-m.com</a></p>

<p class="eoc01"><!--以上--></p>
</div>


<div class="sec2">
<h3>About the treatment of personal information</h3>
</div>


<div class="content02">
<p>1. Company Name:        TBM Co., Ltd</p>

<p>2. Name of the manager of personal information security,  Affiliation or contact address<br>
Name of the manager: Yoshihiro Tsuchiya<br>
Affiliated department: Development department<br>
Contact address: TEL +81 03-6212-7270</p>

<p>3. The goal of using some personal information<br>
Business partner contact information: for handling with business negotiation, inquiries<br>
Staff information: For the sake of health, work and security management<br>
Personal information of job applicants: for the sake of recruit management and contacting job applicants</p>

<p class="mb0">4. Supplying personal information to third-parties<br>
We does not provide personal information to third-parties except in the following cases.<br>
<ol>
<li>1) With the consent of the person concerned</li>
<li>2) Upon law and governmental regulations</li>
<li>3) In case that one's wealth, life or health is in danger, and it is difficult to get the consent of the person concerned</li>
<li>4) For the improvement of the health of infants or public health and sanitation, and it is difficult to get the consent of person concerned</li>
<li>5) If it becomes necessary to work with the government or public groups, depending on the customers decision it could cause problems in the matter.</li>
</ol>
</p>

<p>5. regarding entrusting the handling of personal information.<br>
A part of the human resource management department is entrust to another company. In regards to the consignee, there are times when personal information are entrusted in their care. In such a case, the consignee is bound not to disclose personal information and keep a safe management.</p>

<p>6. Personal information disclosure request<br>
If the customer, or a representative, wishes to change, delete, add, publicize or stop the sharing to third-parties, it can be done by requesting at the inquire reception. In such a case the company needs to confirm the customers, or representatives, identity and it will be done in a reasonably time. For further information regarding requesting publication, please refer to <span class="bd01"><a href="./disclose/">"Disclosure Procedure"</a></span>.</p>

<p>7. The arbitrariness regarding the distribution of personal information<br>
It is up to the person concerned whether his personal information should be provided or not.  But there are cases that some required items cannot be provided due to service status.</p>

<p class="mb20">Contact Information<br>
the 10th Floor of Tokyo Ginko Kyokai Building<br>
1-3-1 Marunouchi Chiyoda-ku, Tokyo 100-0005<br>
TEL: +81 03-6212-7270<br>
FAX: +81 03-6212-7271<br>
TBM Co., Ltd<br>
Person in charge of Personal Information.</p>

</div>



</section><!-- /#body -->

<?php
get_footer('en');
