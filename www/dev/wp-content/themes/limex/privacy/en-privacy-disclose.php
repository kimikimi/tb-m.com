<?php
/**
 * Template Name: en-privacy-disclose
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('en'); ?>

<style>
body.disclose .content01 {
    width: 608px;
    margin: 0 auto 50px;
    text-align: left;
}
body.disclose p {
  font-size: 13.7px;
  line-height: 25px;
  margin-bottom: 30px;
}
body.disclose .content01 ol li {
  font-size: 13.7px;
  line-height: 25px;
  margin-bottom: 30px;
}
body.disclose .pageTtl {
  padding: 0;
}
body.disclose p.mb0 {
  margin-bottom: 0px;
}
</style>

<section id="body">
<div class="pageTtl">
<div class="wrapper">
<h2>PRIVACY POLICY</h2>
</div>
</div>

<div class="sec1">
<h3>Disclosure Procedure</h3>
</div>


<div class="content01">
<p>Upon your or your agent (legal representative or delegatee)'s request for notification of privacy policy, disclosure, correction, addition, deletion, suspension or elimination  of personal information, stopping disclosure to a third party, we will handle your request according to the following procedure.</p>

<p>(How to contact us)<br>
For a request for disclosure, please contact us by mail to the address recorded here below with the attachment of the designated form ("Personal Information Disclosure Application for Specified Personal Information").<br>Please pay attention that any request other than that sent by mail will be rejected.</p>

<p>Mail Address<br>
1-3-1 Marunouchi Chiyoda-ku, Tokyo 100-0005<br>
TBM Co., Ltd<br>
Person in charge of Personal Information.</p>

<p>Disclosure Request Format<br>
For disclosure request for personal information which can be identified yourself, please download the format of <!--<span class="bd01"><a href="./pdf/disclose_privacy.pdf" target="_blank">-->"Personal Information Disclosure Application"(PDF format)<!--</a></span>-->, fill in the required items and mail to us with attachment of related required documents.</p>

<p class="mb0">Our Contact Information
<ol>
<li>1. Company Name<br>
TBM Co., Ltd</li>
<li>2. Person in Charge<br>
Yoshihiro Tsuchiya（Development department)<br>
TEL +81 03-6212-7270　FAX +81 03-6212-7271</li>
<li>3. Purpose of Disclosure<br>
Business partner information: For handling with business partner information, business negotiation and other consulting or inquiries.<br>
Staff information: For management of company staff's personal affairs, performance, health control, security control<br>
Job applicants' personal information: For management of recruiting and contacting job applicants</li>
<li>4. Contact information for Claim or inquiry<br>
1-3-1 Marunouchi Chiyoda-ku, Tokyo 100-0005<br>
TBM Co., Ltd<br>
Person in charge of Personal Information Protection</li>
</ol></p>
</div>

</section><!-- /#body -->

<?php
get_footer('en');
