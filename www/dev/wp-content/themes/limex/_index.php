<?php
/**
 * Template Name: top
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */


get_header(); ?>
<?php
//ALL
//2016/06/28 メディア(id=2)以外を表示
$all_args = array(
//	'post_status'=>'publish',
	'post_type' => 'news',
	'category__not_in' => array(2),
	'posts_per_page' => 6,
);

//press release

$press_args = array(
//	'post_status'=>'publish',
	'category_name' => 'press_release',
	'post_type' => 'news',
	'posts_per_page' => 6,
);
?>
<script type="text/javascript">
$(function(){
	$('.kv_link.n01').on('click',function(){
		$(location).attr("href", "/action/");
	});
	$('.kv_link.n02').on('click',function(){
		$(location).attr("href", "https://tb-m.com/order/explain.php");
	});
	$('.kv_link.n03').on('click',function(){
		$(location).attr("href", "/about/#about_b2i4");
	});
	$('.limited_link_left').on('click',function(){
		$(location).attr("href", "https://tb-m.com/order/explain.php");
	});
	$('.limited_link_right').on('click',function(){
		window.open('http://recruit.tb-m.com/');
	});
});
</script>

<style type="text/css">
	.slide.smp img {
		opacity: 1 !important;
		width: 100%;
	}
	.kv_link{
		cursor:pointer;
	}
</style>
<section id="top">
	<div class="fv">
		<div class="fv_main">
			<div class="fv_img kv_slider">
				<div class="first slide" data-current="1">
					<div class="base-scale-img">
						<img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_main_01.jpg">
					</div>
					<div class="slide-image-overlay">
						<div class="slide-image-top clearfix">
							<div class="slide-image-left-link limited_link_left">
								<img src="<?php echo get_template_directory_uri(); ?>/img/top/slide-dammy.png">
							</div>
							<div class="slide-image-right-link limited_link_right">
								<img src="<?php echo get_template_directory_uri(); ?>/img/top/slide-dammy.png">
							</div>
						</div>
						<div class="slide-image-bottom">
							<img src="<?php echo get_template_directory_uri(); ?>/img/top/slide-dammy-bottom.png">
						</div>
					</div>
					<!-- <img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_main_01.png"> -->
				<!--20160725<div class="fv_logo">
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_stone.png">
							<img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_logo.png">
						</div>
						<img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_c1.png" alt="石灰石からつくる革命的新素材">
					</div>
					<img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_img1.jpg"> -->
				</div>
				<div class="slide" data-current="0"><img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_img2.jpg"></div>
				<div class="slide" data-current="0"><img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_img3.jpg"></div>
				<div class="slide kv_link n03" data-current="0"><img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_img6.jpg"></div>
				<div class="slide kv_link n02" data-current="0"><img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_img5.jpg"></div>
				<div class="slide kv_link n01" data-current="0"><img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_img4.jpg"></div>
<?php if(wp_is_mobile()):?>
				<div class="slide smp" data-current="0"><a href="https://www.youtube.com/embed/ZQ0J8Ur9T-E?autoplay=1&rel=0&showinfo=0&autohide=1&loop=1&playlist=DJJ9zwqDL0c" class="youtube"><img src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_movie_img.jpg"></a></div>
<?php else:?>
				<div class="slide" data-current="0" id="youtube-movie-content">
					<a href="https://www.youtube.com/embed/ZQ0J8Ur9T-E?autoplay=1&rel=0&showinfo=0&autohide=1&loop=1&playlist=DJJ9zwqDL0c" class="youtube overlay_link"></a>
				    <a id="bgndVideo" class="player" data-property="{
				    videoURL:'http://youtu.be/ZQ0J8Ur9T-E',
				    containment:'.video_wrap',
				    ratio:'4/3',
				    optimizeDisplay:false,
				    autoPlay:true,
				    stopMovieOnBlur:false,
				    showControls:false,
				    mute:true,
				    startAt:0,
				    opacity:1
				    }">background movie</a>
				    <div class="section-top-main video_wrap"></div>
				</div>
<?php endif ;?>
			</div>
			<img class="btn_open btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_btn_story.png" alt="LIMEX STORY">
		</div>
		<div class="fv_story">
			<div class="story_bk"></div>
			<div class="story_img">
				<img class="stone" src="<?php echo get_template_directory_uri(); ?>/img/top/top_story_img.png" alt="地球は水の星だ。地球は生命の星だ。でも、それだけじゃない。地球は石の星でもあるのだ。地球に、ほぼ無限にあるもの、それは石。石から紙をつくれば、紙はもっと丈夫で便利になる。紙だけじゃなく、様々な素材をつくれば、化石燃料や、木や水に頼ることのない何度でも循環可能な生活が、世界中に広がってゆく。かつて石は、狩りや調理の道具だった。戦うための武器であり、伝えるためのメディアだった。長いあいだ、人と石は親密な関係だった。いまこそ石を、もっと見直そう。石が持つ無限のチカラに、賭けてみよう。石。それも、地球上のどこにでも豊富にある石灰石＝LIMEから、まだどこにもない可能性＝Xをつくる。それが、LIMEX / ライメックスの使命。そのシンボルロゴには、Xのカタチに割れた「石」から、可能性に満ちた世界を生み出す「意思」が込められている。ライメックス。次の未来は、石からはじまる">
				<img class="story_txt" src="<?php echo get_template_directory_uri(); ?>/img/top/top_story_text.png">
				<img class="mask_left" src="<?php echo get_template_directory_uri(); ?>/img/top/top_story_mask_l.png">
				<img class="mask_right" src="<?php echo get_template_directory_uri(); ?>/img/top/top_story_mask_r.png">
			</div>
			<div class="story_control">
				<img class="btn_prev btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_story_btn_prev.png">
				<img class="btn_next btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_story_btn_next.png">
				<img class="btn_close btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_story_btn_close.png">
			</div>
		</div>
	</div>
	<div class="fv_control cf01" style="opacity: 1; display: block;">
		<img class="btn_slide" data-id="1" src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_btn_slide.png" style="opacity: 1;">
		<img class="btn_slide" data-id="2" src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_btn_slide.png" style="opacity: 0.5;">
		<img class="btn_slide" data-id="3" src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_btn_slide.png" style="opacity: 0.5;">
		<img class="btn_slide" data-id="4" src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_btn_slide.png" style="opacity: 0.5;">
		<img class="btn_slide" data-id="5" src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_btn_slide.png" style="opacity: 0.5;">
		<img class="btn_slide" data-id="6" src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_btn_slide.png" style="opacity: 0.5;">
		<img class="btn_slide" data-id="7" src="<?php echo get_template_directory_uri(); ?>/img/top/top_fv_btn_slide.png" style="opacity: 0.5;">
	</div>
<!--
    <div class="limex_action_bnr btn"><a href="/action/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/top/limex_action_bnr.png" alt="LIMEX ACTION 変幻自在に生まれ変われるライメックスは、様々な製品になって、世界中の生活を変えてゆく。"></a></div>
-->

	<div class="link" style="height:210px; margin-bottom:0;">
		<a href="https://tb-m.com/order/explain.php"><img class="btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_bnr1.jpg" alt="" ></a>
		<a href="/action/"><img class="btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_bnr2.jpg" alt="" ></a>
	</div>
	<div style="width:940px; margin:40px auto 75px auto;"><img style="text-align:center; display:inline;" src="<?php echo get_template_directory_uri(); ?>/img/top/top_text.jpg" alt="" ></div>

	<div class="news">
		<h2 class="over" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/top/top_news_ttl.jpg);">NEWS</h2>
		<a href="https://www.facebook.com/tbm.limex" target="_blank"><img class="fb btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_news_btn_fb.png" alt="LIMEX 公式 Facebook"></a>
		<hr>
		<ul class="category">
			<li class="cate_pr">プレスリリース</li>
			<li class="cate_all cur">ALL</li>
		</ul>
		<div class="news_thumb all">
			<ul>
<?php
$the_query = new WP_Query( $all_args );
//var_dump($the_query);
// ループ
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();

$fields = get_fields(get_the_ID());

$cat = get_the_category();
$cat = $cat[0];
$cat_name = $cat->cat_name;
$category_nicename = $cat->category_nicename;
?>
				<li>
					<a href="/news/#<?php echo get_the_ID();?>">
						<div class="img"><img src="<?php echo $fields['top_list_image']? $fields['top_list_image'] : '/wp-content/themes/limex/thm/news_i1.jpg';?>" alt=""></div>
						<div class="thumb">
							<p class="cate top_<?php echo mb_strtolower($category_nicename) ;?>"><?php echo $cat_name; ?></p>
							<p class="txt"><?php the_title(); ?></p>
							<p class="date"><?php the_time('Y/m/d'); ?></p>
						</div>
					</a>
				</li>
<?php
endwhile;
endif;
?>
			</ul>
		</div>

		<div class="news_thumb press-release">
			<ul>
<?php
$the_query = new WP_Query( $press_args );
// ループ
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();

$fields = get_fields(get_the_ID());
$cat = get_the_category();
$cat = $cat[0];
$cat_name = $cat->cat_name;
$category_nicename = $cat->category_nicename;
?>
				<li>
					<a href="/news/#<?php echo get_the_ID();?>">
						<div class="img"><img src="<?php echo $fields['top_list_image']? $fields['top_list_image'] : '/wp-content/themes/limex/thm/news_i1.jpg';?>" alt=""></div>
						<div class="thumb">
							<p class="cate top_<?php echo mb_strtolower($category_nicename) ;?>"><?php echo $cat_name; ?></p>
							<p class="txt"><?php the_title(); ?></p>
							<p class="date"><?php the_time('Y/m/d'); ?></p>
						</div>
					</a>
				</li>
<?php
endwhile;
endif;
?>
			</ul>
		</div>

		<a href="./news/"><img class="more btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_news_btn_more.png" alt="MORE"></a>
	</div>
	<div class="link">
		<a href="./about/"><img class="btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_about_btn_link.png" alt="ABOUT - LIMEX - 地球上にほぼ無限にある石灰石（＝LIME）から生まれた未来を救う新素材。その名はライメックス。"></a>
		<a href="./mission/"><img class="btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_mission_btn_link.png" alt="MISSION - LIMEXの使命 - どこにでもある石灰石からつくる素材が地球上に増えてゆく。それだけで、環境が、社会が、未来が変わる。"></a>
		<a href="./factory/"><img class="btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_factory_btn_link.png" alt="FACTORY - 株式会社TBMのLIMEXは、石灰石（炭酸カルシウム）を主原料に、紙やプラスチック商品の代替となり、エコロジーとエコノミーを実現する新素材です。新素材を生み出す工場をご紹介します。"></a>
		<a href="./company/"><img class="btn" src="<?php echo get_template_directory_uri(); ?>/img/top/top_company_btn_link.png" alt="COMPANY - 紙やプラスチック商品の代替となり、エコロジーとエコノミーを実現する新素材LIMEXをつくる株式会社TBMの会社情報です。"></a>
	</div>


	<div class="media">
		<h2 class="over" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/top/top_media_ttl.jpg);">MEDIA</h2>
		<hr>
		<div class="news_thumb all">
			<ul>
				<li>
					<a href="/news/#661">
						<div class="img"><img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/09/160901_nhkworld_l.jpg" alt=""></div>
						<div class="thumb">
							<p class="cate top_media">MEDIA</p>
							<p class="txt">NHK WORLD「グレートギア」でLIMEXが紹介</p>
							<p class="date">2016/09/01</p>
						</div>
					</a>
				</li>

				<li>
					<a href="/news/#643">
						<div class="img"><img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/08/news-i-160825-01-1.jpg" alt=""></div>
						<div class="thumb">
							<p class="cate top_media">MEDIA</p>
							<p class="txt">「Forbes」に掲載：<br>「水も木も使わず石から紙をつくる」スタートアップ、世界へ</p>
							<p class="date">2016/08/25</p>
						</div>
					</a>
				</li>

				<li>
					<a href="/news/#603">
						<div class="img"><img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/08/news-i-160804-01-1.jpg" alt=""></div>
						<div class="thumb">
							<p class="cate top_media">MEDIA</p>
							<p class="txt">日経スペシャル「カンブリア宮殿」<br> 10周年500回記念番組で特集</p>
							<p class="date">2016/08/04</p>
						</div>
					</a>
				</li>

				<li>
					<a href="/news/#562">
						<div class="img"><img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/07/news-i-160715-01-2.jpg" alt=""></div>
						<div class="thumb">
							<p class="cate top_media">MEDIA</p>
							<p class="txt">日本政府「We Are Tomodachi Summer 2016」に掲載</p>
							<p class="date">2016/07/15</p>
						</div>
					</a>
				</li>

				<li>
					<a href="/news/page/2/#431">
						<div class="img"><img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/04/news-i-160428-01-1.jpg" alt=""></div>
						<div class="thumb">
							<p class="cate top_media">MEDIA</p>
							<p class="txt">「アメリカの公共放送PBSテレビ」でLIMEXが紹介 〜This Is America & The World〜</p>
							<p class="date">2016/05/05</p>
						</div>
					</a>
				</li>

				<li>
					<a href="/news/page/3/#378">
						<div class="img"><img src="//dxv0bwh6i6vy7h.cdn.jp.idcfcloud.com/wp-content/uploads/2016/03/image160317-01t.jpg" alt=""></div>
						<div class="thumb">
							<p class="cate top_media">MEDIA</p>
							<p class="txt">「日本経済新聞」に掲載： TBM社長山﨑敦義氏、新素材生産、若者を刺激</p>
							<p class="date">2016/03/17</p>
						</div>
					</a>
				</li>

			</ul>
		</div>
	</div>

</section>
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mb.YTPlayer.min.css" type="text/css" media="all">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/top/jquery.mb.YTPlayer.min.js"></script>
<!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/top/jquery.bxslider.js"></script>-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/colorbox.css" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.colorbox-min.js"></script>
<script>
    $(document).ready(function() {
        $(".youtube").colorbox({
            iframe:true,
            innerWidth:1080,
            innerHeight:608
        });
    });
</script>
<?php
get_footer();
