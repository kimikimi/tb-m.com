<?php
/**
 * Template Name: en-factory-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>

<section id="body">
	<div class="pageTtl">
		<h2>LIMEX FACTORY</h2>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/factory/" class="cur">SHIROISHIZAO</a></li>
				<li><a href="/en/factory/tagajo/">TAGAJO</a></li>
			</ul>
		</div>
	</div>
	<h3>Shiroishi Zao<span> ( Japan first plant )</span></h3>
	<div class="photo">&nbsp;</div>
	<div class="wrapper">
		<section class="about">
			<h4>LIMEX FACTORY</h4>
			<p>The first Pilot Plant is located in Shiroishi, Miyagi. Not only being a production base, it functions as a research development and human resource development base in order to found a strong base to expand the business.</p>
			<table>
				<tr>
					<th>Site Area</th>
					<td>10,510㎡</td>
					<th>Buildings Area</th>
					<td>2,884㎡</td>
				</tr>
				<tr>
					<th>Production amount</th>
					<td>6000ton yearly  (500 ton monthly)</td>
					<th>Address</th>
					<td><span style="line-height:1.5;">Otakasawamisawamaewa 55,Shiroishi, Miyagi 989-0213</span></td>
				</tr>
			</table>
		</section>
	</div>
	<section id="adoption">
		<div class="wrapper">
			<h5>Selected for the Innovation Center Establishment Assistance Program: <br>"Subsidy for Advanced Technology Demonstration and Evaluation Facility Development" <br>on February 6th, 2013</h5>
			<div class="img"><img src="<?php echo get_template_directory_uri(); ?>/img/en/factory/image02.jpg" height="220" width="293" alt=""></div>
			<div class="txt">
				<p>Division in Charge: Ministry of Economy, Trade and Industry <br>Industrial Science and Technology Policy and Environment Bureau<br>Research and Development Division<br>
				Release Date: February 6th, 2013</p>
				<p>Overview of the subsidy program:<br>
The programs aim to promote the commercialization of new technology as well as to accelerate recovery from the Great East Japan Earthquake, and to realize a “new growth” of Japan, through subsidizing the cost of developing facilities to demonstrate and evaluate innovative technologies developed in Japan to date and enhancing investment for research and development.<br>
Through this program we have received a subsidy covering two thirds of the plant and facilities subjected.<br> ( From METI homepage) </p>
			</div>
		</div>
	</section>

</section><!-- /#body -->

<?php
get_footer('en');
