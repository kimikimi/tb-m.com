<?php
/**
 * Template Name: factory-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$class_name = 'factory';
get_header(); ?>

<section id="body">
	<div class="pageTtl">
		<div class="wrapper">
			<h2>LIMEX FACTORY</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/factory/" class="cur">白石蔵王</a></li>
				<li><a href="/factory/tagajo/">多賀城</a></li>
			</ul>
		</div>
	</div>

	<h3>白石蔵王<span>(国内第一プラント)</span></h3>
	
	<div class="photo">&nbsp;</div>
	<div class="wrapper">
		<section class="about">
			<h4>LIMEXの工場</h4>
			<p>宮城県白石市の国内第一プラントにおいては、製造拠点とともに、研究開発と人材育成の拠点と位置付け、<br>事業拡大に向けた基盤を確立してまいります。</p>
			<table>
				<tr>
					<th>敷地面積</th>
					<td>10,510㎡</td>
					<th>建物床面積</th>
					<td>2,884㎡</td>
				</tr>
				<tr>
					<th>生産量</th>
					<td>年6,000t （月500t）</td>
					<th>所在地</th>
					<td style="line-height:1.5;">〒989-0213<br />宮城県白石市大鷹沢三沢字前輪55</td>
				</tr>
			</table>
		</section>
	</div>
	<section id="adoption">
		<div class="wrapper">
			<h5>イノベーション拠点立地推進事業「先端技術実証・評価設備整備費等補助金」に<br>2013年2月6日に採択を頂きました。</h5>
			<div class="img"><img src="<?php echo get_template_directory_uri(); ?>/img/factory/image02.jpg" height="220" width="293" alt=""></div>
			<div class="txt">
				<p>担当 : 経済産業政策局 地域経済産業グループ 産業施設課　公表日:平成27年11月27日（金）</p>
				<p>公募概要 : 本補助金は、東日本大震災で特に大きな被害を受けた津波浸水地域（青森県、岩手県、宮城県、福島県、茨城県）及び原子力災害により甚大な被害を受けた避難指示区域（避難指示が解除された地域）等の復興を加速するため、これらの地域において工場等を新増設する企業に対し、その経費の一部を補助することにより、企業の立地を円滑に進め、雇用を創出することを目的とします。</p>
			</div>
		</div>
	</section>

</section><!-- /#body -->

<?php
get_footer();
