<?php
/**
* Template Name: factory-tagajo
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
$class_name = 'factory';
get_header(); ?>

<section id="body">
<div class="pageTtl">
	<div class="wrapper">
	<h2>LIMEX FACTORY</h2>
	</div>
</div>
<div class="sub-navi">
	<div class="inner">
		<ul>
			<li><a href="/factory/">白石蔵王</a></li>
			<li><a href="/factory/tagajo/" class="cur">多賀城</a></li>
		</ul>
	</div>
</div>

<h3>多賀城<span>(国内第二量産プラント)</span></h3>
<div class="photo">&nbsp;</div>
<div class="wrapper">
	<section class="about">
		<h4>LIMEXの工場</h4>
		<p>宮城県多賀城市の国内第二量産プラントにおいては、この量産工場をモデル工場とし、事業拡大に向けた基盤を確立するとともに、水資源の不足している地域を中心として世界中へ技術輸出をすすめます。</p>
		<table>
			<tr>
				<th>敷地面積</th>
				<td>27,500m²</td>
				<th>建物床面積</th>
				<td>約13,000m²</td>
			</tr>
			<tr>
				<th>生産量</th>
				<td>年30,000t （月2,500t）</td>
				<th>所在地</th>
				<td>宮城県多賀城市さんみらい多賀城・復興団地(一本柳工業団地)</td>
			</tr>
		</table>
	</section>
</div>
<section id="adoption">
	<div class="wrapper">
		<h5>株式会社TBMは12月17日（木）に宮城県多賀城市役所にて<br>LIMEX工場第二号立地協定締結式を開催致しましたことをお知らせさせていただきます。</h5>
		<div class="img"><img src="<?php echo get_template_directory_uri(); ?>/img/factory/img-factory-tagajo-02.png" height="220" width="293" alt=""></div>
		<div class="txt">
			<p>本年11月27日、経済産業省の「津波・原子力災害被災地域雇用創出企業立地補助金（製造業等立地支援事業）」に採択され、今後については多賀城市に量産工場を建設する予定です。本工場は年産30,000トンになる見込みで、稼働時までに新規地元雇用を約60人とし、数年内に約80人以上を雇用する計画です。</p>
			<p>建設に先立ち、多賀城市と立地協定を行い、同市の復興牽引拠点と位置付けられているさんみらい多賀城・復興
団地(一本柳工業団地)内に、敷地面積約27,500m²の用地を確保する予定です。将来的にはこの量産工場をモデル工場とし、水資源の不足している地域を中心として世界中へ技術輸出をすすめます。日本のイノベーション技術で世界に雇用と産業を創出し、同時に世界の水・石油資源問題に貢献していきます。</p>
		</div>
	</div>
</section>

</section><!-- /#body -->

<?php
get_footer();
