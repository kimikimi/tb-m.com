<?php
/**
 * Template Name: archive-news
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$class_name = 'news';
get_header(); ?>
<?php
$paged = get_query_var('paged');

$args = array( 
//	'post_status'=>'publish',
	'paged' => $paged,
	'post_type' => 'news',
	'posts_per_page' => 20, 
);
$category = isset($_GET['cat'])? $_GET['cat'] : '';

if(isset($_GET['cat'])){
	$args['category_name']  = $_GET['cat'];
}
if(isset($_GET['y'])){
	$args['year']  = $_GET['y'];
}
$list_args = array('category_name' =>   $_GET['cat']);
?>
<?php

$archives = get_archives_by_fiscal_year($list_args,'news');

?>
<style type="text/css">
.anchor_link{
	margin-top:-95px;
 	padding-top:95px;
}
</style>
<section id="body">
	<div class="pageTtl">
		<h2>COMPANY</h2>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/news/"<?php echo (!isset($_GET['cat'])) ? ' class="cur"' : ''?>>ALL</a></li>
				<li><a href="/news/?cat=press_release"<?php echo (isset($_GET['cat']) && $_GET['cat'] == 'press_release') ? ' class="cur"' : ' class="sub-press_release" '?>>プレスリリース</a></li>
				<li><a href="/news/?cat=topics"<?php echo (isset($_GET['cat']) && $_GET['cat'] == 'topics') ? ' class="cur"' : ''?>>お知らせ</a></li>
				<li><a href="/news/?cat=media"<?php echo (isset($_GET['cat']) && $_GET['cat'] == 'media') ? ' class="cur"' : ''?>>メディア掲載</a></li>
			</ul>
		</div>
	</div>

<?php if(isset($_GET['cat']) && get_category_titile($_GET['cat'],'jp')): ?>
	<h3><?php echo get_category_titile($_GET['cat'],'jp')?></h3>
<?php endif;?>
	<div class="wrapper">
		<ul class="archive<?php echo $_GET['cat'] == '' ? ' news_all' : '';?>">
<?php
foreach($archives as $archive):
?>
			<li><a href="<?php echo home_url() ?>/news/?y=<?php echo esc_html($archive->year) ?><?php echo isset($_GET['cat'])? '&cat='. $_GET['cat'] : '' ;?>"<?php echo ($_GET['y'] == $archive->year) ? ' class="cur"' : '';?>><?php echo esc_html($archive->year) ?><span></span></a></li>
<?php
endforeach;
?>

		</ul>

		<div id="data1" class="data">
<?php
$the_query = new WP_Query( $args );
//var_dump($the_query);
// ループ
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();

$fields = get_fields(get_the_ID());
$cat = get_the_category();
$cat = $cat[0];
$cat_name = $cat->cat_name;
$category_nicename = $cat->category_nicename;
?>
        <article<?php echo $fields['image']? ' class="col-2 list_' . mb_strtolower($category_nicename) . ' anchor_link"' : ' class="list_' . mb_strtolower($category_nicename) . ' anchor_link"';?> id="<?php echo get_the_ID();?>">
        	<a name="news_160223-01"></a>
			<p class="date"><?php the_time('Y/m/d'); ?></p>
			<p class="title"><?php the_title(); ?></p>
<?php if($fields['image']):?>
			<div class="col">
				<div class="img">
					<img src="<?php echo $fields['image']; ?>" alt="">
				</div>
				<div class="txt">
					<?php echo $fields['news_contents']; ?>
				</div>

			</div>
<?php if($fields['news_movie']):?>
				<div class="movie">
	<?php foreach ($fields['news_movie'] as $key => $value):?>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $value['youtube_id'] ;?>" frameborder="0" allowfullscreen></iframe>
	<?php endforeach;?>
				</div>
<?php endif;?>

<?php else:?>
			<div class="txt"><?php echo $fields['news_contents']; ?></div>
<?php if($fields['news_movie']):?>
			<div class="movie">
	<?php foreach ($fields['news_movie'] as $key => $value):?>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $value['youtube_id'] ;?>" frameborder="0" allowfullscreen></iframe>
	<?php endforeach;?>
			</div>
<?php endif;?>
<?php endif ;?>

		</article>
<?php
endwhile;
endif;
?>
		</div><!-- /#data1.data -->

			<?php if(function_exists('wp_pagenavi')) {
				wp_pagenavi(array('query'=>$the_query));
			} ?>
	</div>


</section><!-- /#body -->

<?php
get_footer();
