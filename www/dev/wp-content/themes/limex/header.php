<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$directory_name = get_directory_name();
$heder_js = get_head_js($directory_name);
$class_name = get_body_classname();

?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html>
<!--<![endif]-->
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<!--[if lt IE 9]>
		<script type="text/javascript" src="../js/ie/html5shiv.js"></script>
		<![endif]-->
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<meta name="keywords" content="LIMEX,石灰石,リサイクル,エコノミー,エコロジー,新素材,ストーンペーパー,プラスチック,株式会社TBM">
		<meta name="description" content="株式会社TBMのLIMEXは、石灰石（炭酸カルシウム）を主原料に、紙やプラスチック商品の代替となり、エコロジーとエコノミーを実現する新素材です。">

		<meta property="og:title" content="次世代素材LIMEX 株式会社TBM" />
		<meta property="og:url" content="http://tb-m.com/" />
		<meta property="og:description" content="【株式会社TBM】LIMEXは、石灰石（炭酸カルシウム）を主原料に、紙やプラスチック商品の代替となり、エコロジーとエコノミーを実現する新素材です。" />
<?php if(preg_match('/order/', $_SERVER["REQUEST_URI"])):?>
		<meta property="og:image" content="http://tb-m.com/img/meisi_og.jpg" />
<?php else :?>
		<meta property="og:image" content="http://tb-m.com/img/ogp.jpg" />
<?php endif;?>

		<meta name="viewport" content="width=1280px, maximum-scale=1.0, user-scalable=yes">

		<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css" />


<?php if(file_exists( TEMPLATEPATH. '/css/style_'. $directory_name . '.css')):?>
		<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style_<?php echo $directory_name ;?>.css" />
<?php endif;?>
<?php if(is_current('/',$class_name)):?>
		<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style_top.css" />
<?php endif;?>

		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scrollstop.js" charset="utf-8"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/common02.js" charset="utf-8"></script>
<?php echo ($heder_js) ? $heder_js : '' ;?>
		<LINK REL="SHORTCUT ICON" HREF="<?php echo get_template_directory_uri(); ?>/img/common/favicon.ico">
<?php if(is_current('access',$class_name)):?>
		<script src="https://maps.google.com/maps/api/js?sensor=false&v=3.21"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/map-design.js"></script>
<?php endif;?>

	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js" charset="UTF-8"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/ie/html5shiv.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/ie/jquery.backgroundSize.js"></script>
		<script>
			$(function() {
				$(".header-facebook a").css({backgroundSize: "cover"});
			});
		</script>
	<![endif]-->
<?php if(is_current('story',$class_name)):?>
	<!--[if lt IE 9]>
	<script>
		$(function() {
			$(".story .main-visual, sec02 .left, sec04").css({backgroundSize: "cover"});
		});
	</script>

	<![endif]-->
<?php endif;?>

</head>
<body<?php echo $class_name ?' class="'.$class_name . '"' : '' ;?>>
	<div id="lang01">
		<div id="lang02">
			<a href="<?php echo $_SERVER['REQUEST_URI'] ;?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/lang_ja_on.png" alt="Japanese"></a>
<?php if(!no_en_page()):?>
			<a href="/en/<?php echo $_SERVER['REQUEST_URI'] ;?>">
<?php elseif($_SERVER['REQUEST_URI']== '/order/explain.php'):?>
			<a href="/order/explain.php"> <!-- en stop -->
<?php else: ?>
			<a href="/en/">
<?php endif;?>
				<img src="<?php echo get_template_directory_uri(); ?>/img/common/lang_en_off.png" class="fade01" alt="English">
<?php if(!no_en_page()):?>
			</a>
<?php endif ;?>
		</div>
		<!--<div id="ext01-01"><a href="http://limex-store.jp" target="_blank"><img src="/img/nav_header_ext_store.gif" class="fade01" alt="ONLINE STORE"></a></div>-->

		<div id="ext01-02"><a href="/action/"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav_header_ext_action.gif" class="fade01" alt="TBM ACTION"></a></div>
		<div id="ext01-03">
			<a href="http://recruit.tb-m.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav_header_ext_recruit.gif" class="fade01" alt="RECRUIT"></a>
		</div>
		<div id="ext01-01">
			<a href="/order/explain.php"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav_header_ext_order.png" class="fade01" alt="名刺オーダー"></a>
		</div>
		<div id="ext01-04">
			<a href="/form/contact_lime-x/contact/"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav_header_ext_contact.png" class="fade01" alt="お問い合わせ"></a>
		</div>
		<p class="header-facebook">
			<a href="https://www.facebook.com/tbm.limex" target="_blank" class="icon02">facebook</a>
		</p>
	</div>

	<header>
		<div class="wrapper">
			<h1 class="logo"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/common/logo_header.png" height="44" width="192" alt="LIMEX"></a></h1>
			<div id="gnav">
				<ul>
					<li class="nav1">
						<a data-id="about" href="/about/"<?php echo is_current('about',$directory_name) ? ' class="cur"':'';?>>ABOUT</a>
						<div class="g-sub-navi">
							<ul>
								<li><a href="/about/">LIMEXとは</a></li>
								<li><a href="/about/paper/">LIMEXの紙</a></li>
								<li><a href="/about/plastic/">LIMEXのプラスチック</a></li>
								<li><a href="/about/future/">LIMEXの可能性</a></li>
							</ul>
						</div>
					</li>
					<li class="nav2">
						<a data-id="mission" href="/mission/"<?php echo is_current('mission',$directory_name) ? ' class="cur"':'';?>>MISSION</a>
						<div class="g-sub-navi">
							<ul>
								<li><a href="/mission/">MISSION STORY</a></li>
								<li><a href="/mission/vision/">VISION STORY</a></li>
							</ul>
						</div>
					</li>
					<li class="nav3">
						<a data-id="factory" href="/factory/"<?php echo is_current('factory',$directory_name) ? ' class="cur"':'';?>>FACTORY</a>
						<div class="g-sub-navi">
							<ul>
								<li><a href="/factory/">白石蔵王</a></li>
								<li><a href="/factory/tagajo/">多賀城</a></li>
							</ul>
						</div>
					</li>
					<li class="nav4">
						<a data-id="company" href="/company/"<?php echo is_current('company',$directory_name) ? ' class="cur"':'';?>>COMPANY</a>
						<div class="g-sub-navi">
							<ul>
								<li><a href="/company/">会社概要</a></li>
								<li><a href="/company/profile/">役員</a></li>
								<li><a href="/company/identity/">企業理念体系</a></li>
								<li><a href="/company/history/">沿革</a></li>
								<li><a href="/company/message/">創設者メッセージ</a></li>
								<li><a href="/company/story/">創設ストーリー</a></li>
								<li><a href="/company/access/">アクセス</a></li>
								<li><a href="/company/award/">受賞歴</a></li>
							</ul>
						</div>
					</li>
					<li class="nav5">
						<a data-id="news" href="/news/"<?php echo is_current('news',$directory_name) ? ' class="cur"':'';?>>NEWS</a>
						<div class="g-sub-navi">
							<ul>
								<li><a href="/news/?cat=press_release">プレスリリース</a></li>
								<li><a href="/news/?cat=topics">お知らせ</a></li>
								<li><a href="/news/?cat=media">メディア掲載</a></li>
							</ul>
						</div>
					</li>

				</ul>
			</div>
		</div>
	</header>
