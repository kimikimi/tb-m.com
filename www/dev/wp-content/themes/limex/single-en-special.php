    <?php
/**
 * Template Name: en/special
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$fields = get_fields($post->ID);

?><!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8" />
<title>LIMEX ACTIONに架ける想い / 山﨑 敦義 | SPECIAL STORY | 次世代素材LIMEX 株式会社TBM</title>
<meta name="description" content="日本の技術だからできる新素材ライメックス。LIMEXに携わる人々の想いを伝えます。">
<meta name="Keywords" content="LIMEX,石灰石,リサイクル,エコノミー,エコロジー,新素材,ストーンペーパー,プラスチック,株式会社TBM">
<link rel="SHORTCUT ICON" href="/img/favicon.ico" />

<meta property="og:title" content="次世代素材LIMEX 株式会社TBM" />
<meta property="og:url" content="http://tb-m.com/" />
<meta property="og:description" content="【株式会社TBM】LIMEXは、石灰石（炭酸カルシウム）を主原料に、紙やプラスチック商品の代替となり、エコロジーとエコノミーを実現する新素材です。" />
<meta property="og:image" content="http://tb-m.com/img/ogp.jpg" />


<meta name="viewport" content="width=1280px, maximum-scale=1.0, user-scalable=yes">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/action/assets/css/style.css" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/action/assets/js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scrollstop.js" charset="utf-8"></script>
<script>
$(function(){
	var topBtn = $('#order-menu');
	var deleted = false;
	topBtn.hide();
	
	/* scrollstop */
  $(window).on("scrollstop",function(){
		if ( deleted == false ){
			topBtn.fadeIn();
		}
  });
	
	/* */
	$(window).scroll(function() {
			topBtn.fadeOut();	
  });
	
	$('.btn_delete').on('click', function() {
		deleted = true;
		topBtn.fadeOut();
	});

});
</script>
<style>
#order-menu a {

    text-decoration: none;
}
#order-menu a:hover {
    text-decoration: none;
    opacity: 0.7;
}
#order-menu.bnr {
  position: fixed;
  bottom: 30px;
  right: 0;
  z-index: 10;
  display: none;
}
#order-menu .bnr_close {
  position: absolute;
  top: 3px;
  right: 10px;
  z-index: 1000;
}
</style>
</head>
<body>
    <header>
        <!-- <div class="left"><a href="/action/"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/logo_limex.png" alt="LIMEX ACTION"></a></div> -->
        <div class="right"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font_limex_to.png" alt="LIMEXサイト"></a></div>
    </header>
    <div class="contents special">
        <div class="special_block">
            <h2 class="center"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font_special_story.png" alt="SPECIAL STORY"></h2>
            <p class="center description"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font_special_description.png" alt="LIMEXに携わる人の思いを伝えます。"></p>
            <div class="center img_large">
                <img src="<?php echo $fields['main_visual'] ;?>" alt="CSRの取り組みへの思い　代表取締役社長山崎敦義">
            </div>
            <div class="paragrah_block">
<?php foreach ($fields['contents'] as $key => $value):?>
                <p style="margin-bottom:20px">
                <?php echo $value['contents_text'] ;?>
                </p>
                <div class='img'><img src="<?php echo $value['contents_image'] ;?>" alt="<?php echo $value['contents_alt'] ;?>"></div>
<?php endforeach;?>

            </div>
        </div>
    </div>
<!--
    <div class="bnr" id="order-menu">
      <div class="bnr_img">
        <a href="/order/explain.php"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/bnr_meisi.png" alt=""></a>
      </div>
      <div class="bnr_close btn_delete">
        <img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/bnr_meisi_close.png" alt="" width="20px" height="18px">
      </div> 
    </div>
-->
    <footer>
      <div class="footArea">
        <ul>
            <li><a target="_blank" href="https://twitter.com/intent/tweet?text=%E6%AC%A1%E4%B8%96%E4%BB%A3%E7%B4%A0%E6%9D%90LIMEX%20%E6%A0%AA%E5%BC%8F%E4%BC%9A%E7%A4%BETBM&url=http%3A%2F%2Ftb-m.com%2F&original_referer=http%3A%2F%2Fwww.tb-m.com%2F"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_tw.png" alt="Twitter"></a></li>
            <li><a target="_blank" href="https://www.facebook.com/tbm.limex"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_fb.png" alt="Facebook"></a></li>
            <li><a target="_blank" href="https://www.youtube.com/channel/UCIvfa-J0VGeLl71Aeg6sWRg"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/icon_yt.png" alt="youtube"></a></li>
        </ul>
      </div>
      <p class="copy"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/font_copy.png" alt="Copyright TBM Co.,LTD All Rights Reserved"></p>
    </footer>
		<!-- アクセス解析 -->
		<script type="text/javascript">
			
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-16758707-1']);
			_gaq.push(['_trackPageview']);
			
			(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
			
		</script>
		<!-- // アクセス解析 -->
</body>
</html>