<?php
/**
 * Template Name: mission-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<section id="mission">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>MISSION</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/mission/" class="cur">MISSION STORY</a></li>
				<li><a href="/mission/vision/">VISION STORY</a></li>
			</ul>
		</div>
	</div>
		<h3>LIMEXのMISSION STORY</h3>

			<div class="fv">
				<p class="ts32 over" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/mission/mission_top2.png);">日本の技術で、この星をいいほうへ変えてゆく。</p>
				<p class="ts16">日本の技術だからできる新素材ライメックス。<br>紙とプラスチック。 世界中で大量に使われているこの2つが<br>ライメックスに変わることで、守れるものがいくつもある。<br>それこそが、石灰石から生まれたライメックスの使命。</p>
			</div>
			<div class="view1">
				<img class="bk" src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b1b.jpg">
				<div>
					<div class="bd">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b1t.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b11.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b12.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b13.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b14.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b15.png">
						<p class="ts30 clear">石油に頼らない未来。</p>
						<p class="ts14 clear">これまで日本の産業界は、エネルギー源の大半が石油に支えられてきました。今もなお石油のほぼ全てを海外から輸入しています。ライメックスは石油由来のプラスチックを、石灰石由来に代えていくことにより、石油に依存しない持続可能な生産モデルを推進していきます。<br></p>
					</div>
				</div>
			</div>
			<div class="view2">
				<img class="bk" src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b2b.jpg">
				<div>
					<div class="bd">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b2t.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b21.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b22.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b23.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b24.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b25.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b26.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b27.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b2i.png">
						<p class="ts30 clear">未来の水を守るために。</p>
						<p class="ts14 clear">さらなる人口増加にともなう水の使用量の増加は、世界の水不足を深刻化させるはず。普通紙は約1tつくるために、約100tもの水を利用するのに対し、ライメックスペーパーは水を使用せずにつくれます。例えば、世界の紙の5％がライメックスに置き換わるだけで、2.2億人分の年間最低生活必要水量を確保できます。世界の水資源の問題に、ライメックスなら貢献できます。<br></p>
					</div>
				</div>
			</div>
			<div class="view3">
				<img class="bk" src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b3b.jpg">
				<div>
					<div class="bd">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b3t.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b31.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b32.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b33.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b34.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b35.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b36.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b37.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b38.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b3i.png">
						<p class="ts30 clear">世界中から熱い視線。</p>
						<p class="ts14 clear">水を使わないライメックスペーパーに世界中が注目しています。特に期待しているのは、水の貴重な国々。製紙には大量の水が必要になるため、製紙工場はたいてい水の豊富な沿岸部に建設されますが、ライメックスペーパーなら例えば中東地域のように水資源が少なく、紙を輸入に頼っている国でも工場をつくれるからです。今や世界中で特許を取得しているライメックス。メイドインジャパンで地産地消型の製造・消費・リサイクルモデルを、新しいビジネスインフラとして輸出していきたいと考えています。<br></p>
					</div>
				</div>
			</div>
			<div class="view4">
				<img class="bk" src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b4b.jpg">
				<div>
					<div class="bd">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b4t.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b41.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b42.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b43.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b44.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/mission/mission_b45.png">
						<p class="ts30 clear">豊かな森林を残したい。<br></p>
						<p class="ts14 clear">私たちが住む日本においては、紙の原料の木材はほとんど輸入に頼っています。そして地球上では10分間に、おおよそ野球場と同じ広さの森林が伐採され続けていると言われています。木を使わず、石灰石を使うライメックスの利用を広げることで、貴重な森林資源の保護を推進していきます。<br></p>
					</div>
				</div>
			</div>
		</section>



<?php
get_footer();
