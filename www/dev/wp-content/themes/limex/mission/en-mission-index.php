<?php
/**
 * Template Name: en-mission-index
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header('en'); ?>

<section id="mission">
	<div class="pageTtl">
		<div class="wrapper">
		<h2>MISSION</h2>
		</div>
	</div>
	<div class="sub-navi">
		<div class="inner">
			<ul>
				<li><a href="/en/mission/" class="cur">MISSION STORY</a></li>
				<li><a href="/en/mission/vision/">VISION STORY</a></li>
			</ul>
		</div>
	</div>
	
			<div class="fv">
				<p class="ts32 over" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_top2.png);">With Japanese Technology, We will make the Planet Better</p>
				<p class="ts16">Limex a new material achieved with Japanese Technology.<br>Paper and plastics,<br>If LIMEX replaces these widely used two materials, many things will be saved. <br>The mission of LIMEX - made from limestone-</p>
			</div>
			<div class="view1">
				<img class="bk" src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b1b.jpg">
				<div>
					<div class="bd">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b1t.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b11.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b12.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b13.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b14.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b15.png">
						<p class="ts30 clear">A future not relying on petroleum</p>
						<p class="ts14 clear">The Japanese industry has been dependent on petroleum energy, <br>and still we import almost all the petroleum.<br>Replacing petroleum to limestone by using LIMEX we can contribute to a sustainable industrial model independent from petroleum.<br></p>
					</div>
				</div>
			</div>
			<div class="view2">
				<img class="bk" src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b2b.jpg">
				<div>
					<div class="bd">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b2t.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b21.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b22.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b23.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b24.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b25.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b26.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b27.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b28.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b2i.png">
						<p class="ts30 clear">Protecting our water resources</p>
						<p class="ts14 clear">Population increase will lead to increase in total water consumption deepening the water-shortage problems.<br>Making 1 ton of paper requires 100 tons of water, 1 ton of LIMEX is made without using any water.<br>If 5% of the current paper is replaced by LIMEX, we will be able to cover 220 million people's minimum water quantity required for a living.<br>A change we can make to save the water resouces by using LIMEX.<br></p>
					</div>
				</div>
			</div>
			<div class="view3">
				<img class="bk" src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b3b.jpg">
				<div>
					<div class="bd">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b3t.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b31.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b32.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b33.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b34.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b35.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b36.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b37.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b38.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b39.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b3i.png">
						<p class="ts30 clear">Attention From the World</p>
						<p class="ts14 clear">LIMEX receiving high attention from all over the world, especially from countries facing serious water shortage.<br>Normally paper plants are built along water coasts as plenty of water is required,<br> but LIMEX plants can be built in-land in places/countries facing water shortage and currently relying on paper import like the Middle East.<br> LIMEX has filed for patent all over the world. We will export a new business infrastructure a Made in Japan-Local production,<br> Local consumption and Recycling Model by using LIMEX.<br></p>
					</div>
				</div>
			</div>
			<div class="view4">
				<img class="bk" src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b4b.jpg">
				<div>
					<div class="bd">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b4t.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b41.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b42.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b43.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/mission_b44.png">
						
						<p class="ts30 clear">Leaving Fertile Forests<br></p>
						<p class="ts14 clear">Japan heavily relies on wood import for the paper we consume. Deforestation the size of a baseball field is said to be occurring every 10 minutes in the world. By using LIMEX-paper made of limestone- we can protect our valuable forests. <br></p>
					</div>
				</div>
			</div>
		</section>

<?php
get_footer('en');
