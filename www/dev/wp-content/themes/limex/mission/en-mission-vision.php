<?php
/**
 * Template Name: en-mission-vision
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('en'); ?>

<script>
//アコーディオン
    $(function(){
        $('.vision .image').on('click', function() {
					if($(this).hasClass('open')){
						$(this).next().slideUp();
						$(this).removeClass('open');
						$('.episode',this).attr('src', $('.episode',this).attr('src').replace('_close','_more'));
					}else{
						$(this).next().slideDown();
						$(this).addClass('open');
						$('.episode',this).attr('src', $('.episode',this).attr('src').replace('_more','_close'));
					}
        });
				$('.episode01 .close').on('click', function() {
					$('.episode01 .text_area').slideUp();
					$('.episode01 .image').removeClass('open');
					$('.episode01 .episode').attr('src', $('.episode01 .episode').attr('src').replace('_close','_more'));
				});
				$('.episode02 .close').on('click', function() {
					$('.episode02 .text_area').slideUp();
					$('.episode02 .image').removeClass('open');
					$('.episode02 .episode').attr('src', $('.episode02 .episode').attr('src').replace('_close','_more'));
				});
				$('.episode03 .close').on('click', function() {
					$('.episode03 .text_area').slideUp();
					$('.episode03 .image').removeClass('open');
					$('.episode03 .episode').attr('src', $('.episode03 .episode').attr('src').replace('_close','_more'));
				});

    });
</script>

<section id="mission">
		<div class="pageTtl">
			<div class="wrapper">
			<h2>MISSION</h2>
			</div>
		</div>
		<div class="sub-navi">
			<div class="inner">
				<ul>
					<li><a href="/en/mission/">MISSION STORY</a></li>
					<li><a href="/en/mission/vision/" class="cur">VISION STORY</a></li>
				</ul>
			</div>
		</div>

			<div class="fv">
				<p class="ts32 over" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_top2.png);">How far LIMEX can fly high in the world.</p>
				<p class="ts16">LIMEX has a vision to produce from stone.<br>
          Excellent vision gave us practical visualization as a picture when you close your eyes.<br>
          Like as Martin Luther King's I Have A Dream speech,<br>
          here we open our special web-contents to tell you our such vision.  <br>
          It is the future to impress in the world.</p>
			</div>
			<div class="contents_area">
				<div class="bloc episode01">
					<div class="image"><a href="javascript:void(0);"><img class="episode" src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_image01_more.png"></a></div>
					<div class="text_area">
						<div class="clearfix">
							<p class="right"><img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_episode01_image01.png"></p>
							<p class="left">LIMEX has advantageous value-added various kinds. Using Limestone, the natural resources are inexhaustible. The environmental impact to be minus but made added value. we'll explain about the value of LIMEX step by step. The paper made from stone.  The plastic material made from stone. The product can produce indeterminate with the raw material of Limestone,  the natural resources are inexhaustible. LIMEX has infinite possibilities, as monster. How we can polish the rough stone. It will be greatly influence the future by the strategy. By the way, what is the value? How to determine the value? Everything has value. The worth of a thing is what it will bring? We don't' think so. It might be determined to interpret in one's own way.  If LIMEX is amazing, excellent, valuable, if the technique will save the world? It depends on the interpretation of each person.</p>
						</div>
						<p style="margin-top:0;">If we say that it is the paper made by stone, people think [It's stone paper]. It is not Intrinsic value as stone paper because [stone paper is just goods].  Nowadays, people cannot feel sympathy if it is just goods. It needs the condition of something being meaningful. We are thinking that It is the most important things to show clear definition for the LIMEX's ultimate value and meaningful.  If someone ask us [What is LIMEX? ].  How should we explain for it, in a word, in short.</p>
						<p>Not use any water and tree. It can great reduce oil consumption. Possible semi-permanent recycling. High water resistant and durability. No matter where you are at any condition. Every countries have an inexhaustible supply of Limestone. Possible locally-grown and locally-consumed. These positive points are very important value for us, but It is not transmitted true value by too much information by point.  And such information of point likely to be not see the possibility of LIMEX.</p>
						<p>Eastern music pick up infinite sound lurking in between the sound and the sound. We are thinking that LIMEX is same, it is sleeping  third value in between the value and value with great potential. What is the ultimate value of point to connect each value.</p>
						<p style="margin-top:100px;"><img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_episode01_image02.png"></p>
						<p style="margin-top:70px;"> The trend of developed country has changed when the concept of ecology transcends the concept of economy. [Static] from [dynamic]. It was unavoidable action for the change for the developed country who has economic activities for the main shaft. The Ecology transcends by [Concept], but real intension of [Management] take the priority for economy.  The change has come to developed country by push the [Concept] for [Management].  Live quietly. Break away from the unavoidable moment, to static from dynamic.</p>
						<p>It is not just go back to the past.  Developed country to make an epoch by cutting-edge technology, to exist together the ecology and economy naturally.  Now developed country will change from  the current days, to connect the [Concept = Ecology] and [Management =Economy] by force, to realize easily, without any difficulty. Here, the real [Coexistence] begins.</p>
 						<p>We define the value of LIMEX that it is the value which can change the definition of developed country.</p>
						<div class="close"><a href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/close_btn.png"></a></div>
					</div>
				</div>

				<div class="bloc episode02">
					<div class="image"><a href="javascript:void(0);"><img class="episode" src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_image02_more.png"></a></div>
					<div class="text_area">
						<p> [To change the definition by developed country] means [To make revolution in the world].<br>
							Revolution, it means to change the impossible originally.</p>
						<p style="margin-top:60px;"><img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_episode02_image01.png"></p>
						<p style="margin-top:60px;">Here, we will explain about our revolution. First of all, how to start a revolution?  What's Revolution, it's start to advocate new paradigm. Copernican theory (of the universe), Manned flight of the month,  Dissolution of the Soviet Union, the fall of the Berlin Wall, pro-democracy movement in China, IT revolution; information revolution, Middle East of the Jasmine Revolution. All these revolution started from the advocacy of new paradigm.  Past revolution were by scientist, nation, enterprise, but late years, it is possible to start revolution by each individual person by their small sum of acts, to produce results by their power beyond imagination.  No matter how strong the common sense and dogma, <strong>never give up to advocate new paradigm</strong>.</br>
              We believe that it is the first step of the revolution.</p>
						<p>When doing revolution, what do we need for it?  <strong>It needs big switch of the paradigm's thinking framework.</strong> By case study of Japanese after Would War II, at that time, Japan had the following three(3) issues. (quote  : Mr. Seiichiro Yonekura from [creative destruction -Innovation to create future  2011]</p>
						<p>① There is no natural resources in Japan. There is little oil in Japan and Japan depends on foreign countries for oil. <br>
							② Japan is very small island country of cultivation area, which is surrounded by sea. <br>
							③ Excess population to reach 100 million people in a small land area.
						</p>
						<p><img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_episode02_image02.png"></p>
						<p>However, as to the reconstruction process, the great switch of the thought was done for Japanese people.  physical three(3) conditions redefine the way of Japan as below in the without the any change.</p>
						<p>① There is no natural resources in Japan. There is little oil in Japan and Japan depends on foreign countries for oil. <br>
							<span>There is no natural resources in Japan, so try to be processing trade country.</span>
						</p>
						<p style="margin-top:38px;">② Japan is very small island country of cultivation area , which is surrounded by sea.<br>
							<span>Japan is very small island which is surrounded by sea. It can be optimal location of ocean trade.</span>
						</p>
						<p style="margin-top:38px;">③ Excess population to reach 100 million people in a small land area.<br>
							<span>It can be very strong point for the abundant labor force as 100 million people and huge domestic demand.</span>
						</p>
						<div class="clearfix">
							<p class="right"><img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_episode02_image03.png"></p>
							<p class="left">Like the egg in the Columbus story, the unfavorable conditions to favorable conditions, Embody the new paradigm.  We should abandon the stereotypical view and switch of the paradigm's thinking framework to read today's trends. For the realization of the revolution, idea and execution which can break various vicious cycle are essential. We will challenge by the consciousness transformation from [Reinstate] to [Enter to new world].</p>
							<p class="left">How revolution will become an everyday affair for us?<br>
							The revolution will become by <strong>the number of consent to human.</strong></p>
						</div>
						<p>Internet revolution was successful in everyday affair. The maximum points is the change for the usage applications by users from the transmission of e-mail to search of information. By the browsing technology, users' contact opportunity is much more increased than before and it is the starting point for the everyday affair. Transmission of e-mail is the usage for limited users, but search of information makes value for more and more users.  We showed our opinion that the revolution will become by the number of consent to human, and the maximum point to make the number is to be changed by the revolution advocate of the era by the main player, how to catch the number of group members. Colin Luther Powell Former Secretary of State in USA, he left his words that the growing rate of Internet access around the world depends on the condition of the government, enterprise and NPO by their proposal anew establishment of the  variety of global standard.<p>
						<p> We recognize the essential key for the Limex's everyday affair. It is our activities to obtain the sympathy and consent from the the number of group members,  to look down the knitting system by the government, enterprise and NPO.</p>
						<div class="close"><a href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/close_btn.png"></a></div>
					</div>
				</div>

				<div class="bloc episode03">
					<div class="image"><a href="javascript:void(0);"><img class="episode" src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_image03_more.png"></a></div>
					<div class="text_area">
						<p>So far, we explained about LIMEX's value and the our point of view that LIMEX to start revolution of stone.  We also explained at the point of value's redefinition, LIMEX is not only new material to produce ecological paper or plastic goods.  LIMEX has the value to change the definition of developed country from the  [Static] to [dynamic] and  [ Individual ] to[ Common]. We believe that [the catching method is the proposal of paradigm itself].  Feel the Hope to spread to the infinite and bright future while respect our past.  Then, Stir up a paradigm shift to take everybody's breath away while feeling the meaning of coexistence.</p>
						<p><img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_episode03_image01.png"></p>
						<p><img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/vision_episode03_image02.png"></p>
						<p>Make an sustainable development of the recycling innovation even after 100 years to attention is paid to the origin that the Earth is the star of stone.  Make new worldwide standard someday, nope, very near future, by Japanese technology  that Japanese people can be proud of it through our sharing ecology, to make the world to connect people and people by smile.Please be excited about our challenge of persistent global scale.</p>
						<div class="close"><a href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/img/en/mission/vision/close_btn.png"></a></div>
					</div>
				</div>
			</div>
		</section>



<?php
get_footer('en');
