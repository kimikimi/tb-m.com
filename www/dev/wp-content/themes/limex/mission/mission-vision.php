<?php
/**
 * Template Name: mission-vision
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<script>
//アコーディオン
    $(function(){
        $('.vision .image').on('click', function() {
					if($(this).hasClass('open')){
						$(this).next().slideUp();
						$(this).removeClass('open');
						$('.episode',this).attr('src', $('.episode',this).attr('src').replace('_close','_more'));
					}else{
						$(this).next().slideDown();
						$(this).addClass('open');
						$('.episode',this).attr('src', $('.episode',this).attr('src').replace('_more','_close'));
					}
        });
				$('.episode01 .close').on('click', function() {
					$('.episode01 .text_area').slideUp();
					$('.episode01 .image').removeClass('open');
					$('.episode01 .episode').attr('src', $('.episode01 .episode').attr('src').replace('_close','_more'));
				});
				$('.episode02 .close').on('click', function() {
					$('.episode02 .text_area').slideUp();
					$('.episode02 .image').removeClass('open');
					$('.episode02 .episode').attr('src', $('.episode02 .episode').attr('src').replace('_close','_more'));
				});
				$('.episode03 .close').on('click', function() {
					$('.episode03 .text_area').slideUp();
					$('.episode03 .image').removeClass('open');
					$('.episode03 .episode').attr('src', $('.episode03 .episode').attr('src').replace('_close','_more'));
				});

    });
</script>

<section id="mission">
		<div class="pageTtl">
			<div class="wrapper">
			<h2>MISSION</h2>
			</div>
		</div>
		<div class="sub-navi">
			<div class="inner">
				<ul>
					<li><a href="/mission/">MISSION STORY</a></li>
					<li><a href="/mission/vision/" class="cur">VISION STORY</a></li>
				</ul>
			</div>
		</div>

		<h3>LIMEXのVISION STORY</h3>
			<div class="fv">
				<p class="ts32 over" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_top2.png);">日本の技術で、この星をいいほうへ変えてゆく。</p>
				<p class="ts16">ライメックスには、石から生まれるビジョンがあります。<br>
すぐれたビジョンは、目を閉じるとまぶたに具体的なイメージが映像として浮かんできます。<br>
ここでは、私たちのビジョンをお伝えできればと思い、<br>
みなさまに向けて、スペシャルコンテンツを設けさせて頂きました。<br>
世界がうなる未来です。</p>
			</div>
			<div class="contents_area">
				<div class="bloc episode01">
					<div class="image"><a href="javascript:void(0);"><img class="episode" src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_image01_more.png"></a></div>
					<div class="text_area">
						<div class="clearfix">
							<p class="right"><img src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_episode01_image01.png"></p>
							<p class="left">ライメックスには、様々な価値があります。無尽蔵にある資源、石灰石を使用して、環境負荷をマイナスにしながら、プラスの価値を提供する。まず、ライメックスの価値について順を追って説明します。石から生まれる紙。石から生まれるプラスチック。石灰石という無尽蔵にある資源から無限的に生まれるライメックス製品。無限の可能性を秘めた化け物を、私たちが、どのようにして磨いていくのか。この磨き方が未来を大きく左右すると思います。ところで、価値とはなんでしょうか？どのようにして決められるのでしょうか？すべてのものは価値を有しています。そのモノ自体で価値というものは決まっているのでしょうか？違うと思います。人がどのように解釈するのかで、そのモノの価値が決まります。ライメックスが良いものなのか、どうなのか。役立つものなのか、どうなのか。 世界を救うものなのか、どうなのか。すべては人の解釈で決まってきます。</p>
						</div>
						<p style="margin-top:0;">「これは石の紙です」と言えば、人は「石の紙である」という解釈しかしてくれません。石の紙であることが本質的な価値ではないはずです。「石の紙＝モノ」だからです。今の時代、モノだけで人が共感する時代ではありません。人がプリウスに群がったように、イミで売れる時代です。ライメックスが秘めている究極の価値を、究極のイミを、明確に定義しておくことが何よりも重要に感じています。「ライメックスってなんですか？」と聞かれた時、この究極の価値を、一言でどのように表現できるか。</p>
						<p>水や木を使わない。石油使用量を大幅に削減できる。半永久的にリサイクル可能。耐水性・耐久性がある。工場の場所を選ばない。石灰石は無尽蔵な資源。地産地消が可能。これらは、わたしたちにとって、とても重要な価値ですが、あれもこれもでは、点の情報で伝わらないものです。むしろ点の情報は、ライメックスの価値が固定概念化され、ライメックスの可能性を見えなくする恐れがあります。</p>
						<p>東洋の音楽は、音と音の間に潜む無限の音を拾いますが、ライメックスは同じように、価値と価値の間に潜んでいる第３の価値に極めて大きな可能性を秘めた価値が眠っていると考えています。その価値と価値をつなぐ究極的な線の価値は何か？</p>
						<p style="margin-top:100px;"><img src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_episode01_image02.png"></p>
						<p style="margin-top:70px;">エコロジーという概念が、エコノミーという概念を超越した時から先進国の動き方が変わりました。 「動」から「静」へ。この変化への対応は、経済活動が主軸だった先進国において、やむない対応でした。エコロジーは、「概念」においては超越しましたが、「経営」における本音ではエコノミーが優先されました。「経営」が「概念」に押され、先進国に訪れた変化。おとなしく生きる。このやむなさから脱するのです。静から動へ。</p>
						<p>ただ過去に戻るのではありません。先進国は、最先端の技術によってエコロジーとエコノミーの共存を当たり前化させたあらたな時代へと突入するのです。これからの先進国は、「概念＝エコロジー」と「経営＝エコノミー」を無理矢理にでもつなげなければならない毎日からそれら双方を不都合なく抱きしめられる毎日へ。<br>ここから、本物の「共存」が始まるのです。</p>
 						<p>私たちはライメックスの価値を、「先進国の定義を変える」価値と定義しました。</p>
						<div class="close"><a href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/close_btn.png"></a></div>
					</div>
				</div>

				<div class="bloc episode02">
					<div class="image"><a href="javascript:void(0);"><img class="episode" src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_image02_more.png"></a></div>
					<div class="text_area">
						<p>「先進国の定義を変える」ということは、社会に対して「革命を起こす」ということです。<br>
							革命。それは、「本来ありえない」ことを変えることです。</p>
						<p style="margin-top:60px;"><img src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_episode02_image01.png"></p>
						<p style="margin-top:60px;">私たちが起こしていく革命についてここではご説明します。まず、革命は、どうやって始まるのでしょうか？革命とは、新しいパラダイムの提唱から始まります。コペルニクスの地動説、月の有人飛行、ソ連の崩壊、ベルリンの壁崩壊中国の民主化、インターネット革命、中東のジャスミン革命、すべての革命は、新しいパラダイムの提唱から始まっています。過去の革命は、科学者、国家、企業ですが、近年では、個々人の小さな行為の総和が、想像を越えたパワーや結果を生みだし、革命を起こせる時代です。現存する常識や定説がどんなに強固でも、決して屈せず、<strong>“新しいパラダイムを提唱すること”</strong>それが、革命の第一歩だと思います。</p>
						<p>では、革命を実行するにあたって、何が必要になるのでしょうか？革命の実行には、<strong>思考枠組みの大転換</strong>が必要になってきます。戦後の日本をケーススタディにすると、当時の日本は、以下の3つの課題がありました。（引用：米倉誠一郎　創発的破壊 未来をつくるイノベーション.2011年）</p>
						<p>① 日本には天然資源がない。石油に恵まれていない。<br>
							② 四方を海に囲まれた耕作面積の極めて少ない島国。<br>
							③ 狭い国土面積に1億人に達する過剰人口。
						</p>
						<p><img src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_episode02_image02.png"></p>
						<p>ただし、復興のプロセスにおいて“日本人の思考プロセスの大転換”がなされ、戦後復興という革命を引き起こしました。物理的な３条件は、何ひとつ変化がない中で、日本のあり方を次のように再定義したのです。</p>
						<p>① 日本には天然資源がない。石油に恵まれていない。<br>
							<span>資源がないので輸入による加工貿易立国を目指す。</span>
						</p>
						<p style="margin-top:38px;">② 四方を海に囲まれた耕作面積の極めて少ない島国。<br>
							<span>島国は、四方を海に囲まれた海洋貿易の最適な立地、それを強みにする。</span>
						</p>
						<p style="margin-top:38px;">③ 狭い国土面積に1億人に達する過剰人口。<br>
							<span>1億人という豊富な労働力と巨大内需を強みにする。</span>
						</p>
						<div class="clearfix">
							<p class="right"><img src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_episode02_image03.png"></p>
							<p class="left">コロンブスの卵的な発想で、不利な条件を有利な条件へ。新しいパラダイムを具現化していきました。古い固定観念を捨てて、新しい時代を読み取る思考枠組みの大転換。革命の実現には、これまでの様々な循環を根底から覆すような発想と実行が、必要不可欠になってきます。私たちは、「元に戻す」という発想から、「新しい世界に突入する」という意識転換で挑戦します。</p>
							<p class="left">では、どうやって革命は、わたしたちの暮らしに日常化するのでしょうか？<br>
							革命は、<strong>同意する人間の数</strong>で日常化します。</p>
						</div>
						<p>インターネット革命が日常化した最大のポイントは、メールを「送る」から情報を「探す」ことに、ユーザーの主な利用用途が変わったことです。ブラウジング技術によって、“ユーザーの接触機会が格段に増えたこと”が日常化の起点です。メールを「送る」ことは、限られたユーザーの用途ですが、情報を「探す」ことは、より多くのユーザーに価値をもたらします。革命を日常化するポイントとして同意する人間の数をあげましたが、この“数”の作り方は、時代の革命提唱者の主力プレーヤーによって変わってくるように、現代は、「個の群体の捉え方」が、最大のポイントです。かつてコリンパウエル元国務長官はインターネットに関してこのような言葉を残しています。インターネットの普及は、政府・企業・ＮＰＯが、どれだけ多数かつ多様なグローバルスタンダードを提唱し、確立しうるかに依存すると。<p>
						<p>ライメックスの日常化は、２１世紀の政府、企業、市民・NPO・NGOの編成システムを俯瞰しつつ、個の群体から多くの同意、共感を得る活動が肝心と認識しています。</p>
						<div class="close"><a href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/close_btn.png"></a></div>
					</div>
				</div>

				<div class="bloc episode03">
					<div class="image"><a href="javascript:void(0);"><img class="episode" src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_image03_more.png"></a></div>
					<div class="text_area">
						<p>これまでにライメックスの価値、そして、ライメックスが石の革命を起こしていく考え方を説明させていただきました。価値の再定義のところで説明しましたが、ライメックスは、単にエコロジーな紙やプラスチック製品をつくり出す、新素材ではありません。ライメックスには、先進国の定義を、「静」から「動」へ、「個」から「共」へと変える価値を有しています。この価値の捉え方自体が、「新しいパラダイムの提唱そのもの」だと確信しています。単なるエコロジーではなくもっと無限に広がる希望を感じ、過去を大切にしつつもこれから開ける明るい未来を感じ、それでいながら共存という言葉が持つイミを感じ、はっと我に返るようなパラダイムシフトを起こす。</p>
						<p><img src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_episode03_image01.png"></p>
						<p><img src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/vision_episode03_image02.png"></p>
						<p>私たちは、地球が石の星でもあるという、地球の原点に着目して、100年後も持続可能な循環型のイノベーションを起こしていく。今までに笑顔が人と人をつなぐ世界をつくるために、シェアリング・エコロジーを通して、いつか、いや近い将来、日本の技術で、日本人が誇りに思える、新しい世界のスタンダードをつくっていく。私たちの絶えざる地球規模の挑戦に、ご期待ください。</p>
						<div class="close"><a href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/img/mission/vision/close_btn.png"></a></div>
					</div>
				</div>
			</div>
		</section>



<?php
get_footer();
