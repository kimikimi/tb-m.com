<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<?php if ( ! ( strpos($_SERVER["REQUEST_URI"], "/form/contact_lime-x/contact/") !== false || strpos($_SERVER["REQUEST_URI"], "/order/") !== false )) : ?>
<div class="bnr" id="order-menu">
	<div class="bnr_img">
		<a href="/order/explain.php"><img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/bnr_meisi.png" alt=""></a>
	</div>
	<div class="bnr_close btn_delete">
		<img src="<?php echo get_template_directory_uri(); ?>/action/assets/images/bnr_meisi_close.png" alt="" width="20px" height="18px">
	</div> 
</div>



<?php endif; ?>
		<footer>
			<ul class="cf01">
				<li><a href="https://twitter.com/LIMEX_JAPAN" target="_blank" class="icon01">twitter</a></li>
				<li><a href="https://www.facebook.com/tbm.limex" target="_blank" class="icon02">facebook</a></li>
				<li><a href="https://www.youtube.com/channel/UCIvfa-J0VGeLl71Aeg6sWRg" target="_blank" class="icon03">youtube</a></li>
			</ul>

<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ftbm.global.limex%2F&tabs&width=800&height=154&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId=465448623505236" width="500" height="154" style="border:none;overflow:hidden;margin-bottom:20px;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

			
			<div class="txtLink01"><a href="/company/">ABOUT US</a><a href="http://recruit.tb-m.com" target="_blank">RECRUIT</a><a href="/privacy/">PRIVACY POLICY</a><a href="/company/access/">ACCESS</a><a href="https://tb-m.com/form/contact_lime-x/contact/">CONTACT</a></div>
			<p class="copyright">Copyright TBM Co.,Ltd All Rights Reserved</p>
		</footer>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-46676692-1', 'auto');
			ga('send', 'pageview');
			
		</script>
		<!-- アクセス解析 -->
		<script type="text/javascript">
			
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-16758707-1']);
			_gaq.push(['_trackPageview']);
			
			(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
			
		</script>
		<!-- // アクセス解析 -->
	</body>
</html>