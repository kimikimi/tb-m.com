<?php
/*
Plugin Name: Hardcode theme switcher
Plugin URI: http://ja.forums.wordpress.org/topic/13483
Author: Takuro Hishikawa
Version: 0.1
*/

function is_mobile () {
$useragents = array(
	'iPad',
	'iPhone',
	'iPod',
	'Android',
	'dream',
	'CUPCAKE',
	'blackberry',
	'webOS',
	'incognito',
	'webmate'
);
$pattern = '/'.implode('|', $useragents).'/i';
	return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}

function my_theme_switcher($theme){
	// yes, it's hardcoded!
	//switch (preg_replace('#^/([^/]+)/?.*$#', '$1', $_SERVER['REQUEST_URI'])) {

	if(!is_mobile()){
		return $theme;
	}

	if($_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/en/'  || preg_match('/order/', $_SERVER['REQUEST_URI'])){
		$overrideTheme = 'limex_sp';
	}else{
		$overrideTheme = false;	
	}

	if ( $overrideTheme ) {
		$overrideTheme = wp_get_theme($overrideTheme);
		return $overrideTheme->exists()
			? $overrideTheme['Template']
			: $theme;
	}
	return $theme;
}

add_filter('stylesheet', 'my_theme_switcher', 50);
add_filter('template', 'my_theme_switcher', 50);