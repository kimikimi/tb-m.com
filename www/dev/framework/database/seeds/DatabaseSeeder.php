<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('ColorTableSeeder');
    }

}

class ColorTableSeeder extends Seeder
{

    public function run()
    {
        //delete users table records
        DB::table('colors')->delete();
        //insert some dummy records
        DB::table('colors')->insert(array(
            array('pattern' => '【片面】 カラー'),
            array('pattern' => '【片面】 黒'),
            array('pattern' => '【両面】 カラー / カラー'),
            array('pattern' => '【両面】 カラー / 黒'),
            array('pattern' => '【両面】 黒 / 黒'),
        ));

        DB::table('promotions')->delete();
        DB::table('promotions')->insert(array(
            array('customer_name' => 'good',
                'code' => '1234',
                'start_date' => '2016-08-01',
                'end_date' => '2016-09-30',
                'enabled' => '1',
                'comment' => 'good one'),
            array('customer_name' => 'bad1',
                'code' => '1111',
                'start_date' => '2016-08-01',
                'end_date' => '2016-08-10',
                'enabled' => '1',
                'comment' => 'bad one'),
            array('customer_name' => 'bad2',
                'code' => '2222',
                'start_date' => '2016-08-01',
                'end_date' => '2016-09-30',
                'enabled' => '0',
                'comment' => 'bad two')
            ));
    }

}
