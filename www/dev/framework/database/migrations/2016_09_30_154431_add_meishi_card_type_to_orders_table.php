<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMeishiCardTypeToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('orders', function($table)
            {
                $table->string('meishi_card_type')->nullable();
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
        public function down()
        {
            Schema::table('orders', function($table)
            {
               $table->dropColumn('meishi_card_type');
            });
        }
}
