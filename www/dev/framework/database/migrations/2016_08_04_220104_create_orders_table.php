<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('orderId')->nullable();
			$table->string('meishi_name')->nullable();
			$table->string('meishi_company')->nullable();
			$table->integer('meishi_employees')->nullable();
			$table->string('meishi_kana')->nullable();
			$table->string('meishi_post')->nullable();
			$table->string('meishi_email')->nullable();
			$table->integer('meishi_orders')->nullable();
			$table->string('meishi_tel')->nullable();
			$table->string('meishi_use')->nullable();
			$table->string('related_news')->nullable();
			$table->string('meishi_note')->nullable();
			$table->string('nouhin_company')->nullable();
			$table->string('nouhin_name')->nullable();
			$table->string('nouhin_kana')->nullable();
			$table->string('nouhin_post')->nullable();
			$table->string('nouhin_tel')->nullable();
			$table->string('pr_agree')->nullable();
			$table->string('promo')->nullable();
			$table->string('file_url')->nullable();
			$table->string('meishi_pref')->nullable();
			$table->string('meishi_addr')->nullable();
			$table->string('setNum')->nullable();
			$table->string('buyPattern')->nullable();
			$table->string('meishi_design_pattern')->nullable();
			$table->string('meishi_design_pattern_back')->nullable();
			$table->string('ai')->nullable();
			$table->string('knowPattern')->nullable();
			$table->string('nouhin_pref')->nullable();
			$table->string('nouhin_addr')->nullable();
			$table->string('meishi_tmpnum')->nullable();
			$table->string('answer')->nullable();
			$table->string('file_name')->nullable();
			$table->integer('status');
			$table->string('comment')->nullable();
			$table->integer('side_colorful')->nullable();
			$table->integer('side_black')->nullable();
			$table->integer('both_colorful_colorful')->nullable();
			$table->integer('both_colorful_black')->nullable();
			$table->integer('both_black_black')->nullable();
			$table->integer('price_side_colorful')->nullable();
			$table->integer('price_side_black')->nullable();
			$table->integer('price_both_colorful_colorful')->nullable();
			$table->integer('price_both_colorful_black')->nullable();
			$table->integer('price_both_black_black')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
