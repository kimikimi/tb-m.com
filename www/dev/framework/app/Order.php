<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    const UPLOAD_DIR = '../order/upload/';

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        if ($this->file_url == 'なし') {
            $this->file_url = null;
        }
    }

    protected $fillable = [
        'orderId',
        'meishi_name',
        'meishi_company',
        'meishi_employees',
        'meishi_kana',
        'meishi_post',
        'meishi_pref',
        'meishi_addr',
        'meishi_email',
        'meishi_orders',
        'meishi_tel',
        'meishi_use',
        'related_news',
        'meishi_note',
        'nouhin_company',
        'nouhin_name',
        'nouhin_kana',
        'nouhin_post',
        'nouhin_pref',
        'nouhin_addr',
        'nouhin_tel',
        'pr_agree',
        'promo',
        'file_url',
        'setNum',
        'buyPattern',
        'meishi_card_type',
        'meishi_design_pattern',
        'meishi_design_pattern_back',
        'ai',
        'status',
        'knowPattern',
        'meishi_tmpnum',
        'answer',
        'file_name',
        'comment',
        'side_colorful',
        'side_black',
        'both_colorful_colorful',
        'both_colorful_black',
        'both_black_black',
        'price_side_colorful',
        'price_side_black',
        'price_both_colorful_colorful',
        'price_both_colorful_black',
        'price_both_black_black',
    ];

    /**
     * ファイルパスを返す
     *
     * @param  $fileName string
     * @return  $path string
     *
     */
    public function getFilePathAttribute()
    {
        return base_path() . '/' . self::UPLOAD_DIR . $this->attributes['file_name'];
    }

    public function colors()
    {
        return $this->belongsToMany('App\Color','order_to_colors', 'order_id', 'color_id');
    }
}
