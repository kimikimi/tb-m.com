<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderToColor extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'order_to_colors';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['order_id', 'color_id', 'price'];
}
