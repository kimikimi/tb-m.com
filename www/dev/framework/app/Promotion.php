<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'promotions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_name', 'code', 'start_date', 'end_date', 'enabled', 'comment'];
}
