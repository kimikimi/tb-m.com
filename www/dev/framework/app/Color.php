<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'colors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pattern'];
    public $timestamps = false;

    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }
}
