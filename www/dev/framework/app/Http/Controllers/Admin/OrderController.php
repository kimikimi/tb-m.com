<?php

namespace App\Http\Controllers\Admin;

use Response;
use Input;
use DB;
use Redirect;
use App\Order;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class OrderController extends Controller
{

    // ページングリミット
    const PER_PAGE = '10';
    const UPLOAD_DIR = '../../../../order/upload/';

    // 一覧ページ
    public function index(Request $request)
    {
        $orderBy = $request->input('orderBy');
        $orderOn = $request->input('orderOn');
        $keyword = $request->input('keyword');
        $status = $request->input('status');
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');
        $mode = $request->input('mode');
        $sortcolumns = \Config::get('order.sortcolumns');
        $errormsg = "";

        if ((in_array($orderOn,$sortcolumns)) || $orderOn == null) {
            // 出力ロジック
            $queryBuilder = self::getOrdersQuery($orderBy, $orderOn, $startDate, $endDate, $keyword, $status);
        } else {
            // 無効なOrderOn, default sort
            $queryBuilder = self::getOrdersQuery($orderBy, null, $startDate, $endDate, $keyword, $status);

            $errormsg = "無効なorderOn入力です。";
        }

        // ページングの設定
        $orders = $queryBuilder->paginate(self::PER_PAGE)->appends($request->all());

        $data = [
            'mode'  => $mode,
            'orders' => $orders,
            'keyword' => $keyword,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'orderBy' => $orderBy,
            'orderOn' => $orderOn,
            '_status' => $status,
        ];

        return view('order.index', $data)->with('errormsg',$errormsg);
    }

    // 削除
    public function delete(Order $order)
    {
        $order->delete();
        return redirect('admin/order')->with('message', '削除に成功しました。');
    }

    // ファイル削除
    public function fileDelete(Order $order)
    {
        $uploadFilePath = $order->filePath;
        if (!@unlink($uploadFilePath)) {
            echo 'ファイル削除に失敗しました。';
            return;
        }
        $order->file_url = null;
        DB::transaction(function () use ($order) {
            $order->save();
        });

        return redirect('admin/order')->with('message', 'ファイル削除に成功しました。');
    }

    // csvダウンロード
    public function csv(Request $request)
    {
        $orderOn = $request->input('orderOn');
        $keyword = $request->input('keyword');
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');
        $orderBy = $request->input('orderBy');
        $status = $request->input('status');

        // 出力ロジック
        $orders = self::getOrdersQuery($orderBy, $orderOn, $startDate, $endDate, $keyword, $status)->get();

        // データ整形&配列化
        $orders = self::ordersToArray($orders);

        $csvorders = array();
        foreach ( $orders as $order ) {
            array_shift($order);

            if ( $order["side_colorful"] != 0 || $order["side_colorful"] != null ) {
                $order["side_colorful"] = "○";
            } else {
                $order["side_colorful"] = "";
            }
            if ( $order["side_black"] != 0 || $order["side_colorful"] != null ) {
                $order["side_black"] = "○";
            } else {
                $order["side_black"] = "";
            }
            if ( $order["both_colorful_colorful"] != 0 || $order["side_colorful"] != null ) {
                $order["both_colorful_colorful"] = "○";
            } else {
                $order["both_colorful_colorful"] = "";
            }
            if ( $order["both_colorful_black"] != 0 || $order["side_colorful"] != null ) {
                $order["both_colorful_black"] = "○";
            } else {
                $order["both_colorful_black"] = "";
            }
            if ( $order["both_black_black"] != 0 || $order["side_colorful"] != null ) {
                $order["both_black_black"] = "○";
            } else {
                $order["both_black_black"] = "";
            }
            $csvorders[] = $order;
        }

        // CSVとして整形
        $csv = self::getCsvData($csvorders);

        // HTTPヘッダー
        $filename = 'orders_' . date('Ymd') . '.csv';
        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',
        ];

        return Response::make($csv, 200, $headers);
    }

    /**
     * CSVのデータのQueryBuilderオブジェクトを返す
     *
     * @param  $orderBy string
     * @return Object
     */
    private static function getOrdersQuery($orderBy = 'DESC', $orderOn = null, $startDate = null, $endDate = null, $keyword = null, $status = null)
    {
        $query = DB::table('orders')
        ->select(
            'orders.id',
            'orders.created_at',
            'orders.orderId',
            'orders.meishi_company',
            'orders.meishi_employees',
            'orders.meishi_name',
            'orders.meishi_kana',
            'orders.meishi_post',
            'orders.meishi_pref',
            'orders.meishi_addr',
            'orders.meishi_tel',
            'orders.meishi_email',
            'orders.meishi_use',
            'orders.meishi_orders',
            'orders.setNum',
            'orders.side_colorful',
            'orders.side_black',
            'orders.both_colorful_colorful',
            'orders.both_colorful_black',
            'orders.both_black_black',
            'orders.buyPattern',
            'orders.meishi_card_type',
            'orders.meishi_design_pattern',
            'orders.meishi_design_pattern_back',
            'orders.meishi_tmpnum',
            'orders.ai',
            'orders.file_url',
            'orders.promo',
            'orders.price_side_colorful',
            'orders.price_side_black',
            'orders.price_both_colorful_colorful',
            'orders.price_both_colorful_black',
            'orders.price_both_black_black',
            'orders.knowPattern',
            'orders.answer',
            'orders.related_news',
            'orders.pr_agree',
            'orders.nouhin_company',
            'orders.nouhin_name',
            'orders.nouhin_kana',
            'orders.nouhin_post',
            'orders.nouhin_pref',
            'orders.nouhin_addr',
            'orders.meishi_note',
            'orders.status',
            'orders.comment'
        )->where(function($query) use ($keyword) {
            $query->orWhere('orderId', 'like', "%{$keyword}%")
            ->orWhere('meishi_company', 'like', "%{$keyword}%")
            ->orWhere('meishi_kana', 'like', "%{$keyword}%")
            ->orWhere('meishi_name', 'like', "%{$keyword}%")
            ->orWhere('meishi_tel', 'like', "%{$keyword}%")
            ->orWhere('meishi_email', 'like', "%{$keyword}%");
        });

        if ($startDate && $endDate) {
            $query->whereBetween('created_at', [$startDate, $endDate]);
        } elseif ($startDate) {
            $query->where('created_at', '>=', $startDate);
        }

        if ($status !== '999') {
            $query->where('status', $status);
        }

        if ($orderOn) {
            $query->orderBy($orderOn, $orderBy);
        } else {
            $query->orderBy('created_at', $orderBy);
        }

        return $query;
    }

    /**
     * urlのDESC,ASCのトグル、リダイレクト
     *
     * @param  $request Object
     *
     */
    public function changeSort(Request $request)
    {
        $paramOrderBy = $request->input('orderBy', 'DESC');
        $uri = self::getSortedUrl($paramOrderBy);
        return Redirect::to('/admin/order?' . $uri)->with('view', 'full');
    }

    /**
     *
     * @param  $request Object
     *
     */
    public function changeSortCompact(Request $request)
    {
        $paramOrderBy = $request->input('orderBy', 'DESC');
        $uri = self::getSortedUrl($paramOrderBy);
        return redirect('/admin/order?' . $uri)->with('view', 'compact');
    }

    /**
     *
     * @param  $request Object
     *
     */
    public function changeStatus(Request $request, Order $order)
    {
        $status = $request->input('status');
        if (is_null($status)) {
            abort(400);
        }

        $order->status = $status;
        DB::transaction(function () use ($order) {
            $order->save();
        });

        return redirect()->back()->with('message', 'ステータス変更に成功しました。');
    }

    /**
     *
     * @param  $request Object
     *
     */
    public function changeComment(Request $request, Order $order)
    {
        $comment = $request->input('comment');
        if (is_null($comment)) {
            return Redirect::back();
        }

        $order->comment = $comment;
        DB::transaction(function () use ($order) {
            $order->save();
        });

        return redirect()->back()->with('message', 'コメント変更に成功しました。');
    }

    /**
     * getパラメータからurlを整形して返す
     *
     * @param  $paramOrderBy string
     * @return  $url string
     *
     */
    private static function getSortedUrl($paramOrderBy)
    {
        $paramOrderBy = ($paramOrderBy === 'DESC') ? 'ASC' : 'DESC';
        $queryStrings = Input::get();
        $queryStrings['orderBy'] = $paramOrderBy;
        $uri = urldecode(http_build_query($queryStrings));
        $uri = str_replace('?', '', $uri);
        return $uri;
    }

    /**
     * csvデータを整形
     *
     * @param  $orders array
     * @return  $csv string
     *
     */
    private static function getCsvData(array $orders)
    {
        $csvHeader = \Config::get('order.csvHeader');
        array_unshift($orders, $csvHeader);
        $stream = fopen('php://temp', 'w');
        foreach ($orders as $order) {
            fputcsv($stream, $order);
        }
        rewind($stream);
        $csv = mb_convert_encoding(str_replace(PHP_EOL, "\r\n", stream_get_contents($stream)), 'SJIS-win', 'UTF-8');
        return $csv;
    }

    /**
     * 配列に整形
     *
     * @param  $orders array
     * @return  $orders array
     *
     */
    private static function ordersToArray(array $orders)
    {
        // statusを変換し、オブジェクトを配列に変換
        return collect($orders)->map(function($order) {
                    $order->status = \Config::get('order.status.' . $order->status);
                    return (array) $order;
                })->toArray();
    }

    /*
     * Inserts a new key/value before the key in the array.
     *
     * @param $key
     *   The key to insert before.
     * @param $array
     *   An array to insert in to.
     * @param $new_key
     *   The key to insert.
     * @param $new_value
     *   An value to insert.
     *
     * @return
     *   The new array if the key exists, FALSE otherwise.
     *
     * @see array_insert_after()
     */

    private static function array_insert_before($key, array &$array, $new_key, $new_value)
    {
        if (array_key_exists($key, $array)) {
            $new = array();
            foreach ($array as $k => $value) {
                if ($k === $key) {
                    $new[$new_key] = $new_value;
                }
                $new[$k] = $value;
            }
            return $new;
        }
        return FALSE;
    }

}
