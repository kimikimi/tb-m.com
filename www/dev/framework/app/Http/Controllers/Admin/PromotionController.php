<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Promotion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Database\QueryException;

class PromotionController extends Controller
{

    // ページングリミット
    const PER_PAGE = '20';

    // 一覧ページ
    public function index(Request $request)
    {
        $keyword = $request->input('keyword');
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');

        // 出力ロジック
        $queryBuilder = self::getPromotionsQuery($startDate, $endDate, $keyword);

        // ページングの設定
        $promotions = $queryBuilder->paginate(self::PER_PAGE)->appends($request->all());

        $data = [
            'promotions' => $promotions,
            'keyword' => $keyword,
            'startDate' => $startDate,
            'endDate' => $endDate
        ];

        return view('promotion/index', $data);
    }

    // 新規登録 画面
    public function create()
    {
        return view('promotion/create');
    }

    // 新規登録　DB
    public function store(Request $request)
    {
        $model = $request->input();
        if ($model["start_date"] == "" ) {
            $model["start_date"] = "0000-00-00";
        }
        if ($model["end_date"] == "" ) {
            $model["end_date"] = "0000-00-00";
        }

        $validator = Validator::make($request->all(), [
            'code' => 'required|numeric|unique:promotions',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withInput($model)
                            ->withErrors($validator);
        }
        // store
        $promotion = new Promotion($model);
        DB::transaction(function () use ($promotion) {
            $promotion->save();
        });


        // redirect
        return redirect('admin/promotion')->with('message', '新規登録に成功しました。');
    }

    // 変更　画面
    public function edit(Promotion $promotion)
    {
        $data = [
            'promotion' => $promotion
        ];
        return view('promotion/edit', $data);
    }

    // 変更　DB
    public function update(Request $request, Promotion $promotion)
    {
        $model = $request->input();
        if ($model["start_date"] == "" ) {
            $model["start_date"] = "0000-00-00";
        }
        if ($model["end_date"] == "" ) {
            $model["end_date"] = "0000-00-00";
        }

        $validator = Validator::make($request->all(), [
            'code' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }

        $promotion->update([
            'customer_name' => $request->input('customer_name'),
            'code' => $request->input('code'),
            'start_date' => $model["start_date"],
            'end_date' => $model["end_date"],
            'enabled' => $request->input('enabled'),
            'comment' => $request->input('comment')
        ]);


        // redirect
        return redirect('admin/promotion')->with('message', '更新に成功しました。');
    }

    // 削除
    public function destroy(Promotion $promotion)
    {
        $promotion->delete();
        return redirect('admin/promotion')->with('message', '削除に成功しました。');
    }

    /**
     * 検索のパラメータから注文データのQueryBuilderオブジェクトを返す
     *
     * @param  $startDate DATE
     * @param  $endDate DATE
     * @param  $keyword string
     * @return Object
     */
    private static function getPromotionsQuery($startDate = null, $endDate = null, $keyword = null)
    {
        $query = DB::table('promotions')->select(
            'id',
            'customer_name',
            'code',
            'start_date',
            'end_date',
            'enabled',
            'comment'
        )->where(function($query) use ($keyword) {
            $query->orWhere('customer_name', 'like', "%{$keyword}%")
                  ->orWhere('code', "{$keyword}");
        });

        if ($startDate && $endDate) {
            $query->where('start_date', '>=', $startDate)->where('end_date', '<=', $endDate);
        } else if ($startDate) {
            $query->where('start_date', '>=', $startDate);
        }

        $query->orderBy('created_at', 'desc');

        return $query;
    }

    /**
     * データベースからPromotionテーブルのデータをJSONで返す。
     *
     * @return  $promotions json
     *
     */
    public static function promotionsToJson()
    {
        $promotions = DB::table('promotions')->get();

        return Response()->json($promotions);
    }

}
