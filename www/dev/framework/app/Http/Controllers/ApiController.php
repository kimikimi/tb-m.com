<?php

namespace App\Http\Controllers;

use DB;
use App\Order;
use App\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ApiController extends Controller
{

    /**
     * お問い合わせからメール送信内容を受け取る
     * @return json
     */
    public function order(Request $request)
    {
        $post = $request->input();
        $order = new Order($post);

//        $tmp = json_encode($order);
//        $tmp = json_decode($tmp, true);
//        Log::alert('order_detail:', $tmp);

        if ($order->side_colorful == '1') {
            $order->side_colorful = @$order->buyPattern['1'];
            $order->side_colorful .= PHP_EOL . @$order->meishi_card_type['1'];
            $order->side_colorful .= PHP_EOL . @$order->meishi_design_pattern['1'];
            $order->side_colorful .= PHP_EOL . @$order->meishi_design_pattern_back['1'];
        }
        if ($order->side_black == '2') {
            $order->side_black = @$order->buyPattern['2'];
            $order->side_black .= PHP_EOL . @$order->meishi_card_type['2'];
            $order->side_black .= PHP_EOL . @$order->meishi_design_pattern['2'];
            $order->side_black .= PHP_EOL . @$order->meishi_design_pattern_back['2'];
        }
        if ($order->both_colorful_colorful == '3') {
            $order->both_colorful_colorful = @$order->buyPattern['3'];
            $order->both_colorful_colorful .= PHP_EOL . @$order->meishi_card_type['3'];
            $order->both_colorful_colorful .= PHP_EOL . @$order->meishi_design_pattern['3'];
            $order->both_colorful_colorful .= PHP_EOL . @$order->meishi_design_pattern_back['3'];
        }
        if ($order->both_colorful_black == '4') {
            $order->both_colorful_black = @$order->buyPattern['4'];
            $order->both_colorful_black .= PHP_EOL . @$order->meishi_card_type['4'];
            $order->both_colorful_black .= PHP_EOL . @$order->meishi_design_pattern['4'];
            $order->both_colorful_black .= PHP_EOL . @$order->meishi_design_pattern_back['4'];
        }
        if ($order->both_black_black == '5') {
            $order->both_black_black = @$order->buyPattern['5'];
            $order->both_black_black .= PHP_EOL . @$order->meishi_card_type['5'];
            $order->both_black_black .= PHP_EOL . @$order->meishi_design_pattern['5'];
            $order->both_black_black .= PHP_EOL . @$order->meishi_design_pattern_back['5'];
        }

        $order->buyPattern = 'Array';
        $order->meishi_card_type = 'Array';
        $order->meishi_design_pattern = 'Array';
        $order->meishi_design_pattern_back = 'Array';

//        $tmp = json_encode($order);
//        $tmp = json_decode($tmp, true);
//        Log::alert('order_detail:', $tmp);


        DB::transaction(function () use ($order) {
            $order->save();
        });

        return [
            'result' => 'OK',
        ];
    }

    /**
     *   プロモーションコード探し、有る・無い
     *
     * @param $request Request
     * @return json
     */
    public function checkPromotion(Request $request)
    {
        $exists = Promotion::where('code', '=', $request->input('promotion'))
            ->where('enabled', '=', 1)
            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'))
            ->orWhere(function ($query) use ($request) {
                $query->where('code', '=', $request->input('promotion'))
                    ->where('enabled', '=', 1)
                    ->where('start_date', '=', '0000-00-00')
                    ->where('end_date', '=', '0000-00-00');
            })->exists();

        return [
            'exists' => $exists,
        ];
    }

}
