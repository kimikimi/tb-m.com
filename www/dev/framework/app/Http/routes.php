<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// api
Route::post('api/order', 'ApiController@order');
Route::post('api/checkpromotion', 'ApiController@checkPromotion');

// root
Route::get('/', 'Admin\OrderController@index');
Route::get('/home', 'Admin\OrderController@index');


// 管理
Route::group([
    'namespace' => 'Admin',
    'prefix' => 'admin',
    'middleware' => ['csrf', 'auth'],
], function () {
    Route::get('/', 'OrderController@index');
    Route::get('order/delete/{order}', 'OrderController@delete');
    Route::get('order/fileDelete/{order}', 'OrderController@fileDelete');
    Route::get('order/changeComment/{order}', 'OrderController@changeComment');
    Route::get('order/csv', 'OrderController@csv');
    Route::get('order/changeSort', 'OrderController@changeSort');
    Route::get('order/compact/changeSort', 'OrderController@changeSortCompact');
    Route::get('order/changeStatus/{order}', 'OrderController@changeStatus');
    Route::get('order', 'OrderController@index');

    Route::resource('promotion', 'PromotionController', ['except' => [
        'show'
    ]]);
});

Route::controllers([
    'auth' => 'Auth\AuthController',
]);
