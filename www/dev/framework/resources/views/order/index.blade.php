@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="{{ url('/admin/order') }}" method="GET" class="" role="search">
                            <div class="form-group navbar-form">
                                <label for="keyword">キーワード&nbsp;&nbsp;</label>
                                <input id="keyword" class="form-control" type="text" name="keyword"
                                       value="{{ $keyword }}" style="width: 360px">
                            </div>
                            <div class="form-group navbar-form">
                                <label for="term">期間&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                <input id="term" class="datepicker form-control" type="text" name="startDate"
                                       value="{{ $startDate }}">&nbsp;~&nbsp;<input class="datepicker2 form-control"
                                                                                    type="text" name="endDate"
                                                                                    value="{{ $endDate }}">
                            </div>
                            <div class="form-group navbar-form">
                                <label for="status">ステータス&nbsp;&nbsp;</label>
                                <select id="status" name="status" class="form-control">
                                    <option {{ '999' == $_status ? 'selected="selected"' : null }} value="999">すべて
                                    </option>
                                    @foreach (Config::get('order.status') as $status_value => $status)
                                        <option {{ $status_value == $_status ? 'selected="selected"' : null }} value="{{ $status_value }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <input type="submit" value="検索" class="btn btn-default">
                            &nbsp;&nbsp;
                            <a class="btn btn-default"
                               href="{{ url('/admin/order/csv') }}?keyword={{ $keyword }}&startDate={{ $startDate }}&endDate={{ $endDate }}&orderBy={{ $orderBy }}&status={{ $_status }}&orderOn={{ $orderOn ?: null }}">CSVダウンロード</a>
                            <input type="hidden" name="orderBy" value="{{ $orderBy ? $orderBy : "DESC" }}">
                            <input type="hidden" name="orderOn" value="{{ $orderOn }}">
                            @if ( $mode )
                                <input id="viewMode" type="hidden" name="mode" value="{{ $mode }}">
                            @else
                                <input id="viewMode" type="hidden" name="mode" value="">
                            @endif
                        </form>
                    </div>
                </div>
                <br/>
                @if(Session::has('message'))
                    <p class="alert alert-info">{{ Session::get('message') }}</p>
                @endif
                @if( $errormsg )
                    <p class="alert alert-danger">{{ $errormsg }}</p>
            @endif
            <!-- ページング -->
                <div class="toppages">
                    {!! $orders->render() !!}
                </div>
                <div id="fullview" class="panel panel-default" style="display:none;">
                    <div class="panel-heading">
                        <h3>注文</h3><a style="margin-top: -50px; float:right" class="btn btn-default"
                                      onclick="changeMode()">簡易</a>
                    </div>
                    <div class="orderpanel panel-body">
                        <table class="ordertable">
                            <thead>
                            <tr>
                                <th>ステータス</th>
                                <th>ファイル・データ</th>
                                <th>備考(管理者)</th>
                                <th>注文日時&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=created_at&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>注文番号&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=orderId&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>会社名&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_company&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>従業員数&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_employees&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>お名前&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_name&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>ふりがな&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_kana&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>郵便番号&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_post&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>都道府県&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_pref&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>市区町村番地&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_addr&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>電話番号&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_tel&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>メールアドレス&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_email&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>用途&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_use&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>ご注文人数&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_orders&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>セット数&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=setNum&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>色数 【片面】 カラー&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=side_colorful&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>色数 【片面】 黒&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=side_black&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>色数 【両面】 カラー / カラー&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=both_colorful_colorful&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>色数 【両面】 カラー / 黒&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=both_colorful_black&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>色数 【両面】 黒 / 黒&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=both_black_black&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                {{--<th>購入パターン&nbsp;&nbsp;<a href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=buyPattern&mode=full" class="sortbutton btn btn-primary">↑↓</a></th>--}}
                                {{--<th>名刺のテンプレート&nbsp;&nbsp;<a href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_card_type&mode=full" class="sortbutton btn btn-primary">↑↓</a></th>--}}
                                {{--<th>名刺下部のデザイン（表面）&nbsp;&nbsp;<a href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_design_pattern&mode=full" class="sortbutton btn btn-primary">↑↓</a></th>--}}
                                {{--<th>名刺下部のデザイン（裏面）&nbsp;&nbsp;<a href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_design_pattern_back&mode=full" class="sortbutton btn btn-primary">↑↓</a></th>--}}
                                <th>名刺のテンプレート数&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_tmpnum&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>名刺のai（イラストレータ）データの有無&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=ai&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>名刺データのURL&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=file_url&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>プロモーションコード&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=promo&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>プロモーション単価 【片面】 カラー&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=price_side_colorful&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>プロモーション単価 【片面】 黒&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=price_side_black&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>プロモーション単価 【両面】 カラー / カラー&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=price_both_colorful_colorful&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>プロモーション単価 【両面】 カラー / 黒&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=price_both_colorful_black&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>プロモーション単価 【両面】 黒 / 黒&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=price_both_black_black&mode=full&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>何でLIMEXの名刺をお知りになりましたか？&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=knowPattern&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>アンケートテキスト&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=answer&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>LIMEXに関するニュースとお知らせ&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=related_news&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>名刺発注システムの導入を検討したい&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=pr_agree&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>納品先が違う場合会社名&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=nouhin_company&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>納品先が違う場合お名前&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=nouhin_name&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>納品先が違う場合お名前(ふりがな)&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=nouhin_kana&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>納品先が違う場合郵便番号&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=nouhin_post&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>納品先が違う場合都道府県&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=nouhin_pref&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>納品先が違う場合市区町村番地&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=nouhin_addr&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                                <th>備考欄&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/changeSort') }}?{{ $_SERVER['QUERY_STRING'] }}&orderOn=meishi_note&mode=full"
                                            class="sortbutton btn btn-primary">↑↓</a></th>
                            </tr>
                            </thead>
                            @foreach ($orders as $order)
                                <?php
                                $side_colorful = '';
                                $side_black = '';
                                $both_colorful_colorful = '';
                                $both_colorful_black = '';
                                $both_black_black = '';
                                ?>
                                @if ($order->buyPattern != 'Array')
                                    @if ($order->side_colorful == '1')
                                        <?php $side_colorful .= $order->buyPattern; ?>
                                    @endif

                                    @if ($order->side_black == '2')
                                        <?php $side_black .= $order->buyPattern; ?>
                                    @endif

                                    @if ($order->both_colorful_colorful == '3')
                                        <?php $both_colorful_colorful .= $order->buyPattern; ?>
                                    @endif

                                    @if ($order->both_colorful_black == '4')
                                        <?php $both_colorful_black .= $order->buyPattern; ?>
                                    @endif

                                    @if ($order->both_black_black == '5')
                                        <?php $both_black_black .= $order->buyPattern; ?>
                                    @endif
                                @endif

                                @if ($order->meishi_card_type != 'Array')
                                    @if ($order->side_colorful == '1')
                                        <?php $side_colorful .= PHP_EOL . $order->meishi_card_type; ?>
                                    @endif

                                    @if ($order->side_black == '2')
                                        <?php $side_black .= PHP_EOL . $order->meishi_card_type; ?>
                                    @endif

                                    @if ($order->both_colorful_colorful == '3')
                                        <?php $both_colorful_colorful .= PHP_EOL . $order->meishi_card_type; ?>
                                    @endif

                                    @if ($order->both_colorful_black == '4')
                                        <?php $both_colorful_black .= PHP_EOL . $order->meishi_card_type; ?>
                                    @endif

                                    @if ($order->both_black_black == '5')
                                        <?php $both_black_black .= PHP_EOL . $order->meishi_card_type; ?>
                                    @endif
                                @endif

                                @if ($order->meishi_design_pattern != 'Array')
                                    @if ($order->side_colorful == '1')
                                        <?php $side_colorful .= PHP_EOL . $order->meishi_design_pattern; ?>
                                    @endif

                                    @if ($order->side_black == '2')
                                        <?php $side_black .= PHP_EOL . $order->meishi_design_pattern; ?>
                                    @endif

                                    @if ($order->both_colorful_colorful == '3')
                                        <?php $both_colorful_colorful .= PHP_EOL . $order->meishi_design_pattern; ?>
                                    @endif

                                    @if ($order->both_colorful_black == '4')
                                        <?php $both_colorful_black .= PHP_EOL . $order->meishi_design_pattern; ?>
                                    @endif

                                    @if ($order->both_black_black == '5')
                                        <?php $both_black_black .= PHP_EOL . $order->meishi_design_pattern; ?>
                                    @endif
                                @endif

                                @if ($order->meishi_design_pattern_back != 'Array')
                                    @if ($order->side_colorful == '1')
                                    @endif

                                    @if ($order->side_black == '2')
                                    @endif

                                    @if ($order->both_colorful_colorful == '3')
                                        <?php $both_colorful_colorful .= PHP_EOL . $order->meishi_design_pattern_back; ?>
                                    @endif

                                    @if ($order->both_colorful_black == '4')
                                        <?php $both_colorful_black .= PHP_EOL . $order->meishi_design_pattern_back; ?>
                                    @endif

                                    @if ($order->both_black_black == '5')
                                        <?php $both_black_black .= PHP_EOL . $order->meishi_design_pattern_back; ?>
                                    @endif
                                @endif


                                @if ($side_colorful != '')
                                    <?php $order->side_colorful = $side_colorful ?>
                                @endif
                                @if ($side_black != '')
                                    <?php $order->side_black = $side_black ?>
                                @endif
                                @if ($both_colorful_colorful != '')
                                    <?php $order->both_colorful_colorful = $both_colorful_colorful ?>
                                @endif
                                @if ($both_colorful_black != '')
                                    <?php $order->both_colorful_black = $both_colorful_black ?>
                                @endif
                                @if ($both_black_black != '')
                                    <?php $order->both_black_black = $both_black_black ?>
                                @endif
                                <tr>
                                    <td>
                                        <select name="status" class="form-control" onchange="changeStatus(this)"
                                                data-id="{{ $order->id }}">
                                            @foreach (Config::get('order.status') as $status_value => $status)
                                                <option {{ $status_value == $order->status ? 'selected="selected"' : null }} value="{{ $status_value }}">{{ $status }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        @if ($order->file_url)
                                            <a href="{{ $order->file_url }}" class="btn btn-primary" target="_blank">ダウンロード</a>
                                            <a onclick="return deleteAlert('ファイル削除')"
                                               href="{{ url('/admin/order/fileDelete/') . '/' . $order->id }}"
                                               class="btn btn-danger">ファイル削除</a>
                                        @endif
                                        <a onclick="return deleteAlert('データ削除')"
                                           href="{{ url('/admin/order') }}/delete/{{ $order->id }}"
                                           class="btn btn-danger">削除</a>
                                    </td>
                                    <td>
                                        <textarea id="comment{{ $order->id}}" class="ordercomment"
                                                  data-id="{{ $order->id }}"
                                                  name="comment">{{ $order->comment }}</textarea>
                                        <div class="commentbutton">
                                            <a id="changecomment{{ $order->id }}"
                                               onclick="return changeComment({{ $order->id }})"
                                               class="btn btn-info">保存</a>
                                        </div>
                                    </td>
                                    <td>{{ $order->created_at }}</td>
                                    <td>{{ $order->orderId }}</td>
                                    <td>{{ $order->meishi_company }}</td>
                                    <td>{{ $order->meishi_employees }}</td>
                                    <td>{{ $order->meishi_name }}</td>
                                    <td>{{ $order->meishi_kana }}</td>
                                    <td>{{ $order->meishi_post }}</td>
                                    <td>{{ $order->meishi_pref }}</td>
                                    <td>{{ $order->meishi_addr }}</td>
                                    <td>{{ $order->meishi_tel }}</td>
                                    <td>{{ $order->meishi_email }}</td>
                                    <td>{{ $order->meishi_use }}</td>
                                    <td>{{ $order->meishi_orders }}</td>
                                    <td>{{ $order->setNum }}</td>
                                    <td>
                                        <pre class="order_detail">{{ $order->side_colorful }}</pre>
                                    </td>
                                    <td>
                                        <pre class="order_detail">{{ $order->side_black }}</pre>
                                    </td>
                                    <td>
                                        <pre class="order_detail">{{ $order->both_colorful_colorful }}</pre>
                                    </td>
                                    <td>
                                        <pre class="order_detail">{{ $order->both_colorful_black }}</pre>
                                    </td>
                                    <td>
                                        <pre class="order_detail">{{ $order->both_black_black }}</pre>
                                    </td>
                                    {{--<td>{{ $order->buyPattern }}</td>--}}
                                    {{--<td>{{ $order->meishi_card_type }}</td>--}}
                                    {{--<td>{{ $order->meishi_design_pattern }}</td>--}}
                                    {{--<td>{{ $order->meishi_design_pattern_back }}</td>--}}
                                    <td>{{ $order->meishi_tmpnum }}</td>
                                    <td>{{ $order->ai }}</td>
                                    <td>{{ $order->file_url }}</td>
                                    <td>{{ $order->promo }}</td>
                                    <td>{{ $order->price_side_colorful }}</td>
                                    <td>{{ $order->price_side_black }}</td>
                                    <td>{{ $order->price_both_colorful_colorful }}</td>
                                    <td>{{ $order->price_both_colorful_black }}</td>
                                    <td>{{ $order->price_both_black_black }}</td>
                                    <td>{{ $order->knowPattern }}</td>
                                    <td>{{ $order->answer }}</td>
                                    <td>{{ $order->related_news }}</td>
                                    <td>{{ $order->pr_agree }}</td>
                                    <td>{{ $order->nouhin_company }}</td>
                                    <td>{{ $order->nouhin_name }}</td>
                                    <td>{{ $order->nouhin_kana }}</td>
                                    <td>{{ $order->nouhin_post }}</td>
                                    <td>{{ $order->nouhin_pref }}</td>
                                    <td>{{ $order->nouhin_addr }}</td>
                                    <td>{{ $order->meishi_note }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

                <div id="compactview" class="panel panel-default" style="display:none">
                    <div class="panel-heading"><h3>注文</h3></div>
                    <a style="margin-top: -61px; margin-right: 15px; float:right" class="btn btn-default"
                       onclick="changeMode()">詳細</a>
                    <div class="panel-body">
                        <table class="compacttable">
                            <thead>
                            <tr>
                                <th>注文ID</th>
                                <th>ステータス</th>
                                <th>会社名</th>
                                <th>ユーザー名</th>
                                <th>注文日時&nbsp;&nbsp;<a
                                            href="{{ url('/admin/order/compact') }}/changeSort?{{ $_SERVER['QUERY_STRING'] }}&mode=compact"
                                            class="btn btn-primary">↑↓</a></th>
                                <th>備考(管理者)</th>
                                <th>ファイル・データ</th>
                            </tr>
                            </thead>
                            @foreach ($orders as $order)
                                <tr>
                                    <td>{{ $order->orderId }}</td>
                                    <td>
                                        <select name="status" class="form-control" onchange="changeStatus(this)"
                                                data-id={{ $order->id }}>
                                            @foreach (Config::get('order.status') as $status_value => $status)
                                                <option {{ $status_value == $order->status ? 'selected="selected"' : null }} value="{{ $status_value }}">{{ $status }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>{{ $order->meishi_company }}</td>
                                    <td>{{ $order->meishi_name }}</td>
                                    <td>{{ $order->created_at }}</td>
                                    <td>
                                        <textarea id="compactcomment{{ $order->id}}" class="ordercomment"
                                                  data-id="{{ $order->id }}"
                                                  name="comment">{{ $order->comment }}</textarea>
                                        <div class="commentbutton">
                                            <a id="compactchangecomment{{ $order->id }}"
                                               onclick="return changeCompactComment({{ $order->id }})"
                                               class="btn btn-info">保存</a>
                                        </div>
                                    </td>
                                    <td>
                                        @if ($order->file_url)
                                            <a href="{{ $order->file_url }}" class="btn btn-primary" target="_blank">ダウンロード</a>
                                            <a onclick="return deleteAlert('ファイル削除')"
                                               href="{{ url('/admin/order/fileDelete/') . '/' . $order->id }}"
                                               class="btn btn-danger">ファイル削除</a>
                                        @endif
                                        <a onclick="return deleteAlert('データ削除')"
                                           href="{{ url('/admin/order') }}/delete/{{ $order->id }}"
                                           class="btn btn-danger">削除</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <!-- ページング -->
                <div class="bottompages">
                    {!! $orders->render() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- js -->
    <script>
        // statusの変更
        function changeStatus(object) {
            var value = $(object).val();
            var id = $(object).data('id');
            location.href = "{{ url('/admin/order') }}/changeStatus/" + id + "?status=" + value;
        }
        ;
        function changeComment(id) {
            var value = $('#comment' + id).val();
            var str = value.replace(/(?:\r\n|\r|\n)/g, '%0A');
            location.href = "{{ url('/admin/order') }}/changeComment/" + id + "?comment=" + str;
        }
        ;
        function changeCompactComment(id) {
            var value = $('#compactcomment' + id).val();
            var str = value.replace(/(?:\r\n|\r|\n)/g, '%0A');
            location.href = "{{ url('/admin/order') }}/changeComment/" + id + "?comment=" + str;
        }
        ;
        function changeView() {
            if ($("#viewMode").val() === 'full') {
                $("#fullview").show();
                $("#compactview").hide();
            } else if ($("#viewMode").val() === 'compact') {
                $("#fullview").hide();
                $("#compactview").show();
            } else {
                $("#compactview").show();
                $("#viewMode").val('compact');
            }
        }
        ;

        function changeMode() {
            if ($("#viewMode").val() === 'full') {
                $(".toppages .pagination > li").each(function (index) {
                    if ($(this).find("a").attr('href')) {
                        var href = $(this).find("a").attr('href');
                        var newpage = href.substr(0, href.indexOf("mode"));
                        var newhref = newpage + "mode=compact&page=" + (index);
                        $(this).find("a").prop('href', newhref);
                    }
                });
                $(".bottompages .pagination > li").each(function (index) {
                    if ($(this).find("a").attr('href')) {
                        var href = $(this).find("a").attr('href');
                        var newpage = href.substr(0, href.indexOf("mode"));
                        var newhref = newpage + "mode=compact&page=" + (index);
                        $(this).find("a").prop('href', newhref);
                    }
                });
                $("#fullview").hide();
                $("#compactview").show();
                $("#viewMode").val('compact');
            } else if ($("#viewMode").val() === 'compact') {
                $(".toppages .pagination > li").each(function (index) {
                    if ($(this).find("a").attr('href')) {
                        var href = $(this).find("a").attr('href');
                        var newpage = href.substr(0, href.indexOf("mode"));
                        var newhref = newpage + "mode=full&page=" + (index);
                        $(this).find("a").prop('href', newhref);
                    }
                });
                $(".bottompages .pagination > li").each(function (index) {
                    if ($(this).find("a").attr('href')) {
                        var href = $(this).find("a").attr('href');
                        var newpage = href.substr(0, href.indexOf("mode"));
                        var newhref = newpage + "mode=full&page=" + (index);
                        $(this).find("a").prop('href', newhref);
                    }
                });
                $("#fullview").show();
                $("#compactview").hide();
                $("#viewMode").val('full');
            }
        }

        // ファイル削除のアラート
        function deleteAlert(string) {
            var answer = confirm(string + 'してもよろしいですか?');
            if (!answer) {
                return false;
            }
        }

        $(function () {
            changeView();
            // datepicker初期値
            $(".datepicker, .datepicker2").datepicker({
                'dateFormat': 'yy-mm-dd',
                onSelect: function () {
                    $('.datepicker2').datepicker('option', {
                        minDate: $(this).datepicker('getDate')
                    });
                }
            });
        });
    </script>
    <style>
        pre.order_detail {
            background-color: transparent;
            border: none;
            border-radius: 0;
        }
    </style>
@endsection
