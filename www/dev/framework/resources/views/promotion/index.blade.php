@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{ url('/admin/promotion') }}" method="GET" class="" role="search">
                        <div class="form-group navbar-form">
                            <label for="keyword">キーワード&nbsp;&nbsp;</label>
                            <input id="keyword" class="form-control" type="text" name="keyword" value="{{ $keyword }}" style="width: 360px" placeholder="プロモーションコード, 顧客名">
                        </div>
                        <div class="form-group navbar-form">
                            <label for="term">有効期限&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <input id="term" class="datepicker form-control" type="text" name="startDate" value="{{ $startDate }}">&nbsp;~&nbsp;<input class="datepicker2 form-control" type="text" name="endDate" value="{{ $endDate }}">
                        </div>
                        <br>
                        <input type="submit" value="検索" class="btn btn-default">
                    </form>
                </div>
            </div>

            <!-- ページング -->
            <div>
                <form action="{{ url('/admin/promotion/create') }}" method="GET" class="">
                    <input type="submit" value="新規登録" class="btn btn-info">
                </form>
            </div>
            <br />
            {!! $promotions->render() !!}
            @if(Session::has('message'))
            <p class="alert alert-info">{{ Session::get('message') }}</p>
            @endif
            @if(Session::has('error'))
            <p class="alert alert-danger">{{ Session::get('message') }}</p>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th>顧客名</th>
                            <th>プロモーションコード</th>
                            <th>開始</th>
                            <th>終了</th>
                            <th>有効/無効</th>
                        </tr>
@foreach ($promotions as $promotion)
                        <tr>
                            <td>{{ $promotion->customer_name }}</td>
                            <td>{{ $promotion->code }}</td>
                            <td>{{ $promotion->start_date == '0000-00-00' ? '期限なし' : $promotion->start_date }}</td>
                            <td>{{ $promotion->end_date == '0000-00-00' ? '期限なし' : $promotion->end_date }}</td>
                            <td>{{ $promotion->enabled == '0' ? '無効' : '有効' }}</td>
                            <td>
                                <a href="{{ url('/admin/promotion') }}/{{ $promotion->id }}/edit/" class="btn btn-info">変更</a>
                            </td>
                            <td>
                                <form action="{{ url('/admin/promotion') }}/{{ $promotion->id }}" method="POST" class="">
                                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                   <input name="_method" type="hidden" value="DELETE">
                                   <input onclick="return deleteAlert('データ削除')" type="submit" value="削除" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
@endforeach
                    </table>
                </div>
            </div>
            <!-- ページング -->
            {!! $promotions->render() !!}
        </div>
    </div>
</div>
<!-- js -->
<script>
// ファイル削除のアラート
function deleteAlert(string) {
    var answer = confirm(string + 'してもよろしいですか?');
    if (! answer) {
        return false;
    }
}

$(function() {
    // datepicker初期値
    $(".datepicker, .datepicker2").datepicker({
        'dateFormat': 'yy-mm-dd',
        onSelect: function () {
            $('.datepicker2').datepicker('option', {
                minDate: $(this).datepicker('getDate')
            });
        }
    });
});
</script>
@endsection