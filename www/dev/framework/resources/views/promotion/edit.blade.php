@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">プロモーションコード - 更新</div>
                <div class="panel-body">
                    <form action="{{ url('/admin/promotion/' . $promotion->id ) }}" method="POST" class="">
                        <input name="_method" type="hidden" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $promotion->id }}">
                        <div class="leftcol">
                            <label for="customer_name">顧客名</label><br /><br />
                            <label for="code"><span style="color:red;">[必須]</span>プロモーションコード</label><br />
                            <label style="font-weight: bold;margin-top: 20px;margin-bottom: 10px;">有効期限</label><br /><br />
                            <label for="start_date">開始</label><br /><br />
                            <label for="end_date">終了</label><br /><br /><br />
                            <label for="enabled"><span style="color:red;">[必須]</span>有効/無効</label><br /><br />
                            <label for="comment">備考</label>
                        </div>
                        <div class="rightcol">
                            <input id="customer_name" class="form-control createform" type="text" name="customer_name" value="{{ $promotion->customer_name }}">
                            <input id="code" class="form-control createform" type="text" name="code" value="{{ $promotion->code }}">
                            <br />
                            <br />
                            <br />
                            <input id="start_date" class="datepicker form-control" type="text" name="start_date" value="{{ $promotion->start_date == '0000-00-00' ? '' : $promotion->start_date }}">
                            <br />
                            <input id="end_date" class="datepicker2 form-control" type="text" name="end_date" value="{{ $promotion->end_date == '0000-00-00' ? '' : $promotion->end_date }}">
                            <br />
                            <input type="radio" name="enabled" value="1" {{ $promotion->enabled == '1' ? 'checked' : '' }}>有効&nbsp;&nbsp;<input type="radio" name="enabled" value="0" {{ $promotion->enabled == '0' ? 'checked' : '' }}>無効
                            <br />
                            <br />
                            <textarea id="comment" class="commentbox" name="comment" >{{ $promotion->comment }}</textarea>
                            <br />
                            <br />
                            <input type="submit" value="保存" class="btn btn-info">
                            <a href="{{ URL::previous() }}" class="btn btn-info">キャンセル</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(function() {
    // datepicker初期値
    $(".datepicker, .datepicker2").datepicker({
        'dateFormat': 'yy-mm-dd',
        onSelect: function () {
            $('.datepicker2').datepicker('option', {
                minDate: $(this).datepicker('getDate')
            });
        }
    });
});
</script>
@endsection