<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'o>]zwu+!3jNoELRk*)H=ipL4%0n.(>`m;WKOD1wIKV5<mH|HU9p$-z!/7|TC](L;');
define('SECURE_AUTH_KEY',  'r+%ZQb)j_G)zdYM@pE2g6Jwni{wx!KOWyt7H4<?`~94T?.!V0QxEkqiwmk!$F6.t');
define('LOGGED_IN_KEY',    'O[C|{<<]KG6z?n`sH,.k2G{@ABes{:q$(TMfZ0(u;%?j74]};xUZ%n~dG{H|u$L=');
define('NONCE_KEY',        'cjc|QV s6-O 8kc^:q,-F_zwd|rJ;|s|lW7)A:0D0&~-.Cg<-Th+a{Ai=N!3)vi>');
define('AUTH_SALT',        '(1CBX @)C-E5cE|9T2<.P8[B00cr{@^O-8s|r]hF> %iA^Z/xBF$nVv OxR`$KAc');
define('SECURE_AUTH_SALT', '+E>u@=mwrm-`PE%hrF,h#t}/ +,b<qi+4(v- VyBWtTRc@mpT,T=@h#9j!QWPkLf');
define('LOGGED_IN_SALT',   'O`iu?QoSq]d]/(Lgxf}wPo`xR;ltv-]@P?8@AOV`20D*0JIMblV!,E>N!|$Q|A+4');
define('NONCE_SALT',       'csquNPp>6%h_;CRZk__=#&m!:(#.Yv?ca]=Z7c^}~q9[01_:O+ -(C3&EkTkViNo');


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'JETPACK_DEV_DEBUG', True );
define( 'WP_DEBUG', True );
define( 'FORCE_SSL_ADMIN', False );
define( 'SAVEQUERIES', False );

// Addtional PHP code in the wp-config.php
// These lines are inserted by VCCW.
// You can place addtional PHP code here!


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
