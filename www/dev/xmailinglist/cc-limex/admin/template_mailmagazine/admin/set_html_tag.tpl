        <div id="page_explain">
            <h3>◆自動登録機能</h3>
            <p>
            空メールを送信することで自動入会が可能となるメールアドレスと、入会・退会に用いるフォームのHTMLコードを下記に掲載しております。<br>
            ホームページやブログ等のWebページに、それぞれ貼り付けてご利用ください。
            </p>
        </div>
        <!-- /page_explain -->


        <div class="inner">
            <h4>空メール自動登録用メールアドレス</h4>
            <p>本メールアドレス宛に空メールを送信していただくことで、送信元のメールアドレスを自動的にユーザー登録することができます</p>
            <div id="search_section" class="section">
                <strong>{$auto_apply_mail}</strong>
            </div>

            <h4>登録用フォーム</h4>
            <p>
            登録用フォームを表示する設置タグです。<br>
            下記の設置タグをWebページに貼り付けることで、登録用フォームを設置できます。
            </p>
            <table>
                <tr>
                    <td>
                        <strong>登録用フォームのサンプル</strong><br />
                        <input type="text" /><input type="button" value="登録する" /><br />
                    </td>
                </tr>
                <tr>
                    <td class="tag_field">
                        <strong>登録用フォームの設置タグ</strong><br />
                        <textarea readonly onclick="this.select();">
&lt;script&gt;
function MLFormSubmitOnlyIn( strButton ){
var obj;obj = window.open('{$action_url}','tml_form','width=400,height=300,menubar=no,toolbar=no');document.ml_form_only_in.target = 'tml_form';document.ml_form_only_in.sb_reg.value = strButton;
org = document.charset;document.charset = 'UTF-8';document.ml_form_only_in.submit();document.charset = org;
}
&lt;/script&gt;
&lt;form name="ml_form_only_in" id="ml_form_only_in" method="post" action="{$action_url}?page=MailReg" accept-charset="UTF-8"&gt;
    &lt;input type="text" name="add_mail" /&gt;
    &lt;input type="button" value="登録する" onClick="MLFormSubmitOnlyIn('登録する');" /&gt;&lt;br /&gt;
    &lt;input type="hidden" name="sb_reg" value="" /&gt;&lt;br /&gt;
    &lt;input type="hidden" name="identity" value="{identity}" /&gt;
&lt;/form&gt;</textarea>
                    </td>
                </tr>
            </table>

            <br />

            <h4>退会用フォーム</h4>
            <table>
                <tr>
                    <td>
                        <strong>退会用フォームのサンプル</strong><br />
                        <input type="text" /><input type="button" value="退会する" /><br />
                    </td>
                </tr>
                <tr>
                    <td class="tag_field">
                        <strong>退会用フォームの設置タグ</strong><br />
                        <textarea readonly onclick="this.select();">
&lt;script&gt;
function MLFormSubmitOnlyOut( strButton ){
var obj;obj = window.open('{$action_url}','tml_form','width=400,height=300,menubar=no,toolbar=no');document.ml_form_only_out.target = 'tml_form';document.ml_form_only_out.sb_rel.value = strButton;
org = document.charset;document.charset = 'UTF-8';document.ml_form_only_out.submit();document.charset = org;
}
&lt;/script&gt;
&lt;form name="ml_form_only_out" id="ml_form_only_out" method="post" action="{$action_url}?page=MailRel" accept-charset="UTF-8"&gt;
    &lt;input type="text" name="delete_mail" /&gt;
    &lt;input type="button" value="退会する" onClick="MLFormSubmitOnlyOut('退会する');" /&gt;&lt;br /&gt;
    &lt;input type="hidden" name="sb_rel" value="" /&gt;&lt;br /&gt;
    &lt;input type="hidden" name="identity" value="{identity}" /&gt;
&lt;/form&gt;</textarea>
                    </td>
                </tr>
            </table>

            <br />

            <h4>登録・退会用フォーム</h4>
            <table>
                <tr>
                    <td>
                        <strong>登録・退会用フォームのサンプル</strong><br />
                        <input type="text" /><input type="button" value="登録する" /><br />
                        <input type="text" /><input type="button" value="退会する" /><br />
                    </td>
                </tr>
                <tr>
                    <td class="tag_field">
                        <strong>登録・退会用フォームの設置タグ</strong><br />
                        <textarea readonly onclick="this.select();">
&lt;script&gt;
function MLFormSubmitReg( strButton ){
var obj;obj = window.open('{$action_url}','tml_form','width=400,height=300,menubar=no,toolbar=no');document.ml_form.target = 'tml_form';document.ml_form.action='{$action_url}?page=MailReg';document.ml_form.sb_reg.value = strButton;
org = document.charset;document.charset = 'UTF-8';document.ml_form.submit();document.charset = org;
}
function MLFormSubmitRel( strButton ){
var obj;obj = window.open('{$action_url}','tml_form','width=400,height=300,menubar=no,toolbar=no');document.ml_form.target = 'tml_form';document.ml_form.action='{$action_url}?page=MailRel';document.ml_form.sb_rel.value = strButton;
org = document.charset;document.charset = 'UTF-8';document.ml_form.submit();document.charset = org;
}
&lt;/script&gt;
&lt;form name="ml_form" id="ml_form" method="post" action="{$action_url}" accept-charset="UTF-8"&gt;
    &lt;input type="text" name="add_mail" /&gt;
    &lt;input type="button" value="登録する" onClick="MLFormSubmitReg('登録する');" /&gt;&lt;br /&gt;
    &lt;input type="text" name="delete_mail" /&gt;
    &lt;input type="button" value="退会する" onClick="MLFormSubmitRel('退会する');" /&gt;&lt;br /&gt;
    &lt;input type="hidden" name="sb_reg" value="" /&gt;&lt;br /&gt;
    &lt;input type="hidden" name="sb_rel" value="" /&gt;&lt;br /&gt;
    &lt;input type="hidden" name="identity" value="{identity}" /&gt;
&lt;/form&gt;</textarea>
                    </td>
                </tr>
            </table>

            <br />

        </div>

        <!-- /inner -->
