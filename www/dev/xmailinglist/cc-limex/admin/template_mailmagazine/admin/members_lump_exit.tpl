        <div id="page_explain">
            <h3>◆ユーザー管理 - ユーザーの一括登録・登録確認</h3>
            <p>メールマガジンのユーザーを一括で登録または登録確認メールを一括で送信できます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

            <h4>ユーザーの一括登録</h4>
            {message}<br />
            <br />
            <div class="back_link">
                <a href="./">[ユーザーの管理に戻る]</a>
            </div>

        </div>
        <!-- /inner -->
