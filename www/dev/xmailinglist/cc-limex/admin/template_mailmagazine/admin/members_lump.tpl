        <div id="page_explain">
            <h3>◆ユーザー管理 - ユーザーの一括登録・登録確認</h3>
            <p>メールマガジンのユーザーを一括で登録または登録確認メールを一括で送信できます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">
            <div class="section">
                <h4>ユーザーの一括登録</h4>
                <div class="error_txt">
                    {$error_txt}
                </div>
                <form method="post" action="./?page=MembersAddLump">
                
                    <table class="menu_table">
                        <tr>
                            <th>メールアドレス,メモ</th>
                            <td class="lump_field">
                                ※1行あたり1件ずつ（メールアドレスとメモをカンマ区切りで）入力して、改行後次のデータを入力して下さい。<br />
                                  例．taro@test.xsrv.jp,メモ
                                <textarea name="user_mail_lump">{$user_mail_lump}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>登録方法</th>
                            <td>
                                <select name="register_mode">
                                    <option value="1">今すぐ登録して登録完了通知を送信する</option>
                                    <option value="2">今すぐ登録して登録完了通知を送信しない</option>
                                    <option value="3">登録用URLを送信し、ユーザの了解を得て登録する</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="txt_center">
                                <input type="submit" name="sb_ml_member_add" value="メールマガジンに登録する" {disabled_1}/>
                                </div>
                            </td>
                        </tr>
                    </table>
                
                </form>
            </div>
            <!-- /section -->

            <a href="./">[ユーザーの管理に戻る]</a>

        </div>
        <!-- /inner -->
