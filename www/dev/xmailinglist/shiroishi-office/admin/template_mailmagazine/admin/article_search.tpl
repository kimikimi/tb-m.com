        <div id="page_explain">
            <h3>◆配信済みの情報</h3>
            <p>これまでの配信したメールマガジンを確認できます。</p>
        </div>
        <!-- /page_explain -->

            <div class="inner">

                <div id="search_section" class="section">
                    <form method="post" action="./?page=ArticleSearch">
                        <strong>検索</strong>
                        <input type="text" name="search_word" size="60" value="{$search_word}" />
                        <input type="submit" name="submit_button" value="検索" />
                    </form>
                </div>
                <!-- /section -->

                <div class="section">
                    <h4>過去記事(メール)の検索結果「検索文字：{$search_word}」</h4>
                    <div class="view_mail">
                    {$ml_maildata}
                    </div>
                    <!-- /view_mail -->
                    <br />
                </div>
                <!-- /section -->
                
            </div>
            <!-- /inner -->
