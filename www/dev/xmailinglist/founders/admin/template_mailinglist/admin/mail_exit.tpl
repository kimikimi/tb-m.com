        <div id="page_explain">
            <h3>◆メール作成</h3>
            <p>メンバー宛にメール送信を行うことができます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

            <h4>メール作成</h4>
            メールを送信しました。<br />
            <br />
            <div class="back_link">
                <a href="./?page=MailCreate">[メールの作成に戻る]</a>
            </div>

        </div>
        <!-- /inner -->
