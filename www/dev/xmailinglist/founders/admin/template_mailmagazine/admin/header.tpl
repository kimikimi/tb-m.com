<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css;" />
<title>エックスサーバー/メールマガジン/{$ml_app_name}</title>
<link type="text/css" rel="stylesheet" href="css/layout_mm.css">
<script type="text/javascript" src="./js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.13.custom.min.js"></script>
<script type="text/javascript" src="./js/common_func.js"></script>
<script type="text/javascript" src="./js/design_func.js"></script>

<script>
function mailDisable(){
    if($("input[name=ml_transmit_val]").prop("checked")){
            $("input[name=ml_moderators]").removeAttr("disabled");
    }
    else{
            $("input[name=ml_moderators]").attr("disabled", "disabled");
    }
}
function mMailDisable(){
    if($("input[name=ml_limit_send]").prop("checked")){
            $("input[name=mm_moderators]").removeAttr("disabled");
    }
    else{
            $("input[name=mm_moderators]").attr("disabled", "disabled");
    }
}
$(document).ready(function(){
    $("input[name=ml_transmit_val]").click(mailDisable);
    $("input[name=ml_limit_send]").click(mMailDisable);
    mailDisable();
    mMailDisable();
});
</script>

</head>

<body id="{$select_index}">

<div id="contents_wrapper">

        <div id="header">
        <h1><a href="./">Xserver メールマガジン</a></h1>

        <ul id="header_navi">
            <li><a href="./">トップ</a></li>
            <li><a href="http://www.xserver.ne.jp/manual/man_mail_mailmagazine.php" target="blank">マニュアル</a></li>
            <li><a href="./?page=logout">ログアウト</a></li>
        </ul>
    </div>
    <!-- /header -->

    <div id="target_address">
        <strong>★{$ml_app_name}</strong>
    </div>
    <!-- /target_address -->

    <div id="main">

        <div id="main_navi">
                <ul>
                    <li id="main_navi_members"><a href="./">ユーザー管理</a></li>
                    <li id="main_navi_mail"><a href="./?page=MailCreate">メールマガジンの配信</a></li>
                    <li id="main_navi_news"><a href="./?page=Article">配信済みの情報</a></li>
                    <li id="main_navi_errmail"><a href="./?page=ErrMailList">配信エラー管理</a></li>                    
                    <li id="main_navi_option"><a href="./?page=Option">環境設定</a></li>
                    <li id="main_navi_file"><a href="./?page=EditSystemMail">システムメール</a></li>
                    <li id="main_navi_html" class="list_end"><a href="./?page=SetHtmlTag">自動登録機能</a></li>
                </ul>
        </div>
        <div style="color:#EE0000">
            <noscript>
                    本ツールは、Javascriptを利用しております。
                    お手数ですが、JavascriptをONにして再読み込みしてください。
            </noscript>
        </div>
        <!-- /main_navi -->

