        <div id="page_explain">
            <h3>◆配信済みメール</h3>
            <p>これまでのメールの一覧を閲覧できます。</p>
        </div>
        <!-- /page_explain -->

            <div class="inner">

                <div id="search_section" class="section">
                    <form method="post" action="./?page=ArticleSearch">
                        <strong>検索</strong>
                        <input type="text" name="search_word" size="60" value="{$search_word}" />
                        <input type="submit" name="submit_button" value="検索" />
                    </form>
                </div>
                <!-- /section -->

                <div class="section">
                    <h4>メールの検索結果「検索文字：{$search_word}」</h4>
                    {$ml_maildata}
                    <br />
                </div>
                <!-- /section -->
                
            </div>
            <!-- /inner -->
