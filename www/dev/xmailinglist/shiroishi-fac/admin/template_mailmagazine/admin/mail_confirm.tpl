        <div id="page_explain">
            <h3>◆メールマガジンの配信</h3>
            <p>メンバー宛に新規メールマガジンの配信を行うことができます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

        <form name="mail_menu" method="post" action="./?page=MailDo">

            <h4>メールマガジンの確認</h4>
            <table class="mail_table">
                <tr>
                    <th>件名</th>
                    <td>
                        <span>{$subject_txt}</span>
                        {$mail_subject}
                        <input type="hidden" name="mail_subject" value="{$mail_subject}" />
                    </td>
                </tr>
                <tr>
                    <th>メール本文</th>
					<td>
					    <textarea name="mail_contents" style="height:500px;border:0;" readonly>{$header_txt}{$mail_contents}{$footer_txt}</textarea>
					</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="button_box">
							<input type="hidden" name="header_txt" value="{$header_txt}" />
							<input type="hidden" name="mail_contents" value="{$mail_contents}" />
							<input type="hidden" name="footer_txt" value="{$footer_txt}" />
							<input type="hidden" name="send_contents" value="{$send_contents}" />

                            <INPUT type="submit" id="exe_submit_button1" name="submit_button" value="戻る" onclick="ReturnNow(this);">
                            <INPUT type="submit" id="exe_submit_button2" name="sb_mail_send" value="メールマガジンを配信する" onclick="ExeNow(this);">
                        </div>
                        <!-- /button_box -->
                    </td>
                </tr>
            </table>

        </form>

        </div>
        <!-- /inner -->
