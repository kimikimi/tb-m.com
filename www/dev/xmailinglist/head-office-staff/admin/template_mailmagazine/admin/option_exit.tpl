        <div id="page_explain">
            <h3>◆環境設定</h3>
            <p>メールマガジンに関する各種設定を行うことができます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

            <h4>環境設定</h4>
            環境設定を保存しました。<br />
            <br />
            <div class="back_link">
                <a href="./?page=Option">[環境設定に戻る]</a>
            </div>

        </div>
        <!-- /inner -->
