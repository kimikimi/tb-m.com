        <div id="page_explain">
            <h3>◆環境設定</h3>
            <p>メールマガジンに関する各種設定を行うことが出来ます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

        <form name="option_menu" method="post" action="./?page=OptionDo">

            <h4>環境設定</h4>
            <div class="error_txt">
                {$error_txt}
            </div>
            <table class="menu_table">
                <tr>
                    <th>メールマガジン名</th>
                    <td><input type="text" name="ml_app_name" value="{$ml_app_name}"  style="width:30em" />&nbsp;30文字以内<br>
                        本メールマガジンの名称を設定できます。
                    </td>
                </tr>
                <tr>
                    <th>送信者名</th>
                        <td><input type="text" name="mm_from_name" value="{$mm_from_name}"  style="width:20em" />&nbsp;20文字以内<br>
                            メールマガジンの送信者名を設定できます。
                        </td>
                    </tr>
                <tr>
                <th>配信ナンバーの設定</th>
                    <td>
                     <input type="text" name="auto_number" value="{$auto_number}"/>
                        <br />メールマガジンの通し番号である配信ナンバーを設定できます。
                        <br />配信ナンバーは、メールマガジンが配信される度に1ずつ加算されます。
                    </td>
                </tr>
                <th>メールマガジン件名</th>
                    <td>
                     <input type="text" name="mm_title_name" value="{$mm_title_name}" style="width:320px" />&nbsp;60文字以内<br>
                        メールマガジンの件名の初期値を設定できます。
                        <br />配信日時を設定したい場合、次の雛形を記入することで配信日時が挿入されます。
                        <br />配信ナンバーを設定したい場合、次の雛形を記入することで配信ナンバーが挿入されます。
                        <br />例.『{year}年{month}月{day}日 』&nbsp;⇒&nbsp;『{$year}年{$month}月{$day}日 』
                        <br />&nbsp;&nbsp;&nbsp;&nbsp;『{auto_number}回目 』&nbsp;⇒&nbsp;『{$auto_number}回目 』
                    </td>
                </tr>
                <tr>
                    <th>ヘッダーの設定</th>
                    <td>
                        <textarea name="ml_add_header" rows="6" cols="72">{$ml_add_header}</textarea>
                        <br />入力された内容をメール本文の文頭に追加するよう設定できます。
                        <br />配信日時を設定したい場合、次の雛形を記入することで配信日時が挿入されます。
                        <br />例．『{year}年{month}月{day}日 』&nbsp;⇒&nbsp;『{$year}年{$month}月{$day}日 』
                    </td>
                </tr>
                <tr>
                    <th>フッターの設定</th>
                    <td>
                        <textarea name="ml_add_footer" rows="6" cols="72">{$ml_add_footer}</textarea>
                        <br />入力された内容をメール本文の文末に追加されるよう設定できます。
                        <br />退会用のURLを設定したい場合、次の雛形を利用することで退会用フォームのURLが挿入されます。
                        <br />例．『###退会用URL###』&nbsp;⇒&nbsp;『{$withdraw_url}』
                    </td>
                </tr>
                <tr>
                    <th>メールマガジンの返信先<br />(Reply-to )</th>
                    <td>
                        <input type="text" name="mm_reply_to" value="{$mm_reply_to}" style="width:300px" />
                        <br />メールマガジンの返信先アドレスを設定できます。
                    </td>
                </tr>
                <tr>
                    <th>システム・エラーメール<br />受信設定</th>
                    <td style="padding-left:8px;">
                    <label><input type="checkbox" name="ml_transmit_val" {$ml_transmit_checked} onclick="mailDisable()"/>&nbsp;受信する<br></label>
                        受信用メールアドレス: &nbsp;<input type="text" name="ml_moderators" value="{$ml_moderators}" style="width:300px" />
                        <br />[受信する]に設定した場合は、メールマガジン配信時に発生したシステム・エラーの内容を受信用メールアドレス宛に送信します。<br />
                    </td>
                </tr>
                <tr>
                    <th>管理ツール以外からの<br>配信設定</th>
                    <td>
                        <label>
                            <input id="ml_limit_send" type="checkbox" name="ml_limit_send" {$ml_limit_send} onclick="mMailDisable()"/>&nbsp;
                            有効にする
                        </label>
                        <br />送信用メールアドレス: &nbsp;<input type="text" name="mm_moderators" value="{$mm_moderators}" style="width:300px" />

                        <br />[有効にする]に設定した場合は、普段ご利用になっているメールアドレスから
                        <br />メールマガジンを配信することができます。
                        <br />送信用メールアドレスからメールマガジンの配信用メールアドレスにメールを送信して下さい。
                        <br />その後、配信確認のメールを承認する事でメールマガジンの配信が完了となります。
                        <br />※使い慣れたメールアプリからHTMLメール形式でのメールマガジンの配信が可能となります。
                        <br />
                        <br /><span style="font-weight:bold;">配信用メールアドレス&nbsp;:&nbsp;{$ml_mailaddress}</span>

                        <p class="caution" style="color:#ff3333">
                        <br >配信時には必ず、配信が完了したかどうかを確認して下さい。
                        <br >配信が完了した場合は、送信用メールアドレスに件名が
                        <br >「moderated article[xxxx.......]」のメールが配信完了通知として届きます。
                        <br >※ご利用前に一度テスト配信してみて下さい。
                        </p>
                        <script>
                            function clickLimitSend(){
                                if($("#ml_limit_send").prop("checked")){
                                    $(".caution").show();
                                }else{
                                    $(".caution").hide();
                                }
                            }
                            $("#ml_limit_send").unbind().click(function (){clickLimitSend();});
                            clickLimitSend();
                         </script>
                    </td>
                </tr>
            </table>

            <div class="button_box">
                <input type="submit" name="sb_setting_save" value="設定を保存する" />
            </div>
            <!-- /button_box -->

        </form>

        </div>
        <!-- /inner -->

