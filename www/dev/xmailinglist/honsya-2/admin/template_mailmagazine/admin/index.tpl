        <div id="page_explain">
            <h3>◆ユーザー管理</h3>
            <p>メールマガジンの配信先ユーザーの登録・変更・削除などを行うことができます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

            <div class="section">
                <h4>ユーザー一覧(参加人数：{$user_num}名)
                <div class="max_txt">
                    {MaxCount}
                </div>
                </h4>
                <table class="users_table" id="members_table">
                    <tr>
                        <th>メールアドレス</th>
                        <th>メモ</th>
                        <th>設定変更</th>
                        <th>削除</th>
                    </tr>
                    {$user_list}
                </table>
                
                <div class="pagelink">
                    {$page_link}
                </div>
                
                <div class="button_box" style="{disp_ml_member_del_all};margin: 5px;">
                    <form action="./?page=MembersDeleteAll" method="post">
                        <input type="submit" name="sb_ml_member_del_all" value="ユーザーの一括削除" onclick="return confirm('メールアドレスを全て削除します。\r\nよろしいですか？');"/>
                    </form>
                </div>
                
                <h5>■登録ユーザーの一覧を表示する</h5>
                <p>
                    <a {members_list_link} >ユーザーの一覧表示はこちら</a>
                </p>
            </div>
            <!-- /section -->

            <form method="post" action="./?page=MembersAdd">
            <div class="section">
                
                <h4>ユーザーの登録</h4>
                <h5>■一人ずつユーザーに登録する</h5>
                <div class="error_txt">
                    {$error_txt}
                </div>
                <table class="menu_table">
                    <tr>
                        <th>メールアドレス</th>
                        <td><input name="user_mail" type="text" style="width:320px" value="{$user_mail}" /></td>
                    </tr>
                    <tr>
                        <th>メモ</th>
                        <td>
                            <input name="user_memo" type="text" style="width:320px" value="{$user_memo}" />
                            <br />入力内容の指定はありません。メールアドレスの利用者名を設定するなど、ご自由にお使いください。
                        </td>
                    </tr>
                    <tr>
                        <th>登録方法</th>
                        <td>
                            <select name="register_mode">
                                <option value="1">今すぐ登録して登録完了通知を送信する</option>
                                <option value="2">今すぐ登録して登録完了通知を送信しない</option>
                                <option value="3">購読用URLを送信し、ユーザの了解を得て登録する</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <div class="button_box">
                    <input type="submit" name="sb_ml_member_add" value="メールマガジンに登録する" {disabled_1}/>
                </div>

                <br />
<!--
                <p class="indent_ajust">※root、postmaster、MAILER-DAEMON、msgs、nobody、news、majordomo、listserv、listproc、ML名-help、ML名-subscribe、ML名-unsubscribe　は追加することができません。</p>
-->
                <h5>■複数人を一括でユーザーに登録する</h5>
                <p>
                    <a {lump_link} >ユーザーの一括登録はこちら</a>
                </p>
            </div>
            <!-- /section -->

            </form>
        </div>
        <!-- /inner -->
