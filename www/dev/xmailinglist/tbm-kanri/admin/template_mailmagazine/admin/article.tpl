        <div id="page_explain">
            <h3>◆配信済みの情報</h3>
            <p>これまでの配信したメールマガジンを確認できます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

            <div id="search_section" class="section">
                <form method="post" action="./?page=ArticleSearch">
                    <strong>検索</strong>
                    <input type="text" name="search_word" size="60" />
                    <input type="submit" name="submit_button" value="検索" />
                </form>
            </div>
            <!-- /section -->

            <div class="section">
                   
                <h4>配信済みメールマガジンの閲覧</h4>

                <div id="view_mail_list" class="view_mail">
                {$ml_maildata}
                </div>
                <!-- /view_mail -->

            </div>
            <!-- /section -->

        </div>
        <!-- /inner -->
