        <div id="page_explain">
            <h3>◆メンバーの管理 - メンバーの一括登録・入会確認</h3>
            <p>メーリングリストメンバーを一括で登録または入会確認メールを一括で送信できます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">
            <h4>メンバーの一括登録・入会確認</h4>
            {message}<br />
            <br />
            <div class="back_link">
                <a href="./">[メンバーの管理に戻る]</a>
            </div>

        </div>
        <!-- /inner -->
