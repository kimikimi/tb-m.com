        <div id="page_explain">
            <h3>◆公開設定</h3>
            <p>
                ユーザーへ公開するページである<a href="{$user_url}" target="_blank">公開ページ</a>に関する設定を行うことができます。<br>
                公開ページは、メーリングリストの入退会、および過去に配信されたメールである過去ログの閲覧ができます。
            </p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

        <form name="option_menu" method="post" action="./?page=ViewUserDo">

            <h4>公開設定</h4>
                <div class="error_txt">
                    {$error_txt}
                </div>
            <table class="menu_table">
                <tr>
                    <th>ページの公開</th>
                    <td>
                        <select name="panel_open" onchange="ViewPanelInfo();">
                            <option value="1" {$panel_open_1}>公開</option>
                            <option value="0" {$panel_open_0}>非公開</option>
                        </select><br>
                        [公開]に設定した場合は、メーリングリストのメンバー以外の第三者でもページの閲覧が可能となります。
                    </td>
                </tr>
            </table>

            <div id="userpanel_table" style="{$display_table}">

            <h4>詳細設定</h4>
            <table class="menu_table">
                <tr>
                    <th>公開ページのURL</th>
                    <td>
                        公開ページのURLを表示します。<br />
                        ユーザーの入退会やメール情報の共有場所としてご使用ください。<br />
                        <strong><a href="{$user_url}" target="_blank">{$disp_url}</a></strong><br />
                        <div style="color:#ff3333">
                        <p>SSLを利用する場合は、<a href="http://www.xserver.ne.jp/manual/man_mail_mailinglist.php#ml09">こちらのページ</a>で、SSLを利用した場合のURLを確認してください。</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>過去ログの閲覧機能</th>
                    <td>
                        <select name="panel_maillog" onchange="ViewLoginInfo();">
                            <option value="1" {$panel_maillog_1}>あり</option>
                            <option value="0" {$panel_maillog_0}>なし</option>
                        </select><br />
                        公開ページで過去ログを閲覧出来るようにするかどうかを設定します。<br>
                        [あり]に設定した場合は、公開ページの訪問者が過去ログを閲覧できるようになります
                    </td>
                </tr>
                <tr id="userpanel_login" style="{$display_login}">
                    <th style="width:170px;">
                        過去ログの保護パスワード
                    </th>
                    <td>
                        <label><input type="checkbox" name="pnael_log_protect" {pnael_log_protect_checked}> &nbsp;保護パスワードを設定する&nbsp;&nbsp;</label>
                        保護パスワード：<input type="password" name="panel_user_password" value="{$panel_user_password}" />
                        <br />
                        <p id="password_warning" style="color:#ff3333;margin-bottom:0">
                            保護パスワードを設定しない場合は、メーリングリストのメンバー以外の第三者でも過去ログを閲覧できます。
                        </p>
                        <p style="margin-bottom:0">
                            保護パスワードを設定する場合は、公開ページの利用者に保護パスワードを通知してください。
                        </p>
                    </td>
                </tr>
                <tr>
                    <th>入会用フォーム</th>
                    <td>
                        <select name="panel_apply">
                            <option value="1" {$panel_apply_1}>あり</option>
                            <option value="0" {$panel_apply_0}>なし</option>
                        </select><br>
                        公開ページで訪問者がメーリングリストに入会できるようにするかどうかを設定します。<br>
                        [あり]に設定した場合は、公開ページの訪問者がメーリングリストに入会できるようになります。
                    </td>
                </tr>
                <tr>
                    <th>退会用フォーム</th>
                    <td>
                        <select name="panel_withdraw">
                            <option value="1" {$panel_withdraw_1}>あり</option>
                            <option value="0" {$panel_withdraw_0}>なし</option>
                        </select><br>
                        公開ページでメーリングリストのメンバーが、メーリングリストから退会できるようにするかどうかを<br>設定します。<br>
                        [あり]に設定した場合は、参加メンバーがメーリングリストから退会できるようになります。
                    </td>
                </tr>
            </table>
            
            
            </div>

            <div class="button_box">
                <input type="submit" name="sb_setting_save" value="設定を保存する" />
            </div>
            <!-- /button_box -->

        </form>

        </div>
        <!-- /inner -->
