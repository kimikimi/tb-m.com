<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css;" />
<title>エックスサーバー/メーリングリスト/{$ml_app_name}</title>
<link type="text/css" rel="stylesheet" href="css/layout.css">
</head>

<body>

<div id="contents_wrapper">

        <div id="header">
        <h1><a href="./">Xserver メーリングリスト</a></h1>

        <ul id="header_navi">
            <li><a href="./">トップ</a></li>
            <li><a href="http://www.xserver.ne.jp/manual/man_mail_mailinglist.php" target="blank">マニュアル</a></li>
            <li><a href="./?page=logout">ログアウト</a></li>
        </ul>
    </div>
    <!-- /header -->

    <div id="target_address">
    </div>
    <!-- /target_address -->

    <div id="main">

        <div class="inner">

            <form name="main_menu" method="post" action="./">
            <div class="section">
                
                <h4>管理ツールログイン</h4>
                <table class="menu_table">
                        <tr>
                            <th scope="row">メーリングリストアドレス</th>
                            <td><input type="text" name="username" size="40" /></td>
                        </tr>
                        <tr>
                            <th scope="row">パスワード</th>
                            <td><input type="password" name="password" size="40" /></td>
                        </tr>
                        {DEBUG_MODE}
                    <tr>
                        <td colspan="2">
                            <div class="txt_center">
                                <input type="submit" name="login_button" value="ログイン" />
                            </div>
                        </td>
                    </tr>
                </table>
                {$error_txt}
            </div>
            <!-- /section -->

            </form>

        </div>
        <!-- /inner -->


    </div>
    <!-- /main -->

    <div id="footer">
        <address>Copyright &copy; <script type="text/javascript">NowYear();</script>XSERVER Inc. All Rights Reserved.</address>
    </div>
    <!-- /footer -->

</div>
<!-- /contents_wrapper -->

</body>
</html>

