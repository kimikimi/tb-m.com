        <div id="page_explain">
            <h3>◆配信エラー管理 - 詳細</h3>
            <p>エラーメールの発生日時を確認できます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">
            <div class="section">
                <h4>エラーメール詳細</h4>

                <table class="err_address_table">
                    <tr><th>メールアドレス</th><td>{$address}</td></tr>
                    <tr>
                        <td colspan="2">
                            <div class="txt_center">
                                <form name="member_delete" method="post" action="./?page=ErrMailList">
                                  <input type="hidden" name="now_systemmail" value="3">
                                  <input type="hidden" name="delete_mail[]" value="{$address}" />
                                  <input type="submit" name="sb_Address_delete" value="このアドレスをメンバーから削除する"
                                      onclick='return confirm("メールアドレスを削除します。\r\nよろしいですか？")' />
                                </form>
                            </div>
                        </td>
                    </tr>
                <table>

                <table class="err_detail_table" id="members_table">
                    <tr>
                        <th>発生日時</th>
                    </tr>
                    {$err_list}
                </table>
                <div class="pagelink">
                    {$page_link}
                </div>
                
                <div style="margin-top:30px; margin-bottom:20px">
                    <p>
                        ※エラーメールの発生日時は、メーリングリスト配信日時とは異なります。<br>
                        ※エラーメールの内容を本ツールから確認することはできません。<br>
                         &nbsp;&nbsp;エラーメールの詳細を確認されたい場合は、環境設定画面の[システム・エラーメール受信設定]を<br />
                          【受信する】にして【管理者メールアドレス】にて確認して下さい。
                    </p>
                </div>
                [<a href="#" onclick="document.back.submit()">エラーメール集計へ戻る</a>]
                <form name="back" method="post" action="./?page=ErrMailList">
                  <input type="hidden" name="now_systemmail" value="3">
                </form>
            </div>
            <!-- /section -->
        </div>
        <!-- /inner -->
