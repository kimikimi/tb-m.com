        <div id="page_explain">
            <h3>◆メールマガジンの配信</h3>
            <p>ユーザー宛に新規メールマガジンの配信を行うことができます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

            <h4>メールマガジンの新規作成</h4>
            メールを送信しました。<br />
            <br />
            <div class="back_link">
                <a href="./?page=MailCreate">[メールマガジンの配信に戻る]</a>
            </div>

        </div>
        <!-- /inner -->
