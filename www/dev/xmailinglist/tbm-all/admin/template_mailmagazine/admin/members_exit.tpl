        <div id="page_explain">
            <h3>◆ユーザー管理</h3>
            <p>メールマガジンのユーザーを登録・変更・削除などを行うことができます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

            <h4>ユーザーの登録</h4>
            {message}<br />
            <br />
            <div class="back_link">
                <a href="./">[ユーザーの管理に戻る]</a>
            </div>

        </div>
        <!-- /inner -->
