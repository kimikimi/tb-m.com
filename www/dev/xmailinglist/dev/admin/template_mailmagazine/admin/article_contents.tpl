        <div id="page_explain">
            <h3>◆配信済みの情報</h3>
            <p>これまで配送されたメールマガジンである過去ログを閲覧できます。</p>
        </div>
        <!-- /page_explain -->

            <div class="inner">

                <!-- /section -->


                <div class="section">

                    <h4>過去記事(メール)の閲覧</h4>
                    
                    <div id="search_section" class="section">
                        <form method="post" action="./?page=ArticleSearch">
                            <strong>キーワード</strong>
                            <input type="text" name="search_word" size="60" />
                            <input type="submit" name="submit_button" value="検索" />
                        </form>
                    </div>
                
                    <div id="view_mail_list" class="view_mail">
                    {$ml_maildata}
                    </div>
                    <!-- /view_mail -->
                    <br />
                </div>
                <!-- /section -->
                
            </div>
            <!-- /inner -->
