        <div id="page_explain">
            <h3>◆メールマガジンの配信</h3>
            <p>ユーザー宛に新規メールマガジンの配信を行うことができます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

        <form method="post" action="./?page=MailConfirm">

            <h4>メールマガジンの新規作成</h4>
            <div class="error_txt">
                {$error_txt}
            </div>
            <table class="mail_table">
                <tr>
                    <th>件名</th>
                    <td>
                        <span>{$subject_txt}</span>
                        <input type="text" name="mail_subject" style="width:320px" value="{$mail_subject}" />
                    </td>
                </tr>
                <tr>
                    <th>メール本文</th>
                    <td>
                        <div>
                            <textarea name="header_txt" style="width:630;height:80px;">{$header_txt}</textarea>
                            <textarea name="mail_contents">{$mail_contents}</textarea>
                            <textarea name="footer_txt" style="width:630;height:80px;">{$footer_txt}</textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="button_box">
                            <input type="hidden" name="send_contents" value="{$send_contents}" />
                            <input type="submit" name="sb_confirm" value="確認" />
                        </div>
                        <!-- /button_box -->
                    </td>
                </tr>
            </table>

        </form>

        </div>
        <!-- /inner -->
