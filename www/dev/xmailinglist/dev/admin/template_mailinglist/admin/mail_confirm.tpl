        <div id="page_explain">
            <h3>◆メール作成</h3>
            <p>メンバー宛にメール送信を行うことができます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

        <form name="mail_menu" method="post" action="./?page=MailDo">

            <h4>メール作成</h4>
            <table class="mail_table">
                <tr>
                    <th>件名</th>
                    <td>
                        <span>{$subject_txt}</span>
                        {$mail_subject}
                        <input type="hidden" name="mail_subject" value="{$mail_subject}" />
                    </td>
                </tr>
                <tr>
                    <th>メール本文</th>
                    <td>
                        <div>{$header_txt}</div>
                        {$mail_view_contents}
                        <div>{$footer_txt}</div>
                        <input type="hidden" name="mail_contents" value="{$mail_contents}" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="button_box">
                            <input type="submit" name="submit_button" value="戻る" />
                            <input type="submit" name="sb_mail_send" value="メールを送信する" />
                        </div>
                        <!-- /button_box -->
                    </td>
                </tr>
            </table>

        </form>

        </div>
        <!-- /inner -->
