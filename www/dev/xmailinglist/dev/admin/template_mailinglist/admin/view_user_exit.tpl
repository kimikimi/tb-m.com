        <div id="page_explain">
            <h3>◆公開設定</h3>
            <p>ユーザーへ公開するページに関する各種設定を行うことができます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

            <h4>ユーザー向けページの公開設定</h4>
            設定を保存しました。<br />
            <br />
            <div class="back_link">
                <a href="./?page=ViewUser">[公開設定に戻る]</a>
            </div>

        </div>
        <!-- /inner -->
