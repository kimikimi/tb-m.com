        <div id="page_explain">
            <h3>◆ユーザー管理 - ユーザーの一覧表示</h3>
            <p>メールマガジンのユーザーを一覧で表示します。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">
            <div class="section">
                <h4>ユーザーの一覧表示</h4>
                
                <table class="menu_table">
                    <tr>
                        <th>メールアドレス,メモ</th>
                        <td class="lump_field">
                            ※1行あたり1件ずつ（メールアドレスとメモをカンマ区切りで）表示しています。<br />
                              例．taro@test.xsrv.jp,メモ
                            <textarea name="user_mail_lump" readonly="readonly">{$user_mail_lump_actives}</textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /section -->

            <a href="./">[ユーザーの管理に戻る]</a>

        </div>
        <!-- /inner -->
