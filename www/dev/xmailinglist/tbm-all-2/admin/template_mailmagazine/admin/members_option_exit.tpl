        <div id="page_explain">
            <h3>◆ユーザーの管理 - メンバーの設定変更</h3>
            <p>ユーザーの設定を変更できます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">
            <h4>ユーザーの設定変更</h4>
            設定を変更しました。<br />
            <br />
            <div class="back_link">
                <a href="./">[メンバーの管理に戻る]</a>
            </div>

        </div>
        <!-- /inner -->
