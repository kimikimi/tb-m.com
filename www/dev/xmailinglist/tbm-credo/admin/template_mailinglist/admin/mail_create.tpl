        <div id="page_explain">
            <h3>◆メール作成</h3>
            <p>メンバー宛にメール送信を行うことができます。</p>
        </div>
        <!-- /page_explain -->

        <div class="inner">

        <form method="post" action="./?page=MailConfirm">

            <h4>メール作成</h4>
            <div class="error_txt">
                {$error_txt}
            </div>
            <table class="mail_table">
                <tr>
                    <th>件名</th>
                    <td>
                        <span>{$subject_txt}</span>
                        <input type="text" name="mail_subject" style="width:320px" value="{$mail_subject}" />
                    </td>
                </tr>
                <tr>
                    <th>メール本文</th>
                    <td>
                        <div>{$header_txt}</div>
                        <textarea name="mail_contents">{$mail_contents}</textarea>
                        <div>{$footer_txt}</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="button_box">
                            <input type="submit" name="sb_confirm" value="確認" />
                        </div>
                        <!-- /button_box -->
                    </td>
                </tr>
            </table>

        </form>

        </div>
        <!-- /inner -->
