

復元するために必要な手順


1.Vagrantfileがある、このディレクトリでvagrant upする
2.vagrant sshと入力し、vagrant内部へ行き、mysql と入力し、Mysqlに入る
3.CREATE DATABASE wordpress; と入力
4.show databases;を打ってデータベースが生成されたことを確認
5.exit; と入力し、Mysqlを出る
6.mysql -u root wordpress < /var/www/html/dump.sql とコマンドから叩く
7.ブラウザから192.168.33.151へアクセスして画面が表示されていることを確認する


